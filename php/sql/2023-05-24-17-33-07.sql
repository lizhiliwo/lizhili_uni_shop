/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : linshi

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2023-05-25 17:58:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lizhili_admin
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_admin`;
CREATE TABLE `lizhili_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `isopen` int(11) NOT NULL DEFAULT '1',
  `mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_admin
-- ----------------------------
INSERT INTO `lizhili_admin` VALUES ('1', 'admin', '751aff6be33b5649fde05436dc4cb4f7', '1529570040', '1532252345', '1', '1', '超级管理员不能删除和停用');
INSERT INTO `lizhili_admin` VALUES ('2', 'lizhili', '25eee4665b3053ff200ea3c9b776bc35', '1532685192', null, '2', '1', '');

-- ----------------------------
-- Table structure for lizhili_advertisement
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_advertisement`;
CREATE TABLE `lizhili_advertisement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isopen` tinyint(1) DEFAULT '0' COMMENT '1代表启用，0代表不启用',
  `desc` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL COMMENT '关键字，根据关键字访问',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='广告';

-- ----------------------------
-- Records of lizhili_advertisement
-- ----------------------------

-- ----------------------------
-- Table structure for lizhili_ad_img
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_ad_img`;
CREATE TABLE `lizhili_ad_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `isopen` varchar(255) DEFAULT NULL,
  `ad_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_ad_img
-- ----------------------------

-- ----------------------------
-- Table structure for lizhili_area
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_area`;
CREATE TABLE `lizhili_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `level_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3435 DEFAULT CHARSET=utf8 COMMENT='行政地区省市县';

-- ----------------------------
-- Records of lizhili_area
-- ----------------------------
INSERT INTO `lizhili_area` VALUES ('1', '北京市', '110000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('2', '天津市', '120000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('3', '河北省', '130000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('4', '山西省', '140000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('5', '内蒙古自治区', '150000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('6', '辽宁省', '210000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('7', '吉林省', '220000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('8', '黑龙江省', '230000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('9', '上海市', '310000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('10', '江苏省', '320000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('11', '浙江省', '330000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('12', '安徽省', '340000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('13', '福建省', '350000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('14', '江西省', '360000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('15', '山东省', '370000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('16', '河南省', '410000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('17', '湖北省', '420000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('18', '湖南省', '430000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('19', '广东省', '440000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('20', '广西壮族自治区', '450000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('21', '海南省', '460000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('22', '重庆市', '500000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('23', '四川省', '510000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('24', '贵州省', '520000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('25', '云南省', '530000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('26', '西藏自治区', '540000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('27', '陕西省', '610000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('28', '甘肃省', '620000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('29', '青海省', '630000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('30', '宁夏回族自治区', '640000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('31', '新疆维吾尔自治区', '650000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('32', '台湾', '710000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('33', '香港', '810000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('34', '澳门', '820000', '0', '1');
INSERT INTO `lizhili_area` VALUES ('35', '北京市', '110000', '1', '2');
INSERT INTO `lizhili_area` VALUES ('36', '天津市', '120000', '2', '2');
INSERT INTO `lizhili_area` VALUES ('37', '石家庄市', '130100', '3', '2');
INSERT INTO `lizhili_area` VALUES ('38', '唐山市', '130200', '3', '2');
INSERT INTO `lizhili_area` VALUES ('39', '秦皇岛市', '130300', '3', '2');
INSERT INTO `lizhili_area` VALUES ('40', '邯郸市', '130400', '3', '2');
INSERT INTO `lizhili_area` VALUES ('41', '邢台市', '130500', '3', '2');
INSERT INTO `lizhili_area` VALUES ('42', '保定市', '130600', '3', '2');
INSERT INTO `lizhili_area` VALUES ('43', '张家口市', '130700', '3', '2');
INSERT INTO `lizhili_area` VALUES ('44', '承德市', '130800', '3', '2');
INSERT INTO `lizhili_area` VALUES ('45', '沧州市', '130900', '3', '2');
INSERT INTO `lizhili_area` VALUES ('46', '廊坊市', '131000', '3', '2');
INSERT INTO `lizhili_area` VALUES ('47', '衡水市', '131100', '3', '2');
INSERT INTO `lizhili_area` VALUES ('48', '太原市', '140100', '4', '2');
INSERT INTO `lizhili_area` VALUES ('49', '大同市', '140200', '4', '2');
INSERT INTO `lizhili_area` VALUES ('50', '阳泉市', '140300', '4', '2');
INSERT INTO `lizhili_area` VALUES ('51', '长治市', '140400', '4', '2');
INSERT INTO `lizhili_area` VALUES ('52', '晋城市', '140500', '4', '2');
INSERT INTO `lizhili_area` VALUES ('53', '朔州市', '140600', '4', '2');
INSERT INTO `lizhili_area` VALUES ('54', '晋中市', '140700', '4', '2');
INSERT INTO `lizhili_area` VALUES ('55', '运城市', '140800', '4', '2');
INSERT INTO `lizhili_area` VALUES ('56', '忻州市', '140900', '4', '2');
INSERT INTO `lizhili_area` VALUES ('57', '临汾市', '141000', '4', '2');
INSERT INTO `lizhili_area` VALUES ('58', '吕梁市', '141100', '4', '2');
INSERT INTO `lizhili_area` VALUES ('59', '呼和浩特市', '150100', '5', '2');
INSERT INTO `lizhili_area` VALUES ('60', '包头市', '150200', '5', '2');
INSERT INTO `lizhili_area` VALUES ('61', '乌海市', '150300', '5', '2');
INSERT INTO `lizhili_area` VALUES ('62', '赤峰市', '150400', '5', '2');
INSERT INTO `lizhili_area` VALUES ('63', '通辽市', '150500', '5', '2');
INSERT INTO `lizhili_area` VALUES ('64', '鄂尔多斯市', '150600', '5', '2');
INSERT INTO `lizhili_area` VALUES ('65', '呼伦贝尔市', '150700', '5', '2');
INSERT INTO `lizhili_area` VALUES ('66', '巴彦淖尔市', '150800', '5', '2');
INSERT INTO `lizhili_area` VALUES ('67', '乌兰察布市', '150900', '5', '2');
INSERT INTO `lizhili_area` VALUES ('68', '兴安盟', '152200', '5', '2');
INSERT INTO `lizhili_area` VALUES ('69', '锡林郭勒盟', '152500', '5', '2');
INSERT INTO `lizhili_area` VALUES ('70', '阿拉善盟', '152900', '5', '2');
INSERT INTO `lizhili_area` VALUES ('71', '沈阳市', '210100', '6', '2');
INSERT INTO `lizhili_area` VALUES ('72', '大连市', '210200', '6', '2');
INSERT INTO `lizhili_area` VALUES ('73', '鞍山市', '210300', '6', '2');
INSERT INTO `lizhili_area` VALUES ('74', '抚顺市', '210400', '6', '2');
INSERT INTO `lizhili_area` VALUES ('75', '本溪市', '210500', '6', '2');
INSERT INTO `lizhili_area` VALUES ('76', '丹东市', '210600', '6', '2');
INSERT INTO `lizhili_area` VALUES ('77', '锦州市', '210700', '6', '2');
INSERT INTO `lizhili_area` VALUES ('78', '营口市', '210800', '6', '2');
INSERT INTO `lizhili_area` VALUES ('79', '阜新市', '210900', '6', '2');
INSERT INTO `lizhili_area` VALUES ('80', '辽阳市', '211000', '6', '2');
INSERT INTO `lizhili_area` VALUES ('81', '盘锦市', '211100', '6', '2');
INSERT INTO `lizhili_area` VALUES ('82', '铁岭市', '211200', '6', '2');
INSERT INTO `lizhili_area` VALUES ('83', '朝阳市', '211300', '6', '2');
INSERT INTO `lizhili_area` VALUES ('84', '葫芦岛市', '211400', '6', '2');
INSERT INTO `lizhili_area` VALUES ('85', '长春市', '220100', '7', '2');
INSERT INTO `lizhili_area` VALUES ('86', '吉林市', '220200', '7', '2');
INSERT INTO `lizhili_area` VALUES ('87', '四平市', '220300', '7', '2');
INSERT INTO `lizhili_area` VALUES ('88', '辽源市', '220400', '7', '2');
INSERT INTO `lizhili_area` VALUES ('89', '通化市', '220500', '7', '2');
INSERT INTO `lizhili_area` VALUES ('90', '白山市', '220600', '7', '2');
INSERT INTO `lizhili_area` VALUES ('91', '松原市', '220700', '7', '2');
INSERT INTO `lizhili_area` VALUES ('92', '白城市', '220800', '7', '2');
INSERT INTO `lizhili_area` VALUES ('93', '延边朝鲜族自治州', '222400', '7', '2');
INSERT INTO `lizhili_area` VALUES ('94', '哈尔滨市', '230100', '8', '2');
INSERT INTO `lizhili_area` VALUES ('95', '齐齐哈尔市', '230200', '8', '2');
INSERT INTO `lizhili_area` VALUES ('96', '鸡西市', '230300', '8', '2');
INSERT INTO `lizhili_area` VALUES ('97', '鹤岗市', '230400', '8', '2');
INSERT INTO `lizhili_area` VALUES ('98', '双鸭山市', '230500', '8', '2');
INSERT INTO `lizhili_area` VALUES ('99', '大庆市', '230600', '8', '2');
INSERT INTO `lizhili_area` VALUES ('100', '伊春市', '230700', '8', '2');
INSERT INTO `lizhili_area` VALUES ('101', '佳木斯市', '230800', '8', '2');
INSERT INTO `lizhili_area` VALUES ('102', '七台河市', '230900', '8', '2');
INSERT INTO `lizhili_area` VALUES ('103', '牡丹江市', '231000', '8', '2');
INSERT INTO `lizhili_area` VALUES ('104', '黑河市', '231100', '8', '2');
INSERT INTO `lizhili_area` VALUES ('105', '绥化市', '231200', '8', '2');
INSERT INTO `lizhili_area` VALUES ('106', '大兴安岭地区', '232700', '8', '2');
INSERT INTO `lizhili_area` VALUES ('107', '上海市', '310100', '9', '2');
INSERT INTO `lizhili_area` VALUES ('108', '南京市', '320100', '10', '2');
INSERT INTO `lizhili_area` VALUES ('109', '无锡市', '320200', '10', '2');
INSERT INTO `lizhili_area` VALUES ('110', '徐州市', '320300', '10', '2');
INSERT INTO `lizhili_area` VALUES ('111', '常州市', '320400', '10', '2');
INSERT INTO `lizhili_area` VALUES ('112', '苏州市', '320500', '10', '2');
INSERT INTO `lizhili_area` VALUES ('113', '南通市', '320600', '10', '2');
INSERT INTO `lizhili_area` VALUES ('114', '连云港市', '320700', '10', '2');
INSERT INTO `lizhili_area` VALUES ('115', '淮安市', '320800', '10', '2');
INSERT INTO `lizhili_area` VALUES ('116', '盐城市', '320900', '10', '2');
INSERT INTO `lizhili_area` VALUES ('117', '扬州市', '321000', '10', '2');
INSERT INTO `lizhili_area` VALUES ('118', '镇江市', '321100', '10', '2');
INSERT INTO `lizhili_area` VALUES ('119', '泰州市', '321200', '10', '2');
INSERT INTO `lizhili_area` VALUES ('120', '宿迁市', '321300', '10', '2');
INSERT INTO `lizhili_area` VALUES ('121', '杭州市', '330100', '11', '2');
INSERT INTO `lizhili_area` VALUES ('122', '宁波市', '330200', '11', '2');
INSERT INTO `lizhili_area` VALUES ('123', '温州市', '330300', '11', '2');
INSERT INTO `lizhili_area` VALUES ('124', '嘉兴市', '330400', '11', '2');
INSERT INTO `lizhili_area` VALUES ('125', '湖州市', '330500', '11', '2');
INSERT INTO `lizhili_area` VALUES ('126', '绍兴市', '330600', '11', '2');
INSERT INTO `lizhili_area` VALUES ('127', '金华市', '330700', '11', '2');
INSERT INTO `lizhili_area` VALUES ('128', '衢州市', '330800', '11', '2');
INSERT INTO `lizhili_area` VALUES ('129', '舟山市', '330900', '11', '2');
INSERT INTO `lizhili_area` VALUES ('130', '台州市', '331000', '11', '2');
INSERT INTO `lizhili_area` VALUES ('131', '丽水市', '331100', '11', '2');
INSERT INTO `lizhili_area` VALUES ('132', '合肥市', '340100', '12', '2');
INSERT INTO `lizhili_area` VALUES ('133', '芜湖市', '340200', '12', '2');
INSERT INTO `lizhili_area` VALUES ('134', '蚌埠市', '340300', '12', '2');
INSERT INTO `lizhili_area` VALUES ('135', '淮南市', '340400', '12', '2');
INSERT INTO `lizhili_area` VALUES ('136', '马鞍山市', '340500', '12', '2');
INSERT INTO `lizhili_area` VALUES ('137', '淮北市', '340600', '12', '2');
INSERT INTO `lizhili_area` VALUES ('138', '铜陵市', '340700', '12', '2');
INSERT INTO `lizhili_area` VALUES ('139', '安庆市', '340800', '12', '2');
INSERT INTO `lizhili_area` VALUES ('140', '黄山市', '341000', '12', '2');
INSERT INTO `lizhili_area` VALUES ('141', '滁州市', '341100', '12', '2');
INSERT INTO `lizhili_area` VALUES ('142', '阜阳市', '341200', '12', '2');
INSERT INTO `lizhili_area` VALUES ('143', '宿州市', '341300', '12', '2');
INSERT INTO `lizhili_area` VALUES ('144', '六安市', '341500', '12', '2');
INSERT INTO `lizhili_area` VALUES ('145', '亳州市', '341600', '12', '2');
INSERT INTO `lizhili_area` VALUES ('146', '池州市', '341700', '12', '2');
INSERT INTO `lizhili_area` VALUES ('147', '宣城市', '341800', '12', '2');
INSERT INTO `lizhili_area` VALUES ('148', '福州市', '350100', '13', '2');
INSERT INTO `lizhili_area` VALUES ('149', '厦门市', '350200', '13', '2');
INSERT INTO `lizhili_area` VALUES ('150', '莆田市', '350300', '13', '2');
INSERT INTO `lizhili_area` VALUES ('151', '三明市', '350400', '13', '2');
INSERT INTO `lizhili_area` VALUES ('152', '泉州市', '350500', '13', '2');
INSERT INTO `lizhili_area` VALUES ('153', '漳州市', '350600', '13', '2');
INSERT INTO `lizhili_area` VALUES ('154', '南平市', '350700', '13', '2');
INSERT INTO `lizhili_area` VALUES ('155', '龙岩市', '350800', '13', '2');
INSERT INTO `lizhili_area` VALUES ('156', '宁德市', '350900', '13', '2');
INSERT INTO `lizhili_area` VALUES ('157', '南昌市', '360100', '14', '2');
INSERT INTO `lizhili_area` VALUES ('158', '景德镇市', '360200', '14', '2');
INSERT INTO `lizhili_area` VALUES ('159', '萍乡市', '360300', '14', '2');
INSERT INTO `lizhili_area` VALUES ('160', '九江市', '360400', '14', '2');
INSERT INTO `lizhili_area` VALUES ('161', '新余市', '360500', '14', '2');
INSERT INTO `lizhili_area` VALUES ('162', '鹰潭市', '360600', '14', '2');
INSERT INTO `lizhili_area` VALUES ('163', '赣州市', '360700', '14', '2');
INSERT INTO `lizhili_area` VALUES ('164', '吉安市', '360800', '14', '2');
INSERT INTO `lizhili_area` VALUES ('165', '宜春市', '360900', '14', '2');
INSERT INTO `lizhili_area` VALUES ('166', '抚州市', '361000', '14', '2');
INSERT INTO `lizhili_area` VALUES ('167', '上饶市', '361100', '14', '2');
INSERT INTO `lizhili_area` VALUES ('168', '济南市', '370100', '15', '2');
INSERT INTO `lizhili_area` VALUES ('169', '青岛市', '370200', '15', '2');
INSERT INTO `lizhili_area` VALUES ('170', '淄博市', '370300', '15', '2');
INSERT INTO `lizhili_area` VALUES ('171', '枣庄市', '370400', '15', '2');
INSERT INTO `lizhili_area` VALUES ('172', '东营市', '370500', '15', '2');
INSERT INTO `lizhili_area` VALUES ('173', '烟台市', '370600', '15', '2');
INSERT INTO `lizhili_area` VALUES ('174', '潍坊市', '370700', '15', '2');
INSERT INTO `lizhili_area` VALUES ('175', '济宁市', '370800', '15', '2');
INSERT INTO `lizhili_area` VALUES ('176', '泰安市', '370900', '15', '2');
INSERT INTO `lizhili_area` VALUES ('177', '威海市', '371000', '15', '2');
INSERT INTO `lizhili_area` VALUES ('178', '日照市', '371100', '15', '2');
INSERT INTO `lizhili_area` VALUES ('179', '莱芜市', '371200', '15', '2');
INSERT INTO `lizhili_area` VALUES ('180', '临沂市', '371300', '15', '2');
INSERT INTO `lizhili_area` VALUES ('181', '德州市', '371400', '15', '2');
INSERT INTO `lizhili_area` VALUES ('182', '聊城市', '371500', '15', '2');
INSERT INTO `lizhili_area` VALUES ('183', '滨州市', '371600', '15', '2');
INSERT INTO `lizhili_area` VALUES ('184', '菏泽市', '371700', '15', '2');
INSERT INTO `lizhili_area` VALUES ('185', '郑州市', '410100', '16', '2');
INSERT INTO `lizhili_area` VALUES ('186', '开封市', '410200', '16', '2');
INSERT INTO `lizhili_area` VALUES ('187', '洛阳市', '410300', '16', '2');
INSERT INTO `lizhili_area` VALUES ('188', '平顶山市', '410400', '16', '2');
INSERT INTO `lizhili_area` VALUES ('189', '安阳市', '410500', '16', '2');
INSERT INTO `lizhili_area` VALUES ('190', '鹤壁市', '410600', '16', '2');
INSERT INTO `lizhili_area` VALUES ('191', '新乡市', '410700', '16', '2');
INSERT INTO `lizhili_area` VALUES ('192', '焦作市', '410800', '16', '2');
INSERT INTO `lizhili_area` VALUES ('193', '濮阳市', '410900', '16', '2');
INSERT INTO `lizhili_area` VALUES ('194', '许昌市', '411000', '16', '2');
INSERT INTO `lizhili_area` VALUES ('195', '漯河市', '411100', '16', '2');
INSERT INTO `lizhili_area` VALUES ('196', '三门峡市', '411200', '16', '2');
INSERT INTO `lizhili_area` VALUES ('197', '南阳市', '411300', '16', '2');
INSERT INTO `lizhili_area` VALUES ('198', '商丘市', '411400', '16', '2');
INSERT INTO `lizhili_area` VALUES ('199', '信阳市', '411500', '16', '2');
INSERT INTO `lizhili_area` VALUES ('200', '周口市', '411600', '16', '2');
INSERT INTO `lizhili_area` VALUES ('201', '驻马店市', '411700', '16', '2');
INSERT INTO `lizhili_area` VALUES ('202', '省直辖县级行政区划', '419000', '16', '2');
INSERT INTO `lizhili_area` VALUES ('203', '武汉市', '420100', '17', '2');
INSERT INTO `lizhili_area` VALUES ('204', '黄石市', '420200', '17', '2');
INSERT INTO `lizhili_area` VALUES ('205', '十堰市', '420300', '17', '2');
INSERT INTO `lizhili_area` VALUES ('206', '宜昌市', '420500', '17', '2');
INSERT INTO `lizhili_area` VALUES ('207', '襄阳市', '420600', '17', '2');
INSERT INTO `lizhili_area` VALUES ('208', '鄂州市', '420700', '17', '2');
INSERT INTO `lizhili_area` VALUES ('209', '荆门市', '420800', '17', '2');
INSERT INTO `lizhili_area` VALUES ('210', '孝感市', '420900', '17', '2');
INSERT INTO `lizhili_area` VALUES ('211', '荆州市', '421000', '17', '2');
INSERT INTO `lizhili_area` VALUES ('212', '黄冈市', '421100', '17', '2');
INSERT INTO `lizhili_area` VALUES ('213', '咸宁市', '421200', '17', '2');
INSERT INTO `lizhili_area` VALUES ('214', '随州市', '421300', '17', '2');
INSERT INTO `lizhili_area` VALUES ('215', '恩施土家族苗族自治州', '422800', '17', '2');
INSERT INTO `lizhili_area` VALUES ('216', '省直辖县级行政区划', '429000', '17', '2');
INSERT INTO `lizhili_area` VALUES ('217', '长沙市', '430100', '18', '2');
INSERT INTO `lizhili_area` VALUES ('218', '株洲市', '430200', '18', '2');
INSERT INTO `lizhili_area` VALUES ('219', '湘潭市', '430300', '18', '2');
INSERT INTO `lizhili_area` VALUES ('220', '衡阳市', '430400', '18', '2');
INSERT INTO `lizhili_area` VALUES ('221', '邵阳市', '430500', '18', '2');
INSERT INTO `lizhili_area` VALUES ('222', '岳阳市', '430600', '18', '2');
INSERT INTO `lizhili_area` VALUES ('223', '常德市', '430700', '18', '2');
INSERT INTO `lizhili_area` VALUES ('224', '张家界市', '430800', '18', '2');
INSERT INTO `lizhili_area` VALUES ('225', '益阳市', '430900', '18', '2');
INSERT INTO `lizhili_area` VALUES ('226', '郴州市', '431000', '18', '2');
INSERT INTO `lizhili_area` VALUES ('227', '永州市', '431100', '18', '2');
INSERT INTO `lizhili_area` VALUES ('228', '怀化市', '431200', '18', '2');
INSERT INTO `lizhili_area` VALUES ('229', '娄底市', '431300', '18', '2');
INSERT INTO `lizhili_area` VALUES ('230', '湘西土家族苗族自治州', '433100', '18', '2');
INSERT INTO `lizhili_area` VALUES ('231', '广州市', '440100', '19', '2');
INSERT INTO `lizhili_area` VALUES ('232', '韶关市', '440200', '19', '2');
INSERT INTO `lizhili_area` VALUES ('233', '深圳市', '440300', '19', '2');
INSERT INTO `lizhili_area` VALUES ('234', '珠海市', '440400', '19', '2');
INSERT INTO `lizhili_area` VALUES ('235', '汕头市', '440500', '19', '2');
INSERT INTO `lizhili_area` VALUES ('236', '佛山市', '440600', '19', '2');
INSERT INTO `lizhili_area` VALUES ('237', '江门市', '440700', '19', '2');
INSERT INTO `lizhili_area` VALUES ('238', '湛江市', '440800', '19', '2');
INSERT INTO `lizhili_area` VALUES ('239', '茂名市', '440900', '19', '2');
INSERT INTO `lizhili_area` VALUES ('240', '肇庆市', '441200', '19', '2');
INSERT INTO `lizhili_area` VALUES ('241', '惠州市', '441300', '19', '2');
INSERT INTO `lizhili_area` VALUES ('242', '梅州市', '441400', '19', '2');
INSERT INTO `lizhili_area` VALUES ('243', '汕尾市', '441500', '19', '2');
INSERT INTO `lizhili_area` VALUES ('244', '河源市', '441600', '19', '2');
INSERT INTO `lizhili_area` VALUES ('245', '阳江市', '441700', '19', '2');
INSERT INTO `lizhili_area` VALUES ('246', '清远市', '441800', '19', '2');
INSERT INTO `lizhili_area` VALUES ('247', '东莞市', '441900', '19', '2');
INSERT INTO `lizhili_area` VALUES ('248', '中山市', '442000', '19', '2');
INSERT INTO `lizhili_area` VALUES ('249', '潮州市', '445100', '19', '2');
INSERT INTO `lizhili_area` VALUES ('250', '揭阳市', '445200', '19', '2');
INSERT INTO `lizhili_area` VALUES ('251', '云浮市', '445300', '19', '2');
INSERT INTO `lizhili_area` VALUES ('252', '南宁市', '450100', '20', '2');
INSERT INTO `lizhili_area` VALUES ('253', '柳州市', '450200', '20', '2');
INSERT INTO `lizhili_area` VALUES ('254', '桂林市', '450300', '20', '2');
INSERT INTO `lizhili_area` VALUES ('255', '梧州市', '450400', '20', '2');
INSERT INTO `lizhili_area` VALUES ('256', '北海市', '450500', '20', '2');
INSERT INTO `lizhili_area` VALUES ('257', '防城港市', '450600', '20', '2');
INSERT INTO `lizhili_area` VALUES ('258', '钦州市', '450700', '20', '2');
INSERT INTO `lizhili_area` VALUES ('259', '贵港市', '450800', '20', '2');
INSERT INTO `lizhili_area` VALUES ('260', '玉林市', '450900', '20', '2');
INSERT INTO `lizhili_area` VALUES ('261', '百色市', '451000', '20', '2');
INSERT INTO `lizhili_area` VALUES ('262', '贺州市', '451100', '20', '2');
INSERT INTO `lizhili_area` VALUES ('263', '河池市', '451200', '20', '2');
INSERT INTO `lizhili_area` VALUES ('264', '来宾市', '451300', '20', '2');
INSERT INTO `lizhili_area` VALUES ('265', '崇左市', '451400', '20', '2');
INSERT INTO `lizhili_area` VALUES ('266', '海口市', '460100', '21', '2');
INSERT INTO `lizhili_area` VALUES ('267', '三亚市', '460200', '21', '2');
INSERT INTO `lizhili_area` VALUES ('268', '三沙市', '460300', '21', '2');
INSERT INTO `lizhili_area` VALUES ('269', '儋州市', '460400', '21', '2');
INSERT INTO `lizhili_area` VALUES ('270', '省直辖县级行政区划', '469000', '21', '2');
INSERT INTO `lizhili_area` VALUES ('271', '市辖区', '500100', '22', '2');
INSERT INTO `lizhili_area` VALUES ('272', '县', '500200', '22', '2');
INSERT INTO `lizhili_area` VALUES ('273', '成都市', '510100', '23', '2');
INSERT INTO `lizhili_area` VALUES ('274', '自贡市', '510300', '23', '2');
INSERT INTO `lizhili_area` VALUES ('275', '攀枝花市', '510400', '23', '2');
INSERT INTO `lizhili_area` VALUES ('276', '泸州市', '510500', '23', '2');
INSERT INTO `lizhili_area` VALUES ('277', '德阳市', '510600', '23', '2');
INSERT INTO `lizhili_area` VALUES ('278', '绵阳市', '510700', '23', '2');
INSERT INTO `lizhili_area` VALUES ('279', '广元市', '510800', '23', '2');
INSERT INTO `lizhili_area` VALUES ('280', '遂宁市', '510900', '23', '2');
INSERT INTO `lizhili_area` VALUES ('281', '内江市', '511000', '23', '2');
INSERT INTO `lizhili_area` VALUES ('282', '乐山市', '511100', '23', '2');
INSERT INTO `lizhili_area` VALUES ('283', '南充市', '511300', '23', '2');
INSERT INTO `lizhili_area` VALUES ('284', '眉山市', '511400', '23', '2');
INSERT INTO `lizhili_area` VALUES ('285', '宜宾市', '511500', '23', '2');
INSERT INTO `lizhili_area` VALUES ('286', '广安市', '511600', '23', '2');
INSERT INTO `lizhili_area` VALUES ('287', '达州市', '511700', '23', '2');
INSERT INTO `lizhili_area` VALUES ('288', '雅安市', '511800', '23', '2');
INSERT INTO `lizhili_area` VALUES ('289', '巴中市', '511900', '23', '2');
INSERT INTO `lizhili_area` VALUES ('290', '资阳市', '512000', '23', '2');
INSERT INTO `lizhili_area` VALUES ('291', '阿坝藏族羌族自治州', '513200', '23', '2');
INSERT INTO `lizhili_area` VALUES ('292', '甘孜藏族自治州', '513300', '23', '2');
INSERT INTO `lizhili_area` VALUES ('293', '凉山彝族自治州', '513400', '23', '2');
INSERT INTO `lizhili_area` VALUES ('294', '贵阳市', '520100', '24', '2');
INSERT INTO `lizhili_area` VALUES ('295', '六盘水市', '520200', '24', '2');
INSERT INTO `lizhili_area` VALUES ('296', '遵义市', '520300', '24', '2');
INSERT INTO `lizhili_area` VALUES ('297', '安顺市', '520400', '24', '2');
INSERT INTO `lizhili_area` VALUES ('298', '毕节市', '520500', '24', '2');
INSERT INTO `lizhili_area` VALUES ('299', '铜仁市', '520600', '24', '2');
INSERT INTO `lizhili_area` VALUES ('300', '黔西南布依族苗族自治州', '522300', '24', '2');
INSERT INTO `lizhili_area` VALUES ('301', '黔东南苗族侗族自治州', '522600', '24', '2');
INSERT INTO `lizhili_area` VALUES ('302', '黔南布依族苗族自治州', '522700', '24', '2');
INSERT INTO `lizhili_area` VALUES ('303', '昆明市', '530100', '25', '2');
INSERT INTO `lizhili_area` VALUES ('304', '曲靖市', '530300', '25', '2');
INSERT INTO `lizhili_area` VALUES ('305', '玉溪市', '530400', '25', '2');
INSERT INTO `lizhili_area` VALUES ('306', '保山市', '530500', '25', '2');
INSERT INTO `lizhili_area` VALUES ('307', '昭通市', '530600', '25', '2');
INSERT INTO `lizhili_area` VALUES ('308', '丽江市', '530700', '25', '2');
INSERT INTO `lizhili_area` VALUES ('309', '普洱市', '530800', '25', '2');
INSERT INTO `lizhili_area` VALUES ('310', '临沧市', '530900', '25', '2');
INSERT INTO `lizhili_area` VALUES ('311', '楚雄彝族自治州', '532300', '25', '2');
INSERT INTO `lizhili_area` VALUES ('312', '红河哈尼族彝族自治州', '532500', '25', '2');
INSERT INTO `lizhili_area` VALUES ('313', '文山壮族苗族自治州', '532600', '25', '2');
INSERT INTO `lizhili_area` VALUES ('314', '西双版纳傣族自治州', '532800', '25', '2');
INSERT INTO `lizhili_area` VALUES ('315', '大理白族自治州', '532900', '25', '2');
INSERT INTO `lizhili_area` VALUES ('316', '德宏傣族景颇族自治州', '533100', '25', '2');
INSERT INTO `lizhili_area` VALUES ('317', '怒江傈僳族自治州', '533300', '25', '2');
INSERT INTO `lizhili_area` VALUES ('318', '迪庆藏族自治州', '533400', '25', '2');
INSERT INTO `lizhili_area` VALUES ('319', '拉萨市', '540100', '26', '2');
INSERT INTO `lizhili_area` VALUES ('320', '日喀则市', '540200', '26', '2');
INSERT INTO `lizhili_area` VALUES ('321', '昌都市', '540300', '26', '2');
INSERT INTO `lizhili_area` VALUES ('322', '林芝市', '540400', '26', '2');
INSERT INTO `lizhili_area` VALUES ('323', '山南市', '540500', '26', '2');
INSERT INTO `lizhili_area` VALUES ('324', '那曲地区', '542400', '26', '2');
INSERT INTO `lizhili_area` VALUES ('325', '阿里地区', '542500', '26', '2');
INSERT INTO `lizhili_area` VALUES ('326', '西安市', '610100', '27', '2');
INSERT INTO `lizhili_area` VALUES ('327', '铜川市', '610200', '27', '2');
INSERT INTO `lizhili_area` VALUES ('328', '宝鸡市', '610300', '27', '2');
INSERT INTO `lizhili_area` VALUES ('329', '咸阳市', '610400', '27', '2');
INSERT INTO `lizhili_area` VALUES ('330', '渭南市', '610500', '27', '2');
INSERT INTO `lizhili_area` VALUES ('331', '延安市', '610600', '27', '2');
INSERT INTO `lizhili_area` VALUES ('332', '汉中市', '610700', '27', '2');
INSERT INTO `lizhili_area` VALUES ('333', '榆林市', '610800', '27', '2');
INSERT INTO `lizhili_area` VALUES ('334', '安康市', '610900', '27', '2');
INSERT INTO `lizhili_area` VALUES ('335', '商洛市', '611000', '27', '2');
INSERT INTO `lizhili_area` VALUES ('336', '兰州市', '620100', '28', '2');
INSERT INTO `lizhili_area` VALUES ('337', '嘉峪关市', '620200', '28', '2');
INSERT INTO `lizhili_area` VALUES ('338', '金昌市', '620300', '28', '2');
INSERT INTO `lizhili_area` VALUES ('339', '白银市', '620400', '28', '2');
INSERT INTO `lizhili_area` VALUES ('340', '天水市', '620500', '28', '2');
INSERT INTO `lizhili_area` VALUES ('341', '武威市', '620600', '28', '2');
INSERT INTO `lizhili_area` VALUES ('342', '张掖市', '620700', '28', '2');
INSERT INTO `lizhili_area` VALUES ('343', '平凉市', '620800', '28', '2');
INSERT INTO `lizhili_area` VALUES ('344', '酒泉市', '620900', '28', '2');
INSERT INTO `lizhili_area` VALUES ('345', '庆阳市', '621000', '28', '2');
INSERT INTO `lizhili_area` VALUES ('346', '定西市', '621100', '28', '2');
INSERT INTO `lizhili_area` VALUES ('347', '陇南市', '621200', '28', '2');
INSERT INTO `lizhili_area` VALUES ('348', '临夏回族自治州', '622900', '28', '2');
INSERT INTO `lizhili_area` VALUES ('349', '甘南藏族自治州', '623000', '28', '2');
INSERT INTO `lizhili_area` VALUES ('350', '西宁市', '630100', '29', '2');
INSERT INTO `lizhili_area` VALUES ('351', '海东市', '630200', '29', '2');
INSERT INTO `lizhili_area` VALUES ('352', '海北藏族自治州', '632200', '29', '2');
INSERT INTO `lizhili_area` VALUES ('353', '黄南藏族自治州', '632300', '29', '2');
INSERT INTO `lizhili_area` VALUES ('354', '海南藏族自治州', '632500', '29', '2');
INSERT INTO `lizhili_area` VALUES ('355', '果洛藏族自治州', '632600', '29', '2');
INSERT INTO `lizhili_area` VALUES ('356', '玉树藏族自治州', '632700', '29', '2');
INSERT INTO `lizhili_area` VALUES ('357', '海西蒙古族藏族自治州', '632800', '29', '2');
INSERT INTO `lizhili_area` VALUES ('358', '银川市', '640100', '30', '2');
INSERT INTO `lizhili_area` VALUES ('359', '石嘴山市', '640200', '30', '2');
INSERT INTO `lizhili_area` VALUES ('360', '吴忠市', '640300', '30', '2');
INSERT INTO `lizhili_area` VALUES ('361', '固原市', '640400', '30', '2');
INSERT INTO `lizhili_area` VALUES ('362', '中卫市', '640500', '30', '2');
INSERT INTO `lizhili_area` VALUES ('363', '乌鲁木齐市', '650100', '31', '2');
INSERT INTO `lizhili_area` VALUES ('364', '克拉玛依市', '650200', '31', '2');
INSERT INTO `lizhili_area` VALUES ('365', '吐鲁番市', '650400', '31', '2');
INSERT INTO `lizhili_area` VALUES ('366', '哈密市', '650500', '31', '2');
INSERT INTO `lizhili_area` VALUES ('367', '昌吉回族自治州', '652300', '31', '2');
INSERT INTO `lizhili_area` VALUES ('368', '博尔塔拉蒙古自治州', '652700', '31', '2');
INSERT INTO `lizhili_area` VALUES ('369', '巴音郭楞蒙古自治州', '652800', '31', '2');
INSERT INTO `lizhili_area` VALUES ('370', '阿克苏地区', '652900', '31', '2');
INSERT INTO `lizhili_area` VALUES ('371', '克孜勒苏柯尔克孜自治州', '653000', '31', '2');
INSERT INTO `lizhili_area` VALUES ('372', '喀什地区', '653100', '31', '2');
INSERT INTO `lizhili_area` VALUES ('373', '和田地区', '653200', '31', '2');
INSERT INTO `lizhili_area` VALUES ('374', '伊犁哈萨克自治州', '654000', '31', '2');
INSERT INTO `lizhili_area` VALUES ('375', '塔城地区', '654200', '31', '2');
INSERT INTO `lizhili_area` VALUES ('376', '阿勒泰地区', '654300', '31', '2');
INSERT INTO `lizhili_area` VALUES ('377', '自治区直辖县级行政区划', '659000', '31', '2');
INSERT INTO `lizhili_area` VALUES ('378', '台北', '660100', '32', '2');
INSERT INTO `lizhili_area` VALUES ('379', '高雄', '660200', '32', '2');
INSERT INTO `lizhili_area` VALUES ('380', '基隆', '660300', '32', '2');
INSERT INTO `lizhili_area` VALUES ('381', '台中', '660400', '32', '2');
INSERT INTO `lizhili_area` VALUES ('382', '台南', '660500', '32', '2');
INSERT INTO `lizhili_area` VALUES ('383', '新竹', '660600', '32', '2');
INSERT INTO `lizhili_area` VALUES ('384', '嘉义', '660700', '32', '2');
INSERT INTO `lizhili_area` VALUES ('385', '宜兰', '660800', '32', '2');
INSERT INTO `lizhili_area` VALUES ('386', '桃园', '660900', '32', '2');
INSERT INTO `lizhili_area` VALUES ('387', '苗栗', '661000', '32', '2');
INSERT INTO `lizhili_area` VALUES ('388', '彰化', '661100', '32', '2');
INSERT INTO `lizhili_area` VALUES ('389', '南投', '661200', '32', '2');
INSERT INTO `lizhili_area` VALUES ('390', '云林', '661300', '32', '2');
INSERT INTO `lizhili_area` VALUES ('391', '屏东', '661400', '32', '2');
INSERT INTO `lizhili_area` VALUES ('392', '台东', '661500', '32', '2');
INSERT INTO `lizhili_area` VALUES ('393', '花莲', '661600', '32', '2');
INSERT INTO `lizhili_area` VALUES ('394', '澎湖', '661700', '32', '2');
INSERT INTO `lizhili_area` VALUES ('395', '香港岛', '670100', '33', '2');
INSERT INTO `lizhili_area` VALUES ('396', '九龙', '670200', '33', '2');
INSERT INTO `lizhili_area` VALUES ('397', '新界', '670300', '33', '2');
INSERT INTO `lizhili_area` VALUES ('398', '澳门半岛', '680100', '34', '2');
INSERT INTO `lizhili_area` VALUES ('399', '氹仔岛', '680200', '34', '2');
INSERT INTO `lizhili_area` VALUES ('400', '路环岛', '680300', '34', '2');
INSERT INTO `lizhili_area` VALUES ('401', '路氹城', '680400', '34', '2');
INSERT INTO `lizhili_area` VALUES ('402', '东城区', '110101', '35', '3');
INSERT INTO `lizhili_area` VALUES ('403', '西城区', '110102', '35', '3');
INSERT INTO `lizhili_area` VALUES ('404', '朝阳区', '110105', '35', '3');
INSERT INTO `lizhili_area` VALUES ('405', '丰台区', '110106', '35', '3');
INSERT INTO `lizhili_area` VALUES ('406', '石景山区', '110107', '35', '3');
INSERT INTO `lizhili_area` VALUES ('407', '海淀区', '110108', '35', '3');
INSERT INTO `lizhili_area` VALUES ('408', '门头沟区', '110109', '35', '3');
INSERT INTO `lizhili_area` VALUES ('409', '房山区', '110111', '35', '3');
INSERT INTO `lizhili_area` VALUES ('410', '通州区', '110112', '35', '3');
INSERT INTO `lizhili_area` VALUES ('411', '顺义区', '110113', '35', '3');
INSERT INTO `lizhili_area` VALUES ('412', '昌平区', '110114', '35', '3');
INSERT INTO `lizhili_area` VALUES ('413', '大兴区', '110115', '35', '3');
INSERT INTO `lizhili_area` VALUES ('414', '怀柔区', '110116', '35', '3');
INSERT INTO `lizhili_area` VALUES ('415', '平谷区', '110117', '35', '3');
INSERT INTO `lizhili_area` VALUES ('416', '密云区', '110118', '35', '3');
INSERT INTO `lizhili_area` VALUES ('417', '延庆区', '110119', '35', '3');
INSERT INTO `lizhili_area` VALUES ('418', '和平区', '120101', '36', '3');
INSERT INTO `lizhili_area` VALUES ('419', '河东区', '120102', '36', '3');
INSERT INTO `lizhili_area` VALUES ('420', '河西区', '120103', '36', '3');
INSERT INTO `lizhili_area` VALUES ('421', '南开区', '120104', '36', '3');
INSERT INTO `lizhili_area` VALUES ('422', '河北区', '120105', '36', '3');
INSERT INTO `lizhili_area` VALUES ('423', '红桥区', '120106', '36', '3');
INSERT INTO `lizhili_area` VALUES ('424', '东丽区', '120110', '36', '3');
INSERT INTO `lizhili_area` VALUES ('425', '西青区', '120111', '36', '3');
INSERT INTO `lizhili_area` VALUES ('426', '津南区', '120112', '36', '3');
INSERT INTO `lizhili_area` VALUES ('427', '北辰区', '120113', '36', '3');
INSERT INTO `lizhili_area` VALUES ('428', '武清区', '120114', '36', '3');
INSERT INTO `lizhili_area` VALUES ('429', '宝坻区', '120115', '36', '3');
INSERT INTO `lizhili_area` VALUES ('430', '滨海新区', '120116', '36', '3');
INSERT INTO `lizhili_area` VALUES ('431', '宁河区', '120117', '36', '3');
INSERT INTO `lizhili_area` VALUES ('432', '静海区', '120118', '36', '3');
INSERT INTO `lizhili_area` VALUES ('433', '蓟州区', '120119', '36', '3');
INSERT INTO `lizhili_area` VALUES ('434', '长安区', '130102', '37', '3');
INSERT INTO `lizhili_area` VALUES ('435', '桥西区', '130104', '37', '3');
INSERT INTO `lizhili_area` VALUES ('436', '新华区', '130105', '37', '3');
INSERT INTO `lizhili_area` VALUES ('437', '井陉矿区', '130107', '37', '3');
INSERT INTO `lizhili_area` VALUES ('438', '裕华区', '130108', '37', '3');
INSERT INTO `lizhili_area` VALUES ('439', '藁城区', '130109', '37', '3');
INSERT INTO `lizhili_area` VALUES ('440', '鹿泉区', '130110', '37', '3');
INSERT INTO `lizhili_area` VALUES ('441', '栾城区', '130111', '37', '3');
INSERT INTO `lizhili_area` VALUES ('442', '井陉县', '130121', '37', '3');
INSERT INTO `lizhili_area` VALUES ('443', '正定县', '130123', '37', '3');
INSERT INTO `lizhili_area` VALUES ('444', '行唐县', '130125', '37', '3');
INSERT INTO `lizhili_area` VALUES ('445', '灵寿县', '130126', '37', '3');
INSERT INTO `lizhili_area` VALUES ('446', '高邑县', '130127', '37', '3');
INSERT INTO `lizhili_area` VALUES ('447', '深泽县', '130128', '37', '3');
INSERT INTO `lizhili_area` VALUES ('448', '赞皇县', '130129', '37', '3');
INSERT INTO `lizhili_area` VALUES ('449', '无极县', '130130', '37', '3');
INSERT INTO `lizhili_area` VALUES ('450', '平山县', '130131', '37', '3');
INSERT INTO `lizhili_area` VALUES ('451', '元氏县', '130132', '37', '3');
INSERT INTO `lizhili_area` VALUES ('452', '赵县', '130133', '37', '3');
INSERT INTO `lizhili_area` VALUES ('453', '石家庄高新技术产业开发区', '130171', '37', '3');
INSERT INTO `lizhili_area` VALUES ('454', '石家庄循环化工园区', '130172', '37', '3');
INSERT INTO `lizhili_area` VALUES ('455', '辛集市', '130181', '37', '3');
INSERT INTO `lizhili_area` VALUES ('456', '晋州市', '130183', '37', '3');
INSERT INTO `lizhili_area` VALUES ('457', '新乐市', '130184', '37', '3');
INSERT INTO `lizhili_area` VALUES ('458', '路南区', '130202', '38', '3');
INSERT INTO `lizhili_area` VALUES ('459', '路北区', '130203', '38', '3');
INSERT INTO `lizhili_area` VALUES ('460', '古冶区', '130204', '38', '3');
INSERT INTO `lizhili_area` VALUES ('461', '开平区', '130205', '38', '3');
INSERT INTO `lizhili_area` VALUES ('462', '丰南区', '130207', '38', '3');
INSERT INTO `lizhili_area` VALUES ('463', '丰润区', '130208', '38', '3');
INSERT INTO `lizhili_area` VALUES ('464', '曹妃甸区', '130209', '38', '3');
INSERT INTO `lizhili_area` VALUES ('465', '滦县', '130223', '38', '3');
INSERT INTO `lizhili_area` VALUES ('466', '滦南县', '130224', '38', '3');
INSERT INTO `lizhili_area` VALUES ('467', '乐亭县', '130225', '38', '3');
INSERT INTO `lizhili_area` VALUES ('468', '迁西县', '130227', '38', '3');
INSERT INTO `lizhili_area` VALUES ('469', '玉田县', '130229', '38', '3');
INSERT INTO `lizhili_area` VALUES ('470', '唐山市芦台经济技术开发区', '130271', '38', '3');
INSERT INTO `lizhili_area` VALUES ('471', '唐山市汉沽管理区', '130272', '38', '3');
INSERT INTO `lizhili_area` VALUES ('472', '唐山高新技术产业开发区', '130273', '38', '3');
INSERT INTO `lizhili_area` VALUES ('473', '河北唐山海港经济开发区', '130274', '38', '3');
INSERT INTO `lizhili_area` VALUES ('474', '遵化市', '130281', '38', '3');
INSERT INTO `lizhili_area` VALUES ('475', '迁安市', '130283', '38', '3');
INSERT INTO `lizhili_area` VALUES ('476', '海港区', '130302', '39', '3');
INSERT INTO `lizhili_area` VALUES ('477', '山海关区', '130303', '39', '3');
INSERT INTO `lizhili_area` VALUES ('478', '北戴河区', '130304', '39', '3');
INSERT INTO `lizhili_area` VALUES ('479', '抚宁区', '130306', '39', '3');
INSERT INTO `lizhili_area` VALUES ('480', '青龙满族自治县', '130321', '39', '3');
INSERT INTO `lizhili_area` VALUES ('481', '昌黎县', '130322', '39', '3');
INSERT INTO `lizhili_area` VALUES ('482', '卢龙县', '130324', '39', '3');
INSERT INTO `lizhili_area` VALUES ('483', '秦皇岛市经济技术开发区', '130371', '39', '3');
INSERT INTO `lizhili_area` VALUES ('484', '北戴河新区', '130372', '39', '3');
INSERT INTO `lizhili_area` VALUES ('485', '邯山区', '130402', '40', '3');
INSERT INTO `lizhili_area` VALUES ('486', '丛台区', '130403', '40', '3');
INSERT INTO `lizhili_area` VALUES ('487', '复兴区', '130404', '40', '3');
INSERT INTO `lizhili_area` VALUES ('488', '峰峰矿区', '130406', '40', '3');
INSERT INTO `lizhili_area` VALUES ('489', '肥乡区', '130407', '40', '3');
INSERT INTO `lizhili_area` VALUES ('490', '永年区', '130408', '40', '3');
INSERT INTO `lizhili_area` VALUES ('491', '临漳县', '130423', '40', '3');
INSERT INTO `lizhili_area` VALUES ('492', '成安县', '130424', '40', '3');
INSERT INTO `lizhili_area` VALUES ('493', '大名县', '130425', '40', '3');
INSERT INTO `lizhili_area` VALUES ('494', '涉县', '130426', '40', '3');
INSERT INTO `lizhili_area` VALUES ('495', '磁县', '130427', '40', '3');
INSERT INTO `lizhili_area` VALUES ('496', '邱县', '130430', '40', '3');
INSERT INTO `lizhili_area` VALUES ('497', '鸡泽县', '130431', '40', '3');
INSERT INTO `lizhili_area` VALUES ('498', '广平县', '130432', '40', '3');
INSERT INTO `lizhili_area` VALUES ('499', '馆陶县', '130433', '40', '3');
INSERT INTO `lizhili_area` VALUES ('500', '魏县', '130434', '40', '3');
INSERT INTO `lizhili_area` VALUES ('501', '曲周县', '130435', '40', '3');
INSERT INTO `lizhili_area` VALUES ('502', '邯郸经济技术开发区', '130471', '40', '3');
INSERT INTO `lizhili_area` VALUES ('503', '邯郸冀南新区', '130473', '40', '3');
INSERT INTO `lizhili_area` VALUES ('504', '武安市', '130481', '40', '3');
INSERT INTO `lizhili_area` VALUES ('505', '桥东区', '130502', '41', '3');
INSERT INTO `lizhili_area` VALUES ('506', '桥西区', '130503', '41', '3');
INSERT INTO `lizhili_area` VALUES ('507', '邢台县', '130521', '41', '3');
INSERT INTO `lizhili_area` VALUES ('508', '临城县', '130522', '41', '3');
INSERT INTO `lizhili_area` VALUES ('509', '内丘县', '130523', '41', '3');
INSERT INTO `lizhili_area` VALUES ('510', '柏乡县', '130524', '41', '3');
INSERT INTO `lizhili_area` VALUES ('511', '隆尧县', '130525', '41', '3');
INSERT INTO `lizhili_area` VALUES ('512', '任县', '130526', '41', '3');
INSERT INTO `lizhili_area` VALUES ('513', '南和县', '130527', '41', '3');
INSERT INTO `lizhili_area` VALUES ('514', '宁晋县', '130528', '41', '3');
INSERT INTO `lizhili_area` VALUES ('515', '巨鹿县', '130529', '41', '3');
INSERT INTO `lizhili_area` VALUES ('516', '新河县', '130530', '41', '3');
INSERT INTO `lizhili_area` VALUES ('517', '广宗县', '130531', '41', '3');
INSERT INTO `lizhili_area` VALUES ('518', '平乡县', '130532', '41', '3');
INSERT INTO `lizhili_area` VALUES ('519', '威县', '130533', '41', '3');
INSERT INTO `lizhili_area` VALUES ('520', '清河县', '130534', '41', '3');
INSERT INTO `lizhili_area` VALUES ('521', '临西县', '130535', '41', '3');
INSERT INTO `lizhili_area` VALUES ('522', '河北邢台经济开发区', '130571', '41', '3');
INSERT INTO `lizhili_area` VALUES ('523', '南宫市', '130581', '41', '3');
INSERT INTO `lizhili_area` VALUES ('524', '沙河市', '130582', '41', '3');
INSERT INTO `lizhili_area` VALUES ('525', '竞秀区', '130602', '42', '3');
INSERT INTO `lizhili_area` VALUES ('526', '莲池区', '130606', '42', '3');
INSERT INTO `lizhili_area` VALUES ('527', '满城区', '130607', '42', '3');
INSERT INTO `lizhili_area` VALUES ('528', '清苑区', '130608', '42', '3');
INSERT INTO `lizhili_area` VALUES ('529', '徐水区', '130609', '42', '3');
INSERT INTO `lizhili_area` VALUES ('530', '涞水县', '130623', '42', '3');
INSERT INTO `lizhili_area` VALUES ('531', '阜平县', '130624', '42', '3');
INSERT INTO `lizhili_area` VALUES ('532', '定兴县', '130626', '42', '3');
INSERT INTO `lizhili_area` VALUES ('533', '唐县', '130627', '42', '3');
INSERT INTO `lizhili_area` VALUES ('534', '高阳县', '130628', '42', '3');
INSERT INTO `lizhili_area` VALUES ('535', '容城县', '130629', '42', '3');
INSERT INTO `lizhili_area` VALUES ('536', '涞源县', '130630', '42', '3');
INSERT INTO `lizhili_area` VALUES ('537', '望都县', '130631', '42', '3');
INSERT INTO `lizhili_area` VALUES ('538', '安新县', '130632', '42', '3');
INSERT INTO `lizhili_area` VALUES ('539', '易县', '130633', '42', '3');
INSERT INTO `lizhili_area` VALUES ('540', '曲阳县', '130634', '42', '3');
INSERT INTO `lizhili_area` VALUES ('541', '蠡县', '130635', '42', '3');
INSERT INTO `lizhili_area` VALUES ('542', '顺平县', '130636', '42', '3');
INSERT INTO `lizhili_area` VALUES ('543', '博野县', '130637', '42', '3');
INSERT INTO `lizhili_area` VALUES ('544', '雄县', '130638', '42', '3');
INSERT INTO `lizhili_area` VALUES ('545', '保定高新技术产业开发区', '130671', '42', '3');
INSERT INTO `lizhili_area` VALUES ('546', '保定白沟新城', '130672', '42', '3');
INSERT INTO `lizhili_area` VALUES ('547', '涿州市', '130681', '42', '3');
INSERT INTO `lizhili_area` VALUES ('548', '定州市', '130682', '42', '3');
INSERT INTO `lizhili_area` VALUES ('549', '安国市', '130683', '42', '3');
INSERT INTO `lizhili_area` VALUES ('550', '高碑店市', '130684', '42', '3');
INSERT INTO `lizhili_area` VALUES ('551', '桥东区', '130702', '43', '3');
INSERT INTO `lizhili_area` VALUES ('552', '桥西区', '130703', '43', '3');
INSERT INTO `lizhili_area` VALUES ('553', '宣化区', '130705', '43', '3');
INSERT INTO `lizhili_area` VALUES ('554', '下花园区', '130706', '43', '3');
INSERT INTO `lizhili_area` VALUES ('555', '万全区', '130708', '43', '3');
INSERT INTO `lizhili_area` VALUES ('556', '崇礼区', '130709', '43', '3');
INSERT INTO `lizhili_area` VALUES ('557', '张北县', '130722', '43', '3');
INSERT INTO `lizhili_area` VALUES ('558', '康保县', '130723', '43', '3');
INSERT INTO `lizhili_area` VALUES ('559', '沽源县', '130724', '43', '3');
INSERT INTO `lizhili_area` VALUES ('560', '尚义县', '130725', '43', '3');
INSERT INTO `lizhili_area` VALUES ('561', '蔚县', '130726', '43', '3');
INSERT INTO `lizhili_area` VALUES ('562', '阳原县', '130727', '43', '3');
INSERT INTO `lizhili_area` VALUES ('563', '怀安县', '130728', '43', '3');
INSERT INTO `lizhili_area` VALUES ('564', '怀来县', '130730', '43', '3');
INSERT INTO `lizhili_area` VALUES ('565', '涿鹿县', '130731', '43', '3');
INSERT INTO `lizhili_area` VALUES ('566', '赤城县', '130732', '43', '3');
INSERT INTO `lizhili_area` VALUES ('567', '张家口市高新技术产业开发区', '130771', '43', '3');
INSERT INTO `lizhili_area` VALUES ('568', '张家口市察北管理区', '130772', '43', '3');
INSERT INTO `lizhili_area` VALUES ('569', '张家口市塞北管理区', '130773', '43', '3');
INSERT INTO `lizhili_area` VALUES ('570', '双桥区', '130802', '44', '3');
INSERT INTO `lizhili_area` VALUES ('571', '双滦区', '130803', '44', '3');
INSERT INTO `lizhili_area` VALUES ('572', '鹰手营子矿区', '130804', '44', '3');
INSERT INTO `lizhili_area` VALUES ('573', '承德县', '130821', '44', '3');
INSERT INTO `lizhili_area` VALUES ('574', '兴隆县', '130822', '44', '3');
INSERT INTO `lizhili_area` VALUES ('575', '滦平县', '130824', '44', '3');
INSERT INTO `lizhili_area` VALUES ('576', '隆化县', '130825', '44', '3');
INSERT INTO `lizhili_area` VALUES ('577', '丰宁满族自治县', '130826', '44', '3');
INSERT INTO `lizhili_area` VALUES ('578', '宽城满族自治县', '130827', '44', '3');
INSERT INTO `lizhili_area` VALUES ('579', '围场满族蒙古族自治县', '130828', '44', '3');
INSERT INTO `lizhili_area` VALUES ('580', '承德高新技术产业开发区', '130871', '44', '3');
INSERT INTO `lizhili_area` VALUES ('581', '平泉市', '130881', '44', '3');
INSERT INTO `lizhili_area` VALUES ('582', '新华区', '130902', '45', '3');
INSERT INTO `lizhili_area` VALUES ('583', '运河区', '130903', '45', '3');
INSERT INTO `lizhili_area` VALUES ('584', '沧县', '130921', '45', '3');
INSERT INTO `lizhili_area` VALUES ('585', '青县', '130922', '45', '3');
INSERT INTO `lizhili_area` VALUES ('586', '东光县', '130923', '45', '3');
INSERT INTO `lizhili_area` VALUES ('587', '海兴县', '130924', '45', '3');
INSERT INTO `lizhili_area` VALUES ('588', '盐山县', '130925', '45', '3');
INSERT INTO `lizhili_area` VALUES ('589', '肃宁县', '130926', '45', '3');
INSERT INTO `lizhili_area` VALUES ('590', '南皮县', '130927', '45', '3');
INSERT INTO `lizhili_area` VALUES ('591', '吴桥县', '130928', '45', '3');
INSERT INTO `lizhili_area` VALUES ('592', '献县', '130929', '45', '3');
INSERT INTO `lizhili_area` VALUES ('593', '孟村回族自治县', '130930', '45', '3');
INSERT INTO `lizhili_area` VALUES ('594', '河北沧州经济开发区', '130971', '45', '3');
INSERT INTO `lizhili_area` VALUES ('595', '沧州高新技术产业开发区', '130972', '45', '3');
INSERT INTO `lizhili_area` VALUES ('596', '沧州渤海新区', '130973', '45', '3');
INSERT INTO `lizhili_area` VALUES ('597', '泊头市', '130981', '45', '3');
INSERT INTO `lizhili_area` VALUES ('598', '任丘市', '130982', '45', '3');
INSERT INTO `lizhili_area` VALUES ('599', '黄骅市', '130983', '45', '3');
INSERT INTO `lizhili_area` VALUES ('600', '河间市', '130984', '45', '3');
INSERT INTO `lizhili_area` VALUES ('601', '安次区', '131002', '46', '3');
INSERT INTO `lizhili_area` VALUES ('602', '广阳区', '131003', '46', '3');
INSERT INTO `lizhili_area` VALUES ('603', '固安县', '131022', '46', '3');
INSERT INTO `lizhili_area` VALUES ('604', '永清县', '131023', '46', '3');
INSERT INTO `lizhili_area` VALUES ('605', '香河县', '131024', '46', '3');
INSERT INTO `lizhili_area` VALUES ('606', '大城县', '131025', '46', '3');
INSERT INTO `lizhili_area` VALUES ('607', '文安县', '131026', '46', '3');
INSERT INTO `lizhili_area` VALUES ('608', '大厂回族自治县', '131028', '46', '3');
INSERT INTO `lizhili_area` VALUES ('609', '廊坊经济技术开发区', '131071', '46', '3');
INSERT INTO `lizhili_area` VALUES ('610', '霸州市', '131081', '46', '3');
INSERT INTO `lizhili_area` VALUES ('611', '三河市', '131082', '46', '3');
INSERT INTO `lizhili_area` VALUES ('612', '桃城区', '131102', '47', '3');
INSERT INTO `lizhili_area` VALUES ('613', '冀州区', '131103', '47', '3');
INSERT INTO `lizhili_area` VALUES ('614', '枣强县', '131121', '47', '3');
INSERT INTO `lizhili_area` VALUES ('615', '武邑县', '131122', '47', '3');
INSERT INTO `lizhili_area` VALUES ('616', '武强县', '131123', '47', '3');
INSERT INTO `lizhili_area` VALUES ('617', '饶阳县', '131124', '47', '3');
INSERT INTO `lizhili_area` VALUES ('618', '安平县', '131125', '47', '3');
INSERT INTO `lizhili_area` VALUES ('619', '故城县', '131126', '47', '3');
INSERT INTO `lizhili_area` VALUES ('620', '景县', '131127', '47', '3');
INSERT INTO `lizhili_area` VALUES ('621', '阜城县', '131128', '47', '3');
INSERT INTO `lizhili_area` VALUES ('622', '河北衡水经济开发区', '131171', '47', '3');
INSERT INTO `lizhili_area` VALUES ('623', '衡水滨湖新区', '131172', '47', '3');
INSERT INTO `lizhili_area` VALUES ('624', '深州市', '131182', '47', '3');
INSERT INTO `lizhili_area` VALUES ('625', '小店区', '140105', '48', '3');
INSERT INTO `lizhili_area` VALUES ('626', '迎泽区', '140106', '48', '3');
INSERT INTO `lizhili_area` VALUES ('627', '杏花岭区', '140107', '48', '3');
INSERT INTO `lizhili_area` VALUES ('628', '尖草坪区', '140108', '48', '3');
INSERT INTO `lizhili_area` VALUES ('629', '万柏林区', '140109', '48', '3');
INSERT INTO `lizhili_area` VALUES ('630', '晋源区', '140110', '48', '3');
INSERT INTO `lizhili_area` VALUES ('631', '清徐县', '140121', '48', '3');
INSERT INTO `lizhili_area` VALUES ('632', '阳曲县', '140122', '48', '3');
INSERT INTO `lizhili_area` VALUES ('633', '娄烦县', '140123', '48', '3');
INSERT INTO `lizhili_area` VALUES ('634', '山西转型综合改革示范区', '140171', '48', '3');
INSERT INTO `lizhili_area` VALUES ('635', '古交市', '140181', '48', '3');
INSERT INTO `lizhili_area` VALUES ('636', '城区', '140202', '49', '3');
INSERT INTO `lizhili_area` VALUES ('637', '矿区', '140203', '49', '3');
INSERT INTO `lizhili_area` VALUES ('638', '南郊区', '140211', '49', '3');
INSERT INTO `lizhili_area` VALUES ('639', '新荣区', '140212', '49', '3');
INSERT INTO `lizhili_area` VALUES ('640', '阳高县', '140221', '49', '3');
INSERT INTO `lizhili_area` VALUES ('641', '天镇县', '140222', '49', '3');
INSERT INTO `lizhili_area` VALUES ('642', '广灵县', '140223', '49', '3');
INSERT INTO `lizhili_area` VALUES ('643', '灵丘县', '140224', '49', '3');
INSERT INTO `lizhili_area` VALUES ('644', '浑源县', '140225', '49', '3');
INSERT INTO `lizhili_area` VALUES ('645', '左云县', '140226', '49', '3');
INSERT INTO `lizhili_area` VALUES ('646', '大同县', '140227', '49', '3');
INSERT INTO `lizhili_area` VALUES ('647', '山西大同经济开发区', '140271', '49', '3');
INSERT INTO `lizhili_area` VALUES ('648', '城区', '140302', '50', '3');
INSERT INTO `lizhili_area` VALUES ('649', '矿区', '140303', '50', '3');
INSERT INTO `lizhili_area` VALUES ('650', '郊区', '140311', '50', '3');
INSERT INTO `lizhili_area` VALUES ('651', '平定县', '140321', '50', '3');
INSERT INTO `lizhili_area` VALUES ('652', '盂县', '140322', '50', '3');
INSERT INTO `lizhili_area` VALUES ('653', '山西阳泉经济开发区', '140371', '50', '3');
INSERT INTO `lizhili_area` VALUES ('654', '城区', '140402', '51', '3');
INSERT INTO `lizhili_area` VALUES ('655', '郊区', '140411', '51', '3');
INSERT INTO `lizhili_area` VALUES ('656', '长治县', '140421', '51', '3');
INSERT INTO `lizhili_area` VALUES ('657', '襄垣县', '140423', '51', '3');
INSERT INTO `lizhili_area` VALUES ('658', '屯留县', '140424', '51', '3');
INSERT INTO `lizhili_area` VALUES ('659', '平顺县', '140425', '51', '3');
INSERT INTO `lizhili_area` VALUES ('660', '黎城县', '140426', '51', '3');
INSERT INTO `lizhili_area` VALUES ('661', '壶关县', '140427', '51', '3');
INSERT INTO `lizhili_area` VALUES ('662', '长子县', '140428', '51', '3');
INSERT INTO `lizhili_area` VALUES ('663', '武乡县', '140429', '51', '3');
INSERT INTO `lizhili_area` VALUES ('664', '沁县', '140430', '51', '3');
INSERT INTO `lizhili_area` VALUES ('665', '沁源县', '140431', '51', '3');
INSERT INTO `lizhili_area` VALUES ('666', '山西长治高新技术产业园区', '140471', '51', '3');
INSERT INTO `lizhili_area` VALUES ('667', '潞城市', '140481', '51', '3');
INSERT INTO `lizhili_area` VALUES ('668', '城区', '140502', '52', '3');
INSERT INTO `lizhili_area` VALUES ('669', '沁水县', '140521', '52', '3');
INSERT INTO `lizhili_area` VALUES ('670', '阳城县', '140522', '52', '3');
INSERT INTO `lizhili_area` VALUES ('671', '陵川县', '140524', '52', '3');
INSERT INTO `lizhili_area` VALUES ('672', '泽州县', '140525', '52', '3');
INSERT INTO `lizhili_area` VALUES ('673', '高平市', '140581', '52', '3');
INSERT INTO `lizhili_area` VALUES ('674', '朔城区', '140602', '53', '3');
INSERT INTO `lizhili_area` VALUES ('675', '平鲁区', '140603', '53', '3');
INSERT INTO `lizhili_area` VALUES ('676', '山阴县', '140621', '53', '3');
INSERT INTO `lizhili_area` VALUES ('677', '应县', '140622', '53', '3');
INSERT INTO `lizhili_area` VALUES ('678', '右玉县', '140623', '53', '3');
INSERT INTO `lizhili_area` VALUES ('679', '怀仁县', '140624', '53', '3');
INSERT INTO `lizhili_area` VALUES ('680', '山西朔州经济开发区', '140671', '53', '3');
INSERT INTO `lizhili_area` VALUES ('681', '榆次区', '140702', '54', '3');
INSERT INTO `lizhili_area` VALUES ('682', '榆社县', '140721', '54', '3');
INSERT INTO `lizhili_area` VALUES ('683', '左权县', '140722', '54', '3');
INSERT INTO `lizhili_area` VALUES ('684', '和顺县', '140723', '54', '3');
INSERT INTO `lizhili_area` VALUES ('685', '昔阳县', '140724', '54', '3');
INSERT INTO `lizhili_area` VALUES ('686', '寿阳县', '140725', '54', '3');
INSERT INTO `lizhili_area` VALUES ('687', '太谷县', '140726', '54', '3');
INSERT INTO `lizhili_area` VALUES ('688', '祁县', '140727', '54', '3');
INSERT INTO `lizhili_area` VALUES ('689', '平遥县', '140728', '54', '3');
INSERT INTO `lizhili_area` VALUES ('690', '灵石县', '140729', '54', '3');
INSERT INTO `lizhili_area` VALUES ('691', '介休市', '140781', '54', '3');
INSERT INTO `lizhili_area` VALUES ('692', '盐湖区', '140802', '55', '3');
INSERT INTO `lizhili_area` VALUES ('693', '临猗县', '140821', '55', '3');
INSERT INTO `lizhili_area` VALUES ('694', '万荣县', '140822', '55', '3');
INSERT INTO `lizhili_area` VALUES ('695', '闻喜县', '140823', '55', '3');
INSERT INTO `lizhili_area` VALUES ('696', '稷山县', '140824', '55', '3');
INSERT INTO `lizhili_area` VALUES ('697', '新绛县', '140825', '55', '3');
INSERT INTO `lizhili_area` VALUES ('698', '绛县', '140826', '55', '3');
INSERT INTO `lizhili_area` VALUES ('699', '垣曲县', '140827', '55', '3');
INSERT INTO `lizhili_area` VALUES ('700', '夏县', '140828', '55', '3');
INSERT INTO `lizhili_area` VALUES ('701', '平陆县', '140829', '55', '3');
INSERT INTO `lizhili_area` VALUES ('702', '芮城县', '140830', '55', '3');
INSERT INTO `lizhili_area` VALUES ('703', '永济市', '140881', '55', '3');
INSERT INTO `lizhili_area` VALUES ('704', '河津市', '140882', '55', '3');
INSERT INTO `lizhili_area` VALUES ('705', '忻府区', '140902', '56', '3');
INSERT INTO `lizhili_area` VALUES ('706', '定襄县', '140921', '56', '3');
INSERT INTO `lizhili_area` VALUES ('707', '五台县', '140922', '56', '3');
INSERT INTO `lizhili_area` VALUES ('708', '代县', '140923', '56', '3');
INSERT INTO `lizhili_area` VALUES ('709', '繁峙县', '140924', '56', '3');
INSERT INTO `lizhili_area` VALUES ('710', '宁武县', '140925', '56', '3');
INSERT INTO `lizhili_area` VALUES ('711', '静乐县', '140926', '56', '3');
INSERT INTO `lizhili_area` VALUES ('712', '神池县', '140927', '56', '3');
INSERT INTO `lizhili_area` VALUES ('713', '五寨县', '140928', '56', '3');
INSERT INTO `lizhili_area` VALUES ('714', '岢岚县', '140929', '56', '3');
INSERT INTO `lizhili_area` VALUES ('715', '河曲县', '140930', '56', '3');
INSERT INTO `lizhili_area` VALUES ('716', '保德县', '140931', '56', '3');
INSERT INTO `lizhili_area` VALUES ('717', '偏关县', '140932', '56', '3');
INSERT INTO `lizhili_area` VALUES ('718', '五台山风景名胜区', '140971', '56', '3');
INSERT INTO `lizhili_area` VALUES ('719', '原平市', '140981', '56', '3');
INSERT INTO `lizhili_area` VALUES ('720', '尧都区', '141002', '57', '3');
INSERT INTO `lizhili_area` VALUES ('721', '曲沃县', '141021', '57', '3');
INSERT INTO `lizhili_area` VALUES ('722', '翼城县', '141022', '57', '3');
INSERT INTO `lizhili_area` VALUES ('723', '襄汾县', '141023', '57', '3');
INSERT INTO `lizhili_area` VALUES ('724', '洪洞县', '141024', '57', '3');
INSERT INTO `lizhili_area` VALUES ('725', '古县', '141025', '57', '3');
INSERT INTO `lizhili_area` VALUES ('726', '安泽县', '141026', '57', '3');
INSERT INTO `lizhili_area` VALUES ('727', '浮山县', '141027', '57', '3');
INSERT INTO `lizhili_area` VALUES ('728', '吉县', '141028', '57', '3');
INSERT INTO `lizhili_area` VALUES ('729', '乡宁县', '141029', '57', '3');
INSERT INTO `lizhili_area` VALUES ('730', '大宁县', '141030', '57', '3');
INSERT INTO `lizhili_area` VALUES ('731', '隰县', '141031', '57', '3');
INSERT INTO `lizhili_area` VALUES ('732', '永和县', '141032', '57', '3');
INSERT INTO `lizhili_area` VALUES ('733', '蒲县', '141033', '57', '3');
INSERT INTO `lizhili_area` VALUES ('734', '汾西县', '141034', '57', '3');
INSERT INTO `lizhili_area` VALUES ('735', '侯马市', '141081', '57', '3');
INSERT INTO `lizhili_area` VALUES ('736', '霍州市', '141082', '57', '3');
INSERT INTO `lizhili_area` VALUES ('737', '离石区', '141102', '58', '3');
INSERT INTO `lizhili_area` VALUES ('738', '文水县', '141121', '58', '3');
INSERT INTO `lizhili_area` VALUES ('739', '交城县', '141122', '58', '3');
INSERT INTO `lizhili_area` VALUES ('740', '兴县', '141123', '58', '3');
INSERT INTO `lizhili_area` VALUES ('741', '临县', '141124', '58', '3');
INSERT INTO `lizhili_area` VALUES ('742', '柳林县', '141125', '58', '3');
INSERT INTO `lizhili_area` VALUES ('743', '石楼县', '141126', '58', '3');
INSERT INTO `lizhili_area` VALUES ('744', '岚县', '141127', '58', '3');
INSERT INTO `lizhili_area` VALUES ('745', '方山县', '141128', '58', '3');
INSERT INTO `lizhili_area` VALUES ('746', '中阳县', '141129', '58', '3');
INSERT INTO `lizhili_area` VALUES ('747', '交口县', '141130', '58', '3');
INSERT INTO `lizhili_area` VALUES ('748', '孝义市', '141181', '58', '3');
INSERT INTO `lizhili_area` VALUES ('749', '汾阳市', '141182', '58', '3');
INSERT INTO `lizhili_area` VALUES ('750', '新城区', '150102', '59', '3');
INSERT INTO `lizhili_area` VALUES ('751', '回民区', '150103', '59', '3');
INSERT INTO `lizhili_area` VALUES ('752', '玉泉区', '150104', '59', '3');
INSERT INTO `lizhili_area` VALUES ('753', '赛罕区', '150105', '59', '3');
INSERT INTO `lizhili_area` VALUES ('754', '土默特左旗', '150121', '59', '3');
INSERT INTO `lizhili_area` VALUES ('755', '托克托县', '150122', '59', '3');
INSERT INTO `lizhili_area` VALUES ('756', '和林格尔县', '150123', '59', '3');
INSERT INTO `lizhili_area` VALUES ('757', '清水河县', '150124', '59', '3');
INSERT INTO `lizhili_area` VALUES ('758', '武川县', '150125', '59', '3');
INSERT INTO `lizhili_area` VALUES ('759', '呼和浩特金海工业园区', '150171', '59', '3');
INSERT INTO `lizhili_area` VALUES ('760', '呼和浩特经济技术开发区', '150172', '59', '3');
INSERT INTO `lizhili_area` VALUES ('761', '东河区', '150202', '60', '3');
INSERT INTO `lizhili_area` VALUES ('762', '昆都仑区', '150203', '60', '3');
INSERT INTO `lizhili_area` VALUES ('763', '青山区', '150204', '60', '3');
INSERT INTO `lizhili_area` VALUES ('764', '石拐区', '150205', '60', '3');
INSERT INTO `lizhili_area` VALUES ('765', '白云鄂博矿区', '150206', '60', '3');
INSERT INTO `lizhili_area` VALUES ('766', '九原区', '150207', '60', '3');
INSERT INTO `lizhili_area` VALUES ('767', '土默特右旗', '150221', '60', '3');
INSERT INTO `lizhili_area` VALUES ('768', '固阳县', '150222', '60', '3');
INSERT INTO `lizhili_area` VALUES ('769', '达尔罕茂明安联合旗', '150223', '60', '3');
INSERT INTO `lizhili_area` VALUES ('770', '包头稀土高新技术产业开发区', '150271', '60', '3');
INSERT INTO `lizhili_area` VALUES ('771', '海勃湾区', '150302', '61', '3');
INSERT INTO `lizhili_area` VALUES ('772', '海南区', '150303', '61', '3');
INSERT INTO `lizhili_area` VALUES ('773', '乌达区', '150304', '61', '3');
INSERT INTO `lizhili_area` VALUES ('774', '红山区', '150402', '62', '3');
INSERT INTO `lizhili_area` VALUES ('775', '元宝山区', '150403', '62', '3');
INSERT INTO `lizhili_area` VALUES ('776', '松山区', '150404', '62', '3');
INSERT INTO `lizhili_area` VALUES ('777', '阿鲁科尔沁旗', '150421', '62', '3');
INSERT INTO `lizhili_area` VALUES ('778', '巴林左旗', '150422', '62', '3');
INSERT INTO `lizhili_area` VALUES ('779', '巴林右旗', '150423', '62', '3');
INSERT INTO `lizhili_area` VALUES ('780', '林西县', '150424', '62', '3');
INSERT INTO `lizhili_area` VALUES ('781', '克什克腾旗', '150425', '62', '3');
INSERT INTO `lizhili_area` VALUES ('782', '翁牛特旗', '150426', '62', '3');
INSERT INTO `lizhili_area` VALUES ('783', '喀喇沁旗', '150428', '62', '3');
INSERT INTO `lizhili_area` VALUES ('784', '宁城县', '150429', '62', '3');
INSERT INTO `lizhili_area` VALUES ('785', '敖汉旗', '150430', '62', '3');
INSERT INTO `lizhili_area` VALUES ('786', '科尔沁区', '150502', '63', '3');
INSERT INTO `lizhili_area` VALUES ('787', '科尔沁左翼中旗', '150521', '63', '3');
INSERT INTO `lizhili_area` VALUES ('788', '科尔沁左翼后旗', '150522', '63', '3');
INSERT INTO `lizhili_area` VALUES ('789', '开鲁县', '150523', '63', '3');
INSERT INTO `lizhili_area` VALUES ('790', '库伦旗', '150524', '63', '3');
INSERT INTO `lizhili_area` VALUES ('791', '奈曼旗', '150525', '63', '3');
INSERT INTO `lizhili_area` VALUES ('792', '扎鲁特旗', '150526', '63', '3');
INSERT INTO `lizhili_area` VALUES ('793', '通辽经济技术开发区', '150571', '63', '3');
INSERT INTO `lizhili_area` VALUES ('794', '霍林郭勒市', '150581', '63', '3');
INSERT INTO `lizhili_area` VALUES ('795', '东胜区', '150602', '64', '3');
INSERT INTO `lizhili_area` VALUES ('796', '康巴什区', '150603', '64', '3');
INSERT INTO `lizhili_area` VALUES ('797', '达拉特旗', '150621', '64', '3');
INSERT INTO `lizhili_area` VALUES ('798', '准格尔旗', '150622', '64', '3');
INSERT INTO `lizhili_area` VALUES ('799', '鄂托克前旗', '150623', '64', '3');
INSERT INTO `lizhili_area` VALUES ('800', '鄂托克旗', '150624', '64', '3');
INSERT INTO `lizhili_area` VALUES ('801', '杭锦旗', '150625', '64', '3');
INSERT INTO `lizhili_area` VALUES ('802', '乌审旗', '150626', '64', '3');
INSERT INTO `lizhili_area` VALUES ('803', '伊金霍洛旗', '150627', '64', '3');
INSERT INTO `lizhili_area` VALUES ('804', '海拉尔区', '150702', '65', '3');
INSERT INTO `lizhili_area` VALUES ('805', '扎赉诺尔区', '150703', '65', '3');
INSERT INTO `lizhili_area` VALUES ('806', '阿荣旗', '150721', '65', '3');
INSERT INTO `lizhili_area` VALUES ('807', '莫力达瓦达斡尔族自治旗', '150722', '65', '3');
INSERT INTO `lizhili_area` VALUES ('808', '鄂伦春自治旗', '150723', '65', '3');
INSERT INTO `lizhili_area` VALUES ('809', '鄂温克族自治旗', '150724', '65', '3');
INSERT INTO `lizhili_area` VALUES ('810', '陈巴尔虎旗', '150725', '65', '3');
INSERT INTO `lizhili_area` VALUES ('811', '新巴尔虎左旗', '150726', '65', '3');
INSERT INTO `lizhili_area` VALUES ('812', '新巴尔虎右旗', '150727', '65', '3');
INSERT INTO `lizhili_area` VALUES ('813', '满洲里市', '150781', '65', '3');
INSERT INTO `lizhili_area` VALUES ('814', '牙克石市', '150782', '65', '3');
INSERT INTO `lizhili_area` VALUES ('815', '扎兰屯市', '150783', '65', '3');
INSERT INTO `lizhili_area` VALUES ('816', '额尔古纳市', '150784', '65', '3');
INSERT INTO `lizhili_area` VALUES ('817', '根河市', '150785', '65', '3');
INSERT INTO `lizhili_area` VALUES ('818', '临河区', '150802', '66', '3');
INSERT INTO `lizhili_area` VALUES ('819', '五原县', '150821', '66', '3');
INSERT INTO `lizhili_area` VALUES ('820', '磴口县', '150822', '66', '3');
INSERT INTO `lizhili_area` VALUES ('821', '乌拉特前旗', '150823', '66', '3');
INSERT INTO `lizhili_area` VALUES ('822', '乌拉特中旗', '150824', '66', '3');
INSERT INTO `lizhili_area` VALUES ('823', '乌拉特后旗', '150825', '66', '3');
INSERT INTO `lizhili_area` VALUES ('824', '杭锦后旗', '150826', '66', '3');
INSERT INTO `lizhili_area` VALUES ('825', '集宁区', '150902', '67', '3');
INSERT INTO `lizhili_area` VALUES ('826', '卓资县', '150921', '67', '3');
INSERT INTO `lizhili_area` VALUES ('827', '化德县', '150922', '67', '3');
INSERT INTO `lizhili_area` VALUES ('828', '商都县', '150923', '67', '3');
INSERT INTO `lizhili_area` VALUES ('829', '兴和县', '150924', '67', '3');
INSERT INTO `lizhili_area` VALUES ('830', '凉城县', '150925', '67', '3');
INSERT INTO `lizhili_area` VALUES ('831', '察哈尔右翼前旗', '150926', '67', '3');
INSERT INTO `lizhili_area` VALUES ('832', '察哈尔右翼中旗', '150927', '67', '3');
INSERT INTO `lizhili_area` VALUES ('833', '察哈尔右翼后旗', '150928', '67', '3');
INSERT INTO `lizhili_area` VALUES ('834', '四子王旗', '150929', '67', '3');
INSERT INTO `lizhili_area` VALUES ('835', '丰镇市', '150981', '67', '3');
INSERT INTO `lizhili_area` VALUES ('836', '乌兰浩特市', '152201', '68', '3');
INSERT INTO `lizhili_area` VALUES ('837', '阿尔山市', '152202', '68', '3');
INSERT INTO `lizhili_area` VALUES ('838', '科尔沁右翼前旗', '152221', '68', '3');
INSERT INTO `lizhili_area` VALUES ('839', '科尔沁右翼中旗', '152222', '68', '3');
INSERT INTO `lizhili_area` VALUES ('840', '扎赉特旗', '152223', '68', '3');
INSERT INTO `lizhili_area` VALUES ('841', '突泉县', '152224', '68', '3');
INSERT INTO `lizhili_area` VALUES ('842', '二连浩特市', '152501', '69', '3');
INSERT INTO `lizhili_area` VALUES ('843', '锡林浩特市', '152502', '69', '3');
INSERT INTO `lizhili_area` VALUES ('844', '阿巴嘎旗', '152522', '69', '3');
INSERT INTO `lizhili_area` VALUES ('845', '苏尼特左旗', '152523', '69', '3');
INSERT INTO `lizhili_area` VALUES ('846', '苏尼特右旗', '152524', '69', '3');
INSERT INTO `lizhili_area` VALUES ('847', '东乌珠穆沁旗', '152525', '69', '3');
INSERT INTO `lizhili_area` VALUES ('848', '西乌珠穆沁旗', '152526', '69', '3');
INSERT INTO `lizhili_area` VALUES ('849', '太仆寺旗', '152527', '69', '3');
INSERT INTO `lizhili_area` VALUES ('850', '镶黄旗', '152528', '69', '3');
INSERT INTO `lizhili_area` VALUES ('851', '正镶白旗', '152529', '69', '3');
INSERT INTO `lizhili_area` VALUES ('852', '正蓝旗', '152530', '69', '3');
INSERT INTO `lizhili_area` VALUES ('853', '多伦县', '152531', '69', '3');
INSERT INTO `lizhili_area` VALUES ('854', '乌拉盖管委会', '152571', '69', '3');
INSERT INTO `lizhili_area` VALUES ('855', '阿拉善左旗', '152921', '70', '3');
INSERT INTO `lizhili_area` VALUES ('856', '阿拉善右旗', '152922', '70', '3');
INSERT INTO `lizhili_area` VALUES ('857', '额济纳旗', '152923', '70', '3');
INSERT INTO `lizhili_area` VALUES ('858', '内蒙古阿拉善经济开发区', '152971', '70', '3');
INSERT INTO `lizhili_area` VALUES ('859', '和平区', '210102', '71', '3');
INSERT INTO `lizhili_area` VALUES ('860', '沈河区', '210103', '71', '3');
INSERT INTO `lizhili_area` VALUES ('861', '大东区', '210104', '71', '3');
INSERT INTO `lizhili_area` VALUES ('862', '皇姑区', '210105', '71', '3');
INSERT INTO `lizhili_area` VALUES ('863', '铁西区', '210106', '71', '3');
INSERT INTO `lizhili_area` VALUES ('864', '苏家屯区', '210111', '71', '3');
INSERT INTO `lizhili_area` VALUES ('865', '浑南区', '210112', '71', '3');
INSERT INTO `lizhili_area` VALUES ('866', '沈北新区', '210113', '71', '3');
INSERT INTO `lizhili_area` VALUES ('867', '于洪区', '210114', '71', '3');
INSERT INTO `lizhili_area` VALUES ('868', '辽中区', '210115', '71', '3');
INSERT INTO `lizhili_area` VALUES ('869', '康平县', '210123', '71', '3');
INSERT INTO `lizhili_area` VALUES ('870', '法库县', '210124', '71', '3');
INSERT INTO `lizhili_area` VALUES ('871', '新民市', '210181', '71', '3');
INSERT INTO `lizhili_area` VALUES ('872', '中山区', '210202', '72', '3');
INSERT INTO `lizhili_area` VALUES ('873', '西岗区', '210203', '72', '3');
INSERT INTO `lizhili_area` VALUES ('874', '沙河口区', '210204', '72', '3');
INSERT INTO `lizhili_area` VALUES ('875', '甘井子区', '210211', '72', '3');
INSERT INTO `lizhili_area` VALUES ('876', '旅顺口区', '210212', '72', '3');
INSERT INTO `lizhili_area` VALUES ('877', '金州区', '210213', '72', '3');
INSERT INTO `lizhili_area` VALUES ('878', '普兰店区', '210214', '72', '3');
INSERT INTO `lizhili_area` VALUES ('879', '长海县', '210224', '72', '3');
INSERT INTO `lizhili_area` VALUES ('880', '瓦房店市', '210281', '72', '3');
INSERT INTO `lizhili_area` VALUES ('881', '庄河市', '210283', '72', '3');
INSERT INTO `lizhili_area` VALUES ('882', '铁东区', '210302', '73', '3');
INSERT INTO `lizhili_area` VALUES ('883', '铁西区', '210303', '73', '3');
INSERT INTO `lizhili_area` VALUES ('884', '立山区', '210304', '73', '3');
INSERT INTO `lizhili_area` VALUES ('885', '千山区', '210311', '73', '3');
INSERT INTO `lizhili_area` VALUES ('886', '台安县', '210321', '73', '3');
INSERT INTO `lizhili_area` VALUES ('887', '岫岩满族自治县', '210323', '73', '3');
INSERT INTO `lizhili_area` VALUES ('888', '海城市', '210381', '73', '3');
INSERT INTO `lizhili_area` VALUES ('889', '新抚区', '210402', '74', '3');
INSERT INTO `lizhili_area` VALUES ('890', '东洲区', '210403', '74', '3');
INSERT INTO `lizhili_area` VALUES ('891', '望花区', '210404', '74', '3');
INSERT INTO `lizhili_area` VALUES ('892', '顺城区', '210411', '74', '3');
INSERT INTO `lizhili_area` VALUES ('893', '抚顺县', '210421', '74', '3');
INSERT INTO `lizhili_area` VALUES ('894', '新宾满族自治县', '210422', '74', '3');
INSERT INTO `lizhili_area` VALUES ('895', '清原满族自治县', '210423', '74', '3');
INSERT INTO `lizhili_area` VALUES ('896', '平山区', '210502', '75', '3');
INSERT INTO `lizhili_area` VALUES ('897', '溪湖区', '210503', '75', '3');
INSERT INTO `lizhili_area` VALUES ('898', '明山区', '210504', '75', '3');
INSERT INTO `lizhili_area` VALUES ('899', '南芬区', '210505', '75', '3');
INSERT INTO `lizhili_area` VALUES ('900', '本溪满族自治县', '210521', '75', '3');
INSERT INTO `lizhili_area` VALUES ('901', '桓仁满族自治县', '210522', '75', '3');
INSERT INTO `lizhili_area` VALUES ('902', '元宝区', '210602', '76', '3');
INSERT INTO `lizhili_area` VALUES ('903', '振兴区', '210603', '76', '3');
INSERT INTO `lizhili_area` VALUES ('904', '振安区', '210604', '76', '3');
INSERT INTO `lizhili_area` VALUES ('905', '宽甸满族自治县', '210624', '76', '3');
INSERT INTO `lizhili_area` VALUES ('906', '东港市', '210681', '76', '3');
INSERT INTO `lizhili_area` VALUES ('907', '凤城市', '210682', '76', '3');
INSERT INTO `lizhili_area` VALUES ('908', '古塔区', '210702', '77', '3');
INSERT INTO `lizhili_area` VALUES ('909', '凌河区', '210703', '77', '3');
INSERT INTO `lizhili_area` VALUES ('910', '太和区', '210711', '77', '3');
INSERT INTO `lizhili_area` VALUES ('911', '黑山县', '210726', '77', '3');
INSERT INTO `lizhili_area` VALUES ('912', '义县', '210727', '77', '3');
INSERT INTO `lizhili_area` VALUES ('913', '凌海市', '210781', '77', '3');
INSERT INTO `lizhili_area` VALUES ('914', '北镇市', '210782', '77', '3');
INSERT INTO `lizhili_area` VALUES ('915', '站前区', '210802', '78', '3');
INSERT INTO `lizhili_area` VALUES ('916', '西市区', '210803', '78', '3');
INSERT INTO `lizhili_area` VALUES ('917', '鲅鱼圈区', '210804', '78', '3');
INSERT INTO `lizhili_area` VALUES ('918', '老边区', '210811', '78', '3');
INSERT INTO `lizhili_area` VALUES ('919', '盖州市', '210881', '78', '3');
INSERT INTO `lizhili_area` VALUES ('920', '大石桥市', '210882', '78', '3');
INSERT INTO `lizhili_area` VALUES ('921', '海州区', '210902', '79', '3');
INSERT INTO `lizhili_area` VALUES ('922', '新邱区', '210903', '79', '3');
INSERT INTO `lizhili_area` VALUES ('923', '太平区', '210904', '79', '3');
INSERT INTO `lizhili_area` VALUES ('924', '清河门区', '210905', '79', '3');
INSERT INTO `lizhili_area` VALUES ('925', '细河区', '210911', '79', '3');
INSERT INTO `lizhili_area` VALUES ('926', '阜新蒙古族自治县', '210921', '79', '3');
INSERT INTO `lizhili_area` VALUES ('927', '彰武县', '210922', '79', '3');
INSERT INTO `lizhili_area` VALUES ('928', '白塔区', '211002', '80', '3');
INSERT INTO `lizhili_area` VALUES ('929', '文圣区', '211003', '80', '3');
INSERT INTO `lizhili_area` VALUES ('930', '宏伟区', '211004', '80', '3');
INSERT INTO `lizhili_area` VALUES ('931', '弓长岭区', '211005', '80', '3');
INSERT INTO `lizhili_area` VALUES ('932', '太子河区', '211011', '80', '3');
INSERT INTO `lizhili_area` VALUES ('933', '辽阳县', '211021', '80', '3');
INSERT INTO `lizhili_area` VALUES ('934', '灯塔市', '211081', '80', '3');
INSERT INTO `lizhili_area` VALUES ('935', '双台子区', '211102', '81', '3');
INSERT INTO `lizhili_area` VALUES ('936', '兴隆台区', '211103', '81', '3');
INSERT INTO `lizhili_area` VALUES ('937', '大洼区', '211104', '81', '3');
INSERT INTO `lizhili_area` VALUES ('938', '盘山县', '211122', '81', '3');
INSERT INTO `lizhili_area` VALUES ('939', '银州区', '211202', '82', '3');
INSERT INTO `lizhili_area` VALUES ('940', '清河区', '211204', '82', '3');
INSERT INTO `lizhili_area` VALUES ('941', '铁岭县', '211221', '82', '3');
INSERT INTO `lizhili_area` VALUES ('942', '西丰县', '211223', '82', '3');
INSERT INTO `lizhili_area` VALUES ('943', '昌图县', '211224', '82', '3');
INSERT INTO `lizhili_area` VALUES ('944', '调兵山市', '211281', '82', '3');
INSERT INTO `lizhili_area` VALUES ('945', '开原市', '211282', '82', '3');
INSERT INTO `lizhili_area` VALUES ('946', '双塔区', '211302', '83', '3');
INSERT INTO `lizhili_area` VALUES ('947', '龙城区', '211303', '83', '3');
INSERT INTO `lizhili_area` VALUES ('948', '朝阳县', '211321', '83', '3');
INSERT INTO `lizhili_area` VALUES ('949', '建平县', '211322', '83', '3');
INSERT INTO `lizhili_area` VALUES ('950', '喀喇沁左翼蒙古族自治县', '211324', '83', '3');
INSERT INTO `lizhili_area` VALUES ('951', '北票市', '211381', '83', '3');
INSERT INTO `lizhili_area` VALUES ('952', '凌源市', '211382', '83', '3');
INSERT INTO `lizhili_area` VALUES ('953', '连山区', '211402', '84', '3');
INSERT INTO `lizhili_area` VALUES ('954', '龙港区', '211403', '84', '3');
INSERT INTO `lizhili_area` VALUES ('955', '南票区', '211404', '84', '3');
INSERT INTO `lizhili_area` VALUES ('956', '绥中县', '211421', '84', '3');
INSERT INTO `lizhili_area` VALUES ('957', '建昌县', '211422', '84', '3');
INSERT INTO `lizhili_area` VALUES ('958', '兴城市', '211481', '84', '3');
INSERT INTO `lizhili_area` VALUES ('959', '南关区', '220102', '85', '3');
INSERT INTO `lizhili_area` VALUES ('960', '宽城区', '220103', '85', '3');
INSERT INTO `lizhili_area` VALUES ('961', '朝阳区', '220104', '85', '3');
INSERT INTO `lizhili_area` VALUES ('962', '二道区', '220105', '85', '3');
INSERT INTO `lizhili_area` VALUES ('963', '绿园区', '220106', '85', '3');
INSERT INTO `lizhili_area` VALUES ('964', '双阳区', '220112', '85', '3');
INSERT INTO `lizhili_area` VALUES ('965', '九台区', '220113', '85', '3');
INSERT INTO `lizhili_area` VALUES ('966', '农安县', '220122', '85', '3');
INSERT INTO `lizhili_area` VALUES ('967', '长春经济技术开发区', '220171', '85', '3');
INSERT INTO `lizhili_area` VALUES ('968', '长春净月高新技术产业开发区', '220172', '85', '3');
INSERT INTO `lizhili_area` VALUES ('969', '长春高新技术产业开发区', '220173', '85', '3');
INSERT INTO `lizhili_area` VALUES ('970', '长春汽车经济技术开发区', '220174', '85', '3');
INSERT INTO `lizhili_area` VALUES ('971', '榆树市', '220182', '85', '3');
INSERT INTO `lizhili_area` VALUES ('972', '德惠市', '220183', '85', '3');
INSERT INTO `lizhili_area` VALUES ('973', '昌邑区', '220202', '86', '3');
INSERT INTO `lizhili_area` VALUES ('974', '龙潭区', '220203', '86', '3');
INSERT INTO `lizhili_area` VALUES ('975', '船营区', '220204', '86', '3');
INSERT INTO `lizhili_area` VALUES ('976', '丰满区', '220211', '86', '3');
INSERT INTO `lizhili_area` VALUES ('977', '永吉县', '220221', '86', '3');
INSERT INTO `lizhili_area` VALUES ('978', '吉林经济开发区', '220271', '86', '3');
INSERT INTO `lizhili_area` VALUES ('979', '吉林高新技术产业开发区', '220272', '86', '3');
INSERT INTO `lizhili_area` VALUES ('980', '吉林中国新加坡食品区', '220273', '86', '3');
INSERT INTO `lizhili_area` VALUES ('981', '蛟河市', '220281', '86', '3');
INSERT INTO `lizhili_area` VALUES ('982', '桦甸市', '220282', '86', '3');
INSERT INTO `lizhili_area` VALUES ('983', '舒兰市', '220283', '86', '3');
INSERT INTO `lizhili_area` VALUES ('984', '磐石市', '220284', '86', '3');
INSERT INTO `lizhili_area` VALUES ('985', '铁西区', '220302', '87', '3');
INSERT INTO `lizhili_area` VALUES ('986', '铁东区', '220303', '87', '3');
INSERT INTO `lizhili_area` VALUES ('987', '梨树县', '220322', '87', '3');
INSERT INTO `lizhili_area` VALUES ('988', '伊通满族自治县', '220323', '87', '3');
INSERT INTO `lizhili_area` VALUES ('989', '公主岭市', '220381', '87', '3');
INSERT INTO `lizhili_area` VALUES ('990', '双辽市', '220382', '87', '3');
INSERT INTO `lizhili_area` VALUES ('991', '龙山区', '220402', '88', '3');
INSERT INTO `lizhili_area` VALUES ('992', '西安区', '220403', '88', '3');
INSERT INTO `lizhili_area` VALUES ('993', '东丰县', '220421', '88', '3');
INSERT INTO `lizhili_area` VALUES ('994', '东辽县', '220422', '88', '3');
INSERT INTO `lizhili_area` VALUES ('995', '东昌区', '220502', '89', '3');
INSERT INTO `lizhili_area` VALUES ('996', '二道江区', '220503', '89', '3');
INSERT INTO `lizhili_area` VALUES ('997', '通化县', '220521', '89', '3');
INSERT INTO `lizhili_area` VALUES ('998', '辉南县', '220523', '89', '3');
INSERT INTO `lizhili_area` VALUES ('999', '柳河县', '220524', '89', '3');
INSERT INTO `lizhili_area` VALUES ('1000', '梅河口市', '220581', '89', '3');
INSERT INTO `lizhili_area` VALUES ('1001', '集安市', '220582', '89', '3');
INSERT INTO `lizhili_area` VALUES ('1002', '浑江区', '220602', '90', '3');
INSERT INTO `lizhili_area` VALUES ('1003', '江源区', '220605', '90', '3');
INSERT INTO `lizhili_area` VALUES ('1004', '抚松县', '220621', '90', '3');
INSERT INTO `lizhili_area` VALUES ('1005', '靖宇县', '220622', '90', '3');
INSERT INTO `lizhili_area` VALUES ('1006', '长白朝鲜族自治县', '220623', '90', '3');
INSERT INTO `lizhili_area` VALUES ('1007', '临江市', '220681', '90', '3');
INSERT INTO `lizhili_area` VALUES ('1008', '宁江区', '220702', '91', '3');
INSERT INTO `lizhili_area` VALUES ('1009', '前郭尔罗斯蒙古族自治县', '220721', '91', '3');
INSERT INTO `lizhili_area` VALUES ('1010', '长岭县', '220722', '91', '3');
INSERT INTO `lizhili_area` VALUES ('1011', '乾安县', '220723', '91', '3');
INSERT INTO `lizhili_area` VALUES ('1012', '吉林松原经济开发区', '220771', '91', '3');
INSERT INTO `lizhili_area` VALUES ('1013', '扶余市', '220781', '91', '3');
INSERT INTO `lizhili_area` VALUES ('1014', '洮北区', '220802', '92', '3');
INSERT INTO `lizhili_area` VALUES ('1015', '镇赉县', '220821', '92', '3');
INSERT INTO `lizhili_area` VALUES ('1016', '通榆县', '220822', '92', '3');
INSERT INTO `lizhili_area` VALUES ('1017', '吉林白城经济开发区', '220871', '92', '3');
INSERT INTO `lizhili_area` VALUES ('1018', '洮南市', '220881', '92', '3');
INSERT INTO `lizhili_area` VALUES ('1019', '大安市', '220882', '92', '3');
INSERT INTO `lizhili_area` VALUES ('1020', '延吉市', '222401', '93', '3');
INSERT INTO `lizhili_area` VALUES ('1021', '图们市', '222402', '93', '3');
INSERT INTO `lizhili_area` VALUES ('1022', '敦化市', '222403', '93', '3');
INSERT INTO `lizhili_area` VALUES ('1023', '珲春市', '222404', '93', '3');
INSERT INTO `lizhili_area` VALUES ('1024', '龙井市', '222405', '93', '3');
INSERT INTO `lizhili_area` VALUES ('1025', '和龙市', '222406', '93', '3');
INSERT INTO `lizhili_area` VALUES ('1026', '汪清县', '222424', '93', '3');
INSERT INTO `lizhili_area` VALUES ('1027', '安图县', '222426', '93', '3');
INSERT INTO `lizhili_area` VALUES ('1028', '道里区', '230102', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1029', '南岗区', '230103', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1030', '道外区', '230104', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1031', '平房区', '230108', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1032', '松北区', '230109', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1033', '香坊区', '230110', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1034', '呼兰区', '230111', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1035', '阿城区', '230112', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1036', '双城区', '230113', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1037', '依兰县', '230123', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1038', '方正县', '230124', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1039', '宾县', '230125', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1040', '巴彦县', '230126', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1041', '木兰县', '230127', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1042', '通河县', '230128', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1043', '延寿县', '230129', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1044', '尚志市', '230183', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1045', '五常市', '230184', '94', '3');
INSERT INTO `lizhili_area` VALUES ('1046', '龙沙区', '230202', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1047', '建华区', '230203', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1048', '铁锋区', '230204', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1049', '昂昂溪区', '230205', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1050', '富拉尔基区', '230206', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1051', '碾子山区', '230207', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1052', '梅里斯达斡尔族区', '230208', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1053', '龙江县', '230221', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1054', '依安县', '230223', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1055', '泰来县', '230224', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1056', '甘南县', '230225', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1057', '富裕县', '230227', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1058', '克山县', '230229', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1059', '克东县', '230230', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1060', '拜泉县', '230231', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1061', '讷河市', '230281', '95', '3');
INSERT INTO `lizhili_area` VALUES ('1062', '鸡冠区', '230302', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1063', '恒山区', '230303', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1064', '滴道区', '230304', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1065', '梨树区', '230305', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1066', '城子河区', '230306', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1067', '麻山区', '230307', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1068', '鸡东县', '230321', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1069', '虎林市', '230381', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1070', '密山市', '230382', '96', '3');
INSERT INTO `lizhili_area` VALUES ('1071', '向阳区', '230402', '97', '3');
INSERT INTO `lizhili_area` VALUES ('1072', '工农区', '230403', '97', '3');
INSERT INTO `lizhili_area` VALUES ('1073', '南山区', '230404', '97', '3');
INSERT INTO `lizhili_area` VALUES ('1074', '兴安区', '230405', '97', '3');
INSERT INTO `lizhili_area` VALUES ('1075', '东山区', '230406', '97', '3');
INSERT INTO `lizhili_area` VALUES ('1076', '兴山区', '230407', '97', '3');
INSERT INTO `lizhili_area` VALUES ('1077', '萝北县', '230421', '97', '3');
INSERT INTO `lizhili_area` VALUES ('1078', '绥滨县', '230422', '97', '3');
INSERT INTO `lizhili_area` VALUES ('1079', '尖山区', '230502', '98', '3');
INSERT INTO `lizhili_area` VALUES ('1080', '岭东区', '230503', '98', '3');
INSERT INTO `lizhili_area` VALUES ('1081', '四方台区', '230505', '98', '3');
INSERT INTO `lizhili_area` VALUES ('1082', '宝山区', '230506', '98', '3');
INSERT INTO `lizhili_area` VALUES ('1083', '集贤县', '230521', '98', '3');
INSERT INTO `lizhili_area` VALUES ('1084', '友谊县', '230522', '98', '3');
INSERT INTO `lizhili_area` VALUES ('1085', '宝清县', '230523', '98', '3');
INSERT INTO `lizhili_area` VALUES ('1086', '饶河县', '230524', '98', '3');
INSERT INTO `lizhili_area` VALUES ('1087', '萨尔图区', '230602', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1088', '龙凤区', '230603', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1089', '让胡路区', '230604', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1090', '红岗区', '230605', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1091', '大同区', '230606', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1092', '肇州县', '230621', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1093', '肇源县', '230622', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1094', '林甸县', '230623', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1095', '杜尔伯特蒙古族自治县', '230624', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1096', '大庆高新技术产业开发区', '230671', '99', '3');
INSERT INTO `lizhili_area` VALUES ('1097', '伊春区', '230702', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1098', '南岔区', '230703', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1099', '友好区', '230704', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1100', '西林区', '230705', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1101', '翠峦区', '230706', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1102', '新青区', '230707', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1103', '美溪区', '230708', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1104', '金山屯区', '230709', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1105', '五营区', '230710', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1106', '乌马河区', '230711', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1107', '汤旺河区', '230712', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1108', '带岭区', '230713', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1109', '乌伊岭区', '230714', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1110', '红星区', '230715', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1111', '上甘岭区', '230716', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1112', '嘉荫县', '230722', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1113', '铁力市', '230781', '100', '3');
INSERT INTO `lizhili_area` VALUES ('1114', '向阳区', '230803', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1115', '前进区', '230804', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1116', '东风区', '230805', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1117', '郊区', '230811', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1118', '桦南县', '230822', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1119', '桦川县', '230826', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1120', '汤原县', '230828', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1121', '同江市', '230881', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1122', '富锦市', '230882', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1123', '抚远市', '230883', '101', '3');
INSERT INTO `lizhili_area` VALUES ('1124', '新兴区', '230902', '102', '3');
INSERT INTO `lizhili_area` VALUES ('1125', '桃山区', '230903', '102', '3');
INSERT INTO `lizhili_area` VALUES ('1126', '茄子河区', '230904', '102', '3');
INSERT INTO `lizhili_area` VALUES ('1127', '勃利县', '230921', '102', '3');
INSERT INTO `lizhili_area` VALUES ('1128', '东安区', '231002', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1129', '阳明区', '231003', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1130', '爱民区', '231004', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1131', '西安区', '231005', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1132', '林口县', '231025', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1133', '牡丹江经济技术开发区', '231071', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1134', '绥芬河市', '231081', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1135', '海林市', '231083', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1136', '宁安市', '231084', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1137', '穆棱市', '231085', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1138', '东宁市', '231086', '103', '3');
INSERT INTO `lizhili_area` VALUES ('1139', '爱辉区', '231102', '104', '3');
INSERT INTO `lizhili_area` VALUES ('1140', '嫩江县', '231121', '104', '3');
INSERT INTO `lizhili_area` VALUES ('1141', '逊克县', '231123', '104', '3');
INSERT INTO `lizhili_area` VALUES ('1142', '孙吴县', '231124', '104', '3');
INSERT INTO `lizhili_area` VALUES ('1143', '北安市', '231181', '104', '3');
INSERT INTO `lizhili_area` VALUES ('1144', '五大连池市', '231182', '104', '3');
INSERT INTO `lizhili_area` VALUES ('1145', '北林区', '231202', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1146', '望奎县', '231221', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1147', '兰西县', '231222', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1148', '青冈县', '231223', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1149', '庆安县', '231224', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1150', '明水县', '231225', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1151', '绥棱县', '231226', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1152', '安达市', '231281', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1153', '肇东市', '231282', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1154', '海伦市', '231283', '105', '3');
INSERT INTO `lizhili_area` VALUES ('1155', '加格达奇区', '232701', '106', '3');
INSERT INTO `lizhili_area` VALUES ('1156', '松岭区', '232702', '106', '3');
INSERT INTO `lizhili_area` VALUES ('1157', '新林区', '232703', '106', '3');
INSERT INTO `lizhili_area` VALUES ('1158', '呼中区', '232704', '106', '3');
INSERT INTO `lizhili_area` VALUES ('1159', '呼玛县', '232721', '106', '3');
INSERT INTO `lizhili_area` VALUES ('1160', '塔河县', '232722', '106', '3');
INSERT INTO `lizhili_area` VALUES ('1161', '漠河县', '232723', '106', '3');
INSERT INTO `lizhili_area` VALUES ('1162', '黄浦区', '310101', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1163', '徐汇区', '310104', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1164', '长宁区', '310105', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1165', '静安区', '310106', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1166', '普陀区', '310107', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1167', '虹口区', '310109', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1168', '杨浦区', '310110', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1169', '闵行区', '310112', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1170', '宝山区', '310113', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1171', '嘉定区', '310114', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1172', '浦东新区', '310115', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1173', '金山区', '310116', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1174', '松江区', '310117', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1175', '青浦区', '310118', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1176', '奉贤区', '310120', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1177', '崇明区', '310151', '107', '3');
INSERT INTO `lizhili_area` VALUES ('1178', '玄武区', '320102', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1179', '秦淮区', '320104', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1180', '建邺区', '320105', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1181', '鼓楼区', '320106', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1182', '浦口区', '320111', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1183', '栖霞区', '320113', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1184', '雨花台区', '320114', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1185', '江宁区', '320115', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1186', '六合区', '320116', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1187', '溧水区', '320117', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1188', '高淳区', '320118', '108', '3');
INSERT INTO `lizhili_area` VALUES ('1189', '锡山区', '320205', '109', '3');
INSERT INTO `lizhili_area` VALUES ('1190', '惠山区', '320206', '109', '3');
INSERT INTO `lizhili_area` VALUES ('1191', '滨湖区', '320211', '109', '3');
INSERT INTO `lizhili_area` VALUES ('1192', '梁溪区', '320213', '109', '3');
INSERT INTO `lizhili_area` VALUES ('1193', '新吴区', '320214', '109', '3');
INSERT INTO `lizhili_area` VALUES ('1194', '江阴市', '320281', '109', '3');
INSERT INTO `lizhili_area` VALUES ('1195', '宜兴市', '320282', '109', '3');
INSERT INTO `lizhili_area` VALUES ('1196', '鼓楼区', '320302', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1197', '云龙区', '320303', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1198', '贾汪区', '320305', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1199', '泉山区', '320311', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1200', '铜山区', '320312', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1201', '丰县', '320321', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1202', '沛县', '320322', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1203', '睢宁县', '320324', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1204', '徐州经济技术开发区', '320371', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1205', '新沂市', '320381', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1206', '邳州市', '320382', '110', '3');
INSERT INTO `lizhili_area` VALUES ('1207', '天宁区', '320402', '111', '3');
INSERT INTO `lizhili_area` VALUES ('1208', '钟楼区', '320404', '111', '3');
INSERT INTO `lizhili_area` VALUES ('1209', '新北区', '320411', '111', '3');
INSERT INTO `lizhili_area` VALUES ('1210', '武进区', '320412', '111', '3');
INSERT INTO `lizhili_area` VALUES ('1211', '金坛区', '320413', '111', '3');
INSERT INTO `lizhili_area` VALUES ('1212', '溧阳市', '320481', '111', '3');
INSERT INTO `lizhili_area` VALUES ('1213', '虎丘区', '320505', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1214', '吴中区', '320506', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1215', '相城区', '320507', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1216', '姑苏区', '320508', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1217', '吴江区', '320509', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1218', '苏州工业园区', '320571', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1219', '常熟市', '320581', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1220', '张家港市', '320582', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1221', '昆山市', '320583', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1222', '太仓市', '320585', '112', '3');
INSERT INTO `lizhili_area` VALUES ('1223', '崇川区', '320602', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1224', '港闸区', '320611', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1225', '通州区', '320612', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1226', '海安县', '320621', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1227', '如东县', '320623', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1228', '南通经济技术开发区', '320671', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1229', '启东市', '320681', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1230', '如皋市', '320682', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1231', '海门市', '320684', '113', '3');
INSERT INTO `lizhili_area` VALUES ('1232', '连云区', '320703', '114', '3');
INSERT INTO `lizhili_area` VALUES ('1233', '海州区', '320706', '114', '3');
INSERT INTO `lizhili_area` VALUES ('1234', '赣榆区', '320707', '114', '3');
INSERT INTO `lizhili_area` VALUES ('1235', '东海县', '320722', '114', '3');
INSERT INTO `lizhili_area` VALUES ('1236', '灌云县', '320723', '114', '3');
INSERT INTO `lizhili_area` VALUES ('1237', '灌南县', '320724', '114', '3');
INSERT INTO `lizhili_area` VALUES ('1238', '连云港经济技术开发区', '320771', '114', '3');
INSERT INTO `lizhili_area` VALUES ('1239', '连云港高新技术产业开发区', '320772', '114', '3');
INSERT INTO `lizhili_area` VALUES ('1240', '淮安区', '320803', '115', '3');
INSERT INTO `lizhili_area` VALUES ('1241', '淮阴区', '320804', '115', '3');
INSERT INTO `lizhili_area` VALUES ('1242', '清江浦区', '320812', '115', '3');
INSERT INTO `lizhili_area` VALUES ('1243', '洪泽区', '320813', '115', '3');
INSERT INTO `lizhili_area` VALUES ('1244', '涟水县', '320826', '115', '3');
INSERT INTO `lizhili_area` VALUES ('1245', '盱眙县', '320830', '115', '3');
INSERT INTO `lizhili_area` VALUES ('1246', '金湖县', '320831', '115', '3');
INSERT INTO `lizhili_area` VALUES ('1247', '淮安经济技术开发区', '320871', '115', '3');
INSERT INTO `lizhili_area` VALUES ('1248', '亭湖区', '320902', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1249', '盐都区', '320903', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1250', '大丰区', '320904', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1251', '响水县', '320921', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1252', '滨海县', '320922', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1253', '阜宁县', '320923', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1254', '射阳县', '320924', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1255', '建湖县', '320925', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1256', '盐城经济技术开发区', '320971', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1257', '东台市', '320981', '116', '3');
INSERT INTO `lizhili_area` VALUES ('1258', '广陵区', '321002', '117', '3');
INSERT INTO `lizhili_area` VALUES ('1259', '邗江区', '321003', '117', '3');
INSERT INTO `lizhili_area` VALUES ('1260', '江都区', '321012', '117', '3');
INSERT INTO `lizhili_area` VALUES ('1261', '宝应县', '321023', '117', '3');
INSERT INTO `lizhili_area` VALUES ('1262', '扬州经济技术开发区', '321071', '117', '3');
INSERT INTO `lizhili_area` VALUES ('1263', '仪征市', '321081', '117', '3');
INSERT INTO `lizhili_area` VALUES ('1264', '高邮市', '321084', '117', '3');
INSERT INTO `lizhili_area` VALUES ('1265', '京口区', '321102', '118', '3');
INSERT INTO `lizhili_area` VALUES ('1266', '润州区', '321111', '118', '3');
INSERT INTO `lizhili_area` VALUES ('1267', '丹徒区', '321112', '118', '3');
INSERT INTO `lizhili_area` VALUES ('1268', '镇江新区', '321171', '118', '3');
INSERT INTO `lizhili_area` VALUES ('1269', '丹阳市', '321181', '118', '3');
INSERT INTO `lizhili_area` VALUES ('1270', '扬中市', '321182', '118', '3');
INSERT INTO `lizhili_area` VALUES ('1271', '句容市', '321183', '118', '3');
INSERT INTO `lizhili_area` VALUES ('1272', '海陵区', '321202', '119', '3');
INSERT INTO `lizhili_area` VALUES ('1273', '高港区', '321203', '119', '3');
INSERT INTO `lizhili_area` VALUES ('1274', '姜堰区', '321204', '119', '3');
INSERT INTO `lizhili_area` VALUES ('1275', '泰州医药高新技术产业开发区', '321271', '119', '3');
INSERT INTO `lizhili_area` VALUES ('1276', '兴化市', '321281', '119', '3');
INSERT INTO `lizhili_area` VALUES ('1277', '靖江市', '321282', '119', '3');
INSERT INTO `lizhili_area` VALUES ('1278', '泰兴市', '321283', '119', '3');
INSERT INTO `lizhili_area` VALUES ('1279', '宿城区', '321302', '120', '3');
INSERT INTO `lizhili_area` VALUES ('1280', '宿豫区', '321311', '120', '3');
INSERT INTO `lizhili_area` VALUES ('1281', '沭阳县', '321322', '120', '3');
INSERT INTO `lizhili_area` VALUES ('1282', '泗阳县', '321323', '120', '3');
INSERT INTO `lizhili_area` VALUES ('1283', '泗洪县', '321324', '120', '3');
INSERT INTO `lizhili_area` VALUES ('1284', '宿迁经济技术开发区', '321371', '120', '3');
INSERT INTO `lizhili_area` VALUES ('1285', '上城区', '330102', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1286', '下城区', '330103', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1287', '江干区', '330104', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1288', '拱墅区', '330105', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1289', '西湖区', '330106', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1290', '滨江区', '330108', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1291', '萧山区', '330109', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1292', '余杭区', '330110', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1293', '富阳区', '330111', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1294', '临安区', '330112', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1295', '桐庐县', '330122', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1296', '淳安县', '330127', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1297', '建德市', '330182', '121', '3');
INSERT INTO `lizhili_area` VALUES ('1298', '海曙区', '330203', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1299', '江北区', '330205', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1300', '北仑区', '330206', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1301', '镇海区', '330211', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1302', '鄞州区', '330212', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1303', '奉化区', '330213', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1304', '象山县', '330225', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1305', '宁海县', '330226', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1306', '余姚市', '330281', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1307', '慈溪市', '330282', '122', '3');
INSERT INTO `lizhili_area` VALUES ('1308', '鹿城区', '330302', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1309', '龙湾区', '330303', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1310', '瓯海区', '330304', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1311', '洞头区', '330305', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1312', '永嘉县', '330324', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1313', '平阳县', '330326', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1314', '苍南县', '330327', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1315', '文成县', '330328', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1316', '泰顺县', '330329', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1317', '温州经济技术开发区', '330371', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1318', '瑞安市', '330381', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1319', '乐清市', '330382', '123', '3');
INSERT INTO `lizhili_area` VALUES ('1320', '南湖区', '330402', '124', '3');
INSERT INTO `lizhili_area` VALUES ('1321', '秀洲区', '330411', '124', '3');
INSERT INTO `lizhili_area` VALUES ('1322', '嘉善县', '330421', '124', '3');
INSERT INTO `lizhili_area` VALUES ('1323', '海盐县', '330424', '124', '3');
INSERT INTO `lizhili_area` VALUES ('1324', '海宁市', '330481', '124', '3');
INSERT INTO `lizhili_area` VALUES ('1325', '平湖市', '330482', '124', '3');
INSERT INTO `lizhili_area` VALUES ('1326', '桐乡市', '330483', '124', '3');
INSERT INTO `lizhili_area` VALUES ('1327', '吴兴区', '330502', '125', '3');
INSERT INTO `lizhili_area` VALUES ('1328', '南浔区', '330503', '125', '3');
INSERT INTO `lizhili_area` VALUES ('1329', '德清县', '330521', '125', '3');
INSERT INTO `lizhili_area` VALUES ('1330', '长兴县', '330522', '125', '3');
INSERT INTO `lizhili_area` VALUES ('1331', '安吉县', '330523', '125', '3');
INSERT INTO `lizhili_area` VALUES ('1332', '越城区', '330602', '126', '3');
INSERT INTO `lizhili_area` VALUES ('1333', '柯桥区', '330603', '126', '3');
INSERT INTO `lizhili_area` VALUES ('1334', '上虞区', '330604', '126', '3');
INSERT INTO `lizhili_area` VALUES ('1335', '新昌县', '330624', '126', '3');
INSERT INTO `lizhili_area` VALUES ('1336', '诸暨市', '330681', '126', '3');
INSERT INTO `lizhili_area` VALUES ('1337', '嵊州市', '330683', '126', '3');
INSERT INTO `lizhili_area` VALUES ('1338', '婺城区', '330702', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1339', '金东区', '330703', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1340', '武义县', '330723', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1341', '浦江县', '330726', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1342', '磐安县', '330727', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1343', '兰溪市', '330781', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1344', '义乌市', '330782', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1345', '东阳市', '330783', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1346', '永康市', '330784', '127', '3');
INSERT INTO `lizhili_area` VALUES ('1347', '柯城区', '330802', '128', '3');
INSERT INTO `lizhili_area` VALUES ('1348', '衢江区', '330803', '128', '3');
INSERT INTO `lizhili_area` VALUES ('1349', '常山县', '330822', '128', '3');
INSERT INTO `lizhili_area` VALUES ('1350', '开化县', '330824', '128', '3');
INSERT INTO `lizhili_area` VALUES ('1351', '龙游县', '330825', '128', '3');
INSERT INTO `lizhili_area` VALUES ('1352', '江山市', '330881', '128', '3');
INSERT INTO `lizhili_area` VALUES ('1353', '定海区', '330902', '129', '3');
INSERT INTO `lizhili_area` VALUES ('1354', '普陀区', '330903', '129', '3');
INSERT INTO `lizhili_area` VALUES ('1355', '岱山县', '330921', '129', '3');
INSERT INTO `lizhili_area` VALUES ('1356', '嵊泗县', '330922', '129', '3');
INSERT INTO `lizhili_area` VALUES ('1357', '椒江区', '331002', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1358', '黄岩区', '331003', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1359', '路桥区', '331004', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1360', '三门县', '331022', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1361', '天台县', '331023', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1362', '仙居县', '331024', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1363', '温岭市', '331081', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1364', '临海市', '331082', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1365', '玉环市', '331083', '130', '3');
INSERT INTO `lizhili_area` VALUES ('1366', '莲都区', '331102', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1367', '青田县', '331121', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1368', '缙云县', '331122', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1369', '遂昌县', '331123', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1370', '松阳县', '331124', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1371', '云和县', '331125', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1372', '庆元县', '331126', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1373', '景宁畲族自治县', '331127', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1374', '龙泉市', '331181', '131', '3');
INSERT INTO `lizhili_area` VALUES ('1375', '瑶海区', '340102', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1376', '庐阳区', '340103', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1377', '蜀山区', '340104', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1378', '包河区', '340111', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1379', '长丰县', '340121', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1380', '肥东县', '340122', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1381', '肥西县', '340123', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1382', '庐江县', '340124', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1383', '合肥高新技术产业开发区', '340171', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1384', '合肥经济技术开发区', '340172', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1385', '合肥新站高新技术产业开发区', '340173', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1386', '巢湖市', '340181', '132', '3');
INSERT INTO `lizhili_area` VALUES ('1387', '镜湖区', '340202', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1388', '弋江区', '340203', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1389', '鸠江区', '340207', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1390', '三山区', '340208', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1391', '芜湖县', '340221', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1392', '繁昌县', '340222', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1393', '南陵县', '340223', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1394', '无为县', '340225', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1395', '芜湖经济技术开发区', '340271', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1396', '安徽芜湖长江大桥经济开发区', '340272', '133', '3');
INSERT INTO `lizhili_area` VALUES ('1397', '龙子湖区', '340302', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1398', '蚌山区', '340303', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1399', '禹会区', '340304', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1400', '淮上区', '340311', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1401', '怀远县', '340321', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1402', '五河县', '340322', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1403', '固镇县', '340323', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1404', '蚌埠市高新技术开发区', '340371', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1405', '蚌埠市经济开发区', '340372', '134', '3');
INSERT INTO `lizhili_area` VALUES ('1406', '大通区', '340402', '135', '3');
INSERT INTO `lizhili_area` VALUES ('1407', '田家庵区', '340403', '135', '3');
INSERT INTO `lizhili_area` VALUES ('1408', '谢家集区', '340404', '135', '3');
INSERT INTO `lizhili_area` VALUES ('1409', '八公山区', '340405', '135', '3');
INSERT INTO `lizhili_area` VALUES ('1410', '潘集区', '340406', '135', '3');
INSERT INTO `lizhili_area` VALUES ('1411', '凤台县', '340421', '135', '3');
INSERT INTO `lizhili_area` VALUES ('1412', '寿县', '340422', '135', '3');
INSERT INTO `lizhili_area` VALUES ('1413', '花山区', '340503', '136', '3');
INSERT INTO `lizhili_area` VALUES ('1414', '雨山区', '340504', '136', '3');
INSERT INTO `lizhili_area` VALUES ('1415', '博望区', '340506', '136', '3');
INSERT INTO `lizhili_area` VALUES ('1416', '当涂县', '340521', '136', '3');
INSERT INTO `lizhili_area` VALUES ('1417', '含山县', '340522', '136', '3');
INSERT INTO `lizhili_area` VALUES ('1418', '和县', '340523', '136', '3');
INSERT INTO `lizhili_area` VALUES ('1419', '杜集区', '340602', '137', '3');
INSERT INTO `lizhili_area` VALUES ('1420', '相山区', '340603', '137', '3');
INSERT INTO `lizhili_area` VALUES ('1421', '烈山区', '340604', '137', '3');
INSERT INTO `lizhili_area` VALUES ('1422', '濉溪县', '340621', '137', '3');
INSERT INTO `lizhili_area` VALUES ('1423', '铜官区', '340705', '138', '3');
INSERT INTO `lizhili_area` VALUES ('1424', '义安区', '340706', '138', '3');
INSERT INTO `lizhili_area` VALUES ('1425', '郊区', '340711', '138', '3');
INSERT INTO `lizhili_area` VALUES ('1426', '枞阳县', '340722', '138', '3');
INSERT INTO `lizhili_area` VALUES ('1427', '迎江区', '340802', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1428', '大观区', '340803', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1429', '宜秀区', '340811', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1430', '怀宁县', '340822', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1431', '潜山县', '340824', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1432', '太湖县', '340825', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1433', '宿松县', '340826', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1434', '望江县', '340827', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1435', '岳西县', '340828', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1436', '安徽安庆经济开发区', '340871', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1437', '桐城市', '340881', '139', '3');
INSERT INTO `lizhili_area` VALUES ('1438', '屯溪区', '341002', '140', '3');
INSERT INTO `lizhili_area` VALUES ('1439', '黄山区', '341003', '140', '3');
INSERT INTO `lizhili_area` VALUES ('1440', '徽州区', '341004', '140', '3');
INSERT INTO `lizhili_area` VALUES ('1441', '歙县', '341021', '140', '3');
INSERT INTO `lizhili_area` VALUES ('1442', '休宁县', '341022', '140', '3');
INSERT INTO `lizhili_area` VALUES ('1443', '黟县', '341023', '140', '3');
INSERT INTO `lizhili_area` VALUES ('1444', '祁门县', '341024', '140', '3');
INSERT INTO `lizhili_area` VALUES ('1445', '琅琊区', '341102', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1446', '南谯区', '341103', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1447', '来安县', '341122', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1448', '全椒县', '341124', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1449', '定远县', '341125', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1450', '凤阳县', '341126', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1451', '苏滁现代产业园', '341171', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1452', '滁州经济技术开发区', '341172', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1453', '天长市', '341181', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1454', '明光市', '341182', '141', '3');
INSERT INTO `lizhili_area` VALUES ('1455', '颍州区', '341202', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1456', '颍东区', '341203', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1457', '颍泉区', '341204', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1458', '临泉县', '341221', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1459', '太和县', '341222', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1460', '阜南县', '341225', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1461', '颍上县', '341226', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1462', '阜阳合肥现代产业园区', '341271', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1463', '阜阳经济技术开发区', '341272', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1464', '界首市', '341282', '142', '3');
INSERT INTO `lizhili_area` VALUES ('1465', '埇桥区', '341302', '143', '3');
INSERT INTO `lizhili_area` VALUES ('1466', '砀山县', '341321', '143', '3');
INSERT INTO `lizhili_area` VALUES ('1467', '萧县', '341322', '143', '3');
INSERT INTO `lizhili_area` VALUES ('1468', '灵璧县', '341323', '143', '3');
INSERT INTO `lizhili_area` VALUES ('1469', '泗县', '341324', '143', '3');
INSERT INTO `lizhili_area` VALUES ('1470', '宿州马鞍山现代产业园区', '341371', '143', '3');
INSERT INTO `lizhili_area` VALUES ('1471', '宿州经济技术开发区', '341372', '143', '3');
INSERT INTO `lizhili_area` VALUES ('1472', '金安区', '341502', '144', '3');
INSERT INTO `lizhili_area` VALUES ('1473', '裕安区', '341503', '144', '3');
INSERT INTO `lizhili_area` VALUES ('1474', '叶集区', '341504', '144', '3');
INSERT INTO `lizhili_area` VALUES ('1475', '霍邱县', '341522', '144', '3');
INSERT INTO `lizhili_area` VALUES ('1476', '舒城县', '341523', '144', '3');
INSERT INTO `lizhili_area` VALUES ('1477', '金寨县', '341524', '144', '3');
INSERT INTO `lizhili_area` VALUES ('1478', '霍山县', '341525', '144', '3');
INSERT INTO `lizhili_area` VALUES ('1479', '谯城区', '341602', '145', '3');
INSERT INTO `lizhili_area` VALUES ('1480', '涡阳县', '341621', '145', '3');
INSERT INTO `lizhili_area` VALUES ('1481', '蒙城县', '341622', '145', '3');
INSERT INTO `lizhili_area` VALUES ('1482', '利辛县', '341623', '145', '3');
INSERT INTO `lizhili_area` VALUES ('1483', '贵池区', '341702', '146', '3');
INSERT INTO `lizhili_area` VALUES ('1484', '东至县', '341721', '146', '3');
INSERT INTO `lizhili_area` VALUES ('1485', '石台县', '341722', '146', '3');
INSERT INTO `lizhili_area` VALUES ('1486', '青阳县', '341723', '146', '3');
INSERT INTO `lizhili_area` VALUES ('1487', '宣州区', '341802', '147', '3');
INSERT INTO `lizhili_area` VALUES ('1488', '郎溪县', '341821', '147', '3');
INSERT INTO `lizhili_area` VALUES ('1489', '广德县', '341822', '147', '3');
INSERT INTO `lizhili_area` VALUES ('1490', '泾县', '341823', '147', '3');
INSERT INTO `lizhili_area` VALUES ('1491', '绩溪县', '341824', '147', '3');
INSERT INTO `lizhili_area` VALUES ('1492', '旌德县', '341825', '147', '3');
INSERT INTO `lizhili_area` VALUES ('1493', '宣城市经济开发区', '341871', '147', '3');
INSERT INTO `lizhili_area` VALUES ('1494', '宁国市', '341881', '147', '3');
INSERT INTO `lizhili_area` VALUES ('1495', '鼓楼区', '350102', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1496', '台江区', '350103', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1497', '仓山区', '350104', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1498', '马尾区', '350105', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1499', '晋安区', '350111', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1500', '闽侯县', '350121', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1501', '连江县', '350122', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1502', '罗源县', '350123', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1503', '闽清县', '350124', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1504', '永泰县', '350125', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1505', '平潭县', '350128', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1506', '福清市', '350181', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1507', '长乐市', '350182', '148', '3');
INSERT INTO `lizhili_area` VALUES ('1508', '思明区', '350203', '149', '3');
INSERT INTO `lizhili_area` VALUES ('1509', '海沧区', '350205', '149', '3');
INSERT INTO `lizhili_area` VALUES ('1510', '湖里区', '350206', '149', '3');
INSERT INTO `lizhili_area` VALUES ('1511', '集美区', '350211', '149', '3');
INSERT INTO `lizhili_area` VALUES ('1512', '同安区', '350212', '149', '3');
INSERT INTO `lizhili_area` VALUES ('1513', '翔安区', '350213', '149', '3');
INSERT INTO `lizhili_area` VALUES ('1514', '城厢区', '350302', '150', '3');
INSERT INTO `lizhili_area` VALUES ('1515', '涵江区', '350303', '150', '3');
INSERT INTO `lizhili_area` VALUES ('1516', '荔城区', '350304', '150', '3');
INSERT INTO `lizhili_area` VALUES ('1517', '秀屿区', '350305', '150', '3');
INSERT INTO `lizhili_area` VALUES ('1518', '仙游县', '350322', '150', '3');
INSERT INTO `lizhili_area` VALUES ('1519', '梅列区', '350402', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1520', '三元区', '350403', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1521', '明溪县', '350421', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1522', '清流县', '350423', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1523', '宁化县', '350424', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1524', '大田县', '350425', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1525', '尤溪县', '350426', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1526', '沙县', '350427', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1527', '将乐县', '350428', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1528', '泰宁县', '350429', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1529', '建宁县', '350430', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1530', '永安市', '350481', '151', '3');
INSERT INTO `lizhili_area` VALUES ('1531', '鲤城区', '350502', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1532', '丰泽区', '350503', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1533', '洛江区', '350504', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1534', '泉港区', '350505', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1535', '惠安县', '350521', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1536', '安溪县', '350524', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1537', '永春县', '350525', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1538', '德化县', '350526', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1539', '金门县', '350527', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1540', '石狮市', '350581', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1541', '晋江市', '350582', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1542', '南安市', '350583', '152', '3');
INSERT INTO `lizhili_area` VALUES ('1543', '芗城区', '350602', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1544', '龙文区', '350603', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1545', '云霄县', '350622', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1546', '漳浦县', '350623', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1547', '诏安县', '350624', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1548', '长泰县', '350625', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1549', '东山县', '350626', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1550', '南靖县', '350627', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1551', '平和县', '350628', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1552', '华安县', '350629', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1553', '龙海市', '350681', '153', '3');
INSERT INTO `lizhili_area` VALUES ('1554', '延平区', '350702', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1555', '建阳区', '350703', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1556', '顺昌县', '350721', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1557', '浦城县', '350722', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1558', '光泽县', '350723', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1559', '松溪县', '350724', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1560', '政和县', '350725', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1561', '邵武市', '350781', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1562', '武夷山市', '350782', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1563', '建瓯市', '350783', '154', '3');
INSERT INTO `lizhili_area` VALUES ('1564', '新罗区', '350802', '155', '3');
INSERT INTO `lizhili_area` VALUES ('1565', '永定区', '350803', '155', '3');
INSERT INTO `lizhili_area` VALUES ('1566', '长汀县', '350821', '155', '3');
INSERT INTO `lizhili_area` VALUES ('1567', '上杭县', '350823', '155', '3');
INSERT INTO `lizhili_area` VALUES ('1568', '武平县', '350824', '155', '3');
INSERT INTO `lizhili_area` VALUES ('1569', '连城县', '350825', '155', '3');
INSERT INTO `lizhili_area` VALUES ('1570', '漳平市', '350881', '155', '3');
INSERT INTO `lizhili_area` VALUES ('1571', '蕉城区', '350902', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1572', '霞浦县', '350921', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1573', '古田县', '350922', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1574', '屏南县', '350923', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1575', '寿宁县', '350924', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1576', '周宁县', '350925', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1577', '柘荣县', '350926', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1578', '福安市', '350981', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1579', '福鼎市', '350982', '156', '3');
INSERT INTO `lizhili_area` VALUES ('1580', '东湖区', '360102', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1581', '西湖区', '360103', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1582', '青云谱区', '360104', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1583', '湾里区', '360105', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1584', '青山湖区', '360111', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1585', '新建区', '360112', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1586', '南昌县', '360121', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1587', '安义县', '360123', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1588', '进贤县', '360124', '157', '3');
INSERT INTO `lizhili_area` VALUES ('1589', '昌江区', '360202', '158', '3');
INSERT INTO `lizhili_area` VALUES ('1590', '珠山区', '360203', '158', '3');
INSERT INTO `lizhili_area` VALUES ('1591', '浮梁县', '360222', '158', '3');
INSERT INTO `lizhili_area` VALUES ('1592', '乐平市', '360281', '158', '3');
INSERT INTO `lizhili_area` VALUES ('1593', '安源区', '360302', '159', '3');
INSERT INTO `lizhili_area` VALUES ('1594', '湘东区', '360313', '159', '3');
INSERT INTO `lizhili_area` VALUES ('1595', '莲花县', '360321', '159', '3');
INSERT INTO `lizhili_area` VALUES ('1596', '上栗县', '360322', '159', '3');
INSERT INTO `lizhili_area` VALUES ('1597', '芦溪县', '360323', '159', '3');
INSERT INTO `lizhili_area` VALUES ('1598', '濂溪区', '360402', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1599', '浔阳区', '360403', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1600', '柴桑区', '360404', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1601', '武宁县', '360423', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1602', '修水县', '360424', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1603', '永修县', '360425', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1604', '德安县', '360426', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1605', '都昌县', '360428', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1606', '湖口县', '360429', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1607', '彭泽县', '360430', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1608', '瑞昌市', '360481', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1609', '共青城市', '360482', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1610', '庐山市', '360483', '160', '3');
INSERT INTO `lizhili_area` VALUES ('1611', '渝水区', '360502', '161', '3');
INSERT INTO `lizhili_area` VALUES ('1612', '分宜县', '360521', '161', '3');
INSERT INTO `lizhili_area` VALUES ('1613', '月湖区', '360602', '162', '3');
INSERT INTO `lizhili_area` VALUES ('1614', '余江县', '360622', '162', '3');
INSERT INTO `lizhili_area` VALUES ('1615', '贵溪市', '360681', '162', '3');
INSERT INTO `lizhili_area` VALUES ('1616', '章贡区', '360702', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1617', '南康区', '360703', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1618', '赣县区', '360704', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1619', '信丰县', '360722', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1620', '大余县', '360723', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1621', '上犹县', '360724', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1622', '崇义县', '360725', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1623', '安远县', '360726', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1624', '龙南县', '360727', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1625', '定南县', '360728', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1626', '全南县', '360729', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1627', '宁都县', '360730', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1628', '于都县', '360731', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1629', '兴国县', '360732', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1630', '会昌县', '360733', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1631', '寻乌县', '360734', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1632', '石城县', '360735', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1633', '瑞金市', '360781', '163', '3');
INSERT INTO `lizhili_area` VALUES ('1634', '吉州区', '360802', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1635', '青原区', '360803', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1636', '吉安县', '360821', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1637', '吉水县', '360822', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1638', '峡江县', '360823', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1639', '新干县', '360824', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1640', '永丰县', '360825', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1641', '泰和县', '360826', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1642', '遂川县', '360827', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1643', '万安县', '360828', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1644', '安福县', '360829', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1645', '永新县', '360830', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1646', '井冈山市', '360881', '164', '3');
INSERT INTO `lizhili_area` VALUES ('1647', '袁州区', '360902', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1648', '奉新县', '360921', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1649', '万载县', '360922', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1650', '上高县', '360923', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1651', '宜丰县', '360924', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1652', '靖安县', '360925', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1653', '铜鼓县', '360926', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1654', '丰城市', '360981', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1655', '樟树市', '360982', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1656', '高安市', '360983', '165', '3');
INSERT INTO `lizhili_area` VALUES ('1657', '临川区', '361002', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1658', '东乡区', '361003', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1659', '南城县', '361021', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1660', '黎川县', '361022', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1661', '南丰县', '361023', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1662', '崇仁县', '361024', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1663', '乐安县', '361025', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1664', '宜黄县', '361026', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1665', '金溪县', '361027', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1666', '资溪县', '361028', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1667', '广昌县', '361030', '166', '3');
INSERT INTO `lizhili_area` VALUES ('1668', '信州区', '361102', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1669', '广丰区', '361103', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1670', '上饶县', '361121', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1671', '玉山县', '361123', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1672', '铅山县', '361124', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1673', '横峰县', '361125', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1674', '弋阳县', '361126', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1675', '余干县', '361127', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1676', '鄱阳县', '361128', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1677', '万年县', '361129', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1678', '婺源县', '361130', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1679', '德兴市', '361181', '167', '3');
INSERT INTO `lizhili_area` VALUES ('1680', '历下区', '370102', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1681', '市中区', '370103', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1682', '槐荫区', '370104', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1683', '天桥区', '370105', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1684', '历城区', '370112', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1685', '长清区', '370113', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1686', '章丘区', '370114', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1687', '平阴县', '370124', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1688', '济阳县', '370125', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1689', '商河县', '370126', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1690', '济南高新技术产业开发区', '370171', '168', '3');
INSERT INTO `lizhili_area` VALUES ('1691', '市南区', '370202', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1692', '市北区', '370203', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1693', '黄岛区', '370211', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1694', '崂山区', '370212', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1695', '李沧区', '370213', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1696', '城阳区', '370214', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1697', '即墨区', '370215', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1698', '青岛高新技术产业开发区', '370271', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1699', '胶州市', '370281', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1700', '平度市', '370283', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1701', '莱西市', '370285', '169', '3');
INSERT INTO `lizhili_area` VALUES ('1702', '淄川区', '370302', '170', '3');
INSERT INTO `lizhili_area` VALUES ('1703', '张店区', '370303', '170', '3');
INSERT INTO `lizhili_area` VALUES ('1704', '博山区', '370304', '170', '3');
INSERT INTO `lizhili_area` VALUES ('1705', '临淄区', '370305', '170', '3');
INSERT INTO `lizhili_area` VALUES ('1706', '周村区', '370306', '170', '3');
INSERT INTO `lizhili_area` VALUES ('1707', '桓台县', '370321', '170', '3');
INSERT INTO `lizhili_area` VALUES ('1708', '高青县', '370322', '170', '3');
INSERT INTO `lizhili_area` VALUES ('1709', '沂源县', '370323', '170', '3');
INSERT INTO `lizhili_area` VALUES ('1710', '市中区', '370402', '171', '3');
INSERT INTO `lizhili_area` VALUES ('1711', '薛城区', '370403', '171', '3');
INSERT INTO `lizhili_area` VALUES ('1712', '峄城区', '370404', '171', '3');
INSERT INTO `lizhili_area` VALUES ('1713', '台儿庄区', '370405', '171', '3');
INSERT INTO `lizhili_area` VALUES ('1714', '山亭区', '370406', '171', '3');
INSERT INTO `lizhili_area` VALUES ('1715', '滕州市', '370481', '171', '3');
INSERT INTO `lizhili_area` VALUES ('1716', '东营区', '370502', '172', '3');
INSERT INTO `lizhili_area` VALUES ('1717', '河口区', '370503', '172', '3');
INSERT INTO `lizhili_area` VALUES ('1718', '垦利区', '370505', '172', '3');
INSERT INTO `lizhili_area` VALUES ('1719', '利津县', '370522', '172', '3');
INSERT INTO `lizhili_area` VALUES ('1720', '广饶县', '370523', '172', '3');
INSERT INTO `lizhili_area` VALUES ('1721', '东营经济技术开发区', '370571', '172', '3');
INSERT INTO `lizhili_area` VALUES ('1722', '东营港经济开发区', '370572', '172', '3');
INSERT INTO `lizhili_area` VALUES ('1723', '芝罘区', '370602', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1724', '福山区', '370611', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1725', '牟平区', '370612', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1726', '莱山区', '370613', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1727', '长岛县', '370634', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1728', '烟台高新技术产业开发区', '370671', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1729', '烟台经济技术开发区', '370672', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1730', '龙口市', '370681', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1731', '莱阳市', '370682', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1732', '莱州市', '370683', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1733', '蓬莱市', '370684', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1734', '招远市', '370685', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1735', '栖霞市', '370686', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1736', '海阳市', '370687', '173', '3');
INSERT INTO `lizhili_area` VALUES ('1737', '潍城区', '370702', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1738', '寒亭区', '370703', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1739', '坊子区', '370704', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1740', '奎文区', '370705', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1741', '临朐县', '370724', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1742', '昌乐县', '370725', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1743', '潍坊滨海经济技术开发区', '370772', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1744', '青州市', '370781', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1745', '诸城市', '370782', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1746', '寿光市', '370783', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1747', '安丘市', '370784', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1748', '高密市', '370785', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1749', '昌邑市', '370786', '174', '3');
INSERT INTO `lizhili_area` VALUES ('1750', '任城区', '370811', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1751', '兖州区', '370812', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1752', '微山县', '370826', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1753', '鱼台县', '370827', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1754', '金乡县', '370828', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1755', '嘉祥县', '370829', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1756', '汶上县', '370830', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1757', '泗水县', '370831', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1758', '梁山县', '370832', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1759', '济宁高新技术产业开发区', '370871', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1760', '曲阜市', '370881', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1761', '邹城市', '370883', '175', '3');
INSERT INTO `lizhili_area` VALUES ('1762', '泰山区', '370902', '176', '3');
INSERT INTO `lizhili_area` VALUES ('1763', '岱岳区', '370911', '176', '3');
INSERT INTO `lizhili_area` VALUES ('1764', '宁阳县', '370921', '176', '3');
INSERT INTO `lizhili_area` VALUES ('1765', '东平县', '370923', '176', '3');
INSERT INTO `lizhili_area` VALUES ('1766', '新泰市', '370982', '176', '3');
INSERT INTO `lizhili_area` VALUES ('1767', '肥城市', '370983', '176', '3');
INSERT INTO `lizhili_area` VALUES ('1768', '环翠区', '371002', '177', '3');
INSERT INTO `lizhili_area` VALUES ('1769', '文登区', '371003', '177', '3');
INSERT INTO `lizhili_area` VALUES ('1770', '威海火炬高技术产业开发区', '371071', '177', '3');
INSERT INTO `lizhili_area` VALUES ('1771', '威海经济技术开发区', '371072', '177', '3');
INSERT INTO `lizhili_area` VALUES ('1772', '威海临港经济技术开发区', '371073', '177', '3');
INSERT INTO `lizhili_area` VALUES ('1773', '荣成市', '371082', '177', '3');
INSERT INTO `lizhili_area` VALUES ('1774', '乳山市', '371083', '177', '3');
INSERT INTO `lizhili_area` VALUES ('1775', '东港区', '371102', '178', '3');
INSERT INTO `lizhili_area` VALUES ('1776', '岚山区', '371103', '178', '3');
INSERT INTO `lizhili_area` VALUES ('1777', '五莲县', '371121', '178', '3');
INSERT INTO `lizhili_area` VALUES ('1778', '莒县', '371122', '178', '3');
INSERT INTO `lizhili_area` VALUES ('1779', '日照经济技术开发区', '371171', '178', '3');
INSERT INTO `lizhili_area` VALUES ('1780', '日照国际海洋城', '371172', '178', '3');
INSERT INTO `lizhili_area` VALUES ('1781', '莱城区', '371202', '179', '3');
INSERT INTO `lizhili_area` VALUES ('1782', '钢城区', '371203', '179', '3');
INSERT INTO `lizhili_area` VALUES ('1783', '兰山区', '371302', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1784', '罗庄区', '371311', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1785', '河东区', '371312', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1786', '沂南县', '371321', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1787', '郯城县', '371322', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1788', '沂水县', '371323', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1789', '兰陵县', '371324', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1790', '费县', '371325', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1791', '平邑县', '371326', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1792', '莒南县', '371327', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1793', '蒙阴县', '371328', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1794', '临沭县', '371329', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1795', '临沂高新技术产业开发区', '371371', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1796', '临沂经济技术开发区', '371372', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1797', '临沂临港经济开发区', '371373', '180', '3');
INSERT INTO `lizhili_area` VALUES ('1798', '德城区', '371402', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1799', '陵城区', '371403', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1800', '宁津县', '371422', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1801', '庆云县', '371423', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1802', '临邑县', '371424', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1803', '齐河县', '371425', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1804', '平原县', '371426', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1805', '夏津县', '371427', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1806', '武城县', '371428', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1807', '德州经济技术开发区', '371471', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1808', '德州运河经济开发区', '371472', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1809', '乐陵市', '371481', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1810', '禹城市', '371482', '181', '3');
INSERT INTO `lizhili_area` VALUES ('1811', '东昌府区', '371502', '182', '3');
INSERT INTO `lizhili_area` VALUES ('1812', '阳谷县', '371521', '182', '3');
INSERT INTO `lizhili_area` VALUES ('1813', '莘县', '371522', '182', '3');
INSERT INTO `lizhili_area` VALUES ('1814', '茌平县', '371523', '182', '3');
INSERT INTO `lizhili_area` VALUES ('1815', '东阿县', '371524', '182', '3');
INSERT INTO `lizhili_area` VALUES ('1816', '冠县', '371525', '182', '3');
INSERT INTO `lizhili_area` VALUES ('1817', '高唐县', '371526', '182', '3');
INSERT INTO `lizhili_area` VALUES ('1818', '临清市', '371581', '182', '3');
INSERT INTO `lizhili_area` VALUES ('1819', '滨城区', '371602', '183', '3');
INSERT INTO `lizhili_area` VALUES ('1820', '沾化区', '371603', '183', '3');
INSERT INTO `lizhili_area` VALUES ('1821', '惠民县', '371621', '183', '3');
INSERT INTO `lizhili_area` VALUES ('1822', '阳信县', '371622', '183', '3');
INSERT INTO `lizhili_area` VALUES ('1823', '无棣县', '371623', '183', '3');
INSERT INTO `lizhili_area` VALUES ('1824', '博兴县', '371625', '183', '3');
INSERT INTO `lizhili_area` VALUES ('1825', '邹平县', '371626', '183', '3');
INSERT INTO `lizhili_area` VALUES ('1826', '牡丹区', '371702', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1827', '定陶区', '371703', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1828', '曹县', '371721', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1829', '单县', '371722', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1830', '成武县', '371723', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1831', '巨野县', '371724', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1832', '郓城县', '371725', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1833', '鄄城县', '371726', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1834', '东明县', '371728', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1835', '菏泽经济技术开发区', '371771', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1836', '菏泽高新技术开发区', '371772', '184', '3');
INSERT INTO `lizhili_area` VALUES ('1837', '中原区', '410102', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1838', '二七区', '410103', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1839', '管城回族区', '410104', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1840', '金水区', '410105', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1841', '上街区', '410106', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1842', '惠济区', '410108', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1843', '中牟县', '410122', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1844', '郑州经济技术开发区', '410171', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1845', '郑州高新技术产业开发区', '410172', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1846', '郑州航空港经济综合实验区', '410173', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1847', '巩义市', '410181', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1848', '荥阳市', '410182', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1849', '新密市', '410183', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1850', '新郑市', '410184', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1851', '登封市', '410185', '185', '3');
INSERT INTO `lizhili_area` VALUES ('1852', '龙亭区', '410202', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1853', '顺河回族区', '410203', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1854', '鼓楼区', '410204', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1855', '禹王台区', '410205', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1856', '祥符区', '410212', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1857', '杞县', '410221', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1858', '通许县', '410222', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1859', '尉氏县', '410223', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1860', '兰考县', '410225', '186', '3');
INSERT INTO `lizhili_area` VALUES ('1861', '老城区', '410302', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1862', '西工区', '410303', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1863', '瀍河回族区', '410304', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1864', '涧西区', '410305', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1865', '吉利区', '410306', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1866', '洛龙区', '410311', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1867', '孟津县', '410322', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1868', '新安县', '410323', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1869', '栾川县', '410324', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1870', '嵩县', '410325', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1871', '汝阳县', '410326', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1872', '宜阳县', '410327', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1873', '洛宁县', '410328', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1874', '伊川县', '410329', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1875', '洛阳高新技术产业开发区', '410371', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1876', '偃师市', '410381', '187', '3');
INSERT INTO `lizhili_area` VALUES ('1877', '新华区', '410402', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1878', '卫东区', '410403', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1879', '石龙区', '410404', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1880', '湛河区', '410411', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1881', '宝丰县', '410421', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1882', '叶县', '410422', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1883', '鲁山县', '410423', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1884', '郏县', '410425', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1885', '平顶山高新技术产业开发区', '410471', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1886', '平顶山市新城区', '410472', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1887', '舞钢市', '410481', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1888', '汝州市', '410482', '188', '3');
INSERT INTO `lizhili_area` VALUES ('1889', '文峰区', '410502', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1890', '北关区', '410503', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1891', '殷都区', '410505', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1892', '龙安区', '410506', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1893', '安阳县', '410522', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1894', '汤阴县', '410523', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1895', '滑县', '410526', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1896', '内黄县', '410527', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1897', '安阳高新技术产业开发区', '410571', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1898', '林州市', '410581', '189', '3');
INSERT INTO `lizhili_area` VALUES ('1899', '鹤山区', '410602', '190', '3');
INSERT INTO `lizhili_area` VALUES ('1900', '山城区', '410603', '190', '3');
INSERT INTO `lizhili_area` VALUES ('1901', '淇滨区', '410611', '190', '3');
INSERT INTO `lizhili_area` VALUES ('1902', '浚县', '410621', '190', '3');
INSERT INTO `lizhili_area` VALUES ('1903', '淇县', '410622', '190', '3');
INSERT INTO `lizhili_area` VALUES ('1904', '鹤壁经济技术开发区', '410671', '190', '3');
INSERT INTO `lizhili_area` VALUES ('1905', '红旗区', '410702', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1906', '卫滨区', '410703', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1907', '凤泉区', '410704', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1908', '牧野区', '410711', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1909', '新乡县', '410721', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1910', '获嘉县', '410724', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1911', '原阳县', '410725', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1912', '延津县', '410726', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1913', '封丘县', '410727', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1914', '长垣县', '410728', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1915', '新乡高新技术产业开发区', '410771', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1916', '新乡经济技术开发区', '410772', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1917', '新乡市平原城乡一体化示范区', '410773', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1918', '卫辉市', '410781', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1919', '辉县市', '410782', '191', '3');
INSERT INTO `lizhili_area` VALUES ('1920', '解放区', '410802', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1921', '中站区', '410803', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1922', '马村区', '410804', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1923', '山阳区', '410811', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1924', '修武县', '410821', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1925', '博爱县', '410822', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1926', '武陟县', '410823', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1927', '温县', '410825', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1928', '焦作城乡一体化示范区', '410871', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1929', '沁阳市', '410882', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1930', '孟州市', '410883', '192', '3');
INSERT INTO `lizhili_area` VALUES ('1931', '华龙区', '410902', '193', '3');
INSERT INTO `lizhili_area` VALUES ('1932', '清丰县', '410922', '193', '3');
INSERT INTO `lizhili_area` VALUES ('1933', '南乐县', '410923', '193', '3');
INSERT INTO `lizhili_area` VALUES ('1934', '范县', '410926', '193', '3');
INSERT INTO `lizhili_area` VALUES ('1935', '台前县', '410927', '193', '3');
INSERT INTO `lizhili_area` VALUES ('1936', '濮阳县', '410928', '193', '3');
INSERT INTO `lizhili_area` VALUES ('1937', '河南濮阳工业园区', '410971', '193', '3');
INSERT INTO `lizhili_area` VALUES ('1938', '濮阳经济技术开发区', '410972', '193', '3');
INSERT INTO `lizhili_area` VALUES ('1939', '魏都区', '411002', '194', '3');
INSERT INTO `lizhili_area` VALUES ('1940', '建安区', '411003', '194', '3');
INSERT INTO `lizhili_area` VALUES ('1941', '鄢陵县', '411024', '194', '3');
INSERT INTO `lizhili_area` VALUES ('1942', '襄城县', '411025', '194', '3');
INSERT INTO `lizhili_area` VALUES ('1943', '许昌经济技术开发区', '411071', '194', '3');
INSERT INTO `lizhili_area` VALUES ('1944', '禹州市', '411081', '194', '3');
INSERT INTO `lizhili_area` VALUES ('1945', '长葛市', '411082', '194', '3');
INSERT INTO `lizhili_area` VALUES ('1946', '源汇区', '411102', '195', '3');
INSERT INTO `lizhili_area` VALUES ('1947', '郾城区', '411103', '195', '3');
INSERT INTO `lizhili_area` VALUES ('1948', '召陵区', '411104', '195', '3');
INSERT INTO `lizhili_area` VALUES ('1949', '舞阳县', '411121', '195', '3');
INSERT INTO `lizhili_area` VALUES ('1950', '临颍县', '411122', '195', '3');
INSERT INTO `lizhili_area` VALUES ('1951', '漯河经济技术开发区', '411171', '195', '3');
INSERT INTO `lizhili_area` VALUES ('1952', '湖滨区', '411202', '196', '3');
INSERT INTO `lizhili_area` VALUES ('1953', '陕州区', '411203', '196', '3');
INSERT INTO `lizhili_area` VALUES ('1954', '渑池县', '411221', '196', '3');
INSERT INTO `lizhili_area` VALUES ('1955', '卢氏县', '411224', '196', '3');
INSERT INTO `lizhili_area` VALUES ('1956', '河南三门峡经济开发区', '411271', '196', '3');
INSERT INTO `lizhili_area` VALUES ('1957', '义马市', '411281', '196', '3');
INSERT INTO `lizhili_area` VALUES ('1958', '灵宝市', '411282', '196', '3');
INSERT INTO `lizhili_area` VALUES ('1959', '宛城区', '411302', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1960', '卧龙区', '411303', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1961', '南召县', '411321', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1962', '方城县', '411322', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1963', '西峡县', '411323', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1964', '镇平县', '411324', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1965', '内乡县', '411325', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1966', '淅川县', '411326', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1967', '社旗县', '411327', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1968', '唐河县', '411328', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1969', '新野县', '411329', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1970', '桐柏县', '411330', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1971', '南阳高新技术产业开发区', '411371', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1972', '南阳市城乡一体化示范区', '411372', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1973', '邓州市', '411381', '197', '3');
INSERT INTO `lizhili_area` VALUES ('1974', '梁园区', '411402', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1975', '睢阳区', '411403', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1976', '民权县', '411421', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1977', '睢县', '411422', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1978', '宁陵县', '411423', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1979', '柘城县', '411424', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1980', '虞城县', '411425', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1981', '夏邑县', '411426', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1982', '豫东综合物流产业聚集区', '411471', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1983', '河南商丘经济开发区', '411472', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1984', '永城市', '411481', '198', '3');
INSERT INTO `lizhili_area` VALUES ('1985', '浉河区', '411502', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1986', '平桥区', '411503', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1987', '罗山县', '411521', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1988', '光山县', '411522', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1989', '新县', '411523', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1990', '商城县', '411524', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1991', '固始县', '411525', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1992', '潢川县', '411526', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1993', '淮滨县', '411527', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1994', '息县', '411528', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1995', '信阳高新技术产业开发区', '411571', '199', '3');
INSERT INTO `lizhili_area` VALUES ('1996', '川汇区', '411602', '200', '3');
INSERT INTO `lizhili_area` VALUES ('1997', '扶沟县', '411621', '200', '3');
INSERT INTO `lizhili_area` VALUES ('1998', '西华县', '411622', '200', '3');
INSERT INTO `lizhili_area` VALUES ('1999', '商水县', '411623', '200', '3');
INSERT INTO `lizhili_area` VALUES ('2000', '沈丘县', '411624', '200', '3');
INSERT INTO `lizhili_area` VALUES ('2001', '郸城县', '411625', '200', '3');
INSERT INTO `lizhili_area` VALUES ('2002', '淮阳县', '411626', '200', '3');
INSERT INTO `lizhili_area` VALUES ('2003', '太康县', '411627', '200', '3');
INSERT INTO `lizhili_area` VALUES ('2004', '鹿邑县', '411628', '200', '3');
INSERT INTO `lizhili_area` VALUES ('2005', '河南周口经济开发区', '411671', '200', '3');
INSERT INTO `lizhili_area` VALUES ('2006', '项城市', '411681', '200', '3');
INSERT INTO `lizhili_area` VALUES ('2007', '驿城区', '411702', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2008', '西平县', '411721', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2009', '上蔡县', '411722', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2010', '平舆县', '411723', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2011', '正阳县', '411724', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2012', '确山县', '411725', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2013', '泌阳县', '411726', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2014', '汝南县', '411727', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2015', '遂平县', '411728', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2016', '新蔡县', '411729', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2017', '河南驻马店经济开发区', '411771', '201', '3');
INSERT INTO `lizhili_area` VALUES ('2018', '济源市', '419001', '202', '3');
INSERT INTO `lizhili_area` VALUES ('2019', '江岸区', '420102', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2020', '江汉区', '420103', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2021', '硚口区', '420104', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2022', '汉阳区', '420105', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2023', '武昌区', '420106', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2024', '青山区', '420107', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2025', '洪山区', '420111', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2026', '东西湖区', '420112', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2027', '汉南区', '420113', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2028', '蔡甸区', '420114', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2029', '江夏区', '420115', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2030', '黄陂区', '420116', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2031', '新洲区', '420117', '203', '3');
INSERT INTO `lizhili_area` VALUES ('2032', '黄石港区', '420202', '204', '3');
INSERT INTO `lizhili_area` VALUES ('2033', '西塞山区', '420203', '204', '3');
INSERT INTO `lizhili_area` VALUES ('2034', '下陆区', '420204', '204', '3');
INSERT INTO `lizhili_area` VALUES ('2035', '铁山区', '420205', '204', '3');
INSERT INTO `lizhili_area` VALUES ('2036', '阳新县', '420222', '204', '3');
INSERT INTO `lizhili_area` VALUES ('2037', '大冶市', '420281', '204', '3');
INSERT INTO `lizhili_area` VALUES ('2038', '茅箭区', '420302', '205', '3');
INSERT INTO `lizhili_area` VALUES ('2039', '张湾区', '420303', '205', '3');
INSERT INTO `lizhili_area` VALUES ('2040', '郧阳区', '420304', '205', '3');
INSERT INTO `lizhili_area` VALUES ('2041', '郧西县', '420322', '205', '3');
INSERT INTO `lizhili_area` VALUES ('2042', '竹山县', '420323', '205', '3');
INSERT INTO `lizhili_area` VALUES ('2043', '竹溪县', '420324', '205', '3');
INSERT INTO `lizhili_area` VALUES ('2044', '房县', '420325', '205', '3');
INSERT INTO `lizhili_area` VALUES ('2045', '丹江口市', '420381', '205', '3');
INSERT INTO `lizhili_area` VALUES ('2046', '西陵区', '420502', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2047', '伍家岗区', '420503', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2048', '点军区', '420504', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2049', '猇亭区', '420505', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2050', '夷陵区', '420506', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2051', '远安县', '420525', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2052', '兴山县', '420526', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2053', '秭归县', '420527', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2054', '长阳土家族自治县', '420528', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2055', '五峰土家族自治县', '420529', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2056', '宜都市', '420581', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2057', '当阳市', '420582', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2058', '枝江市', '420583', '206', '3');
INSERT INTO `lizhili_area` VALUES ('2059', '襄城区', '420602', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2060', '樊城区', '420606', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2061', '襄州区', '420607', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2062', '南漳县', '420624', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2063', '谷城县', '420625', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2064', '保康县', '420626', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2065', '老河口市', '420682', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2066', '枣阳市', '420683', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2067', '宜城市', '420684', '207', '3');
INSERT INTO `lizhili_area` VALUES ('2068', '梁子湖区', '420702', '208', '3');
INSERT INTO `lizhili_area` VALUES ('2069', '华容区', '420703', '208', '3');
INSERT INTO `lizhili_area` VALUES ('2070', '鄂城区', '420704', '208', '3');
INSERT INTO `lizhili_area` VALUES ('2071', '东宝区', '420802', '209', '3');
INSERT INTO `lizhili_area` VALUES ('2072', '掇刀区', '420804', '209', '3');
INSERT INTO `lizhili_area` VALUES ('2073', '京山县', '420821', '209', '3');
INSERT INTO `lizhili_area` VALUES ('2074', '沙洋县', '420822', '209', '3');
INSERT INTO `lizhili_area` VALUES ('2075', '钟祥市', '420881', '209', '3');
INSERT INTO `lizhili_area` VALUES ('2076', '孝南区', '420902', '210', '3');
INSERT INTO `lizhili_area` VALUES ('2077', '孝昌县', '420921', '210', '3');
INSERT INTO `lizhili_area` VALUES ('2078', '大悟县', '420922', '210', '3');
INSERT INTO `lizhili_area` VALUES ('2079', '云梦县', '420923', '210', '3');
INSERT INTO `lizhili_area` VALUES ('2080', '应城市', '420981', '210', '3');
INSERT INTO `lizhili_area` VALUES ('2081', '安陆市', '420982', '210', '3');
INSERT INTO `lizhili_area` VALUES ('2082', '汉川市', '420984', '210', '3');
INSERT INTO `lizhili_area` VALUES ('2083', '沙市区', '421002', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2084', '荆州区', '421003', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2085', '公安县', '421022', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2086', '监利县', '421023', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2087', '江陵县', '421024', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2088', '荆州经济技术开发区', '421071', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2089', '石首市', '421081', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2090', '洪湖市', '421083', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2091', '松滋市', '421087', '211', '3');
INSERT INTO `lizhili_area` VALUES ('2092', '黄州区', '421102', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2093', '团风县', '421121', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2094', '红安县', '421122', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2095', '罗田县', '421123', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2096', '英山县', '421124', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2097', '浠水县', '421125', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2098', '蕲春县', '421126', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2099', '黄梅县', '421127', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2100', '龙感湖管理区', '421171', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2101', '麻城市', '421181', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2102', '武穴市', '421182', '212', '3');
INSERT INTO `lizhili_area` VALUES ('2103', '咸安区', '421202', '213', '3');
INSERT INTO `lizhili_area` VALUES ('2104', '嘉鱼县', '421221', '213', '3');
INSERT INTO `lizhili_area` VALUES ('2105', '通城县', '421222', '213', '3');
INSERT INTO `lizhili_area` VALUES ('2106', '崇阳县', '421223', '213', '3');
INSERT INTO `lizhili_area` VALUES ('2107', '通山县', '421224', '213', '3');
INSERT INTO `lizhili_area` VALUES ('2108', '赤壁市', '421281', '213', '3');
INSERT INTO `lizhili_area` VALUES ('2109', '曾都区', '421303', '214', '3');
INSERT INTO `lizhili_area` VALUES ('2110', '随县', '421321', '214', '3');
INSERT INTO `lizhili_area` VALUES ('2111', '广水市', '421381', '214', '3');
INSERT INTO `lizhili_area` VALUES ('2112', '恩施市', '422801', '215', '3');
INSERT INTO `lizhili_area` VALUES ('2113', '利川市', '422802', '215', '3');
INSERT INTO `lizhili_area` VALUES ('2114', '建始县', '422822', '215', '3');
INSERT INTO `lizhili_area` VALUES ('2115', '巴东县', '422823', '215', '3');
INSERT INTO `lizhili_area` VALUES ('2116', '宣恩县', '422825', '215', '3');
INSERT INTO `lizhili_area` VALUES ('2117', '咸丰县', '422826', '215', '3');
INSERT INTO `lizhili_area` VALUES ('2118', '来凤县', '422827', '215', '3');
INSERT INTO `lizhili_area` VALUES ('2119', '鹤峰县', '422828', '215', '3');
INSERT INTO `lizhili_area` VALUES ('2120', '仙桃市', '429004', '216', '3');
INSERT INTO `lizhili_area` VALUES ('2121', '潜江市', '429005', '216', '3');
INSERT INTO `lizhili_area` VALUES ('2122', '天门市', '429006', '216', '3');
INSERT INTO `lizhili_area` VALUES ('2123', '神农架林区', '429021', '216', '3');
INSERT INTO `lizhili_area` VALUES ('2124', '芙蓉区', '430102', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2125', '天心区', '430103', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2126', '岳麓区', '430104', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2127', '开福区', '430105', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2128', '雨花区', '430111', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2129', '望城区', '430112', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2130', '长沙县', '430121', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2131', '浏阳市', '430181', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2132', '宁乡市', '430182', '217', '3');
INSERT INTO `lizhili_area` VALUES ('2133', '荷塘区', '430202', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2134', '芦淞区', '430203', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2135', '石峰区', '430204', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2136', '天元区', '430211', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2137', '株洲县', '430221', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2138', '攸县', '430223', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2139', '茶陵县', '430224', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2140', '炎陵县', '430225', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2141', '云龙示范区', '430271', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2142', '醴陵市', '430281', '218', '3');
INSERT INTO `lizhili_area` VALUES ('2143', '雨湖区', '430302', '219', '3');
INSERT INTO `lizhili_area` VALUES ('2144', '岳塘区', '430304', '219', '3');
INSERT INTO `lizhili_area` VALUES ('2145', '湘潭县', '430321', '219', '3');
INSERT INTO `lizhili_area` VALUES ('2146', '湖南湘潭高新技术产业园区', '430371', '219', '3');
INSERT INTO `lizhili_area` VALUES ('2147', '湘潭昭山示范区', '430372', '219', '3');
INSERT INTO `lizhili_area` VALUES ('2148', '湘潭九华示范区', '430373', '219', '3');
INSERT INTO `lizhili_area` VALUES ('2149', '湘乡市', '430381', '219', '3');
INSERT INTO `lizhili_area` VALUES ('2150', '韶山市', '430382', '219', '3');
INSERT INTO `lizhili_area` VALUES ('2151', '珠晖区', '430405', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2152', '雁峰区', '430406', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2153', '石鼓区', '430407', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2154', '蒸湘区', '430408', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2155', '南岳区', '430412', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2156', '衡阳县', '430421', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2157', '衡南县', '430422', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2158', '衡山县', '430423', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2159', '衡东县', '430424', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2160', '祁东县', '430426', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2161', '衡阳综合保税区', '430471', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2162', '湖南衡阳高新技术产业园区', '430472', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2163', '湖南衡阳松木经济开发区', '430473', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2164', '耒阳市', '430481', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2165', '常宁市', '430482', '220', '3');
INSERT INTO `lizhili_area` VALUES ('2166', '双清区', '430502', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2167', '大祥区', '430503', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2168', '北塔区', '430511', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2169', '邵东县', '430521', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2170', '新邵县', '430522', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2171', '邵阳县', '430523', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2172', '隆回县', '430524', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2173', '洞口县', '430525', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2174', '绥宁县', '430527', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2175', '新宁县', '430528', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2176', '城步苗族自治县', '430529', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2177', '武冈市', '430581', '221', '3');
INSERT INTO `lizhili_area` VALUES ('2178', '岳阳楼区', '430602', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2179', '云溪区', '430603', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2180', '君山区', '430611', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2181', '岳阳县', '430621', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2182', '华容县', '430623', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2183', '湘阴县', '430624', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2184', '平江县', '430626', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2185', '岳阳市屈原管理区', '430671', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2186', '汨罗市', '430681', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2187', '临湘市', '430682', '222', '3');
INSERT INTO `lizhili_area` VALUES ('2188', '武陵区', '430702', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2189', '鼎城区', '430703', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2190', '安乡县', '430721', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2191', '汉寿县', '430722', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2192', '澧县', '430723', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2193', '临澧县', '430724', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2194', '桃源县', '430725', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2195', '石门县', '430726', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2196', '常德市西洞庭管理区', '430771', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2197', '津市市', '430781', '223', '3');
INSERT INTO `lizhili_area` VALUES ('2198', '永定区', '430802', '224', '3');
INSERT INTO `lizhili_area` VALUES ('2199', '武陵源区', '430811', '224', '3');
INSERT INTO `lizhili_area` VALUES ('2200', '慈利县', '430821', '224', '3');
INSERT INTO `lizhili_area` VALUES ('2201', '桑植县', '430822', '224', '3');
INSERT INTO `lizhili_area` VALUES ('2202', '资阳区', '430902', '225', '3');
INSERT INTO `lizhili_area` VALUES ('2203', '赫山区', '430903', '225', '3');
INSERT INTO `lizhili_area` VALUES ('2204', '南县', '430921', '225', '3');
INSERT INTO `lizhili_area` VALUES ('2205', '桃江县', '430922', '225', '3');
INSERT INTO `lizhili_area` VALUES ('2206', '安化县', '430923', '225', '3');
INSERT INTO `lizhili_area` VALUES ('2207', '益阳市大通湖管理区', '430971', '225', '3');
INSERT INTO `lizhili_area` VALUES ('2208', '湖南益阳高新技术产业园区', '430972', '225', '3');
INSERT INTO `lizhili_area` VALUES ('2209', '沅江市', '430981', '225', '3');
INSERT INTO `lizhili_area` VALUES ('2210', '北湖区', '431002', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2211', '苏仙区', '431003', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2212', '桂阳县', '431021', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2213', '宜章县', '431022', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2214', '永兴县', '431023', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2215', '嘉禾县', '431024', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2216', '临武县', '431025', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2217', '汝城县', '431026', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2218', '桂东县', '431027', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2219', '安仁县', '431028', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2220', '资兴市', '431081', '226', '3');
INSERT INTO `lizhili_area` VALUES ('2221', '零陵区', '431102', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2222', '冷水滩区', '431103', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2223', '祁阳县', '431121', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2224', '东安县', '431122', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2225', '双牌县', '431123', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2226', '道县', '431124', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2227', '江永县', '431125', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2228', '宁远县', '431126', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2229', '蓝山县', '431127', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2230', '新田县', '431128', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2231', '江华瑶族自治县', '431129', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2232', '永州经济技术开发区', '431171', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2233', '永州市金洞管理区', '431172', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2234', '永州市回龙圩管理区', '431173', '227', '3');
INSERT INTO `lizhili_area` VALUES ('2235', '鹤城区', '431202', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2236', '中方县', '431221', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2237', '沅陵县', '431222', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2238', '辰溪县', '431223', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2239', '溆浦县', '431224', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2240', '会同县', '431225', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2241', '麻阳苗族自治县', '431226', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2242', '新晃侗族自治县', '431227', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2243', '芷江侗族自治县', '431228', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2244', '靖州苗族侗族自治县', '431229', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2245', '通道侗族自治县', '431230', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2246', '怀化市洪江管理区', '431271', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2247', '洪江市', '431281', '228', '3');
INSERT INTO `lizhili_area` VALUES ('2248', '娄星区', '431302', '229', '3');
INSERT INTO `lizhili_area` VALUES ('2249', '双峰县', '431321', '229', '3');
INSERT INTO `lizhili_area` VALUES ('2250', '新化县', '431322', '229', '3');
INSERT INTO `lizhili_area` VALUES ('2251', '冷水江市', '431381', '229', '3');
INSERT INTO `lizhili_area` VALUES ('2252', '涟源市', '431382', '229', '3');
INSERT INTO `lizhili_area` VALUES ('2253', '吉首市', '433101', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2254', '泸溪县', '433122', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2255', '凤凰县', '433123', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2256', '花垣县', '433124', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2257', '保靖县', '433125', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2258', '古丈县', '433126', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2259', '永顺县', '433127', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2260', '龙山县', '433130', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2261', '湖南吉首经济开发区', '433172', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2262', '湖南永顺经济开发区', '433173', '230', '3');
INSERT INTO `lizhili_area` VALUES ('2263', '荔湾区', '440103', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2264', '越秀区', '440104', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2265', '海珠区', '440105', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2266', '天河区', '440106', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2267', '白云区', '440111', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2268', '黄埔区', '440112', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2269', '番禺区', '440113', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2270', '花都区', '440114', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2271', '南沙区', '440115', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2272', '从化区', '440117', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2273', '增城区', '440118', '231', '3');
INSERT INTO `lizhili_area` VALUES ('2274', '武江区', '440203', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2275', '浈江区', '440204', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2276', '曲江区', '440205', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2277', '始兴县', '440222', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2278', '仁化县', '440224', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2279', '翁源县', '440229', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2280', '乳源瑶族自治县', '440232', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2281', '新丰县', '440233', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2282', '乐昌市', '440281', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2283', '南雄市', '440282', '232', '3');
INSERT INTO `lizhili_area` VALUES ('2284', '罗湖区', '440303', '233', '3');
INSERT INTO `lizhili_area` VALUES ('2285', '福田区', '440304', '233', '3');
INSERT INTO `lizhili_area` VALUES ('2286', '南山区', '440305', '233', '3');
INSERT INTO `lizhili_area` VALUES ('2287', '宝安区', '440306', '233', '3');
INSERT INTO `lizhili_area` VALUES ('2288', '龙岗区', '440307', '233', '3');
INSERT INTO `lizhili_area` VALUES ('2289', '盐田区', '440308', '233', '3');
INSERT INTO `lizhili_area` VALUES ('2290', '龙华区', '440309', '233', '3');
INSERT INTO `lizhili_area` VALUES ('2291', '坪山区', '440310', '233', '3');
INSERT INTO `lizhili_area` VALUES ('2292', '香洲区', '440402', '234', '3');
INSERT INTO `lizhili_area` VALUES ('2293', '斗门区', '440403', '234', '3');
INSERT INTO `lizhili_area` VALUES ('2294', '金湾区', '440404', '234', '3');
INSERT INTO `lizhili_area` VALUES ('2295', '龙湖区', '440507', '235', '3');
INSERT INTO `lizhili_area` VALUES ('2296', '金平区', '440511', '235', '3');
INSERT INTO `lizhili_area` VALUES ('2297', '濠江区', '440512', '235', '3');
INSERT INTO `lizhili_area` VALUES ('2298', '潮阳区', '440513', '235', '3');
INSERT INTO `lizhili_area` VALUES ('2299', '潮南区', '440514', '235', '3');
INSERT INTO `lizhili_area` VALUES ('2300', '澄海区', '440515', '235', '3');
INSERT INTO `lizhili_area` VALUES ('2301', '南澳县', '440523', '235', '3');
INSERT INTO `lizhili_area` VALUES ('2302', '禅城区', '440604', '236', '3');
INSERT INTO `lizhili_area` VALUES ('2303', '南海区', '440605', '236', '3');
INSERT INTO `lizhili_area` VALUES ('2304', '顺德区', '440606', '236', '3');
INSERT INTO `lizhili_area` VALUES ('2305', '三水区', '440607', '236', '3');
INSERT INTO `lizhili_area` VALUES ('2306', '高明区', '440608', '236', '3');
INSERT INTO `lizhili_area` VALUES ('2307', '蓬江区', '440703', '237', '3');
INSERT INTO `lizhili_area` VALUES ('2308', '江海区', '440704', '237', '3');
INSERT INTO `lizhili_area` VALUES ('2309', '新会区', '440705', '237', '3');
INSERT INTO `lizhili_area` VALUES ('2310', '台山市', '440781', '237', '3');
INSERT INTO `lizhili_area` VALUES ('2311', '开平市', '440783', '237', '3');
INSERT INTO `lizhili_area` VALUES ('2312', '鹤山市', '440784', '237', '3');
INSERT INTO `lizhili_area` VALUES ('2313', '恩平市', '440785', '237', '3');
INSERT INTO `lizhili_area` VALUES ('2314', '赤坎区', '440802', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2315', '霞山区', '440803', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2316', '坡头区', '440804', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2317', '麻章区', '440811', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2318', '遂溪县', '440823', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2319', '徐闻县', '440825', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2320', '廉江市', '440881', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2321', '雷州市', '440882', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2322', '吴川市', '440883', '238', '3');
INSERT INTO `lizhili_area` VALUES ('2323', '茂南区', '440902', '239', '3');
INSERT INTO `lizhili_area` VALUES ('2324', '电白区', '440904', '239', '3');
INSERT INTO `lizhili_area` VALUES ('2325', '高州市', '440981', '239', '3');
INSERT INTO `lizhili_area` VALUES ('2326', '化州市', '440982', '239', '3');
INSERT INTO `lizhili_area` VALUES ('2327', '信宜市', '440983', '239', '3');
INSERT INTO `lizhili_area` VALUES ('2328', '端州区', '441202', '240', '3');
INSERT INTO `lizhili_area` VALUES ('2329', '鼎湖区', '441203', '240', '3');
INSERT INTO `lizhili_area` VALUES ('2330', '高要区', '441204', '240', '3');
INSERT INTO `lizhili_area` VALUES ('2331', '广宁县', '441223', '240', '3');
INSERT INTO `lizhili_area` VALUES ('2332', '怀集县', '441224', '240', '3');
INSERT INTO `lizhili_area` VALUES ('2333', '封开县', '441225', '240', '3');
INSERT INTO `lizhili_area` VALUES ('2334', '德庆县', '441226', '240', '3');
INSERT INTO `lizhili_area` VALUES ('2335', '四会市', '441284', '240', '3');
INSERT INTO `lizhili_area` VALUES ('2336', '惠城区', '441302', '241', '3');
INSERT INTO `lizhili_area` VALUES ('2337', '惠阳区', '441303', '241', '3');
INSERT INTO `lizhili_area` VALUES ('2338', '博罗县', '441322', '241', '3');
INSERT INTO `lizhili_area` VALUES ('2339', '惠东县', '441323', '241', '3');
INSERT INTO `lizhili_area` VALUES ('2340', '龙门县', '441324', '241', '3');
INSERT INTO `lizhili_area` VALUES ('2341', '梅江区', '441402', '242', '3');
INSERT INTO `lizhili_area` VALUES ('2342', '梅县区', '441403', '242', '3');
INSERT INTO `lizhili_area` VALUES ('2343', '大埔县', '441422', '242', '3');
INSERT INTO `lizhili_area` VALUES ('2344', '丰顺县', '441423', '242', '3');
INSERT INTO `lizhili_area` VALUES ('2345', '五华县', '441424', '242', '3');
INSERT INTO `lizhili_area` VALUES ('2346', '平远县', '441426', '242', '3');
INSERT INTO `lizhili_area` VALUES ('2347', '蕉岭县', '441427', '242', '3');
INSERT INTO `lizhili_area` VALUES ('2348', '兴宁市', '441481', '242', '3');
INSERT INTO `lizhili_area` VALUES ('2349', '城区', '441502', '243', '3');
INSERT INTO `lizhili_area` VALUES ('2350', '海丰县', '441521', '243', '3');
INSERT INTO `lizhili_area` VALUES ('2351', '陆河县', '441523', '243', '3');
INSERT INTO `lizhili_area` VALUES ('2352', '陆丰市', '441581', '243', '3');
INSERT INTO `lizhili_area` VALUES ('2353', '源城区', '441602', '244', '3');
INSERT INTO `lizhili_area` VALUES ('2354', '紫金县', '441621', '244', '3');
INSERT INTO `lizhili_area` VALUES ('2355', '龙川县', '441622', '244', '3');
INSERT INTO `lizhili_area` VALUES ('2356', '连平县', '441623', '244', '3');
INSERT INTO `lizhili_area` VALUES ('2357', '和平县', '441624', '244', '3');
INSERT INTO `lizhili_area` VALUES ('2358', '东源县', '441625', '244', '3');
INSERT INTO `lizhili_area` VALUES ('2359', '江城区', '441702', '245', '3');
INSERT INTO `lizhili_area` VALUES ('2360', '阳东区', '441704', '245', '3');
INSERT INTO `lizhili_area` VALUES ('2361', '阳西县', '441721', '245', '3');
INSERT INTO `lizhili_area` VALUES ('2362', '阳春市', '441781', '245', '3');
INSERT INTO `lizhili_area` VALUES ('2363', '清城区', '441802', '246', '3');
INSERT INTO `lizhili_area` VALUES ('2364', '清新区', '441803', '246', '3');
INSERT INTO `lizhili_area` VALUES ('2365', '佛冈县', '441821', '246', '3');
INSERT INTO `lizhili_area` VALUES ('2366', '阳山县', '441823', '246', '3');
INSERT INTO `lizhili_area` VALUES ('2367', '连山壮族瑶族自治县', '441825', '246', '3');
INSERT INTO `lizhili_area` VALUES ('2368', '连南瑶族自治县', '441826', '246', '3');
INSERT INTO `lizhili_area` VALUES ('2369', '英德市', '441881', '246', '3');
INSERT INTO `lizhili_area` VALUES ('2370', '连州市', '441882', '246', '3');
INSERT INTO `lizhili_area` VALUES ('2371', '东莞市', '441900', '247', '3');
INSERT INTO `lizhili_area` VALUES ('2372', '中山市', '442000', '248', '3');
INSERT INTO `lizhili_area` VALUES ('2373', '湘桥区', '445102', '249', '3');
INSERT INTO `lizhili_area` VALUES ('2374', '潮安区', '445103', '249', '3');
INSERT INTO `lizhili_area` VALUES ('2375', '饶平县', '445122', '249', '3');
INSERT INTO `lizhili_area` VALUES ('2376', '榕城区', '445202', '250', '3');
INSERT INTO `lizhili_area` VALUES ('2377', '揭东区', '445203', '250', '3');
INSERT INTO `lizhili_area` VALUES ('2378', '揭西县', '445222', '250', '3');
INSERT INTO `lizhili_area` VALUES ('2379', '惠来县', '445224', '250', '3');
INSERT INTO `lizhili_area` VALUES ('2380', '普宁市', '445281', '250', '3');
INSERT INTO `lizhili_area` VALUES ('2381', '云城区', '445302', '251', '3');
INSERT INTO `lizhili_area` VALUES ('2382', '云安区', '445303', '251', '3');
INSERT INTO `lizhili_area` VALUES ('2383', '新兴县', '445321', '251', '3');
INSERT INTO `lizhili_area` VALUES ('2384', '郁南县', '445322', '251', '3');
INSERT INTO `lizhili_area` VALUES ('2385', '罗定市', '445381', '251', '3');
INSERT INTO `lizhili_area` VALUES ('2386', '兴宁区', '450102', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2387', '青秀区', '450103', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2388', '江南区', '450105', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2389', '西乡塘区', '450107', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2390', '良庆区', '450108', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2391', '邕宁区', '450109', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2392', '武鸣区', '450110', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2393', '隆安县', '450123', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2394', '马山县', '450124', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2395', '上林县', '450125', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2396', '宾阳县', '450126', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2397', '横县', '450127', '252', '3');
INSERT INTO `lizhili_area` VALUES ('2398', '城中区', '450202', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2399', '鱼峰区', '450203', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2400', '柳南区', '450204', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2401', '柳北区', '450205', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2402', '柳江区', '450206', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2403', '柳城县', '450222', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2404', '鹿寨县', '450223', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2405', '融安县', '450224', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2406', '融水苗族自治县', '450225', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2407', '三江侗族自治县', '450226', '253', '3');
INSERT INTO `lizhili_area` VALUES ('2408', '秀峰区', '450302', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2409', '叠彩区', '450303', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2410', '象山区', '450304', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2411', '七星区', '450305', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2412', '雁山区', '450311', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2413', '临桂区', '450312', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2414', '阳朔县', '450321', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2415', '灵川县', '450323', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2416', '全州县', '450324', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2417', '兴安县', '450325', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2418', '永福县', '450326', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2419', '灌阳县', '450327', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2420', '龙胜各族自治县', '450328', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2421', '资源县', '450329', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2422', '平乐县', '450330', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2423', '荔浦县', '450331', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2424', '恭城瑶族自治县', '450332', '254', '3');
INSERT INTO `lizhili_area` VALUES ('2425', '万秀区', '450403', '255', '3');
INSERT INTO `lizhili_area` VALUES ('2426', '长洲区', '450405', '255', '3');
INSERT INTO `lizhili_area` VALUES ('2427', '龙圩区', '450406', '255', '3');
INSERT INTO `lizhili_area` VALUES ('2428', '苍梧县', '450421', '255', '3');
INSERT INTO `lizhili_area` VALUES ('2429', '藤县', '450422', '255', '3');
INSERT INTO `lizhili_area` VALUES ('2430', '蒙山县', '450423', '255', '3');
INSERT INTO `lizhili_area` VALUES ('2431', '岑溪市', '450481', '255', '3');
INSERT INTO `lizhili_area` VALUES ('2432', '海城区', '450502', '256', '3');
INSERT INTO `lizhili_area` VALUES ('2433', '银海区', '450503', '256', '3');
INSERT INTO `lizhili_area` VALUES ('2434', '铁山港区', '450512', '256', '3');
INSERT INTO `lizhili_area` VALUES ('2435', '合浦县', '450521', '256', '3');
INSERT INTO `lizhili_area` VALUES ('2436', '港口区', '450602', '257', '3');
INSERT INTO `lizhili_area` VALUES ('2437', '防城区', '450603', '257', '3');
INSERT INTO `lizhili_area` VALUES ('2438', '上思县', '450621', '257', '3');
INSERT INTO `lizhili_area` VALUES ('2439', '东兴市', '450681', '257', '3');
INSERT INTO `lizhili_area` VALUES ('2440', '钦南区', '450702', '258', '3');
INSERT INTO `lizhili_area` VALUES ('2441', '钦北区', '450703', '258', '3');
INSERT INTO `lizhili_area` VALUES ('2442', '灵山县', '450721', '258', '3');
INSERT INTO `lizhili_area` VALUES ('2443', '浦北县', '450722', '258', '3');
INSERT INTO `lizhili_area` VALUES ('2444', '港北区', '450802', '259', '3');
INSERT INTO `lizhili_area` VALUES ('2445', '港南区', '450803', '259', '3');
INSERT INTO `lizhili_area` VALUES ('2446', '覃塘区', '450804', '259', '3');
INSERT INTO `lizhili_area` VALUES ('2447', '平南县', '450821', '259', '3');
INSERT INTO `lizhili_area` VALUES ('2448', '桂平市', '450881', '259', '3');
INSERT INTO `lizhili_area` VALUES ('2449', '玉州区', '450902', '260', '3');
INSERT INTO `lizhili_area` VALUES ('2450', '福绵区', '450903', '260', '3');
INSERT INTO `lizhili_area` VALUES ('2451', '容县', '450921', '260', '3');
INSERT INTO `lizhili_area` VALUES ('2452', '陆川县', '450922', '260', '3');
INSERT INTO `lizhili_area` VALUES ('2453', '博白县', '450923', '260', '3');
INSERT INTO `lizhili_area` VALUES ('2454', '兴业县', '450924', '260', '3');
INSERT INTO `lizhili_area` VALUES ('2455', '北流市', '450981', '260', '3');
INSERT INTO `lizhili_area` VALUES ('2456', '右江区', '451002', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2457', '田阳县', '451021', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2458', '田东县', '451022', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2459', '平果县', '451023', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2460', '德保县', '451024', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2461', '那坡县', '451026', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2462', '凌云县', '451027', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2463', '乐业县', '451028', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2464', '田林县', '451029', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2465', '西林县', '451030', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2466', '隆林各族自治县', '451031', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2467', '靖西市', '451081', '261', '3');
INSERT INTO `lizhili_area` VALUES ('2468', '八步区', '451102', '262', '3');
INSERT INTO `lizhili_area` VALUES ('2469', '平桂区', '451103', '262', '3');
INSERT INTO `lizhili_area` VALUES ('2470', '昭平县', '451121', '262', '3');
INSERT INTO `lizhili_area` VALUES ('2471', '钟山县', '451122', '262', '3');
INSERT INTO `lizhili_area` VALUES ('2472', '富川瑶族自治县', '451123', '262', '3');
INSERT INTO `lizhili_area` VALUES ('2473', '金城江区', '451202', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2474', '宜州区', '451203', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2475', '南丹县', '451221', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2476', '天峨县', '451222', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2477', '凤山县', '451223', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2478', '东兰县', '451224', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2479', '罗城仫佬族自治县', '451225', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2480', '环江毛南族自治县', '451226', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2481', '巴马瑶族自治县', '451227', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2482', '都安瑶族自治县', '451228', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2483', '大化瑶族自治县', '451229', '263', '3');
INSERT INTO `lizhili_area` VALUES ('2484', '兴宾区', '451302', '264', '3');
INSERT INTO `lizhili_area` VALUES ('2485', '忻城县', '451321', '264', '3');
INSERT INTO `lizhili_area` VALUES ('2486', '象州县', '451322', '264', '3');
INSERT INTO `lizhili_area` VALUES ('2487', '武宣县', '451323', '264', '3');
INSERT INTO `lizhili_area` VALUES ('2488', '金秀瑶族自治县', '451324', '264', '3');
INSERT INTO `lizhili_area` VALUES ('2489', '合山市', '451381', '264', '3');
INSERT INTO `lizhili_area` VALUES ('2490', '江州区', '451402', '265', '3');
INSERT INTO `lizhili_area` VALUES ('2491', '扶绥县', '451421', '265', '3');
INSERT INTO `lizhili_area` VALUES ('2492', '宁明县', '451422', '265', '3');
INSERT INTO `lizhili_area` VALUES ('2493', '龙州县', '451423', '265', '3');
INSERT INTO `lizhili_area` VALUES ('2494', '大新县', '451424', '265', '3');
INSERT INTO `lizhili_area` VALUES ('2495', '天等县', '451425', '265', '3');
INSERT INTO `lizhili_area` VALUES ('2496', '凭祥市', '451481', '265', '3');
INSERT INTO `lizhili_area` VALUES ('2497', '秀英区', '460105', '266', '3');
INSERT INTO `lizhili_area` VALUES ('2498', '龙华区', '460106', '266', '3');
INSERT INTO `lizhili_area` VALUES ('2499', '琼山区', '460107', '266', '3');
INSERT INTO `lizhili_area` VALUES ('2500', '美兰区', '460108', '266', '3');
INSERT INTO `lizhili_area` VALUES ('2501', '海棠区', '460202', '267', '3');
INSERT INTO `lizhili_area` VALUES ('2502', '吉阳区', '460203', '267', '3');
INSERT INTO `lizhili_area` VALUES ('2503', '天涯区', '460204', '267', '3');
INSERT INTO `lizhili_area` VALUES ('2504', '崖州区', '460205', '267', '3');
INSERT INTO `lizhili_area` VALUES ('2505', '西沙群岛', '460321', '268', '3');
INSERT INTO `lizhili_area` VALUES ('2506', '南沙群岛', '460322', '268', '3');
INSERT INTO `lizhili_area` VALUES ('2507', '中沙群岛的岛礁及其海域', '460323', '268', '3');
INSERT INTO `lizhili_area` VALUES ('2508', '儋州市', '460400', '269', '3');
INSERT INTO `lizhili_area` VALUES ('2509', '五指山市', '469001', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2510', '琼海市', '469002', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2511', '文昌市', '469005', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2512', '万宁市', '469006', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2513', '东方市', '469007', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2514', '定安县', '469021', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2515', '屯昌县', '469022', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2516', '澄迈县', '469023', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2517', '临高县', '469024', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2518', '白沙黎族自治县', '469025', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2519', '昌江黎族自治县', '469026', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2520', '乐东黎族自治县', '469027', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2521', '陵水黎族自治县', '469028', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2522', '保亭黎族苗族自治县', '469029', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2523', '琼中黎族苗族自治县', '469030', '270', '3');
INSERT INTO `lizhili_area` VALUES ('2524', '万州区', '500101', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2525', '涪陵区', '500102', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2526', '渝中区', '500103', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2527', '大渡口区', '500104', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2528', '江北区', '500105', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2529', '沙坪坝区', '500106', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2530', '九龙坡区', '500107', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2531', '南岸区', '500108', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2532', '北碚区', '500109', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2533', '綦江区', '500110', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2534', '大足区', '500111', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2535', '渝北区', '500112', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2536', '巴南区', '500113', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2537', '黔江区', '500114', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2538', '长寿区', '500115', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2539', '江津区', '500116', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2540', '合川区', '500117', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2541', '永川区', '500118', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2542', '南川区', '500119', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2543', '璧山区', '500120', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2544', '铜梁区', '500151', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2545', '潼南区', '500152', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2546', '荣昌区', '500153', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2547', '开州区', '500154', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2548', '梁平区', '500155', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2549', '武隆区', '500156', '271', '3');
INSERT INTO `lizhili_area` VALUES ('2550', '城口县', '500229', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2551', '丰都县', '500230', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2552', '垫江县', '500231', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2553', '忠县', '500233', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2554', '云阳县', '500235', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2555', '奉节县', '500236', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2556', '巫山县', '500237', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2557', '巫溪县', '500238', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2558', '石柱土家族自治县', '500240', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2559', '秀山土家族苗族自治县', '500241', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2560', '酉阳土家族苗族自治县', '500242', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2561', '彭水苗族土家族自治县', '500243', '272', '3');
INSERT INTO `lizhili_area` VALUES ('2562', '锦江区', '510104', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2563', '青羊区', '510105', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2564', '金牛区', '510106', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2565', '武侯区', '510107', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2566', '成华区', '510108', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2567', '龙泉驿区', '510112', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2568', '青白江区', '510113', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2569', '新都区', '510114', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2570', '温江区', '510115', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2571', '双流区', '510116', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2572', '郫都区', '510117', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2573', '金堂县', '510121', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2574', '大邑县', '510129', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2575', '蒲江县', '510131', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2576', '新津县', '510132', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2577', '都江堰市', '510181', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2578', '彭州市', '510182', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2579', '邛崃市', '510183', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2580', '崇州市', '510184', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2581', '简阳市', '510185', '273', '3');
INSERT INTO `lizhili_area` VALUES ('2582', '自流井区', '510302', '274', '3');
INSERT INTO `lizhili_area` VALUES ('2583', '贡井区', '510303', '274', '3');
INSERT INTO `lizhili_area` VALUES ('2584', '大安区', '510304', '274', '3');
INSERT INTO `lizhili_area` VALUES ('2585', '沿滩区', '510311', '274', '3');
INSERT INTO `lizhili_area` VALUES ('2586', '荣县', '510321', '274', '3');
INSERT INTO `lizhili_area` VALUES ('2587', '富顺县', '510322', '274', '3');
INSERT INTO `lizhili_area` VALUES ('2588', '东区', '510402', '275', '3');
INSERT INTO `lizhili_area` VALUES ('2589', '西区', '510403', '275', '3');
INSERT INTO `lizhili_area` VALUES ('2590', '仁和区', '510411', '275', '3');
INSERT INTO `lizhili_area` VALUES ('2591', '米易县', '510421', '275', '3');
INSERT INTO `lizhili_area` VALUES ('2592', '盐边县', '510422', '275', '3');
INSERT INTO `lizhili_area` VALUES ('2593', '江阳区', '510502', '276', '3');
INSERT INTO `lizhili_area` VALUES ('2594', '纳溪区', '510503', '276', '3');
INSERT INTO `lizhili_area` VALUES ('2595', '龙马潭区', '510504', '276', '3');
INSERT INTO `lizhili_area` VALUES ('2596', '泸县', '510521', '276', '3');
INSERT INTO `lizhili_area` VALUES ('2597', '合江县', '510522', '276', '3');
INSERT INTO `lizhili_area` VALUES ('2598', '叙永县', '510524', '276', '3');
INSERT INTO `lizhili_area` VALUES ('2599', '古蔺县', '510525', '276', '3');
INSERT INTO `lizhili_area` VALUES ('2600', '旌阳区', '510603', '277', '3');
INSERT INTO `lizhili_area` VALUES ('2601', '罗江区', '510604', '277', '3');
INSERT INTO `lizhili_area` VALUES ('2602', '中江县', '510623', '277', '3');
INSERT INTO `lizhili_area` VALUES ('2603', '广汉市', '510681', '277', '3');
INSERT INTO `lizhili_area` VALUES ('2604', '什邡市', '510682', '277', '3');
INSERT INTO `lizhili_area` VALUES ('2605', '绵竹市', '510683', '277', '3');
INSERT INTO `lizhili_area` VALUES ('2606', '涪城区', '510703', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2607', '游仙区', '510704', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2608', '安州区', '510705', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2609', '三台县', '510722', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2610', '盐亭县', '510723', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2611', '梓潼县', '510725', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2612', '北川羌族自治县', '510726', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2613', '平武县', '510727', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2614', '江油市', '510781', '278', '3');
INSERT INTO `lizhili_area` VALUES ('2615', '利州区', '510802', '279', '3');
INSERT INTO `lizhili_area` VALUES ('2616', '昭化区', '510811', '279', '3');
INSERT INTO `lizhili_area` VALUES ('2617', '朝天区', '510812', '279', '3');
INSERT INTO `lizhili_area` VALUES ('2618', '旺苍县', '510821', '279', '3');
INSERT INTO `lizhili_area` VALUES ('2619', '青川县', '510822', '279', '3');
INSERT INTO `lizhili_area` VALUES ('2620', '剑阁县', '510823', '279', '3');
INSERT INTO `lizhili_area` VALUES ('2621', '苍溪县', '510824', '279', '3');
INSERT INTO `lizhili_area` VALUES ('2622', '船山区', '510903', '280', '3');
INSERT INTO `lizhili_area` VALUES ('2623', '安居区', '510904', '280', '3');
INSERT INTO `lizhili_area` VALUES ('2624', '蓬溪县', '510921', '280', '3');
INSERT INTO `lizhili_area` VALUES ('2625', '射洪县', '510922', '280', '3');
INSERT INTO `lizhili_area` VALUES ('2626', '大英县', '510923', '280', '3');
INSERT INTO `lizhili_area` VALUES ('2627', '市中区', '511002', '281', '3');
INSERT INTO `lizhili_area` VALUES ('2628', '东兴区', '511011', '281', '3');
INSERT INTO `lizhili_area` VALUES ('2629', '威远县', '511024', '281', '3');
INSERT INTO `lizhili_area` VALUES ('2630', '资中县', '511025', '281', '3');
INSERT INTO `lizhili_area` VALUES ('2631', '内江经济开发区', '511071', '281', '3');
INSERT INTO `lizhili_area` VALUES ('2632', '隆昌市', '511083', '281', '3');
INSERT INTO `lizhili_area` VALUES ('2633', '市中区', '511102', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2634', '沙湾区', '511111', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2635', '五通桥区', '511112', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2636', '金口河区', '511113', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2637', '犍为县', '511123', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2638', '井研县', '511124', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2639', '夹江县', '511126', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2640', '沐川县', '511129', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2641', '峨边彝族自治县', '511132', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2642', '马边彝族自治县', '511133', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2643', '峨眉山市', '511181', '282', '3');
INSERT INTO `lizhili_area` VALUES ('2644', '顺庆区', '511302', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2645', '高坪区', '511303', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2646', '嘉陵区', '511304', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2647', '南部县', '511321', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2648', '营山县', '511322', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2649', '蓬安县', '511323', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2650', '仪陇县', '511324', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2651', '西充县', '511325', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2652', '阆中市', '511381', '283', '3');
INSERT INTO `lizhili_area` VALUES ('2653', '东坡区', '511402', '284', '3');
INSERT INTO `lizhili_area` VALUES ('2654', '彭山区', '511403', '284', '3');
INSERT INTO `lizhili_area` VALUES ('2655', '仁寿县', '511421', '284', '3');
INSERT INTO `lizhili_area` VALUES ('2656', '洪雅县', '511423', '284', '3');
INSERT INTO `lizhili_area` VALUES ('2657', '丹棱县', '511424', '284', '3');
INSERT INTO `lizhili_area` VALUES ('2658', '青神县', '511425', '284', '3');
INSERT INTO `lizhili_area` VALUES ('2659', '翠屏区', '511502', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2660', '南溪区', '511503', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2661', '宜宾县', '511521', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2662', '江安县', '511523', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2663', '长宁县', '511524', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2664', '高县', '511525', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2665', '珙县', '511526', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2666', '筠连县', '511527', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2667', '兴文县', '511528', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2668', '屏山县', '511529', '285', '3');
INSERT INTO `lizhili_area` VALUES ('2669', '广安区', '511602', '286', '3');
INSERT INTO `lizhili_area` VALUES ('2670', '前锋区', '511603', '286', '3');
INSERT INTO `lizhili_area` VALUES ('2671', '岳池县', '511621', '286', '3');
INSERT INTO `lizhili_area` VALUES ('2672', '武胜县', '511622', '286', '3');
INSERT INTO `lizhili_area` VALUES ('2673', '邻水县', '511623', '286', '3');
INSERT INTO `lizhili_area` VALUES ('2674', '华蓥市', '511681', '286', '3');
INSERT INTO `lizhili_area` VALUES ('2675', '通川区', '511702', '287', '3');
INSERT INTO `lizhili_area` VALUES ('2676', '达川区', '511703', '287', '3');
INSERT INTO `lizhili_area` VALUES ('2677', '宣汉县', '511722', '287', '3');
INSERT INTO `lizhili_area` VALUES ('2678', '开江县', '511723', '287', '3');
INSERT INTO `lizhili_area` VALUES ('2679', '大竹县', '511724', '287', '3');
INSERT INTO `lizhili_area` VALUES ('2680', '渠县', '511725', '287', '3');
INSERT INTO `lizhili_area` VALUES ('2681', '达州经济开发区', '511771', '287', '3');
INSERT INTO `lizhili_area` VALUES ('2682', '万源市', '511781', '287', '3');
INSERT INTO `lizhili_area` VALUES ('2683', '雨城区', '511802', '288', '3');
INSERT INTO `lizhili_area` VALUES ('2684', '名山区', '511803', '288', '3');
INSERT INTO `lizhili_area` VALUES ('2685', '荥经县', '511822', '288', '3');
INSERT INTO `lizhili_area` VALUES ('2686', '汉源县', '511823', '288', '3');
INSERT INTO `lizhili_area` VALUES ('2687', '石棉县', '511824', '288', '3');
INSERT INTO `lizhili_area` VALUES ('2688', '天全县', '511825', '288', '3');
INSERT INTO `lizhili_area` VALUES ('2689', '芦山县', '511826', '288', '3');
INSERT INTO `lizhili_area` VALUES ('2690', '宝兴县', '511827', '288', '3');
INSERT INTO `lizhili_area` VALUES ('2691', '巴州区', '511902', '289', '3');
INSERT INTO `lizhili_area` VALUES ('2692', '恩阳区', '511903', '289', '3');
INSERT INTO `lizhili_area` VALUES ('2693', '通江县', '511921', '289', '3');
INSERT INTO `lizhili_area` VALUES ('2694', '南江县', '511922', '289', '3');
INSERT INTO `lizhili_area` VALUES ('2695', '平昌县', '511923', '289', '3');
INSERT INTO `lizhili_area` VALUES ('2696', '巴中经济开发区', '511971', '289', '3');
INSERT INTO `lizhili_area` VALUES ('2697', '雁江区', '512002', '290', '3');
INSERT INTO `lizhili_area` VALUES ('2698', '安岳县', '512021', '290', '3');
INSERT INTO `lizhili_area` VALUES ('2699', '乐至县', '512022', '290', '3');
INSERT INTO `lizhili_area` VALUES ('2700', '马尔康市', '513201', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2701', '汶川县', '513221', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2702', '理县', '513222', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2703', '茂县', '513223', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2704', '松潘县', '513224', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2705', '九寨沟县', '513225', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2706', '金川县', '513226', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2707', '小金县', '513227', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2708', '黑水县', '513228', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2709', '壤塘县', '513230', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2710', '阿坝县', '513231', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2711', '若尔盖县', '513232', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2712', '红原县', '513233', '291', '3');
INSERT INTO `lizhili_area` VALUES ('2713', '康定市', '513301', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2714', '泸定县', '513322', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2715', '丹巴县', '513323', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2716', '九龙县', '513324', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2717', '雅江县', '513325', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2718', '道孚县', '513326', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2719', '炉霍县', '513327', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2720', '甘孜县', '513328', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2721', '新龙县', '513329', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2722', '德格县', '513330', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2723', '白玉县', '513331', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2724', '石渠县', '513332', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2725', '色达县', '513333', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2726', '理塘县', '513334', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2727', '巴塘县', '513335', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2728', '乡城县', '513336', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2729', '稻城县', '513337', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2730', '得荣县', '513338', '292', '3');
INSERT INTO `lizhili_area` VALUES ('2731', '西昌市', '513401', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2732', '木里藏族自治县', '513422', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2733', '盐源县', '513423', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2734', '德昌县', '513424', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2735', '会理县', '513425', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2736', '会东县', '513426', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2737', '宁南县', '513427', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2738', '普格县', '513428', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2739', '布拖县', '513429', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2740', '金阳县', '513430', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2741', '昭觉县', '513431', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2742', '喜德县', '513432', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2743', '冕宁县', '513433', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2744', '越西县', '513434', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2745', '甘洛县', '513435', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2746', '美姑县', '513436', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2747', '雷波县', '513437', '293', '3');
INSERT INTO `lizhili_area` VALUES ('2748', '南明区', '520102', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2749', '云岩区', '520103', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2750', '花溪区', '520111', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2751', '乌当区', '520112', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2752', '白云区', '520113', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2753', '观山湖区', '520115', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2754', '开阳县', '520121', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2755', '息烽县', '520122', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2756', '修文县', '520123', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2757', '清镇市', '520181', '294', '3');
INSERT INTO `lizhili_area` VALUES ('2758', '钟山区', '520201', '295', '3');
INSERT INTO `lizhili_area` VALUES ('2759', '六枝特区', '520203', '295', '3');
INSERT INTO `lizhili_area` VALUES ('2760', '水城县', '520221', '295', '3');
INSERT INTO `lizhili_area` VALUES ('2761', '盘州市', '520281', '295', '3');
INSERT INTO `lizhili_area` VALUES ('2762', '红花岗区', '520302', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2763', '汇川区', '520303', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2764', '播州区', '520304', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2765', '桐梓县', '520322', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2766', '绥阳县', '520323', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2767', '正安县', '520324', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2768', '道真仡佬族苗族自治县', '520325', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2769', '务川仡佬族苗族自治县', '520326', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2770', '凤冈县', '520327', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2771', '湄潭县', '520328', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2772', '余庆县', '520329', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2773', '习水县', '520330', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2774', '赤水市', '520381', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2775', '仁怀市', '520382', '296', '3');
INSERT INTO `lizhili_area` VALUES ('2776', '西秀区', '520402', '297', '3');
INSERT INTO `lizhili_area` VALUES ('2777', '平坝区', '520403', '297', '3');
INSERT INTO `lizhili_area` VALUES ('2778', '普定县', '520422', '297', '3');
INSERT INTO `lizhili_area` VALUES ('2779', '镇宁布依族苗族自治县', '520423', '297', '3');
INSERT INTO `lizhili_area` VALUES ('2780', '关岭布依族苗族自治县', '520424', '297', '3');
INSERT INTO `lizhili_area` VALUES ('2781', '紫云苗族布依族自治县', '520425', '297', '3');
INSERT INTO `lizhili_area` VALUES ('2782', '七星关区', '520502', '298', '3');
INSERT INTO `lizhili_area` VALUES ('2783', '大方县', '520521', '298', '3');
INSERT INTO `lizhili_area` VALUES ('2784', '黔西县', '520522', '298', '3');
INSERT INTO `lizhili_area` VALUES ('2785', '金沙县', '520523', '298', '3');
INSERT INTO `lizhili_area` VALUES ('2786', '织金县', '520524', '298', '3');
INSERT INTO `lizhili_area` VALUES ('2787', '纳雍县', '520525', '298', '3');
INSERT INTO `lizhili_area` VALUES ('2788', '威宁彝族回族苗族自治县', '520526', '298', '3');
INSERT INTO `lizhili_area` VALUES ('2789', '赫章县', '520527', '298', '3');
INSERT INTO `lizhili_area` VALUES ('2790', '碧江区', '520602', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2791', '万山区', '520603', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2792', '江口县', '520621', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2793', '玉屏侗族自治县', '520622', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2794', '石阡县', '520623', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2795', '思南县', '520624', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2796', '印江土家族苗族自治县', '520625', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2797', '德江县', '520626', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2798', '沿河土家族自治县', '520627', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2799', '松桃苗族自治县', '520628', '299', '3');
INSERT INTO `lizhili_area` VALUES ('2800', '兴义市', '522301', '300', '3');
INSERT INTO `lizhili_area` VALUES ('2801', '兴仁县', '522322', '300', '3');
INSERT INTO `lizhili_area` VALUES ('2802', '普安县', '522323', '300', '3');
INSERT INTO `lizhili_area` VALUES ('2803', '晴隆县', '522324', '300', '3');
INSERT INTO `lizhili_area` VALUES ('2804', '贞丰县', '522325', '300', '3');
INSERT INTO `lizhili_area` VALUES ('2805', '望谟县', '522326', '300', '3');
INSERT INTO `lizhili_area` VALUES ('2806', '册亨县', '522327', '300', '3');
INSERT INTO `lizhili_area` VALUES ('2807', '安龙县', '522328', '300', '3');
INSERT INTO `lizhili_area` VALUES ('2808', '凯里市', '522601', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2809', '黄平县', '522622', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2810', '施秉县', '522623', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2811', '三穗县', '522624', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2812', '镇远县', '522625', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2813', '岑巩县', '522626', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2814', '天柱县', '522627', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2815', '锦屏县', '522628', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2816', '剑河县', '522629', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2817', '台江县', '522630', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2818', '黎平县', '522631', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2819', '榕江县', '522632', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2820', '从江县', '522633', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2821', '雷山县', '522634', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2822', '麻江县', '522635', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2823', '丹寨县', '522636', '301', '3');
INSERT INTO `lizhili_area` VALUES ('2824', '都匀市', '522701', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2825', '福泉市', '522702', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2826', '荔波县', '522722', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2827', '贵定县', '522723', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2828', '瓮安县', '522725', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2829', '独山县', '522726', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2830', '平塘县', '522727', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2831', '罗甸县', '522728', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2832', '长顺县', '522729', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2833', '龙里县', '522730', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2834', '惠水县', '522731', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2835', '三都水族自治县', '522732', '302', '3');
INSERT INTO `lizhili_area` VALUES ('2836', '五华区', '530102', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2837', '盘龙区', '530103', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2838', '官渡区', '530111', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2839', '西山区', '530112', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2840', '东川区', '530113', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2841', '呈贡区', '530114', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2842', '晋宁区', '530115', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2843', '富民县', '530124', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2844', '宜良县', '530125', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2845', '石林彝族自治县', '530126', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2846', '嵩明县', '530127', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2847', '禄劝彝族苗族自治县', '530128', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2848', '寻甸回族彝族自治县', '530129', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2849', '安宁市', '530181', '303', '3');
INSERT INTO `lizhili_area` VALUES ('2850', '麒麟区', '530302', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2851', '沾益区', '530303', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2852', '马龙县', '530321', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2853', '陆良县', '530322', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2854', '师宗县', '530323', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2855', '罗平县', '530324', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2856', '富源县', '530325', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2857', '会泽县', '530326', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2858', '宣威市', '530381', '304', '3');
INSERT INTO `lizhili_area` VALUES ('2859', '红塔区', '530402', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2860', '江川区', '530403', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2861', '澄江县', '530422', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2862', '通海县', '530423', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2863', '华宁县', '530424', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2864', '易门县', '530425', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2865', '峨山彝族自治县', '530426', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2866', '新平彝族傣族自治县', '530427', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2867', '元江哈尼族彝族傣族自治县', '530428', '305', '3');
INSERT INTO `lizhili_area` VALUES ('2868', '隆阳区', '530502', '306', '3');
INSERT INTO `lizhili_area` VALUES ('2869', '施甸县', '530521', '306', '3');
INSERT INTO `lizhili_area` VALUES ('2870', '龙陵县', '530523', '306', '3');
INSERT INTO `lizhili_area` VALUES ('2871', '昌宁县', '530524', '306', '3');
INSERT INTO `lizhili_area` VALUES ('2872', '腾冲市', '530581', '306', '3');
INSERT INTO `lizhili_area` VALUES ('2873', '昭阳区', '530602', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2874', '鲁甸县', '530621', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2875', '巧家县', '530622', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2876', '盐津县', '530623', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2877', '大关县', '530624', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2878', '永善县', '530625', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2879', '绥江县', '530626', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2880', '镇雄县', '530627', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2881', '彝良县', '530628', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2882', '威信县', '530629', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2883', '水富县', '530630', '307', '3');
INSERT INTO `lizhili_area` VALUES ('2884', '古城区', '530702', '308', '3');
INSERT INTO `lizhili_area` VALUES ('2885', '玉龙纳西族自治县', '530721', '308', '3');
INSERT INTO `lizhili_area` VALUES ('2886', '永胜县', '530722', '308', '3');
INSERT INTO `lizhili_area` VALUES ('2887', '华坪县', '530723', '308', '3');
INSERT INTO `lizhili_area` VALUES ('2888', '宁蒗彝族自治县', '530724', '308', '3');
INSERT INTO `lizhili_area` VALUES ('2889', '思茅区', '530802', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2890', '宁洱哈尼族彝族自治县', '530821', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2891', '墨江哈尼族自治县', '530822', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2892', '景东彝族自治县', '530823', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2893', '景谷傣族彝族自治县', '530824', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2894', '镇沅彝族哈尼族拉祜族自治县', '530825', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2895', '江城哈尼族彝族自治县', '530826', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2896', '孟连傣族拉祜族佤族自治县', '530827', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2897', '澜沧拉祜族自治县', '530828', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2898', '西盟佤族自治县', '530829', '309', '3');
INSERT INTO `lizhili_area` VALUES ('2899', '临翔区', '530902', '310', '3');
INSERT INTO `lizhili_area` VALUES ('2900', '凤庆县', '530921', '310', '3');
INSERT INTO `lizhili_area` VALUES ('2901', '云县', '530922', '310', '3');
INSERT INTO `lizhili_area` VALUES ('2902', '永德县', '530923', '310', '3');
INSERT INTO `lizhili_area` VALUES ('2903', '镇康县', '530924', '310', '3');
INSERT INTO `lizhili_area` VALUES ('2904', '双江拉祜族佤族布朗族傣族自治县', '530925', '310', '3');
INSERT INTO `lizhili_area` VALUES ('2905', '耿马傣族佤族自治县', '530926', '310', '3');
INSERT INTO `lizhili_area` VALUES ('2906', '沧源佤族自治县', '530927', '310', '3');
INSERT INTO `lizhili_area` VALUES ('2907', '楚雄市', '532301', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2908', '双柏县', '532322', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2909', '牟定县', '532323', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2910', '南华县', '532324', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2911', '姚安县', '532325', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2912', '大姚县', '532326', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2913', '永仁县', '532327', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2914', '元谋县', '532328', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2915', '武定县', '532329', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2916', '禄丰县', '532331', '311', '3');
INSERT INTO `lizhili_area` VALUES ('2917', '个旧市', '532501', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2918', '开远市', '532502', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2919', '蒙自市', '532503', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2920', '弥勒市', '532504', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2921', '屏边苗族自治县', '532523', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2922', '建水县', '532524', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2923', '石屏县', '532525', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2924', '泸西县', '532527', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2925', '元阳县', '532528', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2926', '红河县', '532529', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2927', '金平苗族瑶族傣族自治县', '532530', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2928', '绿春县', '532531', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2929', '河口瑶族自治县', '532532', '312', '3');
INSERT INTO `lizhili_area` VALUES ('2930', '文山市', '532601', '313', '3');
INSERT INTO `lizhili_area` VALUES ('2931', '砚山县', '532622', '313', '3');
INSERT INTO `lizhili_area` VALUES ('2932', '西畴县', '532623', '313', '3');
INSERT INTO `lizhili_area` VALUES ('2933', '麻栗坡县', '532624', '313', '3');
INSERT INTO `lizhili_area` VALUES ('2934', '马关县', '532625', '313', '3');
INSERT INTO `lizhili_area` VALUES ('2935', '丘北县', '532626', '313', '3');
INSERT INTO `lizhili_area` VALUES ('2936', '广南县', '532627', '313', '3');
INSERT INTO `lizhili_area` VALUES ('2937', '富宁县', '532628', '313', '3');
INSERT INTO `lizhili_area` VALUES ('2938', '景洪市', '532801', '314', '3');
INSERT INTO `lizhili_area` VALUES ('2939', '勐海县', '532822', '314', '3');
INSERT INTO `lizhili_area` VALUES ('2940', '勐腊县', '532823', '314', '3');
INSERT INTO `lizhili_area` VALUES ('2941', '大理市', '532901', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2942', '漾濞彝族自治县', '532922', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2943', '祥云县', '532923', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2944', '宾川县', '532924', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2945', '弥渡县', '532925', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2946', '南涧彝族自治县', '532926', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2947', '巍山彝族回族自治县', '532927', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2948', '永平县', '532928', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2949', '云龙县', '532929', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2950', '洱源县', '532930', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2951', '剑川县', '532931', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2952', '鹤庆县', '532932', '315', '3');
INSERT INTO `lizhili_area` VALUES ('2953', '瑞丽市', '533102', '316', '3');
INSERT INTO `lizhili_area` VALUES ('2954', '芒市', '533103', '316', '3');
INSERT INTO `lizhili_area` VALUES ('2955', '梁河县', '533122', '316', '3');
INSERT INTO `lizhili_area` VALUES ('2956', '盈江县', '533123', '316', '3');
INSERT INTO `lizhili_area` VALUES ('2957', '陇川县', '533124', '316', '3');
INSERT INTO `lizhili_area` VALUES ('2958', '泸水市', '533301', '317', '3');
INSERT INTO `lizhili_area` VALUES ('2959', '福贡县', '533323', '317', '3');
INSERT INTO `lizhili_area` VALUES ('2960', '贡山独龙族怒族自治县', '533324', '317', '3');
INSERT INTO `lizhili_area` VALUES ('2961', '兰坪白族普米族自治县', '533325', '317', '3');
INSERT INTO `lizhili_area` VALUES ('2962', '香格里拉市', '533401', '318', '3');
INSERT INTO `lizhili_area` VALUES ('2963', '德钦县', '533422', '318', '3');
INSERT INTO `lizhili_area` VALUES ('2964', '维西傈僳族自治县', '533423', '318', '3');
INSERT INTO `lizhili_area` VALUES ('2965', '城关区', '540102', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2966', '堆龙德庆区', '540103', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2967', '林周县', '540121', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2968', '当雄县', '540122', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2969', '尼木县', '540123', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2970', '曲水县', '540124', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2971', '达孜县', '540126', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2972', '墨竹工卡县', '540127', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2973', '格尔木藏青工业园区', '540171', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2974', '拉萨经济技术开发区', '540172', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2975', '西藏文化旅游创意园区', '540173', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2976', '达孜工业园区', '540174', '319', '3');
INSERT INTO `lizhili_area` VALUES ('2977', '桑珠孜区', '540202', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2978', '南木林县', '540221', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2979', '江孜县', '540222', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2980', '定日县', '540223', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2981', '萨迦县', '540224', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2982', '拉孜县', '540225', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2983', '昂仁县', '540226', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2984', '谢通门县', '540227', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2985', '白朗县', '540228', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2986', '仁布县', '540229', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2987', '康马县', '540230', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2988', '定结县', '540231', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2989', '仲巴县', '540232', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2990', '亚东县', '540233', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2991', '吉隆县', '540234', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2992', '聂拉木县', '540235', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2993', '萨嘎县', '540236', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2994', '岗巴县', '540237', '320', '3');
INSERT INTO `lizhili_area` VALUES ('2995', '卡若区', '540302', '321', '3');
INSERT INTO `lizhili_area` VALUES ('2996', '江达县', '540321', '321', '3');
INSERT INTO `lizhili_area` VALUES ('2997', '贡觉县', '540322', '321', '3');
INSERT INTO `lizhili_area` VALUES ('2998', '类乌齐县', '540323', '321', '3');
INSERT INTO `lizhili_area` VALUES ('2999', '丁青县', '540324', '321', '3');
INSERT INTO `lizhili_area` VALUES ('3000', '察雅县', '540325', '321', '3');
INSERT INTO `lizhili_area` VALUES ('3001', '八宿县', '540326', '321', '3');
INSERT INTO `lizhili_area` VALUES ('3002', '左贡县', '540327', '321', '3');
INSERT INTO `lizhili_area` VALUES ('3003', '芒康县', '540328', '321', '3');
INSERT INTO `lizhili_area` VALUES ('3004', '洛隆县', '540329', '321', '3');
INSERT INTO `lizhili_area` VALUES ('3005', '边坝县', '540330', '321', '3');
INSERT INTO `lizhili_area` VALUES ('3006', '巴宜区', '540402', '322', '3');
INSERT INTO `lizhili_area` VALUES ('3007', '工布江达县', '540421', '322', '3');
INSERT INTO `lizhili_area` VALUES ('3008', '米林县', '540422', '322', '3');
INSERT INTO `lizhili_area` VALUES ('3009', '墨脱县', '540423', '322', '3');
INSERT INTO `lizhili_area` VALUES ('3010', '波密县', '540424', '322', '3');
INSERT INTO `lizhili_area` VALUES ('3011', '察隅县', '540425', '322', '3');
INSERT INTO `lizhili_area` VALUES ('3012', '朗县', '540426', '322', '3');
INSERT INTO `lizhili_area` VALUES ('3013', '乃东区', '540502', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3014', '扎囊县', '540521', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3015', '贡嘎县', '540522', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3016', '桑日县', '540523', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3017', '琼结县', '540524', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3018', '曲松县', '540525', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3019', '措美县', '540526', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3020', '洛扎县', '540527', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3021', '加查县', '540528', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3022', '隆子县', '540529', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3023', '错那县', '540530', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3024', '浪卡子县', '540531', '323', '3');
INSERT INTO `lizhili_area` VALUES ('3025', '那曲县', '542421', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3026', '嘉黎县', '542422', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3027', '比如县', '542423', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3028', '聂荣县', '542424', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3029', '安多县', '542425', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3030', '申扎县', '542426', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3031', '索县', '542427', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3032', '班戈县', '542428', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3033', '巴青县', '542429', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3034', '尼玛县', '542430', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3035', '双湖县', '542431', '324', '3');
INSERT INTO `lizhili_area` VALUES ('3036', '普兰县', '542521', '325', '3');
INSERT INTO `lizhili_area` VALUES ('3037', '札达县', '542522', '325', '3');
INSERT INTO `lizhili_area` VALUES ('3038', '噶尔县', '542523', '325', '3');
INSERT INTO `lizhili_area` VALUES ('3039', '日土县', '542524', '325', '3');
INSERT INTO `lizhili_area` VALUES ('3040', '革吉县', '542525', '325', '3');
INSERT INTO `lizhili_area` VALUES ('3041', '改则县', '542526', '325', '3');
INSERT INTO `lizhili_area` VALUES ('3042', '措勤县', '542527', '325', '3');
INSERT INTO `lizhili_area` VALUES ('3043', '新城区', '610102', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3044', '碑林区', '610103', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3045', '莲湖区', '610104', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3046', '灞桥区', '610111', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3047', '未央区', '610112', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3048', '雁塔区', '610113', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3049', '阎良区', '610114', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3050', '临潼区', '610115', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3051', '长安区', '610116', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3052', '高陵区', '610117', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3053', '鄠邑区', '610118', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3054', '蓝田县', '610122', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3055', '周至县', '610124', '326', '3');
INSERT INTO `lizhili_area` VALUES ('3056', '王益区', '610202', '327', '3');
INSERT INTO `lizhili_area` VALUES ('3057', '印台区', '610203', '327', '3');
INSERT INTO `lizhili_area` VALUES ('3058', '耀州区', '610204', '327', '3');
INSERT INTO `lizhili_area` VALUES ('3059', '宜君县', '610222', '327', '3');
INSERT INTO `lizhili_area` VALUES ('3060', '渭滨区', '610302', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3061', '金台区', '610303', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3062', '陈仓区', '610304', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3063', '凤翔县', '610322', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3064', '岐山县', '610323', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3065', '扶风县', '610324', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3066', '眉县', '610326', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3067', '陇县', '610327', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3068', '千阳县', '610328', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3069', '麟游县', '610329', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3070', '凤县', '610330', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3071', '太白县', '610331', '328', '3');
INSERT INTO `lizhili_area` VALUES ('3072', '秦都区', '610402', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3073', '杨陵区', '610403', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3074', '渭城区', '610404', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3075', '三原县', '610422', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3076', '泾阳县', '610423', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3077', '乾县', '610424', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3078', '礼泉县', '610425', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3079', '永寿县', '610426', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3080', '彬县', '610427', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3081', '长武县', '610428', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3082', '旬邑县', '610429', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3083', '淳化县', '610430', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3084', '武功县', '610431', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3085', '兴平市', '610481', '329', '3');
INSERT INTO `lizhili_area` VALUES ('3086', '临渭区', '610502', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3087', '华州区', '610503', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3088', '潼关县', '610522', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3089', '大荔县', '610523', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3090', '合阳县', '610524', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3091', '澄城县', '610525', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3092', '蒲城县', '610526', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3093', '白水县', '610527', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3094', '富平县', '610528', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3095', '韩城市', '610581', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3096', '华阴市', '610582', '330', '3');
INSERT INTO `lizhili_area` VALUES ('3097', '宝塔区', '610602', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3098', '安塞区', '610603', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3099', '延长县', '610621', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3100', '延川县', '610622', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3101', '子长县', '610623', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3102', '志丹县', '610625', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3103', '吴起县', '610626', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3104', '甘泉县', '610627', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3105', '富县', '610628', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3106', '洛川县', '610629', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3107', '宜川县', '610630', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3108', '黄龙县', '610631', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3109', '黄陵县', '610632', '331', '3');
INSERT INTO `lizhili_area` VALUES ('3110', '汉台区', '610702', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3111', '南郑区', '610703', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3112', '城固县', '610722', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3113', '洋县', '610723', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3114', '西乡县', '610724', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3115', '勉县', '610725', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3116', '宁强县', '610726', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3117', '略阳县', '610727', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3118', '镇巴县', '610728', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3119', '留坝县', '610729', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3120', '佛坪县', '610730', '332', '3');
INSERT INTO `lizhili_area` VALUES ('3121', '榆阳区', '610802', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3122', '横山区', '610803', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3123', '府谷县', '610822', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3124', '靖边县', '610824', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3125', '定边县', '610825', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3126', '绥德县', '610826', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3127', '米脂县', '610827', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3128', '佳县', '610828', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3129', '吴堡县', '610829', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3130', '清涧县', '610830', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3131', '子洲县', '610831', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3132', '神木市', '610881', '333', '3');
INSERT INTO `lizhili_area` VALUES ('3133', '汉滨区', '610902', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3134', '汉阴县', '610921', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3135', '石泉县', '610922', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3136', '宁陕县', '610923', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3137', '紫阳县', '610924', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3138', '岚皋县', '610925', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3139', '平利县', '610926', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3140', '镇坪县', '610927', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3141', '旬阳县', '610928', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3142', '白河县', '610929', '334', '3');
INSERT INTO `lizhili_area` VALUES ('3143', '商州区', '611002', '335', '3');
INSERT INTO `lizhili_area` VALUES ('3144', '洛南县', '611021', '335', '3');
INSERT INTO `lizhili_area` VALUES ('3145', '丹凤县', '611022', '335', '3');
INSERT INTO `lizhili_area` VALUES ('3146', '商南县', '611023', '335', '3');
INSERT INTO `lizhili_area` VALUES ('3147', '山阳县', '611024', '335', '3');
INSERT INTO `lizhili_area` VALUES ('3148', '镇安县', '611025', '335', '3');
INSERT INTO `lizhili_area` VALUES ('3149', '柞水县', '611026', '335', '3');
INSERT INTO `lizhili_area` VALUES ('3150', '城关区', '620102', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3151', '七里河区', '620103', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3152', '西固区', '620104', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3153', '安宁区', '620105', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3154', '红古区', '620111', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3155', '永登县', '620121', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3156', '皋兰县', '620122', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3157', '榆中县', '620123', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3158', '兰州新区', '620171', '336', '3');
INSERT INTO `lizhili_area` VALUES ('3159', '嘉峪关市', '620201', '337', '3');
INSERT INTO `lizhili_area` VALUES ('3160', '金川区', '620302', '338', '3');
INSERT INTO `lizhili_area` VALUES ('3161', '永昌县', '620321', '338', '3');
INSERT INTO `lizhili_area` VALUES ('3162', '白银区', '620402', '339', '3');
INSERT INTO `lizhili_area` VALUES ('3163', '平川区', '620403', '339', '3');
INSERT INTO `lizhili_area` VALUES ('3164', '靖远县', '620421', '339', '3');
INSERT INTO `lizhili_area` VALUES ('3165', '会宁县', '620422', '339', '3');
INSERT INTO `lizhili_area` VALUES ('3166', '景泰县', '620423', '339', '3');
INSERT INTO `lizhili_area` VALUES ('3167', '秦州区', '620502', '340', '3');
INSERT INTO `lizhili_area` VALUES ('3168', '麦积区', '620503', '340', '3');
INSERT INTO `lizhili_area` VALUES ('3169', '清水县', '620521', '340', '3');
INSERT INTO `lizhili_area` VALUES ('3170', '秦安县', '620522', '340', '3');
INSERT INTO `lizhili_area` VALUES ('3171', '甘谷县', '620523', '340', '3');
INSERT INTO `lizhili_area` VALUES ('3172', '武山县', '620524', '340', '3');
INSERT INTO `lizhili_area` VALUES ('3173', '张家川回族自治县', '620525', '340', '3');
INSERT INTO `lizhili_area` VALUES ('3174', '凉州区', '620602', '341', '3');
INSERT INTO `lizhili_area` VALUES ('3175', '民勤县', '620621', '341', '3');
INSERT INTO `lizhili_area` VALUES ('3176', '古浪县', '620622', '341', '3');
INSERT INTO `lizhili_area` VALUES ('3177', '天祝藏族自治县', '620623', '341', '3');
INSERT INTO `lizhili_area` VALUES ('3178', '甘州区', '620702', '342', '3');
INSERT INTO `lizhili_area` VALUES ('3179', '肃南裕固族自治县', '620721', '342', '3');
INSERT INTO `lizhili_area` VALUES ('3180', '民乐县', '620722', '342', '3');
INSERT INTO `lizhili_area` VALUES ('3181', '临泽县', '620723', '342', '3');
INSERT INTO `lizhili_area` VALUES ('3182', '高台县', '620724', '342', '3');
INSERT INTO `lizhili_area` VALUES ('3183', '山丹县', '620725', '342', '3');
INSERT INTO `lizhili_area` VALUES ('3184', '崆峒区', '620802', '343', '3');
INSERT INTO `lizhili_area` VALUES ('3185', '泾川县', '620821', '343', '3');
INSERT INTO `lizhili_area` VALUES ('3186', '灵台县', '620822', '343', '3');
INSERT INTO `lizhili_area` VALUES ('3187', '崇信县', '620823', '343', '3');
INSERT INTO `lizhili_area` VALUES ('3188', '华亭县', '620824', '343', '3');
INSERT INTO `lizhili_area` VALUES ('3189', '庄浪县', '620825', '343', '3');
INSERT INTO `lizhili_area` VALUES ('3190', '静宁县', '620826', '343', '3');
INSERT INTO `lizhili_area` VALUES ('3191', '平凉工业园区', '620871', '343', '3');
INSERT INTO `lizhili_area` VALUES ('3192', '肃州区', '620902', '344', '3');
INSERT INTO `lizhili_area` VALUES ('3193', '金塔县', '620921', '344', '3');
INSERT INTO `lizhili_area` VALUES ('3194', '瓜州县', '620922', '344', '3');
INSERT INTO `lizhili_area` VALUES ('3195', '肃北蒙古族自治县', '620923', '344', '3');
INSERT INTO `lizhili_area` VALUES ('3196', '阿克塞哈萨克族自治县', '620924', '344', '3');
INSERT INTO `lizhili_area` VALUES ('3197', '玉门市', '620981', '344', '3');
INSERT INTO `lizhili_area` VALUES ('3198', '敦煌市', '620982', '344', '3');
INSERT INTO `lizhili_area` VALUES ('3199', '西峰区', '621002', '345', '3');
INSERT INTO `lizhili_area` VALUES ('3200', '庆城县', '621021', '345', '3');
INSERT INTO `lizhili_area` VALUES ('3201', '环县', '621022', '345', '3');
INSERT INTO `lizhili_area` VALUES ('3202', '华池县', '621023', '345', '3');
INSERT INTO `lizhili_area` VALUES ('3203', '合水县', '621024', '345', '3');
INSERT INTO `lizhili_area` VALUES ('3204', '正宁县', '621025', '345', '3');
INSERT INTO `lizhili_area` VALUES ('3205', '宁县', '621026', '345', '3');
INSERT INTO `lizhili_area` VALUES ('3206', '镇原县', '621027', '345', '3');
INSERT INTO `lizhili_area` VALUES ('3207', '安定区', '621102', '346', '3');
INSERT INTO `lizhili_area` VALUES ('3208', '通渭县', '621121', '346', '3');
INSERT INTO `lizhili_area` VALUES ('3209', '陇西县', '621122', '346', '3');
INSERT INTO `lizhili_area` VALUES ('3210', '渭源县', '621123', '346', '3');
INSERT INTO `lizhili_area` VALUES ('3211', '临洮县', '621124', '346', '3');
INSERT INTO `lizhili_area` VALUES ('3212', '漳县', '621125', '346', '3');
INSERT INTO `lizhili_area` VALUES ('3213', '岷县', '621126', '346', '3');
INSERT INTO `lizhili_area` VALUES ('3214', '武都区', '621202', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3215', '成县', '621221', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3216', '文县', '621222', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3217', '宕昌县', '621223', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3218', '康县', '621224', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3219', '西和县', '621225', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3220', '礼县', '621226', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3221', '徽县', '621227', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3222', '两当县', '621228', '347', '3');
INSERT INTO `lizhili_area` VALUES ('3223', '临夏市', '622901', '348', '3');
INSERT INTO `lizhili_area` VALUES ('3224', '临夏县', '622921', '348', '3');
INSERT INTO `lizhili_area` VALUES ('3225', '康乐县', '622922', '348', '3');
INSERT INTO `lizhili_area` VALUES ('3226', '永靖县', '622923', '348', '3');
INSERT INTO `lizhili_area` VALUES ('3227', '广河县', '622924', '348', '3');
INSERT INTO `lizhili_area` VALUES ('3228', '和政县', '622925', '348', '3');
INSERT INTO `lizhili_area` VALUES ('3229', '东乡族自治县', '622926', '348', '3');
INSERT INTO `lizhili_area` VALUES ('3230', '积石山保安族东乡族撒拉族自治县', '622927', '348', '3');
INSERT INTO `lizhili_area` VALUES ('3231', '合作市', '623001', '349', '3');
INSERT INTO `lizhili_area` VALUES ('3232', '临潭县', '623021', '349', '3');
INSERT INTO `lizhili_area` VALUES ('3233', '卓尼县', '623022', '349', '3');
INSERT INTO `lizhili_area` VALUES ('3234', '舟曲县', '623023', '349', '3');
INSERT INTO `lizhili_area` VALUES ('3235', '迭部县', '623024', '349', '3');
INSERT INTO `lizhili_area` VALUES ('3236', '玛曲县', '623025', '349', '3');
INSERT INTO `lizhili_area` VALUES ('3237', '碌曲县', '623026', '349', '3');
INSERT INTO `lizhili_area` VALUES ('3238', '夏河县', '623027', '349', '3');
INSERT INTO `lizhili_area` VALUES ('3239', '城东区', '630102', '350', '3');
INSERT INTO `lizhili_area` VALUES ('3240', '城中区', '630103', '350', '3');
INSERT INTO `lizhili_area` VALUES ('3241', '城西区', '630104', '350', '3');
INSERT INTO `lizhili_area` VALUES ('3242', '城北区', '630105', '350', '3');
INSERT INTO `lizhili_area` VALUES ('3243', '大通回族土族自治县', '630121', '350', '3');
INSERT INTO `lizhili_area` VALUES ('3244', '湟中县', '630122', '350', '3');
INSERT INTO `lizhili_area` VALUES ('3245', '湟源县', '630123', '350', '3');
INSERT INTO `lizhili_area` VALUES ('3246', '乐都区', '630202', '351', '3');
INSERT INTO `lizhili_area` VALUES ('3247', '平安区', '630203', '351', '3');
INSERT INTO `lizhili_area` VALUES ('3248', '民和回族土族自治县', '630222', '351', '3');
INSERT INTO `lizhili_area` VALUES ('3249', '互助土族自治县', '630223', '351', '3');
INSERT INTO `lizhili_area` VALUES ('3250', '化隆回族自治县', '630224', '351', '3');
INSERT INTO `lizhili_area` VALUES ('3251', '循化撒拉族自治县', '630225', '351', '3');
INSERT INTO `lizhili_area` VALUES ('3252', '门源回族自治县', '632221', '352', '3');
INSERT INTO `lizhili_area` VALUES ('3253', '祁连县', '632222', '352', '3');
INSERT INTO `lizhili_area` VALUES ('3254', '海晏县', '632223', '352', '3');
INSERT INTO `lizhili_area` VALUES ('3255', '刚察县', '632224', '352', '3');
INSERT INTO `lizhili_area` VALUES ('3256', '同仁县', '632321', '353', '3');
INSERT INTO `lizhili_area` VALUES ('3257', '尖扎县', '632322', '353', '3');
INSERT INTO `lizhili_area` VALUES ('3258', '泽库县', '632323', '353', '3');
INSERT INTO `lizhili_area` VALUES ('3259', '河南蒙古族自治县', '632324', '353', '3');
INSERT INTO `lizhili_area` VALUES ('3260', '共和县', '632521', '354', '3');
INSERT INTO `lizhili_area` VALUES ('3261', '同德县', '632522', '354', '3');
INSERT INTO `lizhili_area` VALUES ('3262', '贵德县', '632523', '354', '3');
INSERT INTO `lizhili_area` VALUES ('3263', '兴海县', '632524', '354', '3');
INSERT INTO `lizhili_area` VALUES ('3264', '贵南县', '632525', '354', '3');
INSERT INTO `lizhili_area` VALUES ('3265', '玛沁县', '632621', '355', '3');
INSERT INTO `lizhili_area` VALUES ('3266', '班玛县', '632622', '355', '3');
INSERT INTO `lizhili_area` VALUES ('3267', '甘德县', '632623', '355', '3');
INSERT INTO `lizhili_area` VALUES ('3268', '达日县', '632624', '355', '3');
INSERT INTO `lizhili_area` VALUES ('3269', '久治县', '632625', '355', '3');
INSERT INTO `lizhili_area` VALUES ('3270', '玛多县', '632626', '355', '3');
INSERT INTO `lizhili_area` VALUES ('3271', '玉树市', '632701', '356', '3');
INSERT INTO `lizhili_area` VALUES ('3272', '杂多县', '632722', '356', '3');
INSERT INTO `lizhili_area` VALUES ('3273', '称多县', '632723', '356', '3');
INSERT INTO `lizhili_area` VALUES ('3274', '治多县', '632724', '356', '3');
INSERT INTO `lizhili_area` VALUES ('3275', '囊谦县', '632725', '356', '3');
INSERT INTO `lizhili_area` VALUES ('3276', '曲麻莱县', '632726', '356', '3');
INSERT INTO `lizhili_area` VALUES ('3277', '格尔木市', '632801', '357', '3');
INSERT INTO `lizhili_area` VALUES ('3278', '德令哈市', '632802', '357', '3');
INSERT INTO `lizhili_area` VALUES ('3279', '乌兰县', '632821', '357', '3');
INSERT INTO `lizhili_area` VALUES ('3280', '都兰县', '632822', '357', '3');
INSERT INTO `lizhili_area` VALUES ('3281', '天峻县', '632823', '357', '3');
INSERT INTO `lizhili_area` VALUES ('3282', '大柴旦行政委员会', '632857', '357', '3');
INSERT INTO `lizhili_area` VALUES ('3283', '冷湖行政委员会', '632858', '357', '3');
INSERT INTO `lizhili_area` VALUES ('3284', '茫崖行政委员会', '632859', '357', '3');
INSERT INTO `lizhili_area` VALUES ('3285', '兴庆区', '640104', '358', '3');
INSERT INTO `lizhili_area` VALUES ('3286', '西夏区', '640105', '358', '3');
INSERT INTO `lizhili_area` VALUES ('3287', '金凤区', '640106', '358', '3');
INSERT INTO `lizhili_area` VALUES ('3288', '永宁县', '640121', '358', '3');
INSERT INTO `lizhili_area` VALUES ('3289', '贺兰县', '640122', '358', '3');
INSERT INTO `lizhili_area` VALUES ('3290', '灵武市', '640181', '358', '3');
INSERT INTO `lizhili_area` VALUES ('3291', '大武口区', '640202', '359', '3');
INSERT INTO `lizhili_area` VALUES ('3292', '惠农区', '640205', '359', '3');
INSERT INTO `lizhili_area` VALUES ('3293', '平罗县', '640221', '359', '3');
INSERT INTO `lizhili_area` VALUES ('3294', '利通区', '640302', '360', '3');
INSERT INTO `lizhili_area` VALUES ('3295', '红寺堡区', '640303', '360', '3');
INSERT INTO `lizhili_area` VALUES ('3296', '盐池县', '640323', '360', '3');
INSERT INTO `lizhili_area` VALUES ('3297', '同心县', '640324', '360', '3');
INSERT INTO `lizhili_area` VALUES ('3298', '青铜峡市', '640381', '360', '3');
INSERT INTO `lizhili_area` VALUES ('3299', '原州区', '640402', '361', '3');
INSERT INTO `lizhili_area` VALUES ('3300', '西吉县', '640422', '361', '3');
INSERT INTO `lizhili_area` VALUES ('3301', '隆德县', '640423', '361', '3');
INSERT INTO `lizhili_area` VALUES ('3302', '泾源县', '640424', '361', '3');
INSERT INTO `lizhili_area` VALUES ('3303', '彭阳县', '640425', '361', '3');
INSERT INTO `lizhili_area` VALUES ('3304', '沙坡头区', '640502', '362', '3');
INSERT INTO `lizhili_area` VALUES ('3305', '中宁县', '640521', '362', '3');
INSERT INTO `lizhili_area` VALUES ('3306', '海原县', '640522', '362', '3');
INSERT INTO `lizhili_area` VALUES ('3307', '天山区', '650102', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3308', '沙依巴克区', '650103', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3309', '新市区', '650104', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3310', '水磨沟区', '650105', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3311', '头屯河区', '650106', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3312', '达坂城区', '650107', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3313', '米东区', '650109', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3314', '乌鲁木齐县', '650121', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3315', '乌鲁木齐经济技术开发区', '650171', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3316', '乌鲁木齐高新技术产业开发区', '650172', '363', '3');
INSERT INTO `lizhili_area` VALUES ('3317', '独山子区', '650202', '364', '3');
INSERT INTO `lizhili_area` VALUES ('3318', '克拉玛依区', '650203', '364', '3');
INSERT INTO `lizhili_area` VALUES ('3319', '白碱滩区', '650204', '364', '3');
INSERT INTO `lizhili_area` VALUES ('3320', '乌尔禾区', '650205', '364', '3');
INSERT INTO `lizhili_area` VALUES ('3321', '高昌区', '650402', '365', '3');
INSERT INTO `lizhili_area` VALUES ('3322', '鄯善县', '650421', '365', '3');
INSERT INTO `lizhili_area` VALUES ('3323', '托克逊县', '650422', '365', '3');
INSERT INTO `lizhili_area` VALUES ('3324', '伊州区', '650502', '366', '3');
INSERT INTO `lizhili_area` VALUES ('3325', '巴里坤哈萨克自治县', '650521', '366', '3');
INSERT INTO `lizhili_area` VALUES ('3326', '伊吾县', '650522', '366', '3');
INSERT INTO `lizhili_area` VALUES ('3327', '昌吉市', '652301', '367', '3');
INSERT INTO `lizhili_area` VALUES ('3328', '阜康市', '652302', '367', '3');
INSERT INTO `lizhili_area` VALUES ('3329', '呼图壁县', '652323', '367', '3');
INSERT INTO `lizhili_area` VALUES ('3330', '玛纳斯县', '652324', '367', '3');
INSERT INTO `lizhili_area` VALUES ('3331', '奇台县', '652325', '367', '3');
INSERT INTO `lizhili_area` VALUES ('3332', '吉木萨尔县', '652327', '367', '3');
INSERT INTO `lizhili_area` VALUES ('3333', '木垒哈萨克自治县', '652328', '367', '3');
INSERT INTO `lizhili_area` VALUES ('3334', '博乐市', '652701', '368', '3');
INSERT INTO `lizhili_area` VALUES ('3335', '阿拉山口市', '652702', '368', '3');
INSERT INTO `lizhili_area` VALUES ('3336', '精河县', '652722', '368', '3');
INSERT INTO `lizhili_area` VALUES ('3337', '温泉县', '652723', '368', '3');
INSERT INTO `lizhili_area` VALUES ('3338', '库尔勒市', '652801', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3339', '轮台县', '652822', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3340', '尉犁县', '652823', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3341', '若羌县', '652824', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3342', '且末县', '652825', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3343', '焉耆回族自治县', '652826', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3344', '和静县', '652827', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3345', '和硕县', '652828', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3346', '博湖县', '652829', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3347', '库尔勒经济技术开发区', '652871', '369', '3');
INSERT INTO `lizhili_area` VALUES ('3348', '阿克苏市', '652901', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3349', '温宿县', '652922', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3350', '库车县', '652923', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3351', '沙雅县', '652924', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3352', '新和县', '652925', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3353', '拜城县', '652926', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3354', '乌什县', '652927', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3355', '阿瓦提县', '652928', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3356', '柯坪县', '652929', '370', '3');
INSERT INTO `lizhili_area` VALUES ('3357', '阿图什市', '653001', '371', '3');
INSERT INTO `lizhili_area` VALUES ('3358', '阿克陶县', '653022', '371', '3');
INSERT INTO `lizhili_area` VALUES ('3359', '阿合奇县', '653023', '371', '3');
INSERT INTO `lizhili_area` VALUES ('3360', '乌恰县', '653024', '371', '3');
INSERT INTO `lizhili_area` VALUES ('3361', '喀什市', '653101', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3362', '疏附县', '653121', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3363', '疏勒县', '653122', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3364', '英吉沙县', '653123', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3365', '泽普县', '653124', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3366', '莎车县', '653125', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3367', '叶城县', '653126', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3368', '麦盖提县', '653127', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3369', '岳普湖县', '653128', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3370', '伽师县', '653129', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3371', '巴楚县', '653130', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3372', '塔什库尔干塔吉克自治县', '653131', '372', '3');
INSERT INTO `lizhili_area` VALUES ('3373', '和田市', '653201', '373', '3');
INSERT INTO `lizhili_area` VALUES ('3374', '和田县', '653221', '373', '3');
INSERT INTO `lizhili_area` VALUES ('3375', '墨玉县', '653222', '373', '3');
INSERT INTO `lizhili_area` VALUES ('3376', '皮山县', '653223', '373', '3');
INSERT INTO `lizhili_area` VALUES ('3377', '洛浦县', '653224', '373', '3');
INSERT INTO `lizhili_area` VALUES ('3378', '策勒县', '653225', '373', '3');
INSERT INTO `lizhili_area` VALUES ('3379', '于田县', '653226', '373', '3');
INSERT INTO `lizhili_area` VALUES ('3380', '民丰县', '653227', '373', '3');
INSERT INTO `lizhili_area` VALUES ('3381', '伊宁市', '654002', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3382', '奎屯市', '654003', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3383', '霍尔果斯市', '654004', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3384', '伊宁县', '654021', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3385', '察布查尔锡伯自治县', '654022', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3386', '霍城县', '654023', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3387', '巩留县', '654024', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3388', '新源县', '654025', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3389', '昭苏县', '654026', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3390', '特克斯县', '654027', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3391', '尼勒克县', '654028', '374', '3');
INSERT INTO `lizhili_area` VALUES ('3392', '塔城市', '654201', '375', '3');
INSERT INTO `lizhili_area` VALUES ('3393', '乌苏市', '654202', '375', '3');
INSERT INTO `lizhili_area` VALUES ('3394', '额敏县', '654221', '375', '3');
INSERT INTO `lizhili_area` VALUES ('3395', '沙湾县', '654223', '375', '3');
INSERT INTO `lizhili_area` VALUES ('3396', '托里县', '654224', '375', '3');
INSERT INTO `lizhili_area` VALUES ('3397', '裕民县', '654225', '375', '3');
INSERT INTO `lizhili_area` VALUES ('3398', '和布克赛尔蒙古自治县', '654226', '375', '3');
INSERT INTO `lizhili_area` VALUES ('3399', '阿勒泰市', '654301', '376', '3');
INSERT INTO `lizhili_area` VALUES ('3400', '布尔津县', '654321', '376', '3');
INSERT INTO `lizhili_area` VALUES ('3401', '富蕴县', '654322', '376', '3');
INSERT INTO `lizhili_area` VALUES ('3402', '福海县', '654323', '376', '3');
INSERT INTO `lizhili_area` VALUES ('3403', '哈巴河县', '654324', '376', '3');
INSERT INTO `lizhili_area` VALUES ('3404', '青河县', '654325', '376', '3');
INSERT INTO `lizhili_area` VALUES ('3405', '吉木乃县', '654326', '376', '3');
INSERT INTO `lizhili_area` VALUES ('3406', '石河子市', '659001', '377', '3');
INSERT INTO `lizhili_area` VALUES ('3407', '阿拉尔市', '659002', '377', '3');
INSERT INTO `lizhili_area` VALUES ('3408', '图木舒克市', '659003', '377', '3');
INSERT INTO `lizhili_area` VALUES ('3409', '五家渠市', '659004', '377', '3');
INSERT INTO `lizhili_area` VALUES ('3410', '铁门关市', '659006', '377', '3');
INSERT INTO `lizhili_area` VALUES ('3411', '台北', '660101', '378', '3');
INSERT INTO `lizhili_area` VALUES ('3412', '高雄', '660201', '379', '3');
INSERT INTO `lizhili_area` VALUES ('3413', '基隆', '660301', '380', '3');
INSERT INTO `lizhili_area` VALUES ('3414', '台中', '660401', '381', '3');
INSERT INTO `lizhili_area` VALUES ('3415', '台南', '660501', '382', '3');
INSERT INTO `lizhili_area` VALUES ('3416', '新竹', '660601', '383', '3');
INSERT INTO `lizhili_area` VALUES ('3417', '嘉义', '660701', '384', '3');
INSERT INTO `lizhili_area` VALUES ('3418', '宜兰', '660801', '385', '3');
INSERT INTO `lizhili_area` VALUES ('3419', '桃园', '660901', '386', '3');
INSERT INTO `lizhili_area` VALUES ('3420', '苗栗', '661001', '387', '3');
INSERT INTO `lizhili_area` VALUES ('3421', '彰化', '661101', '388', '3');
INSERT INTO `lizhili_area` VALUES ('3422', '南投', '661201', '389', '3');
INSERT INTO `lizhili_area` VALUES ('3423', '云林', '661301', '390', '3');
INSERT INTO `lizhili_area` VALUES ('3424', '屏东', '661401', '391', '3');
INSERT INTO `lizhili_area` VALUES ('3425', '台东', '661501', '392', '3');
INSERT INTO `lizhili_area` VALUES ('3426', '花莲', '661601', '393', '3');
INSERT INTO `lizhili_area` VALUES ('3427', '澎湖', '661701', '394', '3');
INSERT INTO `lizhili_area` VALUES ('3428', '香港岛', '670101', '395', '3');
INSERT INTO `lizhili_area` VALUES ('3429', '九龙', '670201', '396', '3');
INSERT INTO `lizhili_area` VALUES ('3430', '新界', '670301', '397', '3');
INSERT INTO `lizhili_area` VALUES ('3431', '澳门半岛', '680101', '398', '3');
INSERT INTO `lizhili_area` VALUES ('3432', '氹仔岛', '680201', '399', '3');
INSERT INTO `lizhili_area` VALUES ('3433', '路环岛', '680301', '400', '3');
INSERT INTO `lizhili_area` VALUES ('3434', '路氹城', '680401', '401', '3');

-- ----------------------------
-- Table structure for lizhili_article
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_article`;
CREATE TABLE `lizhili_article` (
  `id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `keyword` varchar(10) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `pic` varchar(160) DEFAULT NULL,
  `text` text,
  `state` smallint(6) unsigned DEFAULT '0',
  `click` mediumint(9) DEFAULT '0',
  `zan` mediumint(9) DEFAULT '0',
  `time` int(10) DEFAULT NULL,
  `cateid` mediumint(9) DEFAULT NULL,
  `faid` int(11) DEFAULT '0' COMMENT '发布者id',
  `laiyuan` varchar(255) DEFAULT NULL,
  `click_wai` mediumint(9) DEFAULT '0' COMMENT '展示数据',
  `isopen` tinyint(1) DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `goods_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_article
-- ----------------------------
INSERT INTO `lizhili_article` VALUES ('1', '商城重要通知', '', '重要通知：\r\n商城从战略发展和客户需求出发，以周到的服务为核心，经过系统短期测试和调整，现已全面升级成功，将于9月5日全面开放，正常运行！', '', null, '<p>重要通知：</p><p>商城从战略发展和客户需求出发，以周到的服务为核心，经过系统短期测试和调整，现已全面升级成功，将于9月5日全面开放，正常运行！</p><p><br/></p>', '0', '0', '0', '1660736151', '1', '1', '', '1040', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('3', '北大数学天才柳智宇下山还俗：要求降薪至2万，不打算买房结婚生子', '北大数学天才', '　　临近春节的一天，柳智宇告诉父母、朋友，他要还俗。脱掉僧袍后，他回到武汉的家中过年，有种“轻松自由，云淡风轻”的感觉。　　22岁时，他满足了人们对天才的想象。', '', null, '<p>　　临近春节的一天，柳智宇告诉父母、朋友，他要还俗。脱掉僧袍后，他回到武汉的家中过年，有种“轻松自由，云淡风轻”的感觉。</p><p>　　22岁时，他满足了人们对天才的想象。国际奥林匹克数学竞赛金牌、保送北大、获得麻省理工学院全额奖学金……可2010年夏天，他没有赴美读书，而是打包行李上山出家。</p><p>　　“出家是一条很好的道路。”柳智宇不后悔当初的决定，他想做些真正回归生命的学问，“这对我和父母来说都是一件很重要的事，它意味着我真正独立，可能独立的方式比较特别。”</p><p>　　尽管他躲在寺庙里不见任何来访者，关于他的新闻依旧层出不穷。那时，很多人认为，他会成为高僧大德，也总有人询问他是否开悟，“仿佛不开悟就是一种罪过”。</p><p>　　还俗的想法在心中徘徊，因为他不想再扮演别人想象中的柳智宇。直到今年，他才真正卸下一身包袱，“回归生命”。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15214581656/1000\" width=\"1000\" height=\"790\"/></center><p>　　柳智宇在办公。图/九派新闻 马婕盈</p><p>　　【1】“他不像个领导”</p><p>　　柳智宇的办公室在中关村的一栋写字楼里，和一众培训机构挤在一起。面积不大，装修简单。今年5月，他到一家心理咨询公司任职，作为事业部部长，带领十余人的小团队，开发心理学课程。</p><p>　　这位僧人几乎能在所有事上给同事提供建议，例如给学员分班、安排助教、设计课程问卷……他还有难得的特质——声音温柔，也坚决果断。</p><p>　　他的管理理念是——同甘共苦。</p><p>　　刚加入团队，公司承诺给他3万月薪，他嫌多，主动要求降到2万，和其他员工一样，“扣完税1万多，我不买房、不买车，更不想生孩子，你说我要那么多钱干啥?还不如多奉献给大家。”</p><p>　　他希望团队早日实现收支平衡，希望大家能够获得自由，不管是精神上的还是经济上的。</p><p>　　“他不像个领导。”一位员工评价。</p><p>　　柳智宇的白色办公桌上没有隔板，既是工位也是餐桌。他坐在中间，旁边摆了一盆长势喜人的兰花。初次见面这天，他极为忙碌，一边盯着电脑，一边拿手机回复消息。坐久了，他站起身围着桌子踱步，却仍把手机举在眼前，担心漏掉新消息。</p><p>　　他不要求团队打卡上下班，也从不催促。有次开会，前述员工心情不好，板着脸的模样被柳智宇注意到，散会后，柳智宇还提醒她多注意休息，“他共情能力特别强。”</p><p>　　“我的员工都挺自觉的。”柳智宇笑着说，周末，一位员工来办公室加班，他却认为，她的工作在家就能完成，不需要过来。</p><p>　　为了上班方便，他租住在公司附近一个十几平方米的卧室，要与室友抢占卫生间。晚上，他还要进行一个线上心理咨询。为了赶时间，他选择买两个豆沙包充饥。他穿polo衫、长裤、运动鞋，背斜挎包。脱掉僧袍后，他与匆忙的人群融为一体。</p><p>　　但柳智宇享受现在的生活——不再被众人关注，有志同道合的伙伴，最重要的，他在做自己喜欢的事。</p><p>　　只是他的身上依然能看出僧人的痕迹，与人打招呼时双手合十、吃素、斋前唱诵……</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15214581658/1000\" width=\"1000\" height=\"858\"/></center><p>　　柳智宇家中的佛堂。图/九派新闻马婕盈</p><p>　　【2】“他们想要侵入我的生活”</p><p>　　过去，柳智宇的生活被各种期待裹挟。</p><p>　　2018年秋，他离开龙泉寺，试图找个寺庙落脚，却屡遭碰壁。</p><p>　　有次，他借住在一位居士家，却因未打扫卫生和未浇花被赶走。这位居士将未清洗的锅具、枯死的绿植拍成视频发给他，柳智宇没说什么，收拾好行李离开，“当时自尊心很受挫，我要是观察能力强一点，看到那个花，应该去浇的。”</p><p>　　那段时间，他在寺庙、酒店、居士家辗转，颇有“流落街头”之感。后来，靠另一位居士接济，才得以有个长期住所。</p><p>　　由于过度劳累，一天中的大部分时间，柳智宇只能在床上度过。难受的时候，他口渴得厉害，水随着食道滑落，胃里又一阵翻腾。他对温度极为敏感，怕冷又怕热，秋冬天的夜晚，更让他难眠。</p><p>　　这些年，柳智宇把工作重心放在心理咨询上，他讲课、开会，并组建了佛系心理服务团队，有20余名心理咨询师和疏导师参与，最多的时候，一个月的服务量超过百次。</p><p>　　“我觉得自己不应该休息，一休息就想我是不是又在虚度光阴了。”柳智宇把自己逼得紧，有时身体虚弱坐不起来，他只好把手机挂在床头躺着讲课。</p><p>　　2021年5月，柳智宇躺在床上参加佛系心理服务团队的线上会议，其中有几位工作人员提到，很多来访者想要见柳智宇，工作人员如实告知他的身体情况后，却遭到来访者质疑。他们要求提供柳智宇的微信，想亲自联系。“我其实是有些愤怒的，我感觉他们想要侵入我的生活。”柳智宇说。</p><p>　　一周后，他在火车上禅修，感受到一种很深的自我否定，“也许是因为太多次被各种人事打断，身心都形成一种记忆，认为自己不能好好禅修下去，不值得拥有安静和安详?”他在自述中写道。</p><p>　　柳智宇逐渐意识到，他无法成为别人期望中的自己。能不能成为高僧大德、能不能给予需要的人陪伴、出家是不是误入歧途……他身上背负着许多包袱。那段时间，还俗的想法常在他心中徘徊。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15214581661/1000\" width=\"1000\" height=\"554\"/></center><p>　　柳智宇在进行心理咨询。图/九派新闻 邵骁歆</p><p>　　【3】“师父早就不是我的偶像了”</p><p>　　包袱从出家伊始就开始叠加，一层一层，直到压得柳智宇喘不过气。</p><p>　　初入龙泉寺，柳智宇就感受到了落差。他曾梦想着回到一种柏拉图式的学习方式，让学术成为真正回归生命的学问，也曾想用佛学引导更多人觉悟。可到了寺庙，他却被各种琐事困扰。</p><p>　　除了诵经拜佛，僧人们还需做饭、盖房子，而动手能力是柳智宇的短板，他总是干得很慢，在寺庙8年，他连敲木鱼都没学会。</p><p>　　相较于学校，寺庙里的人际关系更加复杂。晚上，他上厕所总是忘关灯，这引起室友不满，两人还因此起过争执。</p><p>　　“我在生活细节上不太注意，让他们看不惯的行为应该挺多吧。”他如此评价自己。</p><p>　　2013年，柳智宇参与《南山律典校释》的出版工作，2014年起，他辗转上海、福清、天津等地，将南山诸律典“八大部”系统校勘，按照现代人的阅读习惯，出版一套32本的律宗典籍，柳智宇主动要求总负责最后的修订和出版工作。</p><p>　　如此庞大的工程，使他的身体承受能力达到极限。工作时，身体总是不停地颤抖，趴在桌子上也不能缓解。</p><p>　　越来越多的事和他想象的不同。他多次写报告提建议，却被全部驳回，“师父早就不是我的偶像了。”柳智宇无奈地说。离开寺庙后，他被更多事务裹挟着，难以静心修行。</p><p>　　要不要还俗?他思考了近半年。他担心佛教徒们看到他还俗，对佛教失去信心;也害怕别人对他有更多误解，就像12年前，他遁入空门一样。</p><p>　　柳智宇不断与朋友、长辈讨论，综合取舍后才下了决定。临近春节的一天，他给父母、朋友发消息告诉自己还俗的决定。他脱下僧袍，生活无多变化，但心里轻松许多。他搬离了居士提供的住所，在朋友的介绍下，租住在房租较低的通州。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15214581663/1000\" width=\"1000\" height=\"537\"/></center><p>　　柳智宇下班后骑共享单车回家。图/九派新闻邵骁歆</p><p>　　【4】“要有边界感”</p><p>　　现在，柳智宇的家有很强的公共属性，三室两厅的户型带一个大露台，房间宽敞明亮。客厅被改造成佛堂，地上铺着厚厚的草编席，可供十几人打坐禅修。每个卧室都有两张床，两个朝阳的卧室留给居士，柳智宇则住在阴暗狭小的一间，“那边太晒了，夏天中午睡觉要开空调。”他打趣道。</p><p>　　朋友们每周都会来柳智宇家聚会，他们一起打坐、练瑜伽，或者做些心理学的小活动。柳智宇不喜欢独居，“那样太孤独。”这里曾住着一位癌症患者，柳智宇常给他做针灸，缓解疼痛的不适感。</p><p>　　在龙泉寺，柳智宇是工作狂。还俗后，他允许自己放松。他常去小区里的瑜伽馆锻炼，还根据中医的经络理论开发出一套经络瑜伽，他略带自豪地表示，经过把脉观察，经络瑜伽的效果跟针灸相似。</p><p>　　经过调理，柳智宇的身体渐渐好转，说话时总带着笑，“还俗后，我是来陪大家玩的。”对他来说，这是另一种修行，“现在工作虽然比较忙，但能找到内心的安宁，也能找到为大家付出的喜悦。”</p><p>　　但多年的气虚依旧困扰着他。连续说话一小时后，他便难以再开口。</p><p>　　8月28日上午，柳智宇的第一堂新课开讲，从8点半到12点半，讲课、巡堂、督导，连续工作四个小时后，他大口大口地喝着水，再没回答过任何问题。只是，12年后，柳智宇遇到困难时，会先开解自己。他这么自我开解的时候，脸上会有起伏。</p><p>　　这段时间，柳智宇每周都会安排10个心理咨询，在多位来访者中，他也看到了曾经的自己——与身边的同学格格不入，也想融入班级，却被大家孤立。</p><p>　　遇到这些孩子，柳智宇告诉他们，“你没什么问题，只是还没有遇到和你同类的人”柳智宇说，“我慢慢去交流，这些孩子就打开了，爱说话了。”</p><p>　　柳智宇想起了自己的童年。他很小的时候就表现出对动物的怜悯，他不爱吃肉，因为会想象到动物被杀害的场景。三年级，他的理想是治病救人，并找出一种让人不死的生物学方法。“这也是对生命意义的思考。”柳智宇认真地说。</p><p>　　他一直都希望和人在一起，有时却难以融入。高中时，因班上同学的成绩不理想，柳智宇主动给同学们讲题，有一个主题他连续讲了十讲，源源不断输出自己的巧思，却发现只有一两个人听，“我当时特别伤心。”</p><p>　　他找心理老师寻求帮助，老师建议，“要有边界感，每个人最关键的是靠自己，你不可能过度地帮别人承担责任。”</p><p>　　“我很幸运，在刚接触心理咨询的时候就知道了这一点。”柳智宇说，在无法解决来访者的问题时，他才能够把自己从负面情绪中抽离。</p><p>　　回望过去，柳智宇从不怀疑，“出家是一条很好的路。”他承认，自己是理想主义者，“想找一个特别纯洁的地方”。当时，他看不惯很多事情，对商业持负面评价，认为商人大多利欲熏心，他也不想做细碎的工作。</p><p>　　8月25日，柳智宇的团队组织了一场团建活动，主持人让他们想象10年后的自己，还没等柳智宇开口，同事开玩笑地问：“有孩子了吗?”他正色道：“我不准备结婚，也许10年之后，我，做一些善事，把主要精力用在丰富自己的内心上。”</p><p>　　九派新闻记者马婕盈</p><p><br/></p>', '0', '0', '0', '1664005613', '2', '1', '', '725', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('4', '深圳“全城静默管控”系误读', '全城静默管控', '　　深圳卫视&amp;壹深圳客户端记者了解到，今天深圳部分区新型冠状病毒肺炎疫情防控指挥部发布了通告，严控疫情传播风险，尽快实现社会面动态清零，更好保障居民群众', '李天南', null, '<p>　　深圳卫视&amp;壹深圳客户端记者了解到，今天深圳部分区新型冠状病毒肺炎疫情防控指挥部发布了通告，严控疫情传播风险，尽快实现社会面动态清零，更好保障居民群众健康安全。</p><p>　　但是有人将某区的通告“全域实行如下管控措施”理解为“全域停止一切非必要流动、活动”，甚至传言全城静默管理，此系严重误读。请各位市民按照通告发布的有关防疫政策，安心工作和生活。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15215468903/1000\" width=\"932\" height=\"1000\"/></center><p>　　深圳接连发布防控通告</p><p>　　各区通告有何不同?</p><p>　　日前，深圳市疫情防控指挥部和多个区都发布了通告，对疫情防控措施作出具体安排。各区已发布的全域通告有何不同?</p><p>　　具体来看，截至9月1日，深圳全市已发布严格离深管理的通告。南山区、龙岗区、龙华区、宝安区、大鹏新区已发布全域性管控通告。</p><p>　　全市明确：严格离深管理，广大市民群众非必要不离深，确需离深的，自9月1日零时起，按防控有关规定，须持有48小时内2次核酸检测阴性证明。</p><p>　　强化管控措施，并非“静默”</p><p>　　从各区的通告来看，全部都暂停堂食，社区小区、城中村实行严格管理。各类卡口24小时值守，全面实行“居民白名单制”管理。</p><p>　　此外，值得注意的一点，居民仍可以按规定采购生活物资，除了被要求严格居家的区域外，居民均可外出买菜。超市、农贸市场加强通风，限制人流，提倡非现金支付。</p><p>　　各区通告有哪些差异?</p><p>　　是否居家办公?能否上班?从各区措施来看，也各有差异：</p><p>　　南山区、宝安区、龙华区、大鹏新区，都是明确强化社会面管控，严格限制人员聚集;目前都暂未要求居民居家。</p><p>　　龙岗区要求：严格社区小区管理，所有居民严格居家，除按规范定时定点采核酸、按规定采购生活物资、投放垃圾、紧急就医等情形外，做到“足不出户、严格居家”;所有企业原则上一律居家办公，暂停生产经营活动。除保障基本需要的生活超市(含农贸市场)、药店、医疗机构、餐饮(仅提供外卖)以外，其他营业场所、门店一律停业。</p><p>　　龙华区特别提到：跨区通勤人员保持“单位-家庭”两点一线工作生活模式，不组织或参与聚集性活动。提前规划通勤方式和路线，避免途经中高风险地区。尽可能避免乘坐地铁、公交等公共交通工具。</p><p>　　从各区全域性管控措施实施时间来看：</p><p>　　南山区：实施时间为2022年9月1日18时至9月4日24时。</p><p>　　宝安区：实施时间为2022年9月1日0时至9月3日24时。</p><p>　　龙岗区：实施时间为2022年8月31日21时至9月3日24时。</p><p>　　龙华区：实施时间为2022年9月1日18时至9月4日24时，</p><p>　　大鹏新区：实施时间为2022年9月1日18时至9月4日24时。</p><p>　　疫情防控形势总体平稳可控</p><p>　　今天(9月1日)下午六点，深圳举行的疫情防控新闻发布会上，市卫健委二级巡视员林汉城介绍，近日深圳每日新增阳性个案数有所上升，呈现“多点散发，局部爆发”的态势，预计短期内日新增阳性个案数将维持在较高水平。</p><p>　　但是，1日0-12时新增社会面初筛阳性病例数为4例，社会面阳性个案明显下降，疫情防控形势总体平稳可控。</p><p>　　当前，从多区发布的疫情防控通告来看，各区管控措施的差异化是精准防的体现，而从整体看，是全链条提速，以只争朝夕的紧迫感应战疫情，绷紧防疫弦、筑牢疫情防控铜墙铁壁。在这场疫情防控阻击战中，以快制快，全民合力，用最短时间“围住、捞干、扑灭”疫情。</p><p>　　主笔 / 李天南</p><p>　　编辑 / 毛芸 李雄飞 何欢</p><p><br/></p>', '0', '0', '0', '1664005695', '2', '1', '', '677', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('5', '美国因1600万人患新冠后遗症，导致劳动力短缺', '', '　　8月31日，美国疾病控制和预防中心发布最新报告显示，2021年美国人均预期寿命较前一年缩短近1岁，为76.1岁。这是该项数据连续第二年下降，报告分析称新冠疫', '', null, '<p>　　8月31日，美国疾病控制和预防中心发布最新报告显示，2021年美国人均预期寿命较前一年缩短近1岁，为76.1岁。这是该项数据连续第二年下降，报告分析称新冠疫情是首要原因。2019~2021年，美国总体人均预期寿命下降了2.7岁。其中，男性人均预期寿命缩短了3.1岁，降至73.2岁;女性人均预期寿命缩短了2.3岁，降至79.1岁。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15215540792/1000\" width=\"990\" height=\"665\"/></center><p>　　新冠后遗症影响美国1600万人</p><p>　　不仅如此，新冠后遗症还可能长期困扰人体，导致社会劳动力丧失。美国智库布鲁金斯研究所前不久发表了关于新冠后遗症对社会影响的报告。报告称，18~65岁的美国人群中，大约有1600万人患有被称为“长新冠”(Long \r\nCovid，指持续数月的后遗症，包括气喘、疲劳、认知损害)的新冠后遗症，受后遗症影响，离开工作的人有200万~400万人，美国的劳动力因此减少了约1.8%。</p><p>　　根据美国人口普查局和美国疾病控制和预防中心收集的数据，14.8%的美国成年人表示，他们在某个时间点已经感染了病毒。女性患此病的可能性明显更大，有18.2%呈阳性，而男性只有11.3%。其中，以40~49岁年龄段人群为主，其次是30~39岁和50~59岁人群，也有可能经历这些症状。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15215541111/1000\" width=\"1000\" height=\"1000\"/></center><p>　　首都医科大学附属北京佑安医院感染科副主任李侗曾对“环球时报健康客户端”记者表示，一般来说，患者的年龄越大、病情越重，发生新冠后遗症的几率越高，持续时间越长。但多数后遗症会随着时间逐渐缓解，且感染程度越轻，完全康复的可能性大。我国学者在今年6月曾在《柳叶刀·呼吸医学》上发表研究发现，曾因新冠住院治疗的患者出院2年后，55%的人仍被后遗症困扰，包括疲劳、乏力、呼吸困难和睡眠困难。</p><p>　　新冠后遗症，远比你想象得多</p><p>　　1.肺部——导致肺纤维化</p><p>　　呼吸系统是新冠病毒的重点攻击对象，以气促、胸闷等呼吸症状为主的后遗症并不少见。重症、住院患者出院后更易出现呼吸相关后遗症，甚至因肺部修复过度出现瘢痕，留下无法逆转的肺纤维化，造成气体交换能力下降。</p><p>　　2.大脑——引发大脑萎缩</p><p>　　新冠病毒还会杀死脑细胞。《美国医学会杂志》刊登的研究表明，感染新冠后，患者平均出现“脑雾”(记忆力减退、思维混乱等现象)的时间长达8个月，近1/4的人存在记忆问题。牛津大学发表的研究发现，新冠病毒可造成大脑体积平均萎缩0.2%~2%，相当于提前老了1~10岁，还伴随嗅觉皮层部分受损、皮层厚度下降，引发头痛、注意力不集中、嗅觉味觉丧失等，即使是轻症患者也不例外。</p><p>　　3.心脏——导致心悸胸痛</p><p>　　新冠病毒在体内引发的“免疫风暴”可直接损伤心肌细胞，形成瘢痕，导致心肌纤维化，诱发心律失常、心动过速，甚至心脏疾病。发表在美国《自然医学》杂志上的研究显示，英国2020年5月~2022年3月的住院患者中，1/8的人后来诊断出心肌炎。新冠康复者中，两年后仍有心悸、胸痛症状的人分别占11%、7%，远超非感染者。</p><p>　　4.肾脏——损伤肾功能</p><p>　　肾脏是新冠病毒攻击的靶器官之一。《美国肾病学会临床期刊》刊发的研究发现，感染新冠病毒后，32%的住院患者出现了急性肾损伤，出院时近一半的人肾功能未能完全恢复，甚至可能持续下降;感染6个月内，患肾脏病的风险增加35%，机能衰退的风险高25%。</p><p>　　5.肝脏——诱发肝损伤</p><p>　　美国凯斯西储大学收集了近80万儿童感染者的数据后发现，相比其他呼吸道疾病患儿，10岁以下儿童感染新冠后，肝损伤风险高出2倍以上。近期，多国儿童出现的不明原因严重急性肝炎或与新冠病毒引发的超级抗原相关。</p><p>　　6.循环系统——导致血栓或出血</p><p>　　新冠肺炎是深静脉血栓、肺栓塞和出血的危险因素。一项对100万余名感染者的调查发现，感染后，深静脉血栓的风险最多可增加5倍，肺栓塞风险增加33倍，一般出血事件风险可增加近2倍。即使是轻症患者，深静脉血栓形成风险也增加了3倍，肺栓塞风险增加了7倍。</p><p>　　7.性能力——勃起功能障碍</p><p>　　英国伦敦大学的研究表明，感染新冠病毒的成年男性中，约5%出现睾丸和阴茎体积减小，15%的人反映出现勃起功能障碍。</p><p>　　8.内分泌——增加糖尿病风险</p><p>　　《柳叶刀·糖尿病与内分泌学》刊发的研究显示，新冠康复者的糖尿病患病风险比未感染者高出40%;如果存在肥胖、代谢综合征等风险因素，新冠病毒可加速糖尿病的发生发展。</p><p>　　9.其他困扰</p><p>　　如脱发、皮疹、睡眠障碍等，此外有近1/3的人在感染半年内出现抑郁、焦虑等不良情绪。</p><p>　　英国《自然》杂志中的一项研究发现，新冠后遗症已达到50余种。但就目前来看，新冠病毒对人体的伤害可能大于已知结果，后遗症或许远非这个数字。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15215541985/1000\" width=\"1000\" height=\"563\"/></center><p>　　“动态清零”仍是最优选择</p><p>　　我国一直以来都非常重视新冠后遗症的问题。早在2020年武汉疫情暴发后，我国就针对新冠后遗症开设了“新冠康复门诊”，随着新冠疫情在国内很多城市暴发，更多的新冠康复门诊出现。比如上海中医药大学附属岳阳中西医结合医院自4月中旬开设线上新冠康复门诊后，就诊患者已达百余人，几乎每个患者都存在不同程度的不适。</p><p>　　“‘长新冠’很可能会成为长期困扰人们的问题，需要引起注意。”李侗曾表示，虽然现在奥密克戎变异株致病力在下降，后遗症风险也有所降低，但随着病毒传播速度不断加快，针对不同人群的致病能力、可导致的后遗症等，都需要足够数据来确认。</p><p>　　在与新冠病毒对抗的两年多时间里，有不少国家放弃了“清零”政策，选择走“群体免疫路线”，逐步放宽防疫措施，“与病毒共存”。目前全球疫情仍居高位，“动态清零”政策仍是最优选择，尤其是对我国基数庞大的老年人、基础疾病人群、儿童来说，一旦放松管控，可能会面临大规模的人群感染，出现大量重症、死亡病例及巨大的康复需求，以致医疗资源难以应对。</p><p>　　英国国家统计局前不久曾发布公告称，感染后补种新冠疫苗可将长期新冠综合征发生率降低8.8%。因此，每个人一定要严格遵守防疫政策，戴口罩、勤洗手，全程接种疫苗，这些措施都可以将感染新冠的风险降低。▲</p><p>　　受访专家：首都医科大学附属北京佑安医院感染科副主任 李侗曾</p><p><br/></p>', '0', '0', '1', '1664005794', '2', '1', '', '1798', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('6', '学生疑遭老师霸凌后自杀 调查组已赴山东艺术学院开展调查', '', '　　记者从有关方面获悉，近日，山东艺术学院舞蹈学院2020级舞蹈表演专业学生高某的家属向该校反映，高某已于9月10日在河北省张家口市怀来县新保安镇家中去世。对高', '', null, '<p>　　记者从有关方面获悉，近日，山东艺术学院舞蹈学院2020级舞蹈表演专业学生高某的家属向该校反映，高某已于9月10日在河北省张家口市怀来县新保安镇家中去世。对高某同学的离世，学校师生深感痛惜。</p><p>　　日前，省委教育工委已组建由教育、公安等部门组成的联合调查组，进驻山东艺术学院开展调查。调查结果将及时公布。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275440227/1000\" width=\"623\" height=\"294\"/></center><p><br/></p>', '0', '0', '1', '1664005923', '2', '1', '', '1271', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('7', '今晨，天安门广场“祝福祖国”巨型花果篮亮相', '', '　　北京日报客户端 | 记者 朱松梅 王海欣　　经过深夜吊装，天安门广场上的“祝福祖国”巨型花果篮今晨与市民游客见面了。整个国庆摆花工作将在9月25日全部完成。', '', null, '<p>　　北京日报客户端 | 记者 朱松梅 王海欣</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275730035/1000\" width=\"1000\" height=\"661\"/></center><p>　　经过深夜吊装，天安门广场上的“祝福祖国”巨型花果篮今晨与市民游客见面了。整个国庆摆花工作将在9月25日全部完成。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275730036/1000\" width=\"805\" height=\"1000\"/></center><p>　　正值周末，不少市民赶早儿来到天安门广场，只为一睹花果篮的芳容。这座巨型花果篮顶高18米，在好天气的映衬下，更显鲜艳壮观。</p><p>　　“我前天就在网上预约了广场参观，带家人一起来跟花果篮合个影。”市民张城一一数着蓝里的花果：花儿有十种，包括牡丹、月季、菊花等;果也是十种，有苹果、石榴、葡萄、西瓜、大桃，“硕果累累，十全十美，这寓意太好了”。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275730038/1000\" width=\"1000\" height=\"663\"/></center><p>　　巨型花果篮，是在一夜之间完成主体吊装和拼接的。前些天，花盘在地面上由花艺师指挥完成插制。昨天夜里，运送花坛构件的车辆驶入天安门广场，由两辆吊车将花盘缓缓吊装到篮体上。吊装时，两台吊车需要高度一致，同时起吊、匀速平稳，不能发生剧烈晃动，精准入位。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275730039/1000\" width=\"1000\" height=\"666\"/></center><p>　　栩栩如生花朵，每一朵都重达上百公斤，钢骨架支撑起花瓣结构，阻燃布经颜色打印、立体剪裁后套在骨架上。“重量最大的花是那朵牡丹，直径3.2米，重约300公斤。”北京花木公司总工程师张先哲说，值得一提的是，月季花瓣的PU材质今年经过了改进升级。“添加阻燃剂之后，PU的色彩往往会不够亮丽。我们不断研究开发，通过优化模具并调整阻燃剂浓度，制造出了这朵直径1.6米的PU月季，强度非常好，又艳丽又防火。”</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275730042/1000\" width=\"1000\" height=\"665\"/></center><p>　　今年国庆期间，长安街沿线的主题花坛增至14处。东长安街的7处花坛以新发展理念为主线，展示祖国的辉煌成就;西长安街的7处花坛的主线则是以人民为中心共创美好生活，描绘的是身边触手可及的幸福生活。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275730044/1000\" width=\"1000\" height=\"668\"/></center><p>　　目前，双子座大厦、京西宾馆西门等多个花坛已经落成，其他主题花坛也在工程收尾阶段，将于9月25日晚全部完工。今年摆花将持续至10月底。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275730046/1000\" width=\"1000\" height=\"689\"/></center><p><br/></p>', '0', '0', '0', '1664006059', '2', '1', '北京日报客户端', '1289', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('8', '多个特大城市全面放开落户', '', '　　在济南、昆明、大连之后，又有一个特大城市全面放开落户。　　日前，郑州发布新政，拟进一步放宽中心城区落户条件：凡在中心城区具有合法稳定就业或合法稳定住所(含租', '', null, '<p>　　多个特大城市全面放开落户，郑州最新加入</p><center><img alt=\"\" src=\"http://xiaochengxu.biaotian.ltd/ueditor/20220926/1664157895538134.png\" width=\"640\" height=\"360\"/></center><p>　　在济南、昆明、大连之后，又有一个特大城市全面放开落户。</p><p>　　日前，郑州发布新政，拟进一步放宽中心城区落户条件：凡在中心城区具有合法稳定就业或合法稳定住所(含租赁)的人员，不受社保缴费年限和居住年限的限制，可在郑州申请落户。</p><p>　　第七次全国人口普查数据显示，2020年郑州市常住人口比2010年增加397万人，增长46.07%，常住人口总量达到1260万人，在河南省内从2010年的第三位跃升到第一位，成为河南省唯一一个常住总人口超过1000万的市。</p><p>　　397万的增量位居全国各城市第五。同时，郑州所在的河南是我国户籍人口第一大省，全省户籍人口过亿，常住人口也达到了9883万人，因此未来郑州人口的增长空间仍很大。</p><p>　　根据国家统计局去年9月公布《经济社会发展统计图表：第七次全国人口普查超大、特大城市人口基本情况》，郑州的城区人口为534万人，是14个特大城市之一。另外，从建成区来看，2021年郑州市中心城区城市建成区面积744.15平方公里，市域城市建成区面积为1342.11平方公里。相比2012年年末市区建成区面积373平方公里，近十年来扩大了一倍。</p><p>　　今年7月份，国家发改委印发的《“十四五”新型城镇化实施方案》要求，深化户籍制度改革，放开放宽除个别超大城市外的落户限制，试行以经常居住地登记户口制度。全面取消城区常住人口300万以下的城市落户限制，确保外地与本地农业转移人口进城落户标准一视同仁。全面放宽城区常住人口300万至500万的I型大城市落户条件。完善城区常住人口500万以上的超大特大城市积分落户政策，精简积分项目，确保社会保险缴纳年限和居住年限分数占主要比例，鼓励取消年度落户名额限制。</p><p>　　郑州之外，大连、济南和昆明这几个特大城市也已全面放开落户。其中，今年4月1日，大连市人民政府官网发布关于公开征求《关于全面放开落户条件的通知(征求意见稿)》意见的通知，符合条件的人员，本人、配偶和子女可在大连市落户。</p><p>　　此外，包括福州、石家庄、南昌这几个I型大城市(即城区人口介于300万到500万之间的城市)也全面放开落户门槛。总体上看，目前除了一线城市和个别强二线城市外，大多数城市的落户门槛已经很低，乃至零门槛。</p><p>　　与之相对应的是，新一线、二线城市也正在逐渐成为人口流入的重点。根据第一财经记者统计，2021年，我国共有20个城市的人口增量达到或超过8万人，主要是新一线城市和二线城市。</p><p>　　广东省体改研究会执行会长彭澎对第一财经分析，新一线城市、二线城市相比一线城市，房价没那么高、城市也没那么拥挤，无论是经济发展还是人口集聚都十分迅速，收入与一线城市的差距越来越小，因此这些年吸引了大量的人口集聚。</p><p>　　从各类型城市来看，一线城市普遍生活节奏快，工作压力大，人才竞争激烈，房价高，生活压力大。而三四线城市虽然房价低、环境也好，生活压力小，但是就业机会少，发展空间受限。相比之下，新一线城市、二线城市不仅拥有较多的就业机会，收入水平较高。</p><p>　　今年2月，猎聘发布的《2022新一线城市人才吸引力报告》显示，从2017-2021连续五年来中高端求职者投递一线和新一线城市的占比来看，一线城市呈下降趋势，从2017年的45.33%下降到2021年的36.87%;与此同时，新一线城市的投递占比则从31.52%上升到2021年的35.03%，越来越趋近于一线城市。</p><p><br/></p>', '0', '0', '0', '1664006497', '2', '1', '', '1538', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('9', '央行发布人民币国际化最新进展，下一步怎么走？', '', '　　9月23日，央行发布2022年人民币国际化报告(下称“报告”)。其中，多个重磅数据显示，截至2021年末，境外主体持有境内人民币股票、债券、贷款及存款等金融', '', null, '<p>　　9月23日，央行发布2022年人民币国际化报告(下称“报告”)。其中，多个重磅数据显示，截至2021年末，境外主体持有境内人民币股票、债券、贷款及存款等金融资产金额合计为10.83万亿元，同比增长20.5%。离岸人民币市场逐步回暖、交易更加活跃。截至2021年末，主要离岸市场人民币存款接近1.50万亿元。</p><p>　　报告指出，近年来人民币国际化指数总体呈上升态势。2021年末，人民币国际化综合指数为2.80，同比上升17%。同期，美元、欧元、英镑、日元等主要国际货币国际化指数分别为58.13、21.81、8.77和4.93。2022年第一季度，人民币国际化综合指数为2.86，同比上升14%。同期，美元、欧元、英镑、日元等国际货币国际化指数分别为58.13、21.56、8.87和4.96。</p><p>　　跨境贸易投资使用人民币需求上升</p><p>　　近年来，市场主体在跨境贸易投资中使用人民币以减少货币错配风险的内生需求不断上升。</p><p>　　报告在专栏七“2021年度人民币国际使用市场调查“情况中指出，2021年，中国银行对境内外工商企业使用人民币的情况进行了市场调查，调查样本逾3300家，其中，境内企业约2400家，境外企业约900家。调查显示，人民币结算货币功能持续巩固。约有78.8%的受访境内外工商企业考虑在跨境交易中使用人民币或提升人民币的使用比例，这一比例与2020年度调查情况基本持平。</p><p>　　此外，调查结果显示，有20.9%的受访境内工商企业表示在跨境交易中使用人民币报价，这一比例较2020年有小幅上升。</p><p>　　2021年和2020年的调查结果均显示，在考虑是否使用人民币开展贸易融资时，境外工商企业最关注人民币兑本国货币汇率及汇率避险成本、人民币利率水平两大因素。</p><p>　　央行表示，为更好满足市场需求，需要继续做好制度设计、政策支持和市场培育工作，加强本外币协同，支持市场主体在对外贸易投资中更多使用人民币。进一步推进跨国公司本外币一体化资金池试点。围绕自由贸易试验区(自由贸易港)、粤港澳大湾区及上海国际金融中心建设，推动人民币跨境投融资业务创新，不断提升境内外市场主体在贸易投资中使用人民币的意愿。</p><p>　　人民币汇率弹性增强</p><p>　　近一段时间，市场对人民币汇率关注度较高，特别是在美联储按下加息键后，人民币对美元即期汇率延续下跌势头，一度触及7.13关口。</p><p>　　实际上，虽然近期人民币汇率波动较大，但近一段时间以来，人民币在主要货币中依然表现稳健。对于汇率，报告显示，2021年，人民币汇率以市场供求为基础，双向波动，弹性增强。全年人民币对一篮子货币汇率有所升值。2021年末，中国外汇交易中心(CFETS)人民币汇率指数为102.47，较2020年末上升8.1%。人民币对国际主要货币汇率有所升值，其中人民币对美元、欧元、英镑和日元汇率中间价分别较2020年末升值2.3%、11.2%、3.3%和14.1%。</p><p>　　在完善人民币汇率形成机制方面，2021年，央行继续推进汇率市场化改革，完善以市场供求为基础、参考一篮子货币进行调节、有管理的浮动汇率制度，保持人民币汇率弹性，发挥汇率调节宏观经济和国际收支自动稳定器的作用。2021年全年，人民币对一篮子货币汇率有所升值，汇率弹性增强，企业汇率避险意识提升。总的来看，2021年跨境资本流动和外汇供求基本平衡，市场预期总体平稳，人民币汇率以市场供求为基础，有贬有升，在合理均衡水平上保持基本稳定。</p><p>　　进一步便利境外投资者进入中国市场</p><p>　　我国金融市场开放持续推进，人民币资产对全球投资者保持较高吸引力，证券投资项下人民币跨境收付总体呈净流入态势。</p><p>　　截至2021年末，境外主体持有境内人民币股票、债券、贷款及存款等金融资产金额合计为10.83万亿元，同比增长20.5%。离岸人民币市场逐步回暖、交易更加活跃。截至2021年末，主要离岸市场人民币存款接近1.50万亿元。</p><p>　　报告指出，推动金融市场向全面制度型开放转型，提高人民币金融资产的流动性。进一步便利境外投资者进入中国市场投资，丰富可投资的资产种类，便利境外投资者特别是央行类机构更多配置人民币资产。支持境外主体发行“熊猫债”，继续做好粤港澳大湾区“跨境理财通”试点工作。</p><p>　　下一阶段，央行将坚持改革开放和互利共赢，把握好发展和安全的关系，坚持以市场驱动、企业自主选择为基础，稳步提升人民币国际化水平。始终坚持扩大高水平对外开放，以服务构建“双循环”新发展格局、促进贸易和投资便利化为导向，进一步完善人民币跨境使用的政策支持体系和人民币国际化基础设施，更好服务实体经济。推动更高水平金融市场双向开放，促进人民币在岸、离岸市场良性循环。进一步完善本外币一体化的跨境资本流动宏观审慎管理，牢牢守住不发生系统性风险的底线。</p><p><br/></p>', '0', '0', '1', '1664006533', '2', '1', '', '1054', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('10', '王毅会见美国国务卿布林肯', '', '　　新华社纽约9月23日电(记者邓仙来　杨士龙)当地时间2022年9月23日，国务委员兼外长王毅在中国常驻联合国代表团驻地会见美国国务卿布林肯。　　王毅说，当前', '', null, '<p>　　新华社纽约9月23日电(记者邓仙来　杨士龙)当地时间2022年9月23日，国务委员兼外长王毅在中国常驻联合国代表团驻地会见美国国务卿布林肯。</p><p>　　王毅说，当前中美关系遭遇严重冲击，其中的教训美方需要汲取。中美关系正处于关键当口，亟需双方本着对世界、对历史、对两国人民负责的态度，建立两个大国正确相处之道，推动两国关系止跌回稳。</p><p>　　王毅重点针对近期美方在台湾问题上的错误行径全面阐述了中方的严正立场。强调台湾问题是中国核心利益中的核心，在中国人心中的分量重如泰山。维护国家主权和领土完整是我们的使命所在，从不含糊。美方在台湾问题上对中方是有明确政治承诺的。远有中美三个联合公报，近有美本届政府多次表示不支持“台独”。但美方的行动却与此背道而驰，企图破坏中国的主权和领土完整，阻挠中国的和平统一大业，搞所谓“以台制华”，甚至公开声称要协防台湾，发出了十分错误和危险的信号。美方应原原本本回归中美三个联合公报和一个中国原则，干干净净重申一个中国政策，清清楚楚表明反对各种“台独”分裂活动。</p><p>　　王毅强调，台湾问题是中国内政，以什么方式解决美方无权干预。中方解决台湾问题的立场是一贯的、明确的，将继续坚持“和平统一、一国两制”的基本方针。和平解决与“台独”分裂水火不容。“台独”活动越猖獗，和平解决的可能性就越消减。要真正维护台海和平，就必须明确反对和制止任何“台独”行径。</p><p>　　王毅强调，中美两个大国既有共同利益也有深刻分歧，这一点不会改变。双方从接触第一天起，就知道是在和制度不同的国家打交道，这并未妨碍双方基于共同利益开展合作，也不应该成为中美对立对抗的理由。希望美方端正对华认知，反思和改变以遏制打压为主线的对华政策，不要再试图以实力地位同中国人打交道，不要总想着阻遏中国的发展，不要动辄就搞单边霸凌。要为双方恢复正常交往创造良好氛围，要推动中美关系回到健康稳定的发展轨道。</p><p>　　布林肯表示，美中关系当前处于困难时期，推动双边关系重回稳定轨道符合双方利益。两国以往曾成功管控分歧，美方愿同中方坦诚沟通对话，避免误解误判，找到前行道路。布林肯重申美方不寻求打“新冷战”，一个中国政策没有改变，不支持“台独”。</p><p>　　双方还就乌克兰局势等交换了意见。</p><p>　　双方认为会晤是坦诚、建设性和重要的，同意继续保持沟通。</p><p><br/></p>', '0', '0', '0', '1664006566', '2', '1', '', '1302', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('11', '30分钟、4部、提前7天，国庆档电影“抱团”空降', '', '　　千呼万唤始出来。　　9月23日，距离2022国庆档还有一周时间，四部真人电影共同上演“抱团”定档大戏。上午9点，《万里归途》和《长空之王》发布定档海报。9点', '', null, '<p>　　千呼万唤始出来。</p><p>　　9月23日，距离2022国庆档还有一周时间，四部真人电影共同上演“抱团”定档大戏。上午9点，《万里归途》和《长空之王》发布定档海报。9点10分，《钢铁意志》正式宣布进军国庆档。9点30分，《平凡英雄》正式官宣定档。半小时内四部影片正式确认档期，宣布9月30日上映。</p><p>　　此前，2022年国庆档的日历上，仅有三部低幼动画电影。</p><p>　　受疫情反复影响，2022年上半年大盘表现疲软。下半年疫情得到控制，市场情况有所好转。暑期档票房达到91亿，虽然相较2021年的74亿有所回升，但比起2019年的178亿仍有明显下滑。</p><p>　　作为全年最后一个七天大档期，国庆档理所当然被寄予厚望。据猫眼专业版，2019年至2021年，国庆档票房分别为45亿、40亿和44亿，“我和我的”系列、《姜子牙》《长津湖》等高票房影片均诞生于国庆档。</p><p>　　沉寂已久的电影市场急切需要新片补充能量。</p><p>　　截至23日晚22时，国庆档预售票房突破1554万。《长空之王》预售票房达1252万，《万里归途》预售票房达284万，暂居国庆档票房冠军和亚军。</p><p>　　极限定档继续，但片方已有准备</p><p>　　提前七天才确认国庆档片单，对往年来说或许不可思议，但对2022年而言是习以为常。</p><p>　　2022年下半年，略有体量的影片似乎都未逃过宣传期缩短的命运：《独行月球》提前10天定档，《明日战记》提前15天提档，《新神榜：杨戬》提前3.5天定档，《哥，你好》提前7天定档。</p><p>　　上映日期不定，宣发期被压缩，直接影响影片的票房表现。例如，《新神榜：杨戬》上映一周后，导演赵霁曾对界面文娱坦言：“确实受到宣传期过短的影响，感觉很多人不知道影片上映。”</p><p>　　“空间”也延续到了国庆档。三天前的9月20日，博纳影业回复投资者提问称，“公司国庆档上映信息暂未确定”。9月22日，导演饶晓志发布微博“已在途中——那就一早九点喜大普奔”，电影官微随后转发该微博。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275076451/1000\" width=\"954\" height=\"346\"/></center><p>　　图片来源：深交所互动易</p><p>　　由此看来，或许最近几天国庆档片单才最终确认。不过不少片方早已开始宣发动作，积极“备战”。</p><p>　　档期未定的情况下，《万里归途》9月接连发布两版预告、一组海报、一条特辑。《长空之王》发布一版预告、两组海报、一条特辑。《平凡英雄》则9月19日在乌鲁木齐举行首映式。此外，梁朝伟、王一博主演的主旋律电影《无名》、光线传媒出品动画电影《深海》、邓超俞白眉合作的《中国乒乓》等也释出物料，成为“网传国庆档片单”上的常客。</p><p>　　9月23日，2022年国庆档终于迎来了确定消息。上午9点至9点30分，《万里归途》《长空之王》《钢铁意志》《平凡英雄》四部电影陆续官宣定档。</p><p>　　四部影片题材各不相同，四部真人电影都带有一定主旋律色彩。《万里归途》 \r\n讲述外交官撤侨故事，由张译、王俊凯、殷桃等主演。《长空之王》聚焦新一代战斗机试飞员，由王一博、胡军等主演。《钢铁之王》 \r\n讲述了中国共产党团结带领鞍钢职工，炼出新中国第一炉钢，支援抗美援朝的故事。《平凡英雄》改编自真实事件“救助和田断臂男孩”， \r\n演员阵容包括李冰冰、冯绍峰和黄晓明等。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275076455/1000\" width=\"1000\" height=\"562\"/></center><p>　　《万里归途》剧照</p><p>　　四部真人电影之外，今年国庆档还有《我是霸王龙》《新灰姑娘2》和《新大头儿子和小头爸爸5》三部低幼动画电影，将于10月1日上映。</p><p>　　《我是霸王龙》从2021年开始就多次撤档定档，这次终于公布发行通知。引进电影《新灰姑娘2》原先定档6月1日，后因疫情撤档。系列前作《新灰姑娘》于2018年上映，票房6098万。</p><p>　　唯一没有经历过撤档定档的动画电影是《新大头儿子和小头爸爸5》。该系列前四部作品的票房分别为4230万、9040万、1.58亿和9328万。</p><p>　　博纳参与三部，光线华谊或缺席</p><p>　　目前已定档的七部影片中，饶晓志执导，张译、王俊凯、殷桃主演的《万里归途》猫眼“想看”数量最多，其背后出品方为华策电影。华策电影由A股上市公司华策影视100%持股。</p><p>　　8月回A上市的博纳影业参与国庆档影片数量最多。《平凡英雄》《长空之王》《钢铁意志》三部作品均有博纳的身影。</p><p>　　猫眼专业版显示，博纳影业是《平凡英雄》作为第一出品方。《长空之王》第一出品方是韩寒的亭东影业，上海博纳文化传媒公司位列主出品方末位。《钢铁意志》主出品方为北方联合影视集团有限公司，天眼查显示，该公司疑似控制人为辽宁省广播电视局，博纳影业为该片联合出品方。</p><p>　　不过，博纳影业出品，程耳执导，王一博、梁朝伟主演的《无名》，自月初发布预告后再无消息。9月19日，博纳影业回复投资者称，《无名》上映档期暂未确定。</p><p>　　阿里影业和万达电影分别参与了两部国庆档电影。作为主出品方，阿里影业参与了《平凡英雄》和《长空之王》。万达电影作为《钢铁意志》联合出品方、《新大头儿子和小头爸爸5》的主出品方参与国庆档。不过从出品方排序来看，两部电影并不由万达电影主投主控。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15275076459/1000\" width=\"1000\" height=\"562\"/></center><p>　　《钢铁意志》剧照</p><p>　　截至目前，光线传媒和华谊兄弟没有出现在出品方名单中，暂时缺席国庆档。</p><p>　　光线传媒或曾有过进军国庆档的计划，其出品的动画电影《深海》、真人电影《扫黑·拨云见日》都曾有过宣发动作。</p><p>　　光线传媒的动画电影一直备受期待，《深海》是《西游记之大圣归来》导演田晓鹏新作，制作历时八年，今年多次发布制作特辑。《扫黑·拨云见日》是电视剧《扫黑风暴》的电影版，由肖央、范丞丞等主演。</p><p>　　不过，《深海》最后一次宣发停留在9月2日的角色预告。《扫黑·拨云见日》的消息停留在8月中旬的概念海报。9月9日，光线传媒回复投资者表示，《深海》正在过审中，《扫黑·拨云见日》预计年内上映。</p><p>　　同样缺席国庆档的还有华谊兄弟。不过2022年，华谊兄弟一直在电影市场“存在感”不高。上半年，华谊兄弟仅有参与投资的《月球陨落》年内上映，票房惨淡;参与投资的《穿过寒冬拥抱你》和《反贪风暴5》均于去年12月31日上映，累计票房15.66亿元。</p><p>　　财报显示，华谊兄弟2022年上半年营业收入2.12亿元，同比下滑63.4%，净亏损1.92亿元。</p><p><br/></p>', '0', '0', '0', '1664006598', '2', '1', '', '1131', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('12', '英国警方确认，涉嫌《GTA 6》泄露事件的少年黑客已被捕', '', '　　IT之家 9 月 24 日消息，上周末 R \r\n星备受期待的游戏大作《GTA6》的测试版游戏视频在网上泄露，引起业界轰动，成为有史以来最重大的游戏泄密事件。', '', null, '<p>　　IT之家 9 月 24 日消息，上周末 R \r\n星备受期待的游戏大作《GTA6》的测试版游戏视频在网上泄露，引起业界轰动，成为有史以来最重大的游戏泄密事件。然而让人意外的是，这次泄密事件背后的黑手仅是一个 \r\n16 岁的青少年。</p><p>　　伦敦警方通报称，他们已经在牛津郡逮捕到了一名涉嫌网络攻击事件的 17 岁少年黑客，并表示他目前仍在被拘留，但没有公布任何其他细节。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15274870485/1000\" width=\"527\" height=\"559\"/></center><p>　　警方拒绝透露这次涉案详情，但许多细节与最近高调的黑客活动相吻合。</p><p>　　今年春天，伦敦警方还曾逮捕并释放了七名与 Lapsus$ 黑客组织有关的少年。今天的逮捕也是在两起据信与 Lapsus$ \r\n有关的安全漏洞发生后的几天，最知名的事件便是《侠盗猎车手 6》的早期录像因 &quot;网络入侵&quot; 而泄露。</p><p>　　据悉，黑客组织 Lapsus$ \r\n自去年年底以来已卷入多起黑客事件，此前包括微软、三星、英伟达和育碧在内的许多科技公司都遭到了其攻击，该黑客组织曾表示自己的主要目标是钱，既无政治性，也没有任何人赞助。</p><p>　　IT之家曾报道，R 星之前发文承认《GTA6》遭到了泄露，但表示不会对其正在进行的项目产生长期影响。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15274870486/1000\" width=\"760\" height=\"427\"/></center><p>　　今年 3 月，彭博社报道说，该组织几次重大攻击的幕后黑手都是一名当时只有 16 岁的少年，警方曾到他位于英国牛津郡的牛津附近的家中调查。</p><p>　　此外，美国打车 App 优步(UBER)也表示最近遭到了 Lapsus$ 攻击，现在正在与联邦调查局和美国司法部保持联系。</p><p>　　在 Uber 漏洞发生后的一份声明中，该公司在其博客上写道：“我们认为这个攻击者(或攻击者)隶属于一个名为 Lapsus$ \r\n的黑客组织，该组织在过去一年左右的时间里越来越活跃”，而且《GTA 6》的泄密者在论坛帖子中声称是 Uber 攻击事件的同一人。</p><p>　　伦敦市警方拒绝分享其他细节。</p><p><br/></p>', '0', '0', '0', '1664006632', '2', '1', '', '1616', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('13', '“10斤起步”、“免费装箱” 海运价格跌至年内新低！', '', '　　中新网9月24日电(中新财经记者 \r\n谢艺观)“中国寄美国，海运10斤起步，免费装箱，14元/斤”，“中国寄马来西亚，20斤46元人民币，包税”，“中国集运', '', null, '<p>　　中新网9月24日电(中新财经记者 \r\n谢艺观)“中国寄美国，海运10斤起步，免费装箱，14元/斤”，“中国寄马来西亚，20斤46元人民币，包税”，“中国集运到加拿大，运费低至15元一公斤”……</p><p>　　不少人可能注意到，最近海运的价格已跌至年内新低。去年还是“一箱难求”、价格“狂飙”的国际海运，为何今年突然降起温来?</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/14434273268/1000\" width=\"700\" height=\"466\"/></center><p>　　资料图：宁波舟山港。　汤健凯 摄</p><p>　　运价已较去年高点跌超60%</p><p>　　中新财经记者梳理国内多个出口集装箱运价指数发现，运价上半年经历一波下跌后，下半年持续走低。</p><p>　　9月16日，上海出口集装箱运价指数(SCFI)为2312.65点，较上期下跌9.7%，已连续14周下跌。主要航线运价全数走跌，其中，美西、波斯湾、地中海、南美航线跌幅较大，周跌幅分别为12.5%、16.8%、10.5%与11.7%。</p><p>　　另，Freightos波罗的海指数(FBX)显示，9月21日，全球集装箱运价为4179美元/FEU。2021年9月13日，全球集装箱运价曾一度飙涨至11134.44美元/FEU。这也意味着，目前运价已较去年高点跌超62.5%，从走势图来看，相当于2021年1月份的运价水平。</p><p>　　天津一家国际货运代理有限公司的工作人员向中新财经记者举例，一个40尺标箱海运到美国的西海岸，年初的时候运费大概1万元左右，现在只需要约3000元，价格下降超60%。去年最高点的时候则在1.2万元至1.4万元。</p><p>　　“虽然价格大幅下降，但现在的价格还没有低到疫情前的水平，疫情前的时候，同样的标箱，运费大概只需要1500元。”该工作人员称。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15274137995/1000\" width=\"700\" height=\"524\"/></center><p>　　7月，洋山深水港集装箱吞吐量超210万标箱。　洋山边检站供图</p><p>　　为何海运价格坐上“滑梯”?</p><p>　　2021年，由于供应链中断、港口积压和货物激增，国际进口商争相抢占集装箱船的空间，国际运费出现飙涨，一众海运企业赚得“盆满钵满”。年初，一则“长荣海运年终奖发40个月月薪”的消息，令人直呼“太壕”。</p><p>　　但如商务部国际贸易经济合作研究院研究员梅新育所说，“2020年至2021年，运费价格史无前例地高涨，并非可长期持续的现象，跌下来只是时间问题。”</p><p>　　“今年运价能快速降低，一方面是因为运力增长较快。另一方面，此前多国货币政策‘放水’和俄乌冲突等因素，将通货膨胀的压力推到超预期的水平，商品需求出现下降趋势。”梅新育向中新财经记者表示。</p><p>　　“从美联储开始，今年西方政府缩紧货币政策的幅度和速度都超过了预期。收紧的货币政策叠加地缘政治因素，国际贸易增长预期减弱，运力却在增加，在这种情形下，运费下降就在情理之中。”梅新育补充道。</p><p>　　数据显示，美国进口商品价格指数(剔除石油和石油制品)在5至7月连续三个月环比负增长。美国季调进口商品金额在6至7月连续两个月环比下降。美国零售联合会(NRF)下调了下半年美国需求预测，预计抵达美国大型港口的零售货物总量约1280万TEU，同比下降1.5%。</p><p>　　海运“暴利时代”落幕?</p><p>　　海运运费虽然走低，但由于运价仍处在较高水平等，海运企业上半年继续“富得流油”，集装箱船队规模稳居世界第三的中远海控更是“日赚3.6亿元”，碾压一众上市公司。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15274137997/1000\" width=\"607\" height=\"344\"/></center><p>　　中远海控半年报截图。</p><p>　　不过，近期汇丰全球研究中心的一份报告预测，集装箱航运将在2023至2024年经历不可避免的下行周期，利润将暴跌80%。</p><p>　　包括汇丰在内的多家机构预计，全球集装箱贸易下降与船舶运力增加之间的不匹配将推动运费进一步下滑。而在梅新育看来，这段时间海运价格跌幅有点过猛，年内随着情况的变化，运费或还会有波动。“另外，随着通行费等上涨，运费价格或会出现反弹。长期来看，运力需求还是有增长潜力的。”</p><p>　　不久前，埃及苏伊士运河管理局发表声明，将于2023年1月上调苏伊士运河的船舶通行费。游船和运输干货船只的通行费将上调10%，其余船只通行费将上调15%。苏伊士运河位于欧、亚、非三洲交界地带的要冲，是世界使用最频繁的航道之一。</p><p>　　另据外媒报道，近期随着美国西海岸港口劳工谈判陷入僵局，许多进口商正在将货物从西海岸由更长、更昂贵的路线转移到东海岸和墨西哥湾沿岸的港口。货物激增导致其中几个港口的码头和船只出现拥堵。</p><p>　　此前，上海航运交易所也表示，境外港口的严重拥堵现象并未得到解决，集装箱运力损耗仍将是影响航运市场供求关系与市场运价波动的主要因素之一。此外，新冠疫情形势复杂多变，地缘政治，能源、粮食供应格局调整下的通货膨胀压力等，都对外贸货运需求造成较大的不确定性。(完)</p><p><br/></p>', '0', '0', '0', '1664006690', '2', '1', '', '1532', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('14', '时隔5年，美航母打击群再度驶入韩国釜山海军作战基地', '', '　　【环球时报驻韩国特约记者 刘媛 环球时报特约记者 \r\n韩雯】据韩联社23日报道，美国“里根”号核动力航母打击群当天上午驶入韩国釜山海军作战基地。有分析认为，', '', null, '<p>　　【环球时报驻韩国特约记者 刘媛 环球时报特约记者 \r\n韩雯】据韩联社23日报道，美国“里根”号核动力航母打击群当天上午驶入韩国釜山海军作战基地。有分析认为，针对朝鲜的核威胁，美国通过“战略资产”来向韩国展示对朝进行延伸威慑的承诺，也对朝释放强烈警告信号。</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/15274943162/1000\" width=\"1000\" height=\"647\"/></center><p>　　韩联社称，这是美国航母自2017年10月后时隔5年再度以训练为目的进入釜山作战基地。据报道，航母打击群此行将巩固韩美两国海军之间的友好合作，并将在月底参与韩美联演。</p><p>　　韩国海军作战司令部海洋作战本部长金京哲(音译)，美军第五航母打击群指挥官、海军少将迈克尔·唐纳利等两国海军相关人士参加了当天的入港欢迎仪式。唐纳利在“里根”号航母飞行甲板上举行的记者会上表示，航母群此访将展现韩美两国稳固的同盟关系。他还强调，韩美同盟是现代史上最成功的同盟之一，将借此次访韩进一步巩固同盟关系。</p><p>　　就在美国航母驶入釜山之际，韩国外长朴振、美国国务卿布林肯以及日本外相林芳正于当地时间22日在纽约会晤并发表联合声明。美国国务院网站发布的联合声明称，他们重申，朝鲜若进行核试验将面临国际社会强烈以及坚决的回应。他们还对朝鲜在核武器使用问题上不断升级以及释放“破坏稳定”的信息，包括颁布关于核武力政策的法令表示严重关切。布林肯重申了美国对保卫韩国和日本的承诺。</p><p><br/></p>', '0', '0', '2', '1664006722', '2', '1', '', '1207', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('15', '北京试行二手房“卖一买一”并行办理 改善型置业将更为便捷', '', '　　9月23日，北京市住建委和北京市规自委联合发布《关于试行存量房交易“连环单”业务并行办理的通知》(以下简称《通知》)，将二手房交易“连环单”由过去的串联办理', '杨娟娟', null, '<p>　　9月23日，北京市住建委和北京市规自委联合发布《关于试行存量房交易“连环单”业务并行办理的通知》(以下简称《通知》)，将二手房交易“连环单”由过去的串联办理调整为并联办理，缩短办理时间，方便购房群众。</p><p>　　北京合硕机构首席分析师郭毅表示：“此举针对‘换房’中‘连环单’交易的痛点，将串联办理改为并联办理，在全国来说具有创新意义。同时，此举可以提高二手房交易效率和资金利用率，改善二手房交易环境。”</p><center><img alt=\"\" src=\"https://inews.gtimg.com/newsapp_bt/0/13713552178/1000\" width=\"500\" height=\"333\"/></center><p>　　9月23日，北京市住建委和北京市规自委联合发布《通知》，将二手房交易“连环单”由此前的串联办理调整为并联办理。</p><p>　　“卖一买一”可合并办理</p><p>　　按现行做法，“连环单”家庭一般要在卖出名下住房并办理转移登记后，才能腾出资格购买下一处住房，交易周期较长。为打通“堵点”，自2021年起，北京市住建委在部分经纪机构中进行了试点，将“连环单”业务优化为并行办理，有效压减了交易时长。在总结试点经验的基础上，北京市住建委和北京市规自委于9月23日正式下发《通知》，在全市范围内试行存量房交易“连环单”业务并行办理。</p><p>　　《通知》在继续严格执行商品住房限购政策规定的前提下，优化了购房资格核验操作：纳入“连环单”业务的购房家庭，其卖出住房网上签约后，即可视同该套住房“暂时”转出，进行买入房屋的购房资格审核，待完成买入住房的网签后，可一并办理卖出、买入房屋的贷款、缴税、登记业务。</p><p>　　《通知》还明确了“连环单”业务办理的流程、规则等内容。目前，仅将“A家庭←B家庭←C家庭”涉及三方家庭的连环单业务纳入试行范围。</p><p>　　北京市住建委表示，下一步将会同有关部门，监测评估“连环单”并联办理的实施效果，依托不动产网上办事服务平台，加强交易、缴税、登记各环节的信息流转和业务衔接，在兼顾安全和效率的前提下，逐步完善优化相关措施。</p><p>　　业内：提高效率，满足换房需求</p><p>　　“北京的做法有三点创新之处，一是落实共同申请制度，‘连环单’家庭与卖出方、买入方达成交易意向，需要三方共同提出连环单业务申请;二是优化了资格审查，缩短交易时间;三是明确交易中途停止的处理规则，确保此类连环交易不会出现交易风险。”易居研究院智库中心研究总监严跃进点评称。</p><p>　　“二手房连环交易，往往和改善型住房需求有关，比如卖掉小房子再换大房子，此类交易其实有两笔，过去这两笔分开操作，造成交易周期明显拉长，也使得交易成本较大。而现在北京搭建了此类平台，客观上使得此类交易的成本明显降低，也有助于交易效率提高，叠加当前二手房的各类信贷支持政策，购房方面的便捷度将会明显提高，有利于满足此类家庭的换房需求。”严跃进点评称。</p><p>　　“这也是针对北京房地产市场特点而实施的改善措施。”中指研究院指数事业部研究副总监徐跃进分析表示：“北京房地产市场经过多年的发展，已经逐渐步入存量时代，二手房市场的运转效率会直接影响整体市场资源配置的效率。根据中指监测数据，2021年北京二手房成交面积超过1700万平方米，显著高于新房的1010万平方米。同时，由于购房成本较高，市场中‘连环单’很多，市场运转更加依赖二手房置换改善的链条。”</p><p>　　同时，对于政策的适用范围，徐跃进也强调：“此政策的主要效用在于缩短交易时间，对市场供需的影响或较为有限。同时需要注意的是，目前仅将‘A家庭←B家庭←C家庭’涉及三方家庭的连环单业务纳入试行范围，涉及新房的链条暂不适用。”</p><p>　　从北京二手房市场来看，据北京市住建委网站数据显示，8月北京市二手住宅网签量为13920套，环比增长12.8%，同比下降12.7%。</p><p>　　虽然从网签量看，8月份二手住房市场仍处恢复回升过程，但网签数据具有滞后性。从北京链家成交数据看，8月二手住房市场已进入复苏后的自然回落过程，成交量较7月份小幅下降，成交均价保持平稳;市场预期仍处低位水平，卖方涨价意愿不强，买方观望情绪有所上升。</p><p>　　郭毅表示：“当前购房者对房地产市场信心不足，观望情绪浓厚，此时北京从二手房连环交易这个方向入手，改善二手房交易的环境，从而促使改善型需求更好地释放，对整体市场也将有正向的修复作用。”</p><p>　　新京报记者 徐倩 图/资料图片</p><p>　　编辑 杨娟娟 校对 柳宝庆</p><p><br/></p>', '0', '0', '0', '1664006831', '2', '1', '新京报', '1961', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('16', '9月26日——9月30日播出《法律讲堂》文史版系列节目《政治制度史话》', '', '　　中新社多伦多9月23日电 (记者 余瑞冬)加拿大总理贾斯廷·特鲁多9月23日宣布，任命资深外交官詹妮弗·梅(Jennifer \r\nMay)为新的加拿大驻华大', '刘洁', null, '<center><img alt=\"\" src=\"https://p1.img.cctvpic.com/photoworkspace/contentimg/2022/09/26/2022092609430175817.png\" width=\"1000\" height=\"563\"/></center><p>　　9月26日播出 积重难返的捐纳</p><center><img alt=\"\" src=\"http://xiaochengxu.biaotian.ltd/ueditor/20220926/1664157651448317.png\" width=\"942\" height=\"530\"/></center><p>　　卖官鬻爵在清朝几近疯狂，官爵买卖还被赋予了一个中性的名称：捐纳。捐纳项目花样百出，大小官职明码标价，统治者为什么允许百姓花钱买官?百姓买到的官职能有多大?官爵买卖为何成了清政府无法治愈的制度顽疾?</p><p>　　9月27日播出 清朝立储制度的演变</p><center><img alt=\"\" src=\"https://p3.img.cctvpic.com/photoworkspace/contentimg/2022/09/26/2022092609433582402.png\" width=\"1000\" height=\"563\"/></center><p>　　几千年来，中国的皇位继承都遵循“嫡长子继承制”，然而清康熙年间，九位皇子为争夺皇位手足相残，“嫡长子继承制”的弊端在“九子夺嫡”事件中暴露无遗。那么“嫡长子继承制”究竟有哪些弊端?为避免皇位继承中的骨肉相残，康熙之后的统治者在立储问题上做了怎样的制度改变?</p><p>　　9月28日播出 不差钱的内务府</p><center><img alt=\"\" src=\"https://p4.img.cctvpic.com/photoworkspace/contentimg/2022/09/26/2022092609434317907.png\" width=\"1000\" height=\"563\"/></center><p>　　它是掌管皇家事务的权力机构，负责管理帝后妃嫔的衣食住行;它是清朝油水最大的衙门，大小官吏利用职务中饱私囊、贪腐自肥。作为皇帝的管家，内务府的官吏们都有着怎样的生财之道?他们是如何欺上瞒下，从皇帝身上揩油的?</p><p>　　9月29日播出 锦衣卫的恶名</p><center><img alt=\"\" src=\"http://xiaochengxu.biaotian.ltd/ueditor/20220926/1664157652820320.png\" width=\"1000\" height=\"562\"/></center><p>　　在当代影视作品中，他们身着飞鱼服，腰佩绣春刀，飞檐走壁，武艺高强，是令人闻风丧胆的特务机构;他们阴险毒辣、残害忠良，是明朝政治黑暗残暴的代名词。锦衣卫——这一贯穿于整个明朝的机构，为什么给后世留下了如此负面的名声?它的真实面目到底是什么?</p><p>　　9月30日播出 皇子教育制度</p><center><img alt=\"\" src=\"http://xiaochengxu.biaotian.ltd/ueditor/20220926/1664157652164521.jpg\" width=\"1000\" height=\"506\"/></center><p>　　在君主专制的封建王朝，皇帝素养的高低不仅关系到体制的健康与否，还关系到天下黎民的衣食住行。因此对皇帝接班人——皇子的培养，不仅攸关一家一姓的兴衰，还是天下大事。那么历代统治者为巩固统治都建立起了怎样的皇子教育制度?皇子皇孙们从小会接受怎样的教育?</p><center><img alt=\"\" src=\"http://xiaochengxu.biaotian.ltd/ueditor/20220926/1664157652684558.png\" width=\"942\" height=\"530\"/></center><p>　　主讲人：张程 历史学者</p><p>　　总编导：张振华</p><p>　　编 导：刘梦琦 李沛</p><p><br/></p>', '0', '0', '16', '1664006919', '2', '1', '央视网', '1855', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('17', '人民币成全球第四位支付货币', '', '　　蟹农徐皓刚从水里捞出第一篓蟹，就看到数十只大小不一的蟹顺着网口呼呼啦啦往外跑开。　　9月23日，时至秋分。早上9点不到，江苏省苏州市阳澄湖附近已是热闹非凡。', '谢博韬', null, '<p>　　人民日报北京9月25日电 \r\n(记者吴秋余)中国人民银行日前发布的《2022年人民币国际化报告》显示，2021年以来，人民币跨境收付金额在上年高基数的基础上延续增长态势。2021年，银行代客人民币跨境收付金额合计为36.6万亿元，同比增长29.0%，收付金额创历史新高。人民币跨境收支总体平衡，全年累计净流入4044.7亿元。环球银行金融电信协会(SWIFT)数据显示，人民币国际支付份额于2021年12月提高至2.7%，超过日元成为全球第四位支付货币，2022年1月进一步提升至3.2%，创历史新高。</p><p>　　国际货币基金组织(IMF)发布的官方外汇储备货币构成(COFER)数据显示，2022年一季度，人民币在全球外汇储备中的占比达2.88%，较2016年人民币刚加入特别提款权(SDR)货币篮子时上升1.8个百分点，在主要储备货币中排名第五。</p><p>　　与此同时，实体经济相关跨境人民币结算量保持较快增长，大宗商品、跨境电商等领域成为新的增长点，跨境双向投资活动持续活跃。人民币汇率总体呈现双向波动态势，市场主体使用人民币规避汇率风险的内生需求逐步增长。人民币跨境投融资、交易结算等基础性制度持续完善，服务实体经济能力不断增强。</p><p><br/></p>', '0', '0', '9', '1664006997', '2', '1', '人民日报', '740', '1', '', '2');
INSERT INTO `lizhili_article` VALUES ('19', '平安夜送什么礼物好【女生篇】', '', '巧克力巧克力的物语是“只给最爱的人”，它柔滑的触觉，甜中带苦，就像是恋人之间甜蜜中带着小争吵。市面上有不少巧克力礼盒，特别适合冬季的时候吃一些甜食用于储存能量，', '', '/uploads/20221212/1670835105324075.jpg', '<p>巧克力</p><p>巧克力的物语是“只给最爱的人”，它柔滑的触觉，甜中带苦，就像是恋人之间甜蜜中带着小争吵。市面上有不少巧克力礼盒，特别适合冬季的时候吃一些甜食用于储存能量，直接去购买作为圣诞礼物就行了。如果条件允许的话，还可以亲自制作生巧，独家手工的味道，吃起来会更加甜蜜哦。</p><p>精品首饰</p><p>虽然精品首饰听起来比较庸俗，但是首饰可以保存的时间很长，又精致大方，而且不挑女生的年龄，在圣诞节送出一款美美的珍珠饰品绝对是不错的选择。无论是项链还是手链，都是一种送女性很讨喜的礼物。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221212/1670835105324075.jpg\" alt=\"\"/></p><p>香水</p><p>都说闻香识女人，无论什么样的女人都免不了被香水所吸引。不同的香水代表不同的女性，因此无论你女朋友平常用不用香水都可以考虑送香水，是非常有档次的礼物选择。</p><p>围巾</p><p>银装素裹的圣诞节之夜随浪漫但也寒气逼人。在女孩子最爱的韩剧里面，经常可以看到的一幕就是欧巴们会轻轻地帮女主角围上围巾，捧起女朋友的小手帮她取暖。围巾会使她在这个冬天更加暖意融融，一股小温馨就此融化了女孩儿的心。送围巾表示想要一辈子围住你，给你温暖的寓意。平安夜礼物就送围巾，融化女朋友的心吧。</p><p>玫瑰花</p><p>送花是由来已久的风俗，虽然很俗套，但是很管用。当你拿着一束鲜花站在你女朋友面前的时候，你女朋友肯定会幸福的天旋地转的。当然，配上巧克力就更好了。平安夜，巧克力跟鲜花更配哦！</p><p><br/></p><p style=\"color: rgb(67, 67, 67); font-family: &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; font-size: 18px; white-space: normal;\"><br/></p>', '0', '0', '0', '1670835109', '2', '1', '', '791', '1', '', '22');
INSERT INTO `lizhili_article` VALUES ('20', '平安夜送什么礼物好【男生篇】', '', '袜子然后送男朋友的礼物并不需要多么的贵重，只需要贴心就足够了。那么，送什么礼物会让他感觉到很贴心呢？有一种很好的选择，就是袜子。对于男生来说，袜子是每天都必须要', '', '/uploads/20221212/1670835219642985.jpg', '<p>袜子</p><p>然后送男朋友的礼物并不需要多么的贵重，只需要贴心就足够了。那么，送什么礼物会让他感觉到很贴心呢？有一种很好的选择，就是袜子。对于男生来说，袜子是每天都必须要穿的。只是有时候他们并不爱洗袜子，他们都是穿的旧扔，或者是堆了很多在一起洗。所以，你就可以送他一个袜子套装。这样的话，他就有足够的袜子可以换洗了。</p><p>围巾</p><p>都知道圣诞节是在每年的12月份，所以在这个日子的时候，天气就变得比较冷了。可我们就可以考虑一下送男朋友一些可以保暖的礼物，就比如说围巾。因为有些男生都是要风度，不要温度的，所以你送他围巾的话就正好可以提醒他要记得照顾好自己的身体。你送围巾的话，还有一种特别的寓意，就是将他一直围在你的身边。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221212/1670835219642985.jpg\" alt=\"\"/></p><p>游戏机</p><p>很多妹子的男朋友可能都是一个游戏迷。虽说大多数女生都不喜欢自己朋友过多的沉迷游戏，而忽略了自己。但是，我们并不能因为自己的喜好就剥夺男生玩游戏的权利。甚至是让他在自己和游戏之间作出选择。所以作为女朋友，我们应该更加的宽容一些，适当地送他一些游戏有关的礼物，这样才能够让男朋友更爱我们。</p><p>签名海报</p><p>并不是只有女生才会有自己喜欢的偶像的，男生也会有自己很喜欢的偶像。如果我们可以帮男生拿到他们很喜欢的明星，给他们送出的亲笔签名的海报。那他们肯定会特别的激动，甚至会觉得你就是他的小天使。如果拿不到本人的签名，其实送一些限量版的海报也是不错的。</p><p>球鞋</p><p>男生和女生喜欢的东西大多数时候都是不同的。比如说女生很喜欢收集口红，而男生很有可能就比较喜欢收集球鞋。所以如果你可以帮助男生收集到一双他们很喜欢的球鞋的话，他们一定会更加的爱你，简直就是把你当作他们手心里的宝。甚至还会满足你的一切要求。</p><p><br/></p><p style=\"color: rgb(67, 67, 67); font-family: &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; font-size: 18px; white-space: normal;\"><br/></p>', '0', '0', '0', '1670835220', '2', '1', '', '993', '1', '', '20');
INSERT INTO `lizhili_article` VALUES ('21', '圣诞节吃什么食物', '', '圣诞节是国外非常重要的节日，就好比中国的新年，所以每年圣诞节这些外国人都会准备很多美食，庆祝过节。那么，圣诞节有什么传统美食呢？1、火鸡，烤鹅火鸡，烤鹅，也是圣', '', '/uploads/20221212/1670835492693040.jpg', '<p>圣诞节是国外非常重要的节日，就好比中国的新年，所以每年圣诞节这些外国人都会准备很多美食，庆祝过节。那么，圣诞节有什么传统美食呢？</p><p>1、火鸡，烤鹅</p><p>火鸡，烤鹅，也是圣诞节必备的美食。外国人一般都会用肉作为主食，那么烤火鸡或者烤鹅就是首选。大部分都国家过圣诞节必吃的是烤火鸡，也有部分国家是选择烤鹅，比如英国。</p><p>外国人圣诞节吃火鸡或者烤大鹅就如同我们新年吃饺子一样，被烤的金黄流油的火鸡或者大鹅，放在餐盘中，周围再搭配一些蔬菜或者水果，真让人垂涎三尺。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221212/1670835492693040.jpg\" alt=\"\"/></p><p>2、姜饼</p><p>姜饼是用蜂蜜、蜜饯果皮、生姜、杏仁、红糖以及香辛料制作成的一种饼干，在圣诞节的时候欧洲有很多国家都喜欢吃姜饼，姜饼会做成各种与圣诞节有关的图案，比如圣诞老爷爷的头像，圣诞树、圣诞袜等等。</p><p>3、布丁</p><p>圣诞节一定要吃的就是圣诞布丁，圣诞布丁是用牛奶、燕麦等制成的布丁，布丁中会有一个完整的杏仁，但只有一颗杏仁，所以看家人谁吃到这一颗杏仁谁就是来年运气最好的一个人。</p><p>4、火腿肠</p><p>熏火腿应该是在传统圣诞大餐中的正菜，对于我们国家来讲，熏火腿还吃不习惯，但我们可以吃会退场，搭配一些调味酱食用，火腿肠可以烤也可以炒着吃。</p><p>5、喝红酒</p><p>圣诞节喝红酒是几年耶稣为人类受的苦难，为了感谢他。在西方国家认为，肉象征耶稣的肉身，而红酒就是他的血液，耶稣替他人受难，被钉在十字架上，是应该被大家纪念的。</p><p>6、圣诞蛋糕</p><p>圣诞蛋糕是圣诞节不可错过的美食，还必须要是巧克力味和草莓味的蛋糕，一般圣诞节会把蛋糕做的非常漂亮，用果酱或者巧克力雕刻上“圣诞快乐”四个字样，插上特制的圣诞蜡烛。圣诞蜡烛由主人依次熄灭，蛋糕中会放进三颗豆子，谁吃到了豆子，谁就是王。</p><p>7、圣诞三文鱼</p><p>三文鱼的吃法有很多种，但是对于西方国家过圣诞节来讲，三文鱼是不过或缺的鱼肉，三文鱼也是一种流行的食品，亦是一种健康的食品。鱼肉中富含高蛋白质以及OMEGA-3脂肪酸，脂肪含量较低，一般会用熏烤的三文鱼片搭配新鲜柠檬汁和黄油一起食用。</p><p><br/></p><p style=\"color: rgb(67, 67, 67); font-family: &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; font-size: 18px; white-space: normal;\"><br/></p>', '0', '0', '0', '1670835496', '2', '1', '', '621', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('22', '冬至九九歌顺口溜', '', '黄河中下游的《九九歌》一九二九不出手；三九四九冰上走；五九六九沿河望柳；七九河开，八九雁来；九九加一九，耕牛遍地走。黄河中游：一九二九不出手；三九四九呀门叫狗；', '', '/uploads/20221212/1670835593797181.jpg', '<p>黄河中下游的《九九歌》</p><p>一九二九不出手；三九四九冰上走；五九六九沿河望柳；七九河开，八九雁来；九九加一九，耕牛遍地走。</p><p>黄河中游：一九二九不出手；三九四九呀门叫狗；五九六九隔河看柳；七九河开，八九雁来；九九加一九，犁牛遍地走。</p><p>黄河上游的《九九歌》</p><p>头九暖，二九冷，三九四九冻破石头，五九六九河沿儿看柳，七九八九光屁股娃娃拍手，九九加一九耕牛遍地走。</p><p>江南的《九九歌》</p><p>一九二九相见弗出手；三九二十七，篱头吹筚篥(bìlì)(古代的一种乐器，意指寒风吹得篱笆噼噼响声)；四九三十六，夜晚如鹭宿(晚上寒冷象白鹤一样卷曲着身体睡眠)；五九四十五，太阳开门户，六九五十四，贫儿争意气；七九六十三，布袖担头担；八九七十二，猫儿寻阳地；九九八十一，犁耙一齐出。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221212/1670835593797181.jpg\" alt=\"\"/></p><p>《九九歌》的另一版本</p><p>一九二九不出手；三九四九冰上走；五九和六九，河边看扬柳；七九河开，八九燕来；九九艳阳天。</p><p>一九二九不出手，三九四九冰上走，五九六九隔河看柳，七九河开，八九雁来，九九加一九，耕牛遍地走！</p><p>九九歌（长沙）</p><p>初九二九，相逢不出手（手插在袖筒或口袋里）；</p><p>三九二十七，檐前倒挂笔（冰柱）；</p><p>四九三十六，行人路途宿（回家过春节）；</p><p>五九四十五，穷汉阶前舞（赞春、送财神）；</p><p>六九五十四，枯桠枝发嫩刺；</p><p>七九六十三，行人路上脱衣裳；</p><p>八九七十二，麻拐子（青蛙）田中嗝；</p><p>九九八十一，脱去蓑衣戴斗笠。</p><p>冬至后数九歌谣的意思</p><p>冬至是中国农历中重要节气，也是一个传统节日。早在春秋时代，聪明的先人用土圭观测太阳，测定出了冬至，时间在每年的阳历12月21日至23日之间，这一天是北半球全年中白天最短夜晚最长的一天。</p><p>老北京有“冬至馄饨夏至面”的说法，而在南方一些地方冬至这天吃狗肉，这个习俗相传是从汉代开始的，据传，汉高祖刘邦在冬至这天吃了樊哙煮的狗肉赞不绝口，从此在民间形成冬至吃狗肉习俗，发展到现代人们这天不但吃狗肉，还吃羊肉等各种滋补食品，以强壮身体。</p><p>我想起了成语“数九寒天”。冬至后便开始“数九”，每九天为一个“九”，这个习俗起源于何时，没有确切资料佐证，不过有资料表明至少在南北朝时已经流行，据梁代宗懔《荆楚岁时记》记载：“俗用冬至日数九九八十一日，为寒尽。”</p><p>北方的数九歌，以北京版的通俗民谣为典型：一九、二九不出手，三九、四九冰上走，五九、六九沿河看柳，七九河开，八九雁来，九九加一九，梨牛遍地走。</p><p>而南方的数九歌，典型的是：冬至是头九，两手藏袖口；二九一十八，口中似吃辣椒；三九二十七，见火亲如蜜，四九三十六，关住房门把炉守，五九四十五，开门寻暖处，六九五十四，杨柳树上发青绦，七九六十三，行人脱衣衫，八九七十二，柳絮满地飞，九九八十一，穿起蓑衣戴斗笠。</p><p><br/></p><p><br/></p>', '0', '0', '0', '1670835668', '2', '1', '', '1573', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('23', '腊八节的风俗及寓意', '', '腊八节是每年农历十二月八，寓意是祈福、祈求丰收、祭祀先祖等。腊八节，主要习俗是“喝腊八粥”，是佛教盛大的节日之一。本为佛教节日，后经历代演变，逐渐成为家喻户晓的', '', '/uploads/20221212/1670836366622563.jpg', '<p>腊八节是每年农历十二月八，寓意是祈福、祈求丰收、祭祀先祖等。</p><p>腊八节，主要习俗是“喝腊八粥”，是佛教盛大的节日之一。本为佛教节日，后经历代演变，逐渐成为家喻户晓的民间节日。</p><p>传说，喝了这种粥以后，就可以得到佛祖的保佑，因此，腊八粥也叫“福寿粥”、“福德粥”、和“佛粥”。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221212/1670836366622563.jpg\" alt=\"\"/></p><p>汉应劭《风俗通义》：“夏曰嘉平，殷曰清祀，周曰大蜡，汉改为腊。先秦时期，我国一些地方有在腊月祭祀祖先和神灵（包括门神、户神、宅神、灶神、井神）的习俗，祈求丰收和吉祥。腊者，猎也，言田猎取禽兽，以祭祀其先祖也。”</p><p>在我国北方，有“小孩小孩你别馋，过了腊八就是年”之说，过腊八意味着拉开了过年的序幕。每到腊八节，北方地区忙着剥蒜制醋，泡腊八蒜，吃腊八面腊八粥。在南方腊八很少提，腊八节是典型的北方节日。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221212/1670836367344174.jpg\" alt=\"\"/></p><p>腊八节的风俗</p><p>1、腊八粥</p><p>腊八粥营养价值丰富，暖胃驱寒，所以腊八这一天喝腊八粥是必须的。</p><p>2、腊八豆腐</p><p>安徽省有晒制腊八豆腐的习惯，这一天把小黄豆做成豆腐，切成块，中间挖洞放盐水，在太阳下烤晒，慢慢晒干，味道入口松软、味咸带甜，非常好吃。</p><p>3、腊八面</p><p>陕西省一带，用豆类、面做原料，面做成韭叶面，红豆提前一晚泡水，第二天熬汤把豆子煮熟，用中火煮面。面煮好后放葱花有泼面，这就叫腊八面。</p><p>4、腊八醋</p><p>腊八醋，传统腊八节习俗。在腊八这天用醋泡大蒜的习俗，名“腊八醋”。腊八醋，要泡到大年初一，初一吃饺子，要吃素饺子，取一年素素净净之意，蘸腊八醋吃，别有一番滋味是。“腊八醋”不仅味道醇正，而且久放不坏。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221212/1670836367740138.jpg\" alt=\"\"/></p><p>5、腊八蒜</p><p>一到腊月初八，过年的气氛一天赛过一天，华北大部分地区在腊月初八这天有用醋泡蒜的习俗，叫腊八蒜。腊八蒜就是在阴历腊月初八的这天来泡制蒜，是华北地区的一个习俗。其材料就是醋和大蒜瓣儿。泡腊八蒜是一道主要流行于华北地区的传统小吃，是腊八节的节日食俗。腊八蒜材料其实非常简单，就是醋和大蒜瓣儿。做法也是极其简单，将剥了皮的蒜瓣儿放到一个可以密封的罐子，瓶子之类的容器里面，然后倒入醋，封上口放到一个冷的地方。慢慢地，泡在醋中的蒜就会变绿，最后会变得通体碧绿的，如同翡翠碧玉。</p><p>6、吃冰</p><p>腊八前一天，人们一般用钢盆舀水结冰，等到了腊八节就脱盆冰并把冰敲成碎块。据说这天的冰很神奇，吃了它在以后一年不会肚子疼。</p><p>7、麦仁饭</p><p>西宁腊八节并不喝粥，而是吃麦仁饭。腊月初七晚上将新碾的麦仁，与牛羊肉同煮，加上青盐、姜皮、花椒、草果、苗香等佐料，经一夜文火煮熬。青海传说中说，农历十二月八日是释迦牟尼的成道之日，成道前有牧羊女献乳糜，用香谷及果实造粥供佛，那粥就是麦仁粥，成了后来青海的“腊八粥”。后人根据这种做法，在青海人的饮食里造了这味饮食。也开始在餐馆里流行。</p><p><br/></p><p style=\"color: rgb(67, 67, 67); font-family: &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; font-size: 18px; white-space: normal;\"><br/></p>', '0', '0', '0', '1670836514', '2', '1', '', '1642', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('24', '2022年澳门回归至今多少年了', '', '2022年澳门回归至今23年了，2022年澳门回归纪念日时间：2022年12月20日，农历十一月廿七，星期二。1999年12月20日零时，中葡两国政府在澳门文化', '', '/uploads/20221212/1670836715684042.jpg', '<p>2022年澳门回归至今23年了，2022年澳门回归纪念日时间：2022年12月20日，农历十一月廿七，星期二。</p><p>1999年12月20日零时，中葡两国政府在澳门文化中心举行政权交接仪式，中国政府对澳门恢复行使主权，澳门回归祖国。这是继1997年7月1日香港回归祖国之后，中华民族在实现祖国统一大业中的又一盛事。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221212/1670836715684042.jpg\" alt=\"\"/></p><p>葡萄牙开始对澳门的殖民统治，时间是在1553年（明嘉靖三十二年），葡萄牙人在准备登上澳门时托言商船被风浪冲击缝裂，货船潮湿，要求借地晾晒，并贿赂了地方官吏汪柏，取得了停靠澳门码头进行贸易的权利。并于1557年正式在澳门定居。</p><p>1582年（明万历十年），中葡订澳门借地协约。澳葡每年向香山县缴纳地租500两白银。</p><p>1840年鸦片战争后，清政府战败。葡萄牙借此于1849年后相继占领了澳门半岛、氹仔岛和路环岛。1874年（清同治十三年）葡人闯入香山筑新关闸，擅自以此为澳门之界。</p><p>1887年12月，清政府与葡萄牙王国签订《中葡友好通商条约》中确认葡萄牙可长驻澳门管理。从而占领澳门达四百年之久。</p><p>1985年5月，葡萄牙总统埃亚内斯应邀访问中国，同时任国家总理就解决澳门问题进行友好磋商。双方都认为解决澳门问题时机已经成熟。访问结束后发表联合公报，双方决定将就澳门问题于1986年上半年在北京进行谈判。</p><p>1986年6月，中葡在北京就澳门问题举行首轮会谈。1987年4月13日，中葡两国政府签订了《中华人民共和国政府和葡萄牙共和国政府关于澳门问题的联合声明》，宣布澳门地区（包括澳门半岛、氹仔岛和路环岛）是中国的领土，中华人民共和国将于1999年12月20日对澳门恢复行使主权。</p><p><br/></p><p style=\"color: rgb(67, 67, 67); font-family: &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; font-size: 18px; white-space: normal;\"><br/></p>', '0', '0', '0', '1670836749', '2', '1', '', '520', '1', '', '22');
INSERT INTO `lizhili_article` VALUES ('25', '元旦的由来和风俗', '', '4000多年前，“部落联盟”首领尧勤政于民、德才兼备，深受百姓爱戴，但因其子不成器，所以将首领的位置传位给舜。舜感念尧的黄羊任人、高风亮节，所以率领联盟一起祭祀', '', '/uploads/20221213/1670899025471301.jpg', '<p>4000多年前，“部落联盟”首领尧勤政于民、德才兼备，深受百姓爱戴，但因其子不成器，所以将首领的位置传位给舜。舜感念尧的黄羊任人、高风亮节，所以率领联盟一起祭祀天地和尧。由于舜与尧都同样深受百姓的爱戴，于是人们就把舜祭祀尧与天地的这一天当作一年的开始之日，这就是“元旦”的由来。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670899025471301.jpg\" alt=\"\"/></p><p>元，谓“始”，凡数之始称为“元”；旦，谓“日”；“元旦”意即“初始之日”。</p><p>古代元旦指元月一日，但各个时期的元月时间不一致。秦朝的元月一日相当于现在的10月1日。1912年民国为了与世界接轨采用西元纪年，规定公历1月1日为“元旦”，农历1月1日为“新年”。元旦节在中国叫阳历年，农历1月1日则叫春节。</p><p>2066年前，古罗马凯撒把1月1日这一天定为西历新年的开始，为了祝福双面神“雅努斯”（Janus），这位罗马神话中的起源神。“Janus”后来也演化为英文一月“January”这个词。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670899025706397.jpg\" alt=\"\"/></p><p>元旦的风俗</p><p>元旦是世界性的节日，各国都在庆祝，但风俗习惯截然不同。</p><p>中国</p><p>所有家庭成员会团聚在一起，吃一顿团圆饭，各地都会举办元旦联欢晚会，有的选择旅游。商场会举办节日活动吸引顾客。</p><p>英国</p><p>元旦前一天，家家户户都必须做到瓶中有酒，橱中有肉，类似于中国的年年有余。除此之外，英国还流行新年“打井水”的风俗，人们都争取第一个去打水，认为第一个打水的人为幸福之人，打来的水是吉祥之水。</p><p>比利时</p><p>人们新年第一件事就是走到牛、马、羊、狗、猫等动物身边，煞有介事地向这些生灵通明：“新年快乐！”</p><p>德国</p><p>德国人在元旦期间，家家户户都要摆上一棵枞树和横树，树叶间系满绢花，表示繁花似锦，春满人间。他们在午夜新年光临前一刻，爬到椅子上，钟声一响，他们就跳下椅子，并将一重物抛向椅背后，以示甩去祸患，跳入新年。在德国的农村还流传着一种“爬树比赛”的过新年风俗，以示步步高升。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670899025890204.jpg\" alt=\"\"/></p><p>法国</p><p>以酒来庆祝新年，直到不省人事。并且以风向来判断祸福吉凶。</p><p>意大利</p><p>意大利的除夕是一个狂欢之夜，当夜幕开始降临，成千上万的人们涌向街头，点燃爆竹和焰火，甚至鸣放真枪实弹。男男女女翩翩起舞，直至午夜。家家户户收拾旧物，将屋子里一些可打碎的东西，摔个粉碎，旧盆子、瓶瓶罐罐统统扔到门外，表示去掉厄运和烦恼，这是他们辞旧岁迎新年的传统方式。</p><p>瑞士</p><p>瑞士人有元旦健身的习惯，他们有的成群结队去爬山、滑雪；有的举行踩高跷比赛。他们以健身来迎接新一年的到来。</p><p>罗马尼亚</p><p>元旦前夜，人们在广场上竖起圣诞树，搭起舞台，烧着焰火，一边载歌载舞。</p><p>希腊</p><p>元旦时，家家都要做一个大蛋糕，里面放一枚银币。蛋糕切若干块，谁吃到带有银币的那块蛋糕，谁就成了新年最幸运的人，大家都向他祝贺。</p><p>西班牙</p><p>西班牙人在元旦前夕，所有家庭成员都团聚在一起，十二点的钟声刚开始敲第一响，大家便争着吃葡萄。如果能按钟声吃下12颗，便象征着新年的每个月都一切如意。</p><p><br/></p><p><br/></p>', '0', '0', '0', '1670899027', '2', '1', '', '687', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('26', '大寒的传统习俗', '', '大寒是二十四节气的最后一个，这时寒潮南下频繁，是我国大部分地区一年中的最冷时期，大寒的传统习俗有备年、买“岁”和对鸡头等。备年每到大寒节气，人们便开始忙着为过年', '', '/uploads/20221213/1670899341175135.jpg', '<p>大寒是二十四节气的最后一个，这时寒潮南下频繁，是我国大部分地区一年中的最冷时期，大寒的传统习俗有备年、买“岁”和对鸡头等。</p><p>备年</p><p>每到大寒节气，人们便开始忙着为过年准备、奔波——赶年集、买年货，写春联，准备各种祭祀供品，扫尘洁物，除旧布新，准备年货，腌制各种腊肠、腊肉，或煎炸烹制鸡鸭鱼肉等各种年肴。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670899341175135.jpg\" alt=\"\"/></p><p>买“岁”</p><p>旧时大寒时节人们争会相购买芝麻秸。因为“芝麻开花节节高”，除夕夜，人们将芝麻秸洒在行走之外的路上，供孩童踩碎，谐音吉祥意“踩岁”，同时以“碎”、“岁”谐音寓意“岁岁平安”，讨得新年好口彩。这也使得大寒驱凶迎祥的节日意味更加浓厚。</p><p>对鸡头</p><p>大寒这一天买卖人要设宴款待为自己劳作了一年的伙计们，白斩鸡为宴席上不可缺的一道菜。据说鸡头朝谁，就表示老板明年要解雇谁。因此现在有些老板一般将鸡头朝向自己，以使员工们能放心地享用佳肴，回家后也能过个安稳年。</p><p>尾牙祭</p><p>按我国的风俗，特别是在农村，每到大寒节，人们便开始忙着除旧布新，腌制年肴，准备年货。在大寒至立春这段时间，有很多重要的民俗和节庆。如尾牙祭、祭灶和除夕等，有时甚至连我国最大的节庆春节也处于这一节气中。大寒节气中充满了喜悦与欢乐的气氛，是一个欢快轻松的节气。尾牙源自于拜土地公做“牙”的习俗。所谓二月二为头牙，以后每逢初二和十六都要做“牙”到了农历十二月十六日正好是尾牙。尾牙同二月二一样有春饼吃，这一天买卖人要设宴，白斩鸡为宴席上不可缺的一道菜。据说鸡头朝谁，就表示老板第二年要解雇谁。因此有些老板一般将鸡头朝向自己，以使员工们能放心地享用佳肴，回家后也能过个安稳年。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670899342831351.jpg\" alt=\"\"/></p><p>八宝饭</p><p>民间有大寒节气吃糯米的说法，因为糯米能够补养人体正气，吃了后会周身发热，起到御寒、养胃、滋补的作用。而糯米制作的食品，最典型的就是八宝饭。糯米蒸熟，拌以糖、猪油、桂花，倒入装有红枣、薏米、莲子、桂圆肉等果料的器具内，蒸熟后再浇上糖卤汁即成。味道甜美，是节日和待客佳品。</p><p>关于八宝饭的由来各有说法，一说是周王伐纣后的庆功美食，所谓“八宝”指的是辅佐周王的八位贤士。不过更加靠谱的解释是八宝饭源自于江浙一带，经由江南师傅进京做御厨才传到北方。如今宁波、嵊州、嘉兴也都保留着过年吃八宝饭的习俗。</p><p>年糕</p><p>吃“消寒糕”的习俗在北京由来已久。“消寒糕”是年糕的一种，不但因其糯米比大米含糖量高，食用后全身感觉暖和，有温散风寒、润肺健脾胃的功效，而且老百姓选择在“大寒”这天吃年糕，还有“年高”之意，带着吉祥如意、年年平安、步步高升的好彩头。</p><p>所以老北京的习俗中大寒这天，一家人分吃年糕，既带着吉祥味，也能驱散身上寒意，所以称为“消寒糕”。</p><p>糯米饭</p><p>古语有云：“小寒大寒无风自寒”。小寒、大寒早上吃糯米饭驱寒是传统习俗。民间认为糯米比大米含糖量高，食用后全身感觉暖和，利于驱寒。中医理论认为糯米有补中益气之功效，在寒冷的季节吃糯米饭最适宜。加之糯米饭寓意温暖，从年头到年尾“暖笠笠”，更是有吉祥之意。</p><p>喝鸡汤</p><p>到了寒冬季节，南京人的日常饮食多了炖汤和羹。大寒已是农历四九前后，传统的一九一只鸡食俗仍被不少市民家庭所推崇，南京人选择的多为老母鸡，或单炖、或添加参须、枸杞、黑木耳等合炖，寒冬里喝鸡汤真是一种享受。</p><p>炖汤不宜使用高压锅，因为营养物质在小火慢炖的过程中才能慢慢释放出来。使用高压锅尽管可以缩短煮熟肉的时间，却达不到煲汤的效果。因此，熬制鸡汤时应该选用砂锅，先开大火烧煮10分钟，后调小火慢炖。此外，在熬汤时，尽量不要揭锅盖，不然容易“跑气”影响鸡汤的滋味。</p><p><br/></p><p style=\"color: rgb(67, 67, 67); font-family: &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; font-size: 18px; white-space: normal;\"><br/></p>', '0', '0', '0', '1670899482', '2', '1', '', '1073', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('27', '南方小年习俗', '', '1、掸尘小年这一天家家户户黎明即起，扫房擦窗，清洗衣物，刷洗锅瓢，实施干净彻底的卫生大扫除。小年的前几天，家家打扫房屋，意为不让灶王爷把土带走。据《拾遗记》记载', '', '/uploads/20221213/1670899826915720.jpg', '<p>1、掸尘</p><p>小年这一天家家户户黎明即起，扫房擦窗，清洗衣物，刷洗锅瓢，实施干净彻底的卫生大扫除。小年的前几天，家家打扫房屋，意为不让灶王爷把土带走。据《拾遗记》记载此俗可追溯到三千多年前，当时它汉先民驱疫鬼，祈安康的宗教仪式。后“尘”与“陈”谐音，故扫尘也就是把陈旧的东西一扫而光，这既指庭院内的陈年积垢，也指旧岁中遇到的不快。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670899826915720.jpg\" alt=\"\"/></p><p>2、祭灶</p><p>即祭送灶神升天，因此小年也叫祭灶节。人们在腊月二十四都要祭灶，灶君神像，贴在锅灶旁边正对风匣的墙上。两边配联多为“上天言好事，下界保平安”，下联也有写成“回宫降吉祥”，横批是“一家之主”。中间是灶君夫妇神像，神像旁边往往画两匹马作为坐骑。</p><p>在灶王像前的桌案上供放糖果、清水、料豆、秣草；其中，后三样是为灶王升天的坐骑备料，祭灶时，还要把关东糖用火融化粘住灶王爷的嘴，让它不在玉帝那里讲坏话，也有意为让灶王的嘴甜，光说好话。</p><p>3、吃年糕</p><p>在南方，小年这一天做年糕是很多地方的传统。年糕又称“年年糕”，与“年年高”谐音，意寓人们的工作和生活一年比一年提高。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670899827969439.jpg\" alt=\"\"/></p><p><br/></p><p style=\"color: rgb(67, 67, 67); font-family: &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;WenQuanYi Micro Hei&quot;, &quot;Helvetica Neue&quot;, Arial, sans-serif; font-size: 18px; white-space: normal;\"><br/></p>', '0', '0', '0', '1670899834', '2', '1', '', '1879', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('28', '北方小年习俗', '', '每年腊月二十三，灶王爷都要上天向玉皇大帝禀报这家人的善恶，让玉皇大帝赏罚。因此送灶时，人们在灶王像前的桌案上供放糖果、清水、料豆、秣草，其中后三样是为灶王升天的', '', '/uploads/20221213/1670900504245259.jpg', '<p>每年腊月二十三，灶王爷都要上天向玉皇大帝禀报这家人的善恶，让玉皇大帝赏罚。因此送灶时，人们在灶王像前的桌案上供放糖果、清水、料豆、秣草，其中后三样是为灶王升天的坐骑备料。祭灶时，还要把关东糖用火融化，涂在在灶王爷的嘴上。这样，他就不能在玉帝那里讲坏话了。民间有“男不拜月，女不祭灶”的习俗，因此祭灶王爷，只限于男子。糖瓜、饴糖、麻糖等吃食本为给灶王爷嘴上抹得吃食，逐渐演变成了小孩小年必吃的零食。晋西北有“二十三吃麻糖，吃不上麻糖啃指头”的俗话。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670900504245259.jpg\" alt=\"\"/></p><p>祭灶</p><p>小年这天，也是汉族民间祭灶的日子。汉族民间传说，每年腊月二十四，灶王爷都要上天向玉皇大帝禀报这家人的善恶，让玉皇大帝赏罚。因此送灶时，人们在灶王像前的桌案上供放糖果、清水、料豆、秣祭灶对联</p><p>草；其中，后三样是为灶王升天的坐骑备料。祭灶时，还要把关东糖用火融化，涂在灶王爷的嘴上。这样，他就不能在玉帝那里讲坏话了。另外，大年三十的晚上，灶王还要与诸神来人间过年，那天还得有&quot;接灶&quot;、&quot;接神&quot;的仪式。等到家家户户烧轿马，洒酒三杯，送走灶神以后，便轮到祭拜祖宗。</p><p>柳腔戏《张郎休妻》、茂腔戏《火龙记》都是说的灶王爷这段故事。</p><p>灶王最初只管火，后来受天帝委派为掌管一家的监护神，被封为一家之主。他权力很大，却连个土地庙大小的庙宇也没有，只有一张画像（木板印制的年画）贴在灶墙上。两旁贴上&quot;上天言好事，回宫降吉祥&quot;或&quot;东厨司命主，南方火帝君&quot;的对联，横批是&quot;一家之主&quot;。</p><p>祭灶时要摆上枣和糖瓜等果品，糖瓜是用大麦发酵糖化而成的食品，据说是让灶王吃了嘴甜，也有的说为了欺住他的嘴，叫他上天光说好话不说坏话。焚香祭拜后，将旧灶君像揭下焚化。换上新像，就算送灶王爷上天找王皇大帝汇报去了。</p><p>祭灶时还要供上碗面汤（面条），俗话说：&quot;灶王爷本姓张，一年一顿杂面汤。&quot;杂面汤是用白面、豆面、地瓜面混合制成，可见灶王爷在人们心目中的地位。</p><p>灶王腊月二十三上天，初一五更回来，就算完成汇报任务，带着吉祥保佑一家过平安日子了。</p><p>如今，祭灶王的人越来越少了，但过小年吃糖瓜的习俗仍在汉族民间盛行。</p><p>腊月二十四日以后，人们开始打扫庭院、居室，清除积垢，置办年货，制作节日食品，一直要忙到除夕。</p><p><br/></p><p><br/></p>', '0', '0', '0', '1670900507', '2', '1', '', '970', '1', '', '0');
INSERT INTO `lizhili_article` VALUES ('29', '除夕的来历和由来', '', '据《吕氏春秋·季冬记》记载，古人在新年的前一天用击鼓的方法来驱逐“疫疬之鬼”(夕)，这就是“除夕”节令的由来。除夕来自先秦时期的逐除的习俗。周、秦时期，每年将尽', '', '/uploads/20221213/1670901203829514.jpg', '<p>据《吕氏春秋·季冬记》记载，古人在新年的前一天用击鼓的方法来驱逐“疫疬之鬼”(夕)，这就是“除夕”节令的由来。</p><p>除夕来自先秦时期的逐除的习俗。周、秦时期，每年将尽的时候，皇宫里要举行“大傩”的仪式，击鼓驱逐疫疠之鬼，称为“逐除”，后又称除夕的前一天为小除，即小年夜；除夕为大除，即大年夜。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670901203829514.jpg\" alt=\"\"/></p><p>除夕节来历故事</p><p>相传，古时候有一个叫“夕”的妖怪，专门害人，尤其是漂亮的女孩。“夕”神出鬼没，只在太阳落山后出来，半夜后又不见其踪影，没有人知道它住在哪儿。老百姓对它恨之入骨，但又无可奈何。</p><p><img src=\"https://lizhili.biaotian.ltd/ueditor/20221213/1670901204473741.jpg\" alt=\"\"/></p><p>一个叫七郎的猎人，力大无比，箭无虚发，猎狗也非常厉害，见乡亲们被“夕”所害，决心除掉“夕”。他带着狗到处找“夕”，找了一年。这天已是腊月三十，他来到一个镇上，见人们都在欢欢喜喜准备过年，心想，这个镇大、人多、姑娘也多，说不定“夕”要来。他找到镇上的人们商量，说“夕”最怕响声，叫大家天黑了不要睡觉，多找些敲得响的东西放在家里，一有动静就使劲敲，好把“夕”吓出来除掉。</p><p>这天晚上“夕”果然来了，他刚闯进一户人家，这家人就敲起了盆盆罐罐，接着整个镇子也跟着敲起来。“夕”吓得四处乱跑，结果被七郎看见。“夕”跟七郎和狗打了起来。“夕”力不从心，想逃跑，哪知后腿被猎狗死死咬着，七郎趁机一箭把“夕”射死。从那以后，人们就把腊月三十叫“除夕”。这天晚上，家家户户都要守岁、放火炮，表示驱除不祥、迎接幸福祥瑞。</p><p><br/></p><p><br/></p>', '0', '0', '1', '1670901234', '2', '1', '', '1737', '1', '', '0');

-- ----------------------------
-- Table structure for lizhili_article_hui
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_article_hui`;
CREATE TABLE `lizhili_article_hui` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `zan` int(11) DEFAULT '0',
  `aid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_article_hui
-- ----------------------------
INSERT INTO `lizhili_article_hui` VALUES ('1', '4', '真好', '1665631015', '0', '17');

-- ----------------------------
-- Table structure for lizhili_article_img
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_article_img`;
CREATE TABLE `lizhili_article_img` (
  `id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `pic` varchar(160) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `aid` int(11) DEFAULT NULL COMMENT '对应ariticle id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_article_img
-- ----------------------------
INSERT INTO `lizhili_article_img` VALUES ('47', null, '/img_pic/20220926/ed40d6617f0ff290fd9bdc4e167957ba.jpg', '1664158335', '1664158335', '12');
INSERT INTO `lizhili_article_img` VALUES ('35', null, '/img_pic/20220924/9fbd0cead5cc44ddbde71a3d80ddfd51.jpg', '1664015575', '1664015575', '14');
INSERT INTO `lizhili_article_img` VALUES ('37', null, '/img_pic/20220924/283a2b67799a137a61a8583e1c4130b0.jpg', '1664015604', '1664015604', '15');
INSERT INTO `lizhili_article_img` VALUES ('43', null, '/img_pic/20220926/33217d1b5caf06da07c59c6315fd599b.jpg', '1664157752', '1664157752', '16');
INSERT INTO `lizhili_article_img` VALUES ('42', null, '/img_pic/20220926/ddf5c3ac06bac67518fe58a0d00f9d21.jpg', '1664157479', '1664157479', '17');
INSERT INTO `lizhili_article_img` VALUES ('44', null, '/img_pic/20220926/18b3c956d1cc0a90c1333a7f3c7a62fc.png', '1664157898', '1664157898', '8');
INSERT INTO `lizhili_article_img` VALUES ('45', null, '/img_pic/20220926/8de29d5d7bd5a277ba89d1696e0f37b1.jpg', '1664158060', '1664158060', '13');
INSERT INTO `lizhili_article_img` VALUES ('46', null, '/img_pic/20220926/58c4e4c4bd72eb84bdc9166896f644dd.jpg', '1664158060', '1664158060', '13');
INSERT INTO `lizhili_article_img` VALUES ('49', null, '/img_pic/20220926/40ef86873f6dabc9916f86e05860f5cd.jpg', '1664158470', '1664158470', '11');
INSERT INTO `lizhili_article_img` VALUES ('40', null, '/img_pic/20220924/ae7e3c1e9414ffc099b99930141f6447.jpg', '1664015833', '1664015833', '9');
INSERT INTO `lizhili_article_img` VALUES ('39', null, '/img_pic/20220924/91750f611a77cb324e8208c47965dc05.jpg', '1664015800', '1664015800', '10');
INSERT INTO `lizhili_article_img` VALUES ('38', null, '/img_pic/20220924/f4da7920b222c770f62125d329532cea.jpg', '1664015691', '1664015691', '7');
INSERT INTO `lizhili_article_img` VALUES ('24', null, '/img_pic/20220924/489800f4fed12bccb49b5b59381d323c.jpg', '1664015284', '1664015284', '4');
INSERT INTO `lizhili_article_img` VALUES ('25', null, '/img_pic/20220924/2f10c0eb92a6c5c0ce45976ac56be02a.jpg', '1664015381', '1664015381', '10');
INSERT INTO `lizhili_article_img` VALUES ('26', null, '/img_pic/20220924/05c9f5a3af65f5336d80f26f366ae63d.jpg', '1664015408', '1664015408', '9');
INSERT INTO `lizhili_article_img` VALUES ('27', null, '/img_pic/20220924/634ba4c1d55d3d64ac2222a2e4f14bbd.jpg', '1664015425', '1664015425', '8');
INSERT INTO `lizhili_article_img` VALUES ('48', null, '/img_pic/20220926/474b48a13d9bb28e5c029480742e16a6.jpg', '1664158470', '1664158470', '11');
INSERT INTO `lizhili_article_img` VALUES ('51', null, '/img_pic/20221213/497d73c795d37cc6ff39cf16d9783c04.jpg', '1670901415', '1670901415', '29');
INSERT INTO `lizhili_article_img` VALUES ('52', null, '/img_pic/20221213/c38743fbcf44ea0b18537b1654a8bd8e.jpg', '1670901415', '1670901415', '29');
INSERT INTO `lizhili_article_img` VALUES ('53', null, '/img_pic/20221214/54cab0084f5f08dc3d60ae4a68503af4.jpg', '1670983723', '1670983723', '30');
INSERT INTO `lizhili_article_img` VALUES ('54', null, '/img_pic/20221214/e537a11af1262c0734049c50d0531b7d.jpg', '1670983723', '1670983723', '30');

-- ----------------------------
-- Table structure for lizhili_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_auth_group`;
CREATE TABLE `lizhili_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` char(80) NOT NULL DEFAULT '',
  `sort` tinyint(4) DEFAULT '0',
  `desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_auth_group
-- ----------------------------
INSERT INTO `lizhili_auth_group` VALUES ('1', '超级管理员', '1', '1,3,2,10,7,9,8,4,6,5,11', '0', '拥有至高无上的权利');
INSERT INTO `lizhili_auth_group` VALUES ('2', '内容发布员', '1', '1,3,2,10,7,9,8,4,6,5,11', '0', '只能管理内容');

-- ----------------------------
-- Table structure for lizhili_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_auth_rule`;
CREATE TABLE `lizhili_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `fid` mediumint(9) DEFAULT '0',
  `level` tinyint(4) DEFAULT '0',
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_auth_rule
-- ----------------------------
INSERT INTO `lizhili_auth_rule` VALUES ('1', 'article/all', '资讯总权限', '1', '1', '', '0', '0', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('2', 'article/add', '添加资讯', '1', '1', '', '1', '1', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('3', 'article/edit', '资讯修改', '1', '1', '', '1', '1', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('4', 'link/all', '友情链接', '1', '1', '', '0', '0', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('5', 'link/add', '添加友情链接', '1', '1', '', '4', '1', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('6', 'link/edit', '修改友情链接', '1', '1', '', '4', '1', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('7', 'slide/all', '幻灯片总权限', '1', '1', '', '0', '0', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('8', 'slide/add', '添加幻灯片', '1', '1', '', '7', '1', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('9', 'slide/edit', '修改幻灯片', '1', '1', '', '7', '1', '0');
INSERT INTO `lizhili_auth_rule` VALUES ('11', 'message/all', '留言总权限', '1', '1', '', '0', '0', '0');

-- ----------------------------
-- Table structure for lizhili_cate
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_cate`;
CREATE TABLE `lizhili_cate` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `catename` varchar(30) DEFAULT NULL,
  `en_name` varchar(30) DEFAULT NULL,
  `fid` tinyint(4) DEFAULT NULL,
  `type` tinyint(3) DEFAULT NULL COMMENT '1代表是列表，2代表是单页，3代表图片列表，4代表打开链接',
  `keyword` varchar(255) DEFAULT NULL COMMENT '栏目关键字',
  `mark` varchar(255) DEFAULT NULL,
  `editorValue` text COMMENT '单页的数据',
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `catehtml` varchar(255) DEFAULT NULL COMMENT '栏目模版',
  `showhtml` varchar(255) DEFAULT NULL COMMENT '详情模版',
  `tiao_type` tinyint(1) DEFAULT '0' COMMENT '1代表栏目，2代表文章',
  `tiao_id` int(11) DEFAULT '0' COMMENT '跳转的id',
  `pic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_cate
-- ----------------------------
INSERT INTO `lizhili_cate` VALUES ('1', '通知', 'tongzhi', '0', '1', '', '', null, '0', '1660736094', '1660736094', '1', '', '', '', '0', '0', null);
INSERT INTO `lizhili_cate` VALUES ('2', '新闻动态', 'xinwendongtai', '0', '3', '', '', null, '0', '1663991996', '1663991996', '1', '', '', '', '0', '0', null);

-- ----------------------------
-- Table structure for lizhili_cms
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_cms`;
CREATE TABLE `lizhili_cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `iswo` tinyint(1) DEFAULT '1',
  `baidu_token` varchar(255) DEFAULT NULL COMMENT '百度的token，seo使用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_cms
-- ----------------------------
INSERT INTO `lizhili_cms` VALUES ('1', '<p style=\"overflow-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; white-space: normal; text-indent: 20px;\">感谢您一年来对我们的支持和包容。为了更好的服务大家，在2018年6月份，我们全新发布了后台管理系统版本。我们的发布离不开广大用户给出的建议和意见。我们整合了更多优秀插件；优化了框架的体积。当然相比目前行业其他管理系统还有很多不足。但初心不改，实实在在把事做好，做用户最喜欢的框架。更好为客户服务。</p><p style=\"overflow-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; white-space: normal; text-indent: 20px;\">我们在2018年版本上面，先进行了，大量的技术更新，包括了秒杀，团购，即时通讯，购物，等等功能的扩展。然后在2019年的9月和11月份，我们又进行了重构，大量的精简了原始代码，把原始的一些插件进行了替换，删除没有必要的程序增加水印缩略图等功能，速度是2018年第一版的3倍以上。从最早网站开发，到现在我们已经经历过了6个年头，我们经历过的项目数百个，每一次修改后台我们都抱着不忘初心的态度，努力的写好每一句代码，希望我们的努力，可以得到您的认可。你们的肯定就是对我们最大支持！</p><p style=\"overflow-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; white-space: normal; text-indent: 20px;\">2020年，添加了广告和水印的判断，修复了bug。添加了数据备份还原功能。添加了关闭网站后的302重定向。5月份添加了后台动态修改菜单功能。6月修改了sql逻辑，添加了广告分类等等。添加了商品的操作。添加了大量的功能。7月修改分销的逻辑，已经发现的bug。8月份添加面包屑等前台功能。模仿dede添加多级循环等标签！</p><p style=\"overflow-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; white-space: normal; text-indent: 20px;\">2020年12月，进行大量修改bug，更新了下载，搜索，SEO，等等功能，更新手册！2021年2月份进行了，修改已知bug，修改了操作手册！2021年6月，修改已知bug，添加多个类库组件。适配php8.0版本。8月份，又进行大量修改，包括上传视频，合并标签，修改手册等功能。10月份添加一键生成模块功能，大大减少了代码开发，<span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; text-indent: 20px;\">非常实用</span>！！！11月，修改了认证和SEO，更好用了！！！又添加了定时器和消息队列的使用等。2022年6月，添加了统计和一键压缩文件功能，添加发送邮件组件和微信支付回调等等。8月添加了webman，修改bug。2022年9月，添加商城功能，并配套前端代码，使用uniapp开发。可以生成APP和小程序，网站。如果有使用上面问题可以联系我，lizhilimaster@163.com。</p><p><br/></p>', '0', '');

-- ----------------------------
-- Table structure for lizhili_config
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_config`;
CREATE TABLE `lizhili_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `shuo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_config
-- ----------------------------
INSERT INTO `lizhili_config` VALUES ('1', 'watermark', '0', '水印');
INSERT INTO `lizhili_config` VALUES ('2', 'shui_weizhi', '9', '水印位置具体看手册');
INSERT INTO `lizhili_config` VALUES ('3', 'shui_neirong', '李志立 lizhilimaster@163.com', '水印内容');
INSERT INTO `lizhili_config` VALUES ('4', 'thumbnail', '0', '缩率图');
INSERT INTO `lizhili_config` VALUES ('5', 't_w', '10001', '缩略图宽');
INSERT INTO `lizhili_config` VALUES ('6', 't_h', '300', '缩略图高');
INSERT INTO `lizhili_config` VALUES ('7', 'shui_zihao', '18', '水印字号');
INSERT INTO `lizhili_config` VALUES ('8', 'shui_yanse', '#ffffff', '水印颜色');
INSERT INTO `lizhili_config` VALUES ('9', 'is_ya', '1', '是否开启压缩图片');
INSERT INTO `lizhili_config` VALUES ('10', 'ya_w', '1000', '压缩后的图片大小');

-- ----------------------------
-- Table structure for lizhili_download
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_download`;
CREATE TABLE `lizhili_download` (
  `id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `keyword` varchar(10) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `pic` varchar(160) DEFAULT NULL,
  `text` text,
  `state` smallint(6) unsigned DEFAULT '0',
  `click` mediumint(9) DEFAULT '0',
  `zan` mediumint(9) DEFAULT '0',
  `time` int(10) DEFAULT NULL,
  `faid` int(11) DEFAULT '0' COMMENT '发布者id',
  `laiyuan` varchar(255) DEFAULT NULL,
  `click_wai` mediumint(9) DEFAULT '0' COMMENT '展示数据',
  `isopen` tinyint(1) DEFAULT '1',
  `file` varchar(255) DEFAULT NULL COMMENT '下载地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_download
-- ----------------------------

-- ----------------------------
-- Table structure for lizhili_goods
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_goods`;
CREATE TABLE `lizhili_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(300) DEFAULT NULL COMMENT '商品名称',
  `goods_code` char(16) DEFAULT NULL COMMENT '商品编号',
  `goods_img` varchar(200) DEFAULT NULL COMMENT '原图',
  `postage` decimal(10,2) DEFAULT '0.00' COMMENT '邮费',
  `markte_price` decimal(10,2) DEFAULT NULL COMMENT '销售价格',
  `isopen` tinyint(1) DEFAULT '1' COMMENT '是否上架，1为上架，0为下架',
  `goods_desc` longtext COMMENT '商品描述',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `sales_num` int(11) DEFAULT '0' COMMENT '已经销售的数据',
  `liu_num` int(11) DEFAULT '0' COMMENT '产品浏览量',
  `is_tui` tinyint(1) DEFAULT '0' COMMENT '首页推荐',
  `cate_id` int(11) DEFAULT '0',
  `kucun_num` int(11) DEFAULT '100000' COMMENT '库存',
  `lei` tinyint(1) DEFAULT '1' COMMENT '1是商城商品，2是消费，3是积分兑换,4是团购',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '这个是创业价格',
  `fen_money` decimal(10,2) DEFAULT '0.00' COMMENT '分润价格',
  `delete_time` int(11) DEFAULT NULL,
  `sort` mediumint(6) DEFAULT '50',
  PRIMARY KEY (`id`),
  KEY `goods_num` (`goods_code`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_goods
-- ----------------------------
INSERT INTO `lizhili_goods` VALUES ('2', '贝蜜清Clearays 玫瑰护手霜 ', '1664003116515315', '/uploads/20220924/738dce530348de9232e3232b1ba44155.png', '0.00', '3.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/200508/14/17170/357471/61922a24E33ad0da5/6e7727fad46dd16b.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/204709/8/15405/202784/61922a24E00558ded/360dab280bb3afec.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/223367/3/2776/348492/61922a24E7146e398/a3ea461fffae629b.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/206646/26/9572/294781/61922a24E28de6a3b/7052ee299e8b5fd6.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/209221/19/9477/234264/61922a24Eb55cb8a3/54d4c2fafc6f0ba9.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/209629/39/9536/286177/61922a24E5b4d3c26/08c49ae89c10c75d.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/202905/2/15434/187206/61922a24E207eb7af/d88772e163e096c6.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/196936/18/17438/331509/61922a24E706607e9/1eb31be6dc08cfe2.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/223235/34/2705/230742/61922a24E4e14cd4f/5e1dd9851d8586b6.jpg\" alt=\"\"/></p>', '1664003116', '1664003116', '3830', '9440', '1', '2', '100000', '1', '2.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('3', '素萃紧致亮皙美颈霜', '1664003305869102', '/uploads/20220924/64bf7a04074f9592db9aaa5d2acee864.png', '0.00', '52.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/156437/5/29623/94256/6305be29Ea0274b9b/c341bafc4d717887.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/59681/21/20801/119729/6306d161E5b0206c3/9eb440433eee6b1a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/187166/23/27013/53892/6305be29E013d6c92/c003fb5e118264f0.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/63706/24/21331/21650/6305be29Ebd5c8d54/621cc117e7ba9525.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/14936/1/18200/63969/6305be29Ea1c0344d/e9d6cc7e96ad5329.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/72641/26/17993/123248/6305be29E13fb828c/9e00c2e986b7fcd1.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/198985/8/26207/82762/6305be29Efccdf1dd/0b28777c6d95a785.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/188206/14/27072/65638/6305be29E882a82a8/d72ee034924bfd87.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/6724/19/23726/131377/6305be2aE85a12086/fcc77cea76359130.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/205806/26/25686/50221/6305be29E9dc926d0/36b4f738c2d6d981.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/23488/18/18481/69599/6306d181E30073ce3/a8bd38f97af29744.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/193602/27/27491/68427/6305be29E6e9b4f6a/a06f9605c9110853.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/48180/8/21770/69666/6305be29E2d28c5ac/0a4bdf7335988508.jpg\" alt=\"\"/></p>', '1664003305', '1664003305', '2251', '2740', '1', '2', '100000', '1', '49.50', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('4', '格力（GREE） 空调 ', '1664003744497547', '/uploads/20220924/84b0acdad6e6f127d585f48266afb00f.jpg', '0.00', '10599.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/181595/16/28304/77284/6318491fE3cfb9807/3adf814e5a8fac12.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/138507/12/29362/64816/6318491fE4db58957/6dd7ae081ceb4064.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/18897/34/18624/146006/6318491fE05be91f8/94901a293fd4aa36.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/123987/7/29327/60751/6318491fE201d2c6e/92acb2a77220ab10.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/92852/40/26668/213133/62567dc1E34dc8f25/9fc87a45402cdfd5.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/109944/23/26787/571839/62567b4dE3602b8da/94351112c03babf4.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/190440/33/19719/198650/61270dfeE25d40ad8/d6ce8297982d0eda.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/198178/36/5120/131984/6127602fE2866c16c/5e7485f6e6a309f2.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/168925/12/12544/265830/604dcc94E9873861b/7928e2fd48260823.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/170148/28/11985/135068/604ae208E3c697ee4/559760bc2dbaf310.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/157565/16/12699/216077/604ae208E45aa83a8/5285581434d609cb.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/162634/19/24854/120428/62a7e2adE32a99cd4/a267b3a9e53e13ff.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/161651/37/11958/184199/604ae208Eeb5ae06a/38d22eaef5a14966.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/216279/17/18748/165941/62a7e3dcEfe326ee4/a9da6742ba342b31.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/160813/35/12109/142668/604ae208Ef893d65b/d4069465eb3da788.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/95488/32/20820/422404/61e0fa02Ea2b4410c/324e640fb06e5672.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/161741/27/11955/126257/604ae208E17c9ce06/c89b1014207cf638.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/187599/28/8942/90959/60cc75f2E3f26f579/279ad4ffbba50b49.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/195861/2/8864/88697/60cc75f2Efb988cb3/8d82180519521945.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/167831/18/12057/117366/604ae208Eabb2525b/b0b1165a9ebc2bec.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/163332/1/12021/84735/604ae207Efb824d51/4ae1443728c9b6c9.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/164920/20/11846/127829/604ae208Ea748c9e1/20dc295cb98b1bdd.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/191856/12/27457/124810/63184a5fE11663ec4/7eb183237bfd2b77.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/157680/17/30341/99098/63184a5fEbba025dc/c5213fa91adef571.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/189241/27/28426/141302/63184a5fE6676cee8/6683577db8f36dbb.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/46383/25/21128/49536/63184927E57ac048a/2a3f477b2e836d3b.jpg\" alt=\"\"/></p>', '1664003744', '1664004212', '2546', '1402', '1', '3', '100000', '4', '10199.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('5', '澳洛莉（OLORISA）玫瑰面部精油', '1664003810141450', '/uploads/20220924/ed5d342aea04a9771426fc100a481ec1.png', '0.00', '72.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/23727/27/16364/303589/62874675E610b94ee/fb8ff1a281910426.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/22529/22/17419/370167/62a2b8bcEeeaef284/87ea730f3775cf39.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/153933/26/22002/729146/62736bc4E79c0e17b/cacc309a07334509.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/83548/27/18350/302620/62736bc3Eeea9431f/fb388565c92e450f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/62550/35/17581/346513/62736bc3Ebef5a854/8f3b835cc77d0e4c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/208906/3/20938/513321/62736bc3Eaf3d066f/73a2ff9eb972334a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/131969/15/27906/309671/62736bc3Eaa55302c/51acc034e6a4eb7f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/131408/16/25700/489868/62736bc3E63206f9b/1bcb264c3f6eb331.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/183169/25/24365/277078/62736bc3E75c08ae2/de73d8c5f8986cc7.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/78874/18/17784/143280/62736bc3E7216de0d/771bb00721379503.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/200642/1/21641/251084/62736bc3Efaa6854d/9bf7dabb12ef29e4.jpg\" alt=\"\"/></p>', '1664003810', '1664003810', '2689', '6975', '1', '2', '100000', '1', '59.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('6', '倩滋（QIANZI）香氛润泽护手霜', '1664003955888583', '/uploads/20220924/890483455fb9ea5d2df67ea272cb93c5.png', '0.00', '29.90', '0', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/124606/29/23710/375284/62b3dfdbEa7be7702/e70ecbeb0c54f1a9.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/199872/2/25777/68451/62b3dfdbEb2d26c4f/db04cc5c669fd1ce.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/204772/4/24722/175642/62b3dfdbE5255928b/cb7b1aa95e02c172.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/99581/20/24932/315308/62b3dfdbEb8a92768/ef570273229da2b0.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/49635/4/19464/226160/62b3dfdbEce6a88dc/0dc3342cc91b9339.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/24126/12/17514/284479/62b3dfdbE794cc965/afe2d6c7348174aa.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/190798/13/25445/206313/62b3dfdbEde677c7d/fb65cf897066a4bf.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/60109/8/19313/176302/62b3dfdbEcf8765c9/b257d3b1776d5b7f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/83891/6/19976/128680/62b3dfdbEe178aa11/fc36f51984395a69.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/194995/35/26494/121905/62b3dfdbEde05e926/c4a080116f7eb408.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/216270/4/20819/380077/62b3dfdbE8b45d500/711c17d72a6b0ccf.jpg\" alt=\"\"/></p>', '1664003955', '1664003955', '3377', '9985', '1', '2', '100000', '4', '19.90', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('7', '北欧实木餐椅', '1664004251710680', '/uploads/20220924/502de634be2e8afbce91857089c69767.png', '0.00', '148.00', '1', '<p><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/184368/10/8471/107898/60c25485E9d7589f7/761a7b309edcd483.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/183884/9/8416/86956/60c25485E89f49eb4/f04911487ddac0ee.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/181938/25/8615/118562/60c25485E7310d15e/de7c8129cc5dd7fc.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/173929/16/14111/120287/60c25486E217eef51/cf40ef51dc0973ec.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/185317/31/8608/110009/60c25486E5dcec684/2444038cb8584865.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/174918/18/14151/95591/60c25486E38432945/bbaf2496be5ecbdc.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/195093/4/7438/194922/60c25486E53876a5b/d47bf8b5caa80def.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/108184/9/20423/176540/60c25486E4d303629/6d20a01185dbdba3.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/188127/32/7747/249125/60c25486Ec8affda8/2aa85e1bb529f4f3.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/191636/18/7585/39488/60c25486Edce8c331/311a25f07d14df2a.jpg\" alt=\"\"/></p>', '1664004251', '1664004251', '4272', '5046', '0', '1', '100000', '1', '128.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('8', '美的(Midea)535升变频对开双开门家用冰箱智能', '1664004398482692', '/uploads/20220924/43f556534e3ff1da8f1609b381b37aed.jpg', '0.00', '555555.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/135085/3/27096/489827/62329d65Ed1339bff/892d8f3c7e825dce.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/125510/12/25512/417972/623325f3Ec4281a00/c986104d05315e5c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/61948/22/19701/225896/62e376c9E0cc9abae/c1063d43375d1b69.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/118882/27/22301/561566/62329d65E648c1904/b7720032bd871e9f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/195694/11/27427/225348/62e86acfE2b9ec9a4/3509ec595675763e.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/181658/33/21951/323361/62329d65Eaa5f2c0f/a5b0408045894d4a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/134031/39/26338/494528/62329d65Ead938303/2774b4505790ec62.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/115467/34/22156/227905/6233f613Ec877eb5d/39a58cfebbb65b91.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/124025/26/29140/134967/6285abe2Ee57e8a6c/eaa53d9b1ee74ccf.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/168378/31/28709/185745/62329d64E176b3243/aa79553b7d726e4a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/120421/31/25739/307022/62329d65E926e90f8/59d4bb31a104e462.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/144497/33/25033/225507/62329dbeEa68f6f8b/2896dc1d8edde502.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/148753/8/23915/197182/62329de8Ef7f2e965/a996568aa6dc9b86.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/130918/17/21876/401884/62329dfeEf7fe79b7/a88a0513bb35340e.jpg\" alt=\"\"/></p>', '1664004398', '1664004398', '3334', '1098', '0', '3', '100000', '4', '3333.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('9', '印尼进口 皇冠（danisa）丹麦曲奇饼干454g铁罐装', '1664004655411867', '/uploads/20220924/35e29bbe50f6f36e3d67ab5b85e28163.avif', '0.00', '60.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/6507/17/9698/361766/60b0a512Ec9df03c7/31a723e8bc2e48ad.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/185052/14/6214/268549/60b0a512E768c6076/96a958d10808c9af.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/181325/4/6252/172604/60b0a512Efa88d21f/7a2ea2a1f56f1da8.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/6409/33/9958/335924/60b0a512Ef3195f6d/14b5a4ad6e632ce7.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/40342/20/16198/213616/60b0a512E3207b51e/703edb12d1b47deb.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/160594/11/4565/41320/6010d58cE613d85a6/b4ece0d7d8d8326a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/8246/9/17910/416200/62aacb98Ec9aa6942/a27aa40eeb12ed60.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/182190/25/6310/265540/60b0a512E0f86a367/1c6d89ea278639ff.jpg\" alt=\"\"/></p>', '1664004655', '1664004655', '3099', '8025', '0', '5', '100000', '1', '45.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('10', '坚果J10投影仪家用办公 智能家庭影院 （ 2400ANSI高亮度 丹拿专业调音 自动对屏 京东小家智能生态）', '1664004924198476', '/uploads/20220924/bea3daf4fe18482c8554a90874a46204.avif', '0.00', '6230.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/201099/20/7291/23380/61444747E6835b3f2/61738e55f7637a52.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/113068/31/27778/88770/62beb8ebE10a3b0f9/fea6f885d5c0999e.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/104787/17/23022/361663/6218a442Ec5060d2d/f6a25f3ac1f65db2.gif\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/203475/22/1135/118737/61161473Edd72acbe/079aaf329335022a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/39826/6/16402/421788/60fa9943E0d426df8/f4aae35e13d908cb.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/190199/39/17921/355964/6113b44dEd648e404/71b1d460a461c328.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/190767/7/17953/167077/6113b44dE7af96290/665075d865d62051.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/211470/27/1768/210156/614c2edaE7ab1f2dd/3d4a40e5480daf5c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/166399/32/23725/134051/614c2edaE34c1d1fe/8631edf261018055.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/175088/20/26558/422212/6180bba8Ef072c2c2/1075dac49da84969.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/177722/39/18919/1006098/6113b44eE81fb1a58/09e3a999931de3ef.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/204790/32/792/749326/6113b44eEb5697cef/518fb379b95c638d.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/176805/27/18851/388910/6113b44dEc78b3105/be3fda4a77256bee.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/179345/1/18552/161964/6113b4b5E904e5613/83ffd4d2fc38610e.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/181553/20/18623/696445/6113b44eE29525117/cb50cb1bbd817876.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/178476/4/18843/517217/6113b44eE4d6d96cd/a1ccd576f94c4795.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/185947/11/18564/535054/6113b44eE2e7b9364/01e9c19964f1813d.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/192087/37/17845/26955/6113b44dE99521ca6/069aed71053cf19c.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/202099/26/893/1021768/6113b44eEf980106f/081fea740b06912a.gif\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/185618/5/18732/34296/6113b44dEf3700662/96d517414d2a4669.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/178345/35/18684/501154/6113b44eEe1ac250b/329b2ea6106c9bc4.gif\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/201085/27/1214/207119/6113b44dE058e06a3/a07fa3ca71df54d5.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/203170/35/894/320882/6113b44dEa53cd3b6/de9f832f6afac364.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/208423/29/4200/87630/61601c4eE4c0ea1c4/e2c60af2405ac587.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/198709/29/2667/678309/6113b44eEa6a7fe0d/752f065d11cd02a4.gif\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/198091/22/2699/404665/6113b44dEa697081b/798d61f0e5b0b33b.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/202787/22/883/56781/6113b44dEdcf3647c/04a64d355b33e609.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/178637/38/18805/553748/6113b44eE2a8acf06/9ada012d01f3e9d9.gif\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/193125/38/17740/539012/6113b44eE49094a3c/fd08129bd39695c0.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/184370/29/18801/554144/6113b44eE16ce9e58/c6773393b687b87d.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/190919/18/17838/211743/6113b44dE03d79519/9b19ef7e64987d5e.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/201680/32/871/156972/6113b4b5Ebdc8fb7d/e1ce30aae88bb01e.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/186630/15/17973/668029/6113b44eE3053bf7b/df36e5223b6e6dba.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/197381/6/2736/448258/6113b44eE70b35188/c4410d4b73e3de8a.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/203283/29/889/1023907/6113b44eE381b33e8/3c7f338d56209270.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/201289/31/1164/12765/6113b44dE6cb1a184/3b12f27a059b37e8.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/185610/6/18631/27858/6113b44dE29015c62/4fc01e42459fffd7.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/183969/15/18756/234190/6113b44dE0e320007/705100b1ee89cad2.png\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/189712/25/17929/300306/6113b44dE0e304a4f/303714acc2e3e10b.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/201998/30/7852/66994/614c2edaEe83230fd/f6f83112ff6d23d4.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/198397/40/855/115288/6105626eEceccb102/00137094ac8d36ec.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/196858/16/2735/686633/6113b44eE6ea1f5aa/b70b6e4687fae40f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/182572/15/18485/48436/6113b44dE0e15c7e3/3189f5c103b6fa11.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/96833/36/27176/119947/62417199Ec857e9a9/071de1c95ee15b90.jpg\" alt=\"\"/></p>', '1664004924', '1664004924', '3344', '4239', '0', '4', '100000', '1', '4999.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('11', '积木部落 电脑椅家用电竞椅', '1664004932715889', '/uploads/20220924/825f3398abfd786312efa857d51776d7.png', '0.00', '610.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/205898/10/25209/140275/62dfb4f5Ebc054e4b/f303f348de1129f8.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/34230/36/17037/15907/62dfb4f4E5a0fa85a/b630dd72f712a209.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/126103/10/29833/172328/62dfb4f5Ea8534e24/d4a4a9555fc6d78d.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/31060/36/16764/169541/62e2545eE12b2533d/a35ba404919618f2.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/211500/13/24502/44038/62dfb4f4E3278ca05/f584411585246704.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/120287/22/29697/153265/62dfb4f5Effc663ee/19ee3d3a7e25e1d1.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/209926/3/25495/135236/62dfb4f5E752a82b1/f7eeb276b2e83ef7.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/137250/19/26612/145367/62dfb4f5E1aa50f4b/b152034f566caba9.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/194169/29/26588/121704/62dfb4f5E2e7ad4b6/6340e958b744072e.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/45726/5/20272/184227/62dfb4f5E3268f8ef/3a929cd1d7246edf.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/177630/36/27102/191929/62dfb4f5Ea1c1766c/44dce2571aaec982.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/140854/19/28102/9585/62dfb4f4E74364d32/7158c76c5b85d448.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/120230/27/28069/115304/62dfb4f5Ebe0419c6/2a89e6b0894d6dff.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/150836/10/28268/120968/62dfb4f5E0931d05d/db031b9a526d96a5.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/79134/33/20239/90240/62dfb4f5Eca789c8c/0a2de120280a87a3.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/107647/27/24636/42260/62dfb4f4Ef69882c2/ac2eea72d5d6b14d.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/146588/12/29306/59127/62dfb4f4E02f1550e/184674244457d2e2.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/117405/18/28522/41334/62dfb4f4Ee5f7ea07/ac14b681557d5acf.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/43799/27/17866/92814/62dfb4f5Ebe98d1e1/67a6a71c34cf2d50.jpg\" alt=\"\"/></p>', '1664004932', '1664004932', '3060', '3225', '0', '1', '100000', '1', '480.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('12', '戴森(Dyson) 新一代吹风机 Dyson Supersonic 电吹风 负离子 进口家用 礼物推荐 HD08 中国红', '1664005081532735', '/uploads/20220924/cf407d648cf38981f54eeb1482929f59.avif', '0.00', '3590.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/223623/23/3050/14485/61dfdbe4E2c63724e/d1b20f64588a69fc.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/136979/19/20722/83113/620a24afE1bfa5cc7/f6d72a3dce791274.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/169020/30/23853/84403/618b7956Ec3276099/41e7833cc684c19d.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/220047/30/12584/196169/620de3c4E91819b56/d3aa7049dc9b000a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/161562/1/23620/199686/618b7958E9f873934/7334cc0d997b97e5.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/216067/17/3929/423670/618b7959E5d5b5050/16c441ee929cc4a3.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/200870/32/15216/425479/618b795aE1cdf4077/eb504aace2065a33.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/197698/29/16641/226627/618b7958Ed1c914ab/4aa89e8603eefca8.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/156024/19/24242/493318/618b795aE238498b3/dc41d5d988aeac99.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/162375/38/25194/238028/618b7958E861acea1/b860db095960cf2f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/213378/16/3883/142615/618b7957Eda10c832/589215bc4ffbbdfa.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/208680/17/8849/259768/618b7959Ebe7d986e/a1dfe12d8015ca1f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/219790/8/3807/127624/618b7956Ef2317134/545ab7f280a15684.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/161799/28/23921/225180/618b7958Ef0a51a7b/824907eee4d80b9a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/202229/30/14855/321473/618b7959E784dba2e/6829d703d9cc384e.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/6038/7/21000/190948/618b7958Ee824d1c0/dfc04bf2b8da2c6a.jpg\" alt=\"\"/></p>', '1664005081', '1664005081', '1830', '9949', '0', '6', '100000', '1', '3290.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('13', ' 海马牌 CRT-SPS', '1664005197986817', '/uploads/20220924/e181254e8fd80a99e53a7b6b126886c6.png', '0.00', '3000.00', '1', '<p><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/99292/28/20103/113710/630dc78dE21bffbe7/7a12f2561de3dea2.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/60482/9/21114/120962/630dc7a2Ed0d3e262/0a815a4c5fb81bcf.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/91770/26/10169/140193/5e16d267E1e43537a/f554cd74c2a7bcbc.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/92397/5/10146/119204/5e16d267Ef1b67238/d3f62ea036a7954a.jpg\" alt=\"\"/></p>', '1664005197', '1664005197', '2090', '4206', '0', '1', '100000', '4', '2890.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('14', '红袖单排扣针织开衫冬季2022新款女装亮丝落肩圆领毛衣', '1664005280126408', '/uploads/20220924/b868eea6bd660f7274f6bbeece19753d.avif', '0.00', '499.00', '1', '<p><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/204034/25/26015/222832/630eb72fE5beaad8b/657e59e13dbf442b.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/34990/6/17755/6431/630eb72fEad32d0f1/0a5fc845e2313b27.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/44004/7/18615/28264/630eb730Ef300afe7/6b93214377ef9668.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/129134/30/30482/1468/630eb730E5181d822/ba58e58d975ab2fa.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/185739/31/27343/16101/630eb730Ecb4cf137/070604a9225a46b8.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/108214/33/32117/1468/630eb731E5ee44e56/1f272aa88ec2b811.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/213925/18/20965/17487/630eb731E89cf6aa9/1925808c414d4c3d.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/194091/7/28219/1468/630eb732E7cf85c25/b02ae2cea07f7ad8.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/64605/5/22271/83905/630eb733Ed505d8d0/cbd0cb89a05a1813.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/223957/32/12439/124250/630eb734Ece95d23b/7dcbb9263ebb5450.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/33525/4/16265/120024/630eb735E0e35b1bc/f0fb77d18d0b2dcd.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/196168/33/28186/77935/630eb736E8aaf7a76/e91076d898791fe1.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/105262/2/30797/22015/630eb737E0eab9cfb/cf0490886331fe39.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/126596/32/31055/798/630eb737E771fa355/500a5ea2f8c2e3bb.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/26333/14/19118/15942/630eb737Ef80b2962/026dda6a5dea8b36.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/119346/33/30283/842/630eb738E3ba1f57c/b571447d68e76ac0.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/165332/11/28575/21184/630eb739Ebab1d8da/167eef21f7671f75.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/176475/8/27709/873/630eb739Ebf773e45/bc9b56178c22b3a3.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/132594/13/29127/27191/630eb73aE7346d5b5/17f7e9edb32562b3.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/64543/24/21990/866/630eb73aE50b65283/e57a92c78fe943db.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/202739/26/26162/13534/630eb73bE0f34cf38/0d18dc5bba6b000d.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/209144/1/25859/899/630eb73bE1ab27eb1/60a5a9b850efc849.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/44422/1/22631/17452/630eb73cE3f7e30df/26d30e803dc4c782.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/114757/35/29993/910/630eb73cE82c11722/6ec6a2b405d6410a.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/158751/20/30186/78675/630eb73dE764b2225/476d6784e43905ab.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/42179/18/17923/5793/630eb73dE14924629/dd90d95229af14ad.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/104367/23/31253/1688/630eb73dE858df4ed/d8bdc797a74192a7.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/45563/39/20274/1371/630eb73eE45da4faf/2e9dda85161eeb99.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/80867/14/21376/1806/630eb73eEa65a2529/a2d0bc0c1c1afd5a.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/133227/30/24496/1293/630eb73eE1f5dda12/fa45ed8e70f8d2f3.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/120236/32/25918/1471/630eb73fE868029bd/10e76bc31a1f0867.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/113834/37/29678/3482/630eb73fEb37e3bdb/70cfbb94712a8bb6.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/221584/33/20830/19414/630eb740Ec2c99bff/132675447b70bbef.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/211295/20/26005/23538/630eb740E020d198e/b9db5bfdd5b232ff.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/139768/3/28673/24149/630eb741E53bd9e88/0950ff86a08d84a7.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/119659/25/29481/17018/630eb741Ebc73fd1d/8c844373d6656096.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/185673/14/28354/17729/630eb742E77050eae/2ef1ea7484f10fd5.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/140018/18/29079/1335/630eb742Ec27db871/142d2518ef5c07a7.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/14483/25/18996/3259/630eb743E6dd10b04/5b1127ed4f4155b3.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/149408/28/29595/73903/630eb744Eb0f7cc28/16576bfa9de6fe8f.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/188330/9/28287/157960/630eb745E4c8ad93d/30eb9d672dd5d1ab.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/203923/5/25864/120382/630eb746Ecdfb2dee/fffaa2166af23fb1.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/209535/4/24885/102939/630eb747E814d131d/3e97367bf1251ca5.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/178098/8/28032/117956/630eb748Edf3c0558/b5537922dcf62fba.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/25797/18/18987/124090/630eb749E18ae1a86/20dfded0655eaf7c.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/211721/6/21179/140200/630eb74aEf6edbb6c/cd742debc1ffc8e3.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/45493/12/19447/110316/630eb74bEdfd2c82f/f42c00389598c458.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/176077/11/28993/105760/630eb74cEf8d1d0a2/989d198be42ec0ac.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/38850/6/18034/142314/630eb74dE387a85ad/25d31e13ed4fe4fb.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/176529/40/29071/140469/630eb74eE1a3458d0/6b1cc4ded3878470.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/189440/30/29092/105728/630eb74fE77cc6108/69de2321fa2145f6.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/44620/32/20582/194718/630eb751E0415d8a1/b229b364698a3404.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/153407/20/25121/4968/630eb751E3a2de12e/bab507182ca92f8f.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/61795/6/21789/8808/630eb751Eaacd64cb/bf8e8214df36955b.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/161495/17/30162/19084/630eb752Eb5c61358/39520ab2f1cfa684.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/205343/37/26272/9194/630eb752E90ef3ee0/49881e3f231fb461.png\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/107729/39/14729/216592/630eb753Ede2788ce/6c65966bbd62a8f4.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/48103/12/21461/277856/630eb756Ee58fb248/9e31c6d8c408b7fb.jpg\" alt=\"\"/></p>', '1664005280', '1664005280', '4287', '7846', '0', '7', '100000', '1', '369.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('15', '幻狐斗柜轻奢五斗橱家用储物柜', '1664005372778587', '/uploads/20220924/423556af8e5e757af4113814ee24960d.png', '0.00', '400.00', '1', '<p><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/187809/32/22553/186197/623fd390E0a9242ce/fe9470a63a444b8e.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/88872/5/25592/130762/623fd390E74826129/1cd367125435b63f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/96694/3/26226/143267/623fd390Ea2ee545e/8b01f0aa4578f174.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/134362/18/27195/75454/623fd391Ec07399de/0a31e8c4aff5de38.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/223552/31/8371/143311/623fd391E2ecc6550/f8bcbbfb7e2de257.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/133163/28/26778/116568/623fd391E0f7c6d54/5a3e3b62ee5875c9.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/68409/33/17484/140876/626f8afcE1e633070/f7feb8e8993ba80d.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/162126/7/27079/167065/623fd391E964d01a7/adbd9e2b7c9d3850.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/129819/8/27245/115754/623fd391Ee317dc30/3a86942673dcc638.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/116495/1/22468/117221/623fd391E5beb0d40/c1e02c47893b404c.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/213436/21/15948/115182/623fd391Ea1a14fa8/b305a29c43689121.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/134569/37/27264/138601/623fd392E2b9cfd97/665cea3c843d9e55.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/182673/38/24870/147892/62ca3f6eEde192d55/c347f849811d0965.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/66181/32/20452/60823/62ca3f52Eae16915a/226c225ff6631406.jpg\" alt=\"\"/></p>', '1664005372', '1664005372', '4106', '8489', '0', '1', '100000', '1', '190.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('16', '安踏运动外套男装长袖2022夏季保暖拉链跑步健身篮球服骑车风衣运动服上衣夹克', '1664005399458641', '/uploads/20220924/e1406a3994440af7c199f3957d233c5f.avif', '0.00', '239.00', '1', '<p><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/206545/18/5482/192762/61681d3fE955f8091/7aa4a9df4eb62c4e.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/218311/3/353/80123/61681d3eEca89f009/d0f2df11d8fb6de3.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/198795/38/13153/112065/61681d3fE9e180c1b/317001eb25ac73a4.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/107853/21/20122/297105/61681d3fE1105cd47/afdbc13ec5bdb774.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/155603/23/21252/205294/61681d40Ea3669bbd/58238c5d270d7a68.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/205972/27/10979/197550/61681d40E79e992a0/f0c70fabfd10c043.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/196862/7/13093/231797/61681d41E759b378c/24fcb1a330c528df.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/216627/16/338/260617/61681d41E8b6956c0/6d7941e66a677ba5.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/199141/2/13212/296657/61681d42E1456bee6/b6fc18a376654a08.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/85283/37/25151/104427/61681d42E7b038da4/567522f134c5a32d.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/219770/27/327/100587/61681d43E4aa2a47a/7ca93d247fcd80fb.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/170416/25/21784/109947/61681d45E3b605efe/225beb01b3fa311b.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/220839/30/318/92622/61681d44E4485ac17/28828311121b2243.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/216573/31/325/119268/61681d44E74f54057/103337583ec4ffc9.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/202712/40/11355/115547/61681d45Ea49b2dee/c684ba5ef423dd4c.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/213883/6/388/83364/61681d45E003de83c/ad4f0e5e03a1ba08.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/200727/32/11772/167575/61681d46E4241df01/6b7e6368dd87a3c4.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/212712/19/337/158677/61681d46Ed105a210/b0f8b321ecd4898e.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/220791/35/340/137306/61681d68Ebf87ac00/e38e2f4060a6021f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/198035/20/13080/93555/61681d68Ea8b869e0/c5a024e36f77ac0d.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/208583/25/5196/183259/61681d68Ea3ea2854/cdbcee72fd592bf4.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/205910/6/9497/62347/61681d69E69405202/c6e8bade6ec7e74e.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/206947/12/5333/179200/61681d69E47ec9f74/3a3be18e78d083f7.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/174024/11/24923/153629/61681d6aEf3242930/4111b79b52de0819.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/207179/1/5273/81626/61681d6aE1cb9b0bc/1776164172cca97b.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/200010/19/13132/75161/61681d6aE5809ac00/e2ce3feb82f84ed7.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/198763/34/13136/126326/61681d6bE9d526a60/d59ff003b42a9c2e.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/210456/2/5080/95488/61681d6bE3e70d798/a9b9800573e6e5f2.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/132186/19/20366/160526/61681d6cE61bca4ce/5252946ccf28e02f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/211104/23/5179/117073/61681d6cEd06a8d7a/103987da1924f3e7.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/214172/16/395/72564/61681d6cE568a36f5/539013daea6ee451.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/219510/35/319/81296/61681d6dEa7130136/ce5800d8d2052775.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/93256/33/12906/246017/5e4e45b5E72605194/2d1831d40249a310.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/102299/13/12996/24229/5e4e45b5E12c50e98/cc495ae3f56da7e2.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/117773/5/3475/177902/5ea7e26aE508a8311/893058dd45b3cd58.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/113258/36/3455/123231/5ea7e26aE2904c64b/9adf5f23f80547bf.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/110577/23/14644/116731/5ea7e26aE82bfb1ff/ba60e534459347c6.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/115984/26/3446/160286/5ea7e26aEefe48e1f/487a016ce4f9b78f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/110053/21/14692/158690/5ea7e26bE56acc5a3/5c6dc068563728c0.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/113772/8/3409/6877/5ea7e286Ed8d026c3/bbb451dbead3aa20.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/103555/12/12815/160726/5e4e45b5Ef2709e32/e44051c95fbaf434.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/84716/11/12782/113226/5e4e45b5E2da025a8/4edb72c5521e2baf.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/96723/10/12636/122895/5e4e45b6Ef3025686/f7b6c190c47662c5.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/101496/13/12754/141982/5e4e45b6Ee425ece6/ff91d6b211ec0256.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/109788/38/6554/6839/5e4e45b8E0afc54cb/1e5c5e6c41c16c6e.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/85647/12/12921/145592/5e4e45b9E58407a6b/17f0630472b2da32.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/107073/30/12603/103932/5e4e45b9E533d216f/d44eee4c8885a1a6.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/97199/15/12738/125412/5e4e45b9Edc00c8a2/4f1fc18230fd2981.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/93280/35/12765/143813/5e4e45b9Efd158cbe/314280f05173e67d.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/104155/28/12790/145672/5e4e45baE9a3d688f/07ac1da20ba267a3.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/80097/35/8596/77548/5d65ea41E784de9fe/a7151d8cf8b344aa.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/80259/22/8414/181661/5d65ea42E5aeb8141/9332f242ba0b8dfb.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/72402/36/8611/153562/5d65ea42E212a7d49/c62b171c47844230.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/54524/15/8822/97232/5d65ea42Ebe7062a1/02428ddea95ac6ae.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/64668/38/8559/69218/5d65ea42Ea9b19cb5/53ef36e30d5c829d.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/61656/21/8440/148799/5d65ea42Edfabc50e/8ad3c07abf341691.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/53459/19/9030/93746/5d65ea42Ebc60c247/89d50c21c0279715.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/57067/33/8971/74803/5d65ea43E25aebbb0/a453edea100e1975.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/81745/4/8402/149735/5d65ea43Eb5dfe59f/bbc0259504e13a5d.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/81526/15/8479/100026/5d65ea43E890bd3db/22d56fb19bb5ddbd.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/83067/13/8528/78405/5d65ea43E9097a783/6295fc081edac781.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/42908/28/13251/116995/5d65ea44E5626cc1d/df93c91c882dd4e5.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/41598/28/13220/118418/5d65ea44Ea3b09f52/b832c5578da3e0e2.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/51224/18/8834/101605/5d65ea44E3af7c833/710d991f2adca763.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/50134/20/9208/96414/5d65ea44E454d0d0e/5963a666a6dbb124.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/78464/1/8582/85961/5d65ea44E9ae7e75c/47739eff1dd3c694.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/79702/23/8484/94253/5d65ea45E64d34791/3426c3d617c0c2ba.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/73170/31/8463/180706/5d65ea47Ede5205e6/90e82d0e8c4ea1f3.jpg\" alt=\"\"/></p>', '1664005399', '1664005422', '1275', '7498', '0', '8', '100000', '4', '209.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('17', '日本进口零食Tirol松尾多彩什锦巧克力', '1664005590156409', '/uploads/20220924/50ee38c7161d9cf31dcae8c10e897c93.png', '0.00', '72.00', '1', '<p><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/208902/22/15304/607381/61d81b38E3c879e22/8d8906a6ca07f10c.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/173913/14/26284/358281/61d81b38E1e778315/75a716a6eb6c8c13.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/121285/35/20462/342842/61d81b38Edde71a7c/98b04bf69c8fc7e4.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/121368/11/20964/574232/61d81b39E8afc0e33/3f79dc0eab36c409.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/221114/38/8515/500494/61d81b3aEfc7f2ccc/2e7245d5b55a8411.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/129004/1/20924/649404/61d81b3aE59c8f968/775a36d0b67c260b.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/212191/12/10384/688920/61d81b3bEe3e9517f/57f67537f0faac54.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/104180/14/20556/582434/61d81b3cE1f50833b/6e9491b3be421595.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/125086/16/21824/459160/61d81b3cEa1d0b6e8/0cf8f4c2765fea3f.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/219573/9/10286/355975/61d81b3cE83f460b6/6b96917e4b65aa4e.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/218931/16/10463/312570/61d81b3dE3c4fccb4/75128f1c255c2c37.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/88429/14/20386/588138/61d81b3eE41f255db/87633853cc155ffb.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/222149/27/8464/285878/61d81b3dE6c429108/c834d33d8e3912b8.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/86063/37/20477/419367/61d81b3eE569b606d/2415da53d7ba3731.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/165292/29/24872/535145/61d81b3fE6590a99e/cec5f4c8c7be0e4d.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/213009/26/10371/474765/61d81b3fE620c3c57/b97ffbbbc53fd421.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/206969/21/14943/266647/61d81b3fE71ca9bc0/68f01ed4419a7ee4.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/219558/34/10318/266113/61d81b40Ecd05b451/e859cb51e3a70679.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/134990/8/21938/274350/61d81b40E8be8e4e1/2bbc751f03ab80a8.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/97833/21/21386/301895/61d81b40Eaa8ff9fa/cada6684b172056a.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/213810/20/10300/616595/61d81b4dE63138611/9d0d22cb1dafeefe.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/167558/2/25544/402243/61d81b4dE8bf925e9/3ea564ed273e9399.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/164400/37/24125/127684/61d81b4dEbe589af6/da6a1309bec54185.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/94728/8/20677/122659/61d81b4dEaf00ca44/8384c06d351641b5.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/107222/20/22243/53417/61d81b4eE0b5db7dd/19b0c251b5f9b3d2.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/219181/32/10400/89269/61d81b4eEc8d7b83c/7f84f7381b6cd157.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/212536/10/10371/106914/61d81b4eE565bfe72/ad9f5a7e2ca54f48.jpg\" alt=\"\"/></p>', '1664005590', '1664005590', '1195', '9625', '0', '5', '100000', '1', '43.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('18', '法芙娜法国原装进口烘焙黑巧克力', '1664005737900434', '/uploads/20220924/82abbd5286be94ae9c422e73eb80337e.png', '0.00', '300.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/153709/36/5755/115424/5fadf580Ece0435d1/ea2ab2e94a35e7e8.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/121748/4/18482/326653/5fadf59dEab9eb9fe/ba6a6c3b34a54f0f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/131015/38/12996/233555/5f8eb434E58273ded/af85f6a64aa44387.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/151002/30/6501/349300/5faceb88E8463267d/c7a89097e8650fb4.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/204902/31/15002/588944/618dfd80E19faade7/6366e356781b4a93.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/125251/38/15623/516852/5f8eb434E3f5c8d8d/1346087149d10764.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/125181/18/15535/399312/5f8eb434E12ecb435/43937a2b9af831ea.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/123018/5/18516/454143/5fadf5b6E1a7cc997/0593e705cebdc3cc.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/141329/5/11438/526249/5f8eb434Ef6a46efd/04c81520462dcf47.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/145309/17/11542/364842/5f8eb434E7af51af0/b43371acbd2b6c84.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/150860/23/3732/400142/5f8eb434E5f85b196/1df1647725e98b92.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/134450/24/13078/441270/5f8eb434E90fe31f8/fd2949f916753865.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/152686/19/2979/523498/5f8eb434Ee55ccef4/d7f2d152b745c9fe.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/150588/12/3653/635990/5f8eb434Eb737e462/9057d42c9e6e0ac8.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/135217/33/12846/181683/5f8eb434E983fcd6a/4cfd5544f570005d.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/153319/29/2884/607631/5f8eb434E87164f35/8fda68be9eed68b9.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/129008/31/15599/116106/5f8eb434Efc41a365/237ccdef262bd1ec.jpg\" alt=\"\"/></p>', '1664005737', '1664005737', '3093', '9041', '0', '5', '100000', '1', '270.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('19', '诺伦（NORNIR）丹麦进口 天然矿泉水', '1664005933651544', '/uploads/20220924/f6596bf95f6c4e25cc2d3e78012dcfbb.png', '0.00', '84.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/205069/26/2559/116476/61233b1dE09fec6cd/ce07a1b81c5d9b6d.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/117592/15/2732/456467/5ea3f429E3c45f2f5/c9bbccdab8c9027e.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/111620/5/2779/400273/5ea3f429E66931efa/4ba2a3785afcd9e0.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/115501/17/2795/459491/5ea3f42aE73ad282c/2e8553542d21553c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/98827/29/17755/108579/5e8c366bEdffdd307/e588a1f9cf29e68c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/117224/25/2810/386020/5ea3f429E5f4de033/8ce9c4470185f6d7.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/109477/34/13856/515882/5ea3f42aE030082e0/4515d213c044fcb3.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/108814/5/13946/324029/5ea3f429Ef305b45c/3c787e2ff28f32dd.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/118327/31/2748/292136/5ea3f429Ecb3e0e9e/aa4e1017b171d486.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/119696/14/1377/192332/5ea3f429Ea961ba81/44b2b893fd92a70c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/213840/29/5300/363014/619b5e50E406b2a0f/0f0c35e1ccff7447.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/141917/25/21791/384928/619b5bf2E7684a21e/c77e1411b14e2d7b.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/119093/6/15589/679096/5f3deff5Efd56cfb7/66444f3949b3b94c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/105457/30/17921/333177/5e8c366bE96943420/a59c97f8949deee4.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/113215/25/2769/475857/5ea3f42aE53018560/0d42dcbe8ca018dc.jpg\" alt=\"\"/></p>', '1664005933', '1684912737', '2524', '5445', '0', '5', '100000', '1', '72.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('20', '2022新款素颜金边斯文败类眼镜', '1664006365693697', '/uploads/20220924/d5809ca55a2fdd1331d1d17c10d01f58.png', '0.00', '730.00', '1', '<p><img src alt=\"\"/><img src=\"http://img12.360buyimg.com/imgzone/jfs/t1/113677/27/26887/182244/62b07b9bE71af3f61/819fd8801d2ec84f.jpg\" alt=\"\"/><img src=\"http://img12.360buyimg.com/imgzone/jfs/t1/40601/20/18669/403939/62b07b9cEd091a849/d35a14c80bcda3b5.jpg\" alt=\"\"/><img src=\"http://img12.360buyimg.com/imgzone/jfs/t1/129123/7/23452/441574/62b07b9cE873eff45/70a71a9fd0bb9d3b.jpg\" alt=\"\"/><img src=\"http://img12.360buyimg.com/imgzone/jfs/t1/181619/11/26106/463068/62b07ba5E1bb95e51/f5cf818ad8dc9344.jpg\" alt=\"\"/><img src=\"http://img12.360buyimg.com/imgzone/jfs/t1/76087/17/19430/308068/62b07bb0E8325c9dc/7a65014f653c5e4a.jpg\" alt=\"\"/><img src alt=\"\"/><img src alt=\"\"/><img src alt=\"\"/><img src alt=\"\"/><img src alt=\"\"/><img src alt=\"\"/><img src alt=\"\"/><img src alt=\"\"/><img src alt=\"\"/></p>', '1664006365', '1684912737', '3993', '1476', '0', '7', '100000', '4', '690.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('21', '潮酷闪电小熊钥匙扣', '1664006573641763', '/uploads/20220924/eb4a1118a6a8f35fc1ee0d8fc426f808.png', '0.00', '39.90', '1', '<p><img src=\"https://img14.360buyimg.com/imgzone/jfs/t1/217397/9/16414/179601/6250ede5Ec8644595/137a2d6595ed63c4.jpg\" alt=\"\"/><img src=\"https://img30.360buyimg.com/imgzone/jfs/t1/97419/6/25539/141491/6250ede6E8be339cc/a3ba8fcbd937107b.jpg\" alt=\"\"/><img src=\"https://img11.360buyimg.com/imgzone/jfs/t1/117900/32/24406/162302/6250ede7Ef39d6e0b/75b4da3daffc4cf1.jpg\" alt=\"\"/><img src=\"https://img20.360buyimg.com/imgzone/jfs/t1/202813/30/20659/179089/6250ede7E8f8c7ec3/82454010099930a3.jpg\" alt=\"\"/><img src=\"https://img13.360buyimg.com/imgzone/jfs/t1/123200/8/27941/233232/6250ede8E9ab5deb2/28fe92c100d0e024.jpg\" alt=\"\"/><img src=\"https://img14.360buyimg.com/imgzone/jfs/t1/163540/19/21840/254719/6250edeaE2ef0f29b/fa57a3d9dbfcf144.jpg\" alt=\"\"/></p>', '1664006573', '1684912737', '1709', '7769', '0', '7', '100000', '1', '29.90', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('22', '故宫文化 紫禁花语挂饰', '1664006712679434', '/uploads/20220924/7578aa3749d40589d720047fa8465892.png', '0.00', '99.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/120652/14/8099/41052/5f1e77bdE616be7eb/a8deff33767dab89.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/125480/23/8074/406682/5f1e775eE3026be85/43754a3f6a2f1ab7.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/114980/9/13313/275135/5f1e775cE71ec9a64/8b0a346b56cb2fed.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/142176/7/3898/114166/5f1e775aE35fb5215/2c298f2350dfbe22.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/128428/22/8130/80273/5f1e7759E977b5b6d/a30066c0d77c2141.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/147472/21/3851/467890/5f1e775eEc0d661d6/df9cf92f67977bda.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/139206/36/4002/392458/5f1e775eE2392a687/8ee9490f6d924ef3.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/141705/9/3873/478875/5f1e775eEa8230cce/1ea3d61a12a4fad6.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/132488/9/5394/346420/5f1e775dE4f594de0/6396a1f61811387f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/141657/21/3952/337395/5f1e775dE4ec5e208/3e0a01eb8a5126c4.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/116353/2/13288/449765/5f1e775eE97c8e7b6/4aabf7fd2623588c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/137186/3/5434/195652/5f1e775bE54f0a84f/e950d6cb053ddad0.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/128122/28/8142/206766/5f1e775bE89ee8fdb/eaf946745d7e2bc9.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/122645/20/8039/131321/5f1e775aE9ec471f5/9eff8a4bb0b3c3b4.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/114055/23/13411/86647/5f1e7759E83f148af/91050e2d113a6d1b.jpg\" alt=\"\"/></p>', '1664006712', '1684912737', '2626', '4714', '0', '7', '100000', '4', '88.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('23', '美利達（MERIDA）名驹 NOVA 21速', '1664007051735595', '/uploads/20220924/02d4d3a5bca7330fa206b56794844670.png', '0.00', '1820.00', '1', '<p><img src=\"//img30.360buyimg.com/imgzone/jfs/t1/205758/20/26153/87010/6309622dE2a3715ce/32e783e861d75b02.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/131130/35/7111/87963/5f37a289Ef88fb24e/79565150a3461ed5.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/120502/16/9789/110611/5f37a289E37ac3f55/5bd1e1995826f69f.png\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/127265/39/9574/99320/5f37a289E1fdf16bd/83c2a41932cf0840.png\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/127880/6/9496/69430/5f37a289E9c725318/4101d41a68c0f2fe.png\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/125261/8/9808/144245/5f37a289E41f88bd0/8c64a9e099df97b7.png\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/133793/4/7093/196577/5f37a289E8357d90b/15dd5ae4135a30ad.png\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/87412/36/27401/134930/625e01c0Efbee56b8/3c8b06669e53aab1.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/144736/7/5442/162912/5f37a289E222d5a1e/90166371d9d1b7e0.png\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/134126/1/21044/26440/625e0c7eE4d3f17fc/4e7378738897e493.png\" alt=\"\"/><img src=\"//img13.360buyimg.com/imgzone/jfs/t1/106905/39/12706/527929/5e4df54cEef5b0846/bb7fac0a68349472.jpg\" alt=\"\"/><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/171555/33/29491/70673/630965ecE67ae17de/d3dd46c7ab258ac0.jpg\" alt=\"\"/><img src=\"//img12.360buyimg.com/imgzone/jfs/t1/77981/26/20197/221311/62b525d6E6a8e5bfd/c0082f32aea9d131.jpg\" alt=\"\"/><img src=\"//img20.360buyimg.com/imgzone/jfs/t1/83427/14/19844/173694/62b525d6E4e99fba0/09bdbfd82d7ff9c9.jpg\" alt=\"\"/></p>', '1664007051', '1684912737', '3931', '1300', '0', '8', '100000', '1', '1599.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('24', '洛克兄弟（ROCKBROS） 骑行服骑行裤', '1664007215235644', '/uploads/20220924/fedaedbca84153d82a082d164b644f06.png', '0.00', '120.00', '1', '<p><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/148679/23/4225/354772/5f2505e7Eba13197f/4c58d3dad63b668a.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/140241/13/4345/224688/5f2505e7Ecbe7ca60/5439615edc0bfa1f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/118736/3/13698/108130/5f2505e7Ea2146543/dfe193e9f2391e2a.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/146240/10/4317/85821/5f2505e8E555ea63a/4b04f5e7c78fbcf9.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/113391/20/13880/138886/5f2505e8Ec0d81381/f7000c6d464662e5.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/139339/25/4163/468974/5f2505e8E0532391c/84eee5f986266730.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/120319/15/8490/163025/5f2505e8E6a2b4a8b/afb96212b9377b54.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/148991/15/4320/227370/5f2505e9E970b8295/bd0bc6dacffba0f3.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/148144/4/4328/143488/5f2505e9Ee3d409dd/113dacb9c5f72b25.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/137924/11/4207/182489/5f2505e9E82ec8f17/71a7e5f0d4321b0f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/127394/11/8417/131132/5f250694E02c75dd5/18755032701beb35.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/113590/26/13636/80245/5f250694Ef74aa4f7/13669f9539ac0b9f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/63995/11/10234/43793/5d7cabbbE98c600ba/6ea46a7ab64deb75.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/76583/4/10341/119967/5d7cabbbEf7216269/72943f6ed6bcc258.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/60574/38/10304/69032/5d7cabbcEec5292f4/310f85ebff81abe5.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/79495/32/10327/91652/5d7cabbcE3c28deba/d4c6819e32be0069.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/79306/8/10167/93126/5d7cabbcEc858d7e1/882c8ba82d770101.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/58480/11/10715/199603/5d7cabbcE0c89dc85/4d62a4133475e0ae.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWaterMark/jfs/t1/61809/11/10279/189862/5d7cabbdEbbf97b9d/05dc2beb4bf730df.jpg\" alt=\"\"/></p>', '1664007215', '1684912737', '4285', '8005', '0', '8', '100000', '4', '99.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('25', '骑兵连Cavalry自行车尾灯', '1664007398908128', '/uploads/20220924/e7fec7a6862036f9e13b6ab4a1c6c4ab.png', '0.00', '42.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/116494/12/14025/234867/5f2648caE54382d1f/0355dcc9ae7cc4b1.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/136667/22/25088/309461/623a7c78Ecffa0590/c6b2fcc0ba4c79d3.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/121078/17/8535/277622/5f2648caE21e1e41c/f3f0ecb72782cdf2.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/135564/35/5884/157497/5f2648caE31e700d0/b7d992e766c39e60.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/147541/32/4357/346606/5f2648caEa60edafa/01e1ce483944e044.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/145727/39/4312/215832/5f2648caE6c662ad3/b1cfb1a345c53cbe.gif\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/129713/11/5920/124028/5f2648caEf74490a9/b3cafd1596f71e8d.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/147320/1/4440/246464/5f2648caE6a1420bc/d61f0f6757d41291.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/125790/7/8607/233499/5f2648caEa14a609d/a9f8e489c8c5636a.jpg\" alt=\"\"/></p>', '1664007398', '1684912737', '4700', '5950', '0', '8', '100000', '1', '34.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('26', '嵌入式电磁炉', '1664007692306029', '/uploads/20220924/2887d3e641268888d5be78165c03190f.png', '0.00', '720.00', '1', '<p><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/180762/18/9614/174526/60cac371E501a78dd/4674c8969e3e3423.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/14029/9/13778/191989/60cac371E3bc1b6f7/bc171ab66824d805.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/184699/7/9504/147821/60cac371Ed308a82b/5f5ae22f50e1dbdd.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/6584/38/14515/160492/60cac371E2ed9bab5/80c53d761e7b7fad.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/177275/15/9577/182117/60cac372E5a94b5ca/586ebc905a70fd0f.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/192795/7/8614/159115/60cac371Efd2adbe7/f0d29d67fbbdcc44.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/176156/10/15073/146011/60cac371E1f63aa47/f0b4c2a5d14cf791.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/177277/11/9506/134514/60cac373E7eac5fda/52534cfef600c859.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/129716/25/19948/197104/60cac373Ec5ac3051/73d5e8867aaa297c.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/178768/36/9652/86240/60cac372Ed628c566/be32286db9865733.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/191509/10/8569/144244/60cac372Eb438efc3/5d8f04a365dec3de.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/191398/24/8669/89930/60cac373E1c9de03b/7423df6e05b6c9ce.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/129758/6/20266/92614/60cac373Ecbf79787/4438fba5df84ba5f.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/184735/31/9713/102259/60cac375E7414ef05/f06514bfb763a1fb.jpg\" alt=\"\"/><img src=\"http://img10.360buyimg.com/imgzone/jfs/t1/175615/28/15249/100630/60cac373E4a03a594/c28cbd66c07f9656.jpg\" alt=\"\"/></p>', '1664007692', '1684912737', '4345', '7555', '0', '3', '100000', '1', '600.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('27', '奥克斯（AUX） 红酒柜', '1664007889970967', '/uploads/20220924/32f990fded344351df47aa98fea59691.png', '0.00', '900.00', '1', '<p><img src=\"//img10.360buyimg.com/imgzone/jfs/t1/160322/35/14641/233336/6055ece3E22db986e/303c211c22a9a0c8.jpg\" alt=\"\"/><img src=\"//img13.360buyimg.com/imgzone/jfs/t1/163509/11/16738/895549/606c6078Eac4ccc44/ac150ebab00e0ee3.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/157822/10/13542/112863/6055f41aE25a144d6/3ccb1d3df80f2495.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/165499/31/13275/169469/6055f41aE0b2d9963/0fa8acf30ce8b4fe.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/168202/9/13867/193012/6055f41bE82e06e36/3e10a031ce532a34.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/165404/31/13750/207386/6055f41bE6ebf138a/b3ceae0d94a23d9a.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/158255/27/14177/218927/6055f41bE91bb95c9/2b021c01a8b9310d.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/157531/19/14483/123254/6055f41cE0e6bdae4/17d7867dd71d0a4d.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/160495/6/15822/108164/6055f41cE8de4c631/9f0fee348e92da4c.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/167296/39/13604/224555/6055f41cE050d707b/8f8013d9e995374f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/168937/16/13962/174875/6055f41cE8fc599f8/c6e6b3ce65f0cee8.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/158750/4/14334/117165/6055f41dE9d004371/e569b68dafd808d6.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/161298/12/13962/104584/6055f41dE698c8fa1/a8beeb405b15271f.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/164340/28/13724/108994/6055f41dEcfd92ed4/30e2eb7c2b3de52a.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/157078/13/13527/233078/6055f41dEecb4b1d0/189a8e229d1e870a.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/155974/7/17118/135429/6055f41dEce41b274/fb665e2a632ac65c.jpg\" alt=\"\"/><img src=\"http://img30.360buyimg.com/popWareDetail/jfs/t1/161682/33/14011/124316/6055f41eE3db14d02/ac91b557403bdb7a.jpg\" alt=\"\"/><img src=\"//img12.360buyimg.com/imgzone/jfs/t1/159841/11/17412/98655/606c60c8E102464e9/56f81f4f98bc7bee.jpg\" alt=\"\"/></p>', '1664007889', '1684912737', '2805', '4330', '0', '3', '100000', '1', '800.00', '0.00', null, '50');
INSERT INTO `lizhili_goods` VALUES ('28', ' 思锐（SIRUI）三脚架', '1664008389681455', '/uploads/20220924/7faa2ff166ad5994f0bcf6aea652493d.png', '0.00', '540.00', '1', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/181074/22/16101/477775/60ffbbfdE1d9dd3e4/b58216bc23d5efbb.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/191087/20/15603/119742/61020efaE0db9a47d/b533141bae5c48c5.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/179856/26/16017/188742/60ffbbfdE8de66163/230f6c9e3742a42a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/184551/10/16120/191039/60ffbbfdE7abcc07f/4e5f36fbe56d24e9.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/195521/4/15224/207622/60ffbbfdE584133cf/21fba2eba10cce1a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/180949/20/16071/255414/60ffbbfdEeb7ffe3c/d95074696a74242a.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/189662/15/15324/204876/60ffbbfdE0c6e2937/992a1e1eeacffe8b.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/198259/8/61/405758/60ffbbfdE4721b969/9004487d9c5613e7.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/199230/39/1539/804780/610b4517E970ee015/58e8deda24e8e4cc.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/188013/40/15308/491372/60ffbbfdEe219c88e/a5fbf5974d9e8b06.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/198735/8/73/198341/60ffbbfdEa33632dc/8eab6141d981446c.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/170595/38/27341/326885/61b83ad6Ed3c3cd2c/f43bfc8b5ea555f6.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/187277/12/15513/285338/61010e29Eb15a6cfc/ea504885f91c1b40.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/176846/28/16418/465353/61010e29E2edadda3/9177fb69858ca095.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/188553/35/15228/203437/60ffbbfdE7a3be551/8b0afdec44137b1f.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/197346/35/1711/249328/610b435dE128bdceb/fe48f147226e5058.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/198957/31/67/386083/60ffbbfdE4a8bd7d4/cce1260c4fddc017.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/182158/34/16286/487844/60ffbbfdEe8cd095b/6204e9f6329e4e88.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/192260/28/15373/397685/60ffbbfdE00135f4f/ca881bf040c6e024.jpg\" alt=\"\"/><img src=\"//img30.360buyimg.com/sku/jfs/t1/201560/11/19097/275771/61b9444aEd512d527/0a89b4019f73bd42.jpg\" alt=\"\"/></p>', '1664008389', '1684912737', '2407', '2596', '0', '4', '100000', '1', '430.00', '10.00', null, '50');

-- ----------------------------
-- Table structure for lizhili_goods_attr
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_goods_attr`;
CREATE TABLE `lizhili_goods_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_goods_attr
-- ----------------------------
INSERT INTO `lizhili_goods_attr` VALUES ('1', '1', '颜色', '红,绿,蓝');
INSERT INTO `lizhili_goods_attr` VALUES ('2', '4', '颜色', '白色,黑色,蓝色');
INSERT INTO `lizhili_goods_attr` VALUES ('3', '7', '原木', '黄色,粉色,蓝色');
INSERT INTO `lizhili_goods_attr` VALUES ('4', '8', '颜色', '白色,黑色,蓝色');
INSERT INTO `lizhili_goods_attr` VALUES ('5', '9', '口味', '经典,原味,心形,树形');
INSERT INTO `lizhili_goods_attr` VALUES ('6', '10', '版本', '单机,大屏,防抖');
INSERT INTO `lizhili_goods_attr` VALUES ('7', '11', '颜色', '灰色,米色');
INSERT INTO `lizhili_goods_attr` VALUES ('8', '12', '颜色', '白色,黑色,蓝色');
INSERT INTO `lizhili_goods_attr` VALUES ('9', '13', '尺码', '120*190*17.9cm,150*190*17.9cm,180*200*17.9cm');
INSERT INTO `lizhili_goods_attr` VALUES ('10', '14', '尺码', 'S,M,L,XL');
INSERT INTO `lizhili_goods_attr` VALUES ('11', '15', '颜色', '暖白色,橡木色');
INSERT INTO `lizhili_goods_attr` VALUES ('12', '16', '颜色', '白色,黑色,蓝色');
INSERT INTO `lizhili_goods_attr` VALUES ('13', '17', '重量', '0.25kg,0.5kg,1kg');
INSERT INTO `lizhili_goods_attr` VALUES ('14', '18', '重量', '1kg');
INSERT INTO `lizhili_goods_attr` VALUES ('15', '19', '容积', '1L,2L');
INSERT INTO `lizhili_goods_attr` VALUES ('16', '20', '颜色', '金色,黑色,银色');
INSERT INTO `lizhili_goods_attr` VALUES ('17', '21', '颜色', '黑色,紫色');
INSERT INTO `lizhili_goods_attr` VALUES ('18', '22', '颜色', '粉色');
INSERT INTO `lizhili_goods_attr` VALUES ('19', '23', '颜色', '蓝色,黑色');
INSERT INTO `lizhili_goods_attr` VALUES ('20', '24', '尺码', 'S,L,XL,XXL,XXXL');
INSERT INTO `lizhili_goods_attr` VALUES ('21', '25', '颜色', '红色');
INSERT INTO `lizhili_goods_attr` VALUES ('22', '26', '颜色', '白色,黑色');
INSERT INTO `lizhili_goods_attr` VALUES ('23', '27', '颜色', '黑色');
INSERT INTO `lizhili_goods_attr` VALUES ('24', '28', '颜色', '黑色');

-- ----------------------------
-- Table structure for lizhili_goods_cate
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_goods_cate`;
CREATE TABLE `lizhili_goods_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `sort` mediumint(6) DEFAULT '50',
  `isopen` tinyint(1) NOT NULL DEFAULT '1',
  `pic` varchar(255) DEFAULT NULL,
  `url` varchar(1500) DEFAULT NULL COMMENT '链接远程地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_goods_cate
-- ----------------------------
INSERT INTO `lizhili_goods_cate` VALUES ('1', '家居百货', null, '50', '1', '/uploads/20220730/ed27376a109bd95dc63af6920bd63050.png', null);
INSERT INTO `lizhili_goods_cate` VALUES ('2', '化妆品', null, '50', '1', '/uploads/20220730/350824897edb1e5392a0ac74d81fccb0.png', null);
INSERT INTO `lizhili_goods_cate` VALUES ('3', '家电', null, '50', '1', '/uploads/20220730/e717dab580793b211296050a6abd16df.png', null);
INSERT INTO `lizhili_goods_cate` VALUES ('4', '数码', null, '50', '1', '/uploads/20220730/d97862498834b68f2f407b6a3e6638d0.png', null);
INSERT INTO `lizhili_goods_cate` VALUES ('5', '3C', null, '50', '1', '/uploads/20230211/559f7c4b908b99bc66246951346a844f.png', null);
INSERT INTO `lizhili_goods_cate` VALUES ('6', '洗护用品', null, '50', '1', null, null);
INSERT INTO `lizhili_goods_cate` VALUES ('7', '服装配饰', null, '50', '1', null, null);
INSERT INTO `lizhili_goods_cate` VALUES ('8', '运动户外', null, '50', '1', null, null);
INSERT INTO `lizhili_goods_cate` VALUES ('10', '拼多多', null, '49', '1', '/uploads/20230524/1e36d61f55c6d98ee1d8dd7e8b626f46.png', null);

-- ----------------------------
-- Table structure for lizhili_gu
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_gu`;
CREATE TABLE `lizhili_gu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00',
  `gufen` tinyint(3) DEFAULT '0',
  `time` int(11) DEFAULT NULL,
  `end_time` int(11) DEFAULT NULL,
  `days` tinyint(6) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_gu
-- ----------------------------
INSERT INTO `lizhili_gu` VALUES ('3', '2', '李志立', '18611111111', '1000.00', '10', '1677072631', '1685712631', '100', '1', '1677072631', '1677072631');
INSERT INTO `lizhili_gu` VALUES ('4', '2', '李志立', '18611111111', '10000.00', '4', '1677072648', '1678023048', '11', '1', '1677072648', '1677072648');
INSERT INTO `lizhili_gu` VALUES ('5', '2', '李志立', '18611111111', '200.00', '1', '1677072818', '1677936818', '10', '1', '1677072818', '1677072818');

-- ----------------------------
-- Table structure for lizhili_huodong
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_huodong`;
CREATE TABLE `lizhili_huodong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `goods_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_huodong
-- ----------------------------
INSERT INTO `lizhili_huodong` VALUES ('1', '天天特价', null, '/huodong/20220924/6558e0f36547e56cf680368780c5dece.png', '1分起', '2');
INSERT INTO `lizhili_huodong` VALUES ('2', '有好料', null, '/huodong/20220924/be93bcf89beb6bb71e8bf63d2e125156.png', '2折', '0');
INSERT INTO `lizhili_huodong` VALUES ('3', '包邮区', null, '/huodong/20220924/6566d79f31718ab4ebd009b007730862.png', '一律包邮', '0');

-- ----------------------------
-- Table structure for lizhili_jobs
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_jobs`;
CREATE TABLE `lizhili_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) NOT NULL,
  `payload` longtext NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='这个用于thinphp 自带的的 消息队列';

-- ----------------------------
-- Records of lizhili_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for lizhili_link
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_link`;
CREATE TABLE `lizhili_link` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `linkurl` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1',
  `pic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_link
-- ----------------------------

-- ----------------------------
-- Table structure for lizhili_log
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_log`;
CREATE TABLE `lizhili_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `ip` char(15) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT '0' COMMENT '是否成功',
  `password` varchar(1000) DEFAULT NULL,
  `is_cheng` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_log
-- ----------------------------
INSERT INTO `lizhili_log` VALUES ('1', 'admin', '106.117.91.121', '1684978979', '1', 'YkdsNmFHbHNhVEV5TXc9PWxpemhpbGk=', '1');

-- ----------------------------
-- Table structure for lizhili_mall_config
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_mall_config`;
CREATE TABLE `lizhili_mall_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '50',
  `time` int(11) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `is_bian` tinyint(1) DEFAULT '0',
  `type` tinyint(1) DEFAULT '1' COMMENT '1是输入框，2是上传图片',
  `kxvalue` varchar(255) DEFAULT NULL,
  `fenlei` varchar(255) NOT NULL COMMENT '分类名字',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_mall_config
-- ----------------------------
INSERT INTO `lizhili_mall_config` VALUES ('1', '微信小程序的appid', 'wx_xcx_appid', '', '1', '1672107960', null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('2', '微信v3支付密钥', 'wx_v3_secret', '', '3', '1672107960', null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('3', '微信商户号', 'wx_merchat_id', '', '3', '1672107960', null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('4', '微信app支付回调', 'wx_app_notify_url', null, '4', null, null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('5', '七牛ak', 'qiniu_ak', null, '5', null, null, '0', '1', '', '静态云存储');
INSERT INTO `lizhili_mall_config` VALUES ('6', '七牛sk', 'qinu_sk', null, '6', null, null, '0', '1', '', '静态云存储');
INSERT INTO `lizhili_mall_config` VALUES ('10', '短信密钥', 'think_secret', '', '7', '1665474773', null, '0', '1', '', '短信配置');
INSERT INTO `lizhili_mall_config` VALUES ('8', 'APP描述', 'app_desc', '一款专业的省钱app', '10', '1675137954', null, '1', '1', '', '信息配置');
INSERT INTO `lizhili_mall_config` VALUES ('9', 'APPlogo', 'app_logo', '/mall_config/20221011/e2a4e5e2617d7de6c933b1d94c6b4046.png', '11', null, null, '1', '2', '', '信息配置');
INSERT INTO `lizhili_mall_config` VALUES ('11', '短信签名', 'think_qian', '', '8', '1665474773', null, '0', '1', '', '短信配置');
INSERT INTO `lizhili_mall_config` VALUES ('12', '短信模版', 'think_moban', '', '9', '1665474773', null, '0', '1', '', '短信配置');
INSERT INTO `lizhili_mall_config` VALUES ('13', 'APP名称', 'app_name', '省钱商城', '12', '1675137954', null, '1', '1', '', '信息配置');
INSERT INTO `lizhili_mall_config` VALUES ('14', '微信开放appid', 'wx_app_appid', null, '3', null, null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('15', '微信开放密钥', 'wx_app_secret', null, '3', null, null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('16', '微信小程序密钥', 'wx_xcx_secret', '', '2', '1672111717', null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('17', '微信小程序支付回调', 'wx_xcx_notify_url', '', '4', '1672110841', null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('18', '公司名称', 'shop_name', '省钱商城网络科技', '13', '1675137954', null, '1', '1', '', '信息配置');
INSERT INTO `lizhili_mall_config` VALUES ('19', '证书序列号', 'wx_zhengshu', '', '4', '1672108098', null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('20', '腾讯地图key', 'txmap_key', '', '6', '1675137954', null, '0', '1', null, '');
INSERT INTO `lizhili_mall_config` VALUES ('21', '广告状态', 'ad_status', '关闭', '50', null, null, '1', '3', '开启,关闭', '广告配置');
INSERT INTO `lizhili_mall_config` VALUES ('22', '小程序订阅模版', 'xcx_dingyue', 'MUChbhOqdYNfy6EXSU6kH_xlE0AF4dMBQq9WBJa-5XU', '50', null, null, '0', '1', '', '小程序配置');
INSERT INTO `lizhili_mall_config` VALUES ('24', '广告id', 'ad_id', null, '50', null, null, '0', '1', '', '广告配置');

-- ----------------------------
-- Table structure for lizhili_mall_config_order
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_mall_config_order`;
CREATE TABLE `lizhili_mall_config_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(60) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '50',
  `time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lizhili_mall_config_order
-- ----------------------------
INSERT INTO `lizhili_mall_config_order` VALUES ('3', 'hehuo_fen', '5', '渠道分成', '50', '1677026596');
INSERT INTO `lizhili_mall_config_order` VALUES ('4', 'fuye_fen', '15', '代理分成', '50', '1677026596');

-- ----------------------------
-- Table structure for lizhili_mall_update
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_mall_update`;
CREATE TABLE `lizhili_mall_update` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_v` varchar(20) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1' COMMENT '是否提示升级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_mall_update
-- ----------------------------

-- ----------------------------
-- Table structure for lizhili_message
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_message`;
CREATE TABLE `lizhili_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `neirong` text,
  `isopen` tinyint(4) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_message
-- ----------------------------

-- ----------------------------
-- Table structure for lizhili_money
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_money`;
CREATE TABLE `lizhili_money` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `money` decimal(10,4) DEFAULT '0.0000',
  `type` tinyint(1) DEFAULT '0' COMMENT '1是团购订单返现，2是提现，3是消费商城分润',
  `time` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lizhili_money
-- ----------------------------

-- ----------------------------
-- Table structure for lizhili_order
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_order`;
CREATE TABLE `lizhili_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_trade_no` char(32) DEFAULT NULL COMMENT '订单号',
  `goods_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `goods_price` varchar(255) DEFAULT '0.00' COMMENT '产品单价',
  `order_num` mediumint(10) DEFAULT '0' COMMENT '订单数量',
  `payment` tinyint(1) DEFAULT '0' COMMENT '支付方式 1:支付宝 2:微信 3:余额，4，积分换购',
  `pay_status` tinyint(1) DEFAULT '0' COMMENT '支付状态，1是支付了，0是未支付',
  `order_type` tinyint(1) DEFAULT '0' COMMENT '0,未发货，1已经发货，2，已经收货，3，退换货,4已经删除',
  `order_status` tinyint(1) DEFAULT '0' COMMENT '订单状态: 0:未确认 1:已经确认',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `order_time` varchar(20) DEFAULT NULL,
  `goods_price_zong` decimal(10,2) DEFAULT '0.00' COMMENT '产品总价',
  `isping` tinyint(1) DEFAULT '0',
  `address` varchar(255) DEFAULT NULL,
  `sheng_html` varchar(255) DEFAULT '' COMMENT '地址',
  `phone` varchar(11) DEFAULT NULL,
  `pay_price` decimal(10,2) DEFAULT '0.00' COMMENT '支付的钱',
  `kuaidi_on` varchar(255) DEFAULT NULL COMMENT '快递号',
  `kuaidi_name` varchar(255) DEFAULT NULL COMMENT '快递公司',
  `wx_order_tui` varchar(255) DEFAULT NULL COMMENT '微信退款使用单号',
  `notify_id` varchar(255) DEFAULT NULL COMMENT '支付宝退款使用',
  `trade_no` varchar(255) DEFAULT NULL COMMENT '支付宝退款使用',
  `is_fen` tinyint(1) DEFAULT '0' COMMENT '是否分钱',
  `beizhu` varchar(500) DEFAULT NULL,
  `attr` varchar(255) DEFAULT NULL COMMENT '产品属性',
  `is_tui` tinyint(1) DEFAULT '0' COMMENT '是否退还',
  `postage` decimal(10,2) DEFAULT '0.00' COMMENT '邮费',
  `lei` int(11) DEFAULT '0',
  `tuan_id` int(11) DEFAULT '0' COMMENT '团购地址id',
  `hexiao` tinyint(1) DEFAULT '0' COMMENT '是否核销',
  `hexiao_time` int(11) DEFAULT NULL COMMENT '核销时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_order
-- ----------------------------
INSERT INTO `lizhili_order` VALUES ('1', 'zong_22167496444576652', '22', '2', '88.00', '1', '2', '0', '0', '0', '1674964445', '1674964445', null, null, '88.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18633456271', '0.00', null, null, null, null, null, '0', '', '颜色: 粉色', '0', '0.00', '4', '0', '0', null);
INSERT INTO `lizhili_order` VALUES ('2', 'zong_2167516276840042', '2', '2', '2.00', '1', '2', '0', '0', '0', '1675162768', '1675162768', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '0', '0', null);
INSERT INTO `lizhili_order` VALUES ('3', 'zong_4167532084489662', '4', '2', '10199.00', '1', '2', '0', '0', '0', '1675320844', '1675320844', null, null, '10199.00', '0', '河北省石家庄市桥西区塔谈大街16号', '塔谈国际', '18611111111', '0.00', null, null, null, null, null, '0', '', '颜色: 白色', '0', '0.00', '4', '0', '0', null);
INSERT INTO `lizhili_order` VALUES ('4', 'zong_5167550317835962', '5', '2', '59.00', '1', '2', '0', '0', '0', '1675503178', '1675503178', null, null, '59.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '0', '0', null);
INSERT INTO `lizhili_order` VALUES ('5', 'zong_24167550325362032', '24', '2', '99.00', '1', '2', '0', '0', '0', '1675503253', '1675503253', null, null, '99.00', '0', '河北省石家庄市桥西区塔谈大街16号', '塔谈国际', '18611111111', '0.00', null, null, null, null, null, '0', '', '尺码: S', '0', '0.00', '4', '0', '0', null);
INSERT INTO `lizhili_order` VALUES ('6', 'zong_4167608635724962', '4', '2', '10199.00', '1', '2', '0', '0', '0', '1676086357', '1676086357', null, null, '10199.00', '0', '河北省石家庄市新华区泰华街133号', '火车站附近小区', '18611111111', '0.00', null, null, null, null, null, '0', '', '颜色: 白色', '0', '0.00', '4', '0', '0', null);
INSERT INTO `lizhili_order` VALUES ('7', 'zong_4167608653364402', '4', '2', '10199.00', '1', '2', '1', '2', '0', '1676086533', '1676086533', null, null, '10199.00', '0', '河北省石家庄市新华区泰华街133号', '火车站附近小区', '18611111111', '100.00', null, null, null, null, null, '0', '', '颜色: 白色', '0', '0.00', '4', '2', '1', '1676099066');
INSERT INTO `lizhili_order` VALUES ('8', 'zong_20167609959920572', '20', '2', '690.00', '1', '2', '0', '0', '0', '1676099599', '1676099599', null, null, '690.00', '0', '河北省石家庄市新华区泰华街133号', '火车站附近小区', '18611111111', '0.00', null, null, null, null, null, '0', '', '颜色: 金色', '0', '0.00', '4', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('9', 'zong_2167677783939802', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777839', '1676777839', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('10', 'zong_2167677784393292', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777843', '1676777843', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('11', 'zong_2167677784632832', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777846', '1676777846', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('12', 'zong_2167677784785142', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777847', '1676777847', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('13', 'zong_2167677784884972', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777848', '1676777848', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('14', 'zong_2167677784952382', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777849', '1676777849', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('15', 'zong_2167677785034362', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777850', '1676777850', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('16', 'zong_2167677785199492', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777851', '1676777851', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('17', 'zong_2167677785630242', '2', '2', '2.00', '1', '2', '0', '0', '0', '1676777856', '1676777856', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('18', 'zong_2167711563860445', '2', '5', '2.00', '1', '2', '0', '0', '0', '1677115638', '1677115638', null, null, '2.00', '0', '石家庄市桥西区海悦天地B座913室', '河北省,石家庄市,桥西区', '17610395777', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '0', '0', null);
INSERT INTO `lizhili_order` VALUES ('19', 'zong_2167775099136482', '2', '2', '2.00', '1', '2', '0', '0', '0', '1677750991', '1677750991', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('20', 'zong_2167775125065502', '2', '2', '2.00', '1', '2', '0', '0', '0', '1677751250', '1677751250', null, null, '2.00', '0', '红旗大街333号', '河北省,石家庄市,长安区', '18611111111', '0.00', null, null, null, null, null, '0', '', '', '0', '0.00', '1', '2', '0', null);
INSERT INTO `lizhili_order` VALUES ('21', 'zong_15168111597892128', '15', '8', '190.00', '1', '2', '0', '0', '0', '1681115978', '1681115978', null, null, '190.00', '0', '红旗大街', '河北省,石家庄市,桥西区', '15830880039', '0.00', null, null, null, null, null, '0', '', '颜色: 橡木色', '0', '0.00', '1', '0', '0', null);

-- ----------------------------
-- Table structure for lizhili_order_fen
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_order_fen`;
CREATE TABLE `lizhili_order_fen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_trade_no` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `to_uid` varchar(255) DEFAULT NULL,
  `bili` smallint(6) DEFAULT '0',
  `money` decimal(10,4) DEFAULT '0.0000',
  `time` int(11) DEFAULT NULL,
  `is_wan` tinyint(1) DEFAULT '0',
  `wx_order_tui` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lizhili_order_fen
-- ----------------------------
INSERT INTO `lizhili_order_fen` VALUES ('1', '1673256014946752', 'zong_31167325600071582', '1546030541', '10', '0.0010', '1673256014', '0', '4200001675202301097584350209', null);
INSERT INTO `lizhili_order_fen` VALUES ('2', '1673256131970002', 'zong_31167325612057302', '1546030541', '10', '0.0010', '1673256131', '0', '4200001680202301091256814813', null);
INSERT INTO `lizhili_order_fen` VALUES ('3', '1673256718397382', 'zong_31167325670570862', '1546030541', '10', '0.0010', '1673256718', '0', '4200001669202301091490131049', null);
INSERT INTO `lizhili_order_fen` VALUES ('4', '1673257927774768', 'zong_31167323786035928', '1546030541', '10', '0.0010', '1673257927', '0', '4200001653202301092454851451', null);
INSERT INTO `lizhili_order_fen` VALUES ('5', '16732584208742713', 'zong_311673238362383313', '1546030541', '10', '0.0010', '1673258420', '0', '4200001680202301093620293048', null);

-- ----------------------------
-- Table structure for lizhili_pilot_list
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_pilot_list`;
CREATE TABLE `lizhili_pilot_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` smallint(6) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `fid` int(11) DEFAULT '0',
  `icon` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1',
  `pn_id` int(11) DEFAULT '1' COMMENT '头部导航',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COMMENT='后台侧面导航';

-- ----------------------------
-- Records of lizhili_pilot_list
-- ----------------------------
INSERT INTO `lizhili_pilot_list` VALUES ('1', '1', '栏目管理', '0', '&#xe681;', '没有地址', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('2', '2', '会员管理', '0', '&#xe60d;', '没有地址', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('3', '3', '管理员管理', '0', '&#xe653;', null, '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('4', '4', '系统管理', '0', '&#xe62e;', null, '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('5', '6', '登陆日志', '0', '&#xe645;', 'login/log', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('6', '7', '数据库管理', '0', '&#xe639;', 'sql/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('8', '1', '资讯管理', '0', '&#xe616;', null, '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('9', '10', '友情链接', '0', '&#xe6f1;', null, '0', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('10', '11', '图片管理', '0', '&#xe613;', null, '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('12', '13', '反馈留言', '0', '&#xe68a;', null, '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('13', '0', '栏目管理', '1', '', 'cate/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('14', '0', '会员列表', '2', '', 'user/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('16', '0', '角色管理', '3', '', 'auth_group/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('17', '0', '权限管理', '3', '', 'auth_rule/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('18', '0', '管理员列表', '3', null, 'admin/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('19', '0', '设置管理', '4', null, 'system/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('20', '0', '系统设置', '4', null, 'system/show', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('21', '0', '常用配置', '4', null, 'system/config', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('22', '0', '屏蔽词', '4', null, 'system/shield', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('23', '0', '顶部导航', '31', null, 'pilot/nav', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('24', '8', '清除缓存', '0', '&#xe60b;', 'admin/cahe', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('25', '0', '资讯管理', '8', null, 'article/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('26', '0', '链接管理', '9', null, 'link/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('27', '0', '幻灯片管理', '10', null, 'slide/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('28', '0', '广告管理', '10', null, 'advertisement/index', '0', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('30', '0', '反馈留言', '12', null, 'message/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('31', '5', '导航设置', '0', '&#xe6b6;', null, '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('32', '0', '侧面导航', '31', null, 'pilot/lit', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('44', '0', '说明管理', '31', '', 'pilot/cms', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('50', '5', 'SEO优化', '0', '&#xe72b;', '没有地址', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('51', '0', '生成地图', '50', '', 'map/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('52', '2', '下载管理', '0', '&#xe641;', '', '0', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('53', '0', '下载管理', '52', '', 'download/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('54', '0', '添加demo', '31', '', 'pilot/demo', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('57', '5', '说明管理', '0', '&#xe6b3;', '', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('58', '0', '说明列表', '57', '', 'shuoming/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('59', '0', '图片压缩', '4', '', 'system/ya', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('60', '0', '网站统计', '4', '', 'system/tongji', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('61', '0', '商品管理', '0', '&#xe720;', '', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('62', '0', '添加商品', '61', '', 'goods/add', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('63', '0', '商品列表', '61', '', 'goods/index', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('64', '0', '商品分类', '61', '', 'goods_cate/index', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('65', '0', '订单管理', '0', '&#xe64b;', '', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('66', '0', '订单列表', '65', '', 'order/index', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('67', '0', '活动管理', '0', '&#xe6ce;', '', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('68', '0', '活动列表', '67', '', 'huodong/index', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('69', '100', '商城配置', '0', '&#xe62e;', '', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('70', '0', '常规配置', '69', '', 'mall_config/index', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('71', '0', 'app升级包', '69', '', 'mall_update/index', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('72', '0', '提现管理', '0', '&#xe727;', '', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('73', '0', '提现列表', '72', '', 'ti/index', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('74', '0', '分账设置', '69', '', 'mall_config/order', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('76', '0', '团购管理', '0', '&#xe667;', '没有地址', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('77', '0', '团购列表', '76', '', 'tuan/index', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('78', '0', '股份管理', '0', '&#xe647;', '', '1', '4');
INSERT INTO `lizhili_pilot_list` VALUES ('79', '0', '股份列表', '78', '', 'gu/index', '1', '4');

-- ----------------------------
-- Table structure for lizhili_pilot_nav
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_pilot_nav`;
CREATE TABLE `lizhili_pilot_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` smallint(6) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='后台头部导航';

-- ----------------------------
-- Records of lizhili_pilot_nav
-- ----------------------------
INSERT INTO `lizhili_pilot_nav` VALUES ('1', '0', '网站配置', '1');
INSERT INTO `lizhili_pilot_nav` VALUES ('2', '0', '内容管理', '1');
INSERT INTO `lizhili_pilot_nav` VALUES ('4', '0', '商城管理', '1');

-- ----------------------------
-- Table structure for lizhili_shield
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_shield`;
CREATE TABLE `lizhili_shield` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shield` text,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_shield
-- ----------------------------
INSERT INTO `lizhili_shield` VALUES ('1', '她妈|它妈|他妈|你妈|去死|贱人|1090tv|10jil|21世纪中国基金会|2c8|3p|4kkasi|64惨案|64惨剧|64学生运动|64运动|64运动民國|89惨案|89惨剧|89学生运动|89运动|adult|asiangirl|avxiu|av女|awoodong|A片|bbagoori|bbagury|bdsm|binya|bitch|bozy|bunsec|bunsek|byuntae|B样|fa轮|fuck|ＦｕｃΚ|gay|hrichina|jiangzemin|j女|kgirls|kmovie|lihongzhi|MAKELOVE|NND|nude|petish|playbog|playboy|playbozi|pleybog|pleyboy|q奸|realxx|s2x|sex|shit|sorasex|tmb|TMD|tm的|tongxinglian|triangleboy|UltraSurf|unixbox|ustibet|voa|admin|lizhili|manage', null, null);

-- ----------------------------
-- Table structure for lizhili_shuoming
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_shuoming`;
CREATE TABLE `lizhili_shuoming` (
  `id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `keyword` varchar(10) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `pic` varchar(160) DEFAULT NULL,
  `text` text,
  `state` smallint(6) unsigned DEFAULT '0',
  `click` mediumint(9) DEFAULT '0',
  `zan` mediumint(9) DEFAULT '0',
  `time` int(10) DEFAULT NULL,
  `faid` int(11) DEFAULT '0' COMMENT '发布者id',
  `laiyuan` varchar(255) DEFAULT NULL,
  `click_wai` mediumint(9) DEFAULT '0' COMMENT '展示数据',
  `isopen` tinyint(1) DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_shuoming
-- ----------------------------
INSERT INTO `lizhili_shuoming` VALUES ('3', '隐私政策', '', '商城隐私政策更新日期：【2021】年【4】月【27】日生效日期：【2021】年【4】月【27】日本应用尊重并保护所有使用服务用户的个人隐私权。为了给您提供更准确', '', null, '<p>商城隐私政策</p><p>更新日期：【2021】年【4】月【27】日生效日期：【2021】年【4】月【27】日</p><p>本应用尊重并保护所有使用服务用户的个人隐私权。为了给您提供更准确、更有个性化的服务，本应用会按照本隐私权政策的规定使用和披露您的个人信息。但本应用将以高度的勤勉、审慎义务对待这些信息。除本隐私权政策另有规定外，在未征得您事先许可的情况下，本应用不会将这些信息对外披露或向第三方提供。本应用会不时更新本隐私权政策。 您在同意本应用服务使用协议之时，即视为您已经同意本隐私权政策全部内容。本隐私权政策属于本应用服务使用协议不可分割的一部分。我们将逐一说明我们收集的您的个人信息类型及其对应的用途，以便您了解我们针对某一特定功能所收集的具体个人信息的类别、使用理由及收集方式。</p><p>1.权限的获取及用途</p><p>（1）获取相机权限，用来保存分享时所生成</p><p>（2）拉起应用权限，用来进行微信登录；</p><p>（3）获取通知权限，用来进行消息推送。</p><p>（4）使用网络权限，用来连接网络</p><p>（5）获取文件权限，用来保存应用数据</p><p>（6）获取屏幕权限，保持亮屏</p><p>2. 信息使用</p><p>信息使用</p><p>(a)本应用不会向任何无关第三方提供、出售、出租、分享或交易您的个人信息，除非事先得到您的许可，或该第三方和本应用（含本应用关联公司）单独或共同为您提供服务，且在该服务结束后，其将被禁止访问包括其以前能够访问的所有这些资料。</p><p>(b) 本应用亦不允许任何第三方以任何手段收集、编辑、出售或者无偿传播您的个人信息。任何本应用平台用户如从事上述活动，一经发现，本应用有权立即终止与该用户的服务协议。</p><p>(c)您注册商城帐号时，我们会收集您的昵称、头像、手机号码。收集这些信息是为了帮助您完成商城帐号的注册，保护您的帐号安全。手机号码属于敏感信息，收集此类信息是为了满足相关法律、法规关于网络实名制要求。若您不提供这类信息，您可能无法正常使用我们的服务。您还可以根据自身需求选择填写性别、地区等信息。</p><p>另外，根据相关法律法规及国家标准，以下情形中，我们可能会收集、使用您的相关个人信息无需征求您的授权同意:</p><p>1) 与国家安全、国防安全等国家利益直接相关的；与公共安全、公共卫生、公众知情等重大公共利益直接相关的；</p><p>2) 与犯罪侦查、起诉、审判和判决执行等直接相关的；</p><p>3) 出于维护您或其他个人的生命、财产、声誉等重大合法权益但又很难得到本人同意的；</p><p>4) 所收集的个人信息是您自行向社会公众公开的；</p><p>5) 从合法公开披露的信息中收集个人信息的，如合法的新闻报道、政府信息公开等渠道；</p><p>6) 根据您要求签订和履行合同所必需的；</p><p>7) 用于维护所提供的产品或服务的安全稳定运行所必需的，例如发现、处置产品或服务的故障；</p><p>8) 为开展合法的新闻报道所必需的；</p><p>9) 出于公共利益开展统计或学术研究所必要，且其对外提供学术研究或描述的结果时，对结果中所包含的个人信息进行去标识化处理的；v</p><p>10) 法律法规规定的其他情形。</p><p>请您理解，我们向您提供的功能和服务是不断更新和发展的，如果某一功能或服务未在前述说明中且收集了您的信息，我们会通过页面提示、交互流程、网站公告等方式另行向您说明信息收集的内容、范围和目的，以征得您的同意。</p><p>信息披露</p><p>在如下情况下，本应用将依据您的个人意愿或法律的规定全部或部分的披露您的个人信息：</p><p>(a) 经您事先同意，向第三方披露；</p><p>(b)为提供您所要求的产品和服务，而必须和第三方分享您的个人信息；</p><p>© 根据法律的有关规定，或者行政或司法机构的要求，向第三方或者行政、司法机构披露；</p><p>(d) 如您出现违反中国有关法律、法规或者本应用服务协议或相关规则的情况，需要向第三方披露；</p><p>(e) 如您是适格的知识产权投诉人并已提起投诉，应被投诉人要求，向被投诉人披露，以便双方处理可能的权利纠纷；</p><p>(f) 在本应用平台上创建的某一交易中，如交易任何一方履行或部分履行了交易义务并提出信息披露请求的，本应用有权决定向该用户提供其交易对方的联络方式等必要信息，以促成交易的完成或纠纷的解决。</p><p>(g) 其它本应用根据法律、法规或者网站政策认为合适的披露。</p><p>信息存储和交换</p><p>我们会按照法律法规规定，将境内收集的用户个人信息存储于中国境内。</p><p>目前我们不会跨境传输或存储您的个人信息。将来如需跨境传输或存储的，我们会向您告知信息出境的目的、接收方、安全保证措施和安全风险，并征得您的同意。</p><p>手机号码：若您需要使用商城服务，我们需要一直保存您的手机号码，以保证您正常使用该服务，当您注销商城帐户后，我们将删除相应的信息；</p><p>图片：当您使用分享功能发布了生成了海报，我们需要保存您上述信息及相关评论，以保证您正常使用；当您主动删除上述信息后，我们将删除相应的信息。 当我们的产品或服务发生停止运营的情形时，我们将以推送通知、公告等形式通知您，并在合理的期限内删除您的个人信息或进行匿名化处理</p><p>Cookie的使用</p><p>(a) 在您未拒绝接受cookies的情况下，本应用会在您的计算机上设定或取用cookies ，以便您能登录或使用依赖于cookies的本应用平台服务或功能。本应用使用cookies可为您提供更加周到的个性化服务，包括推广服务。</p><p>(b) 您有权选择接受或拒绝接受cookies。您可以通过修改浏览器设置的方式拒绝接受cookies。但如果您选择拒绝接受cookies，则您可能无法登录或使用依赖于cookies的本应用网络服务或功能。</p><p>© 通过本应用所设cookies所取得的有关信息，将适用本政策。</p><p>信息安全</p><p>安全事件处置措施</p><p>若发生个人信息泄露、损毁、丢失等安全事件，我们会启动应急预案，阻止安全事件扩大。安全事件发生后，我们会及时以推送通知、邮件等形式告知您安全事件的基本情况、我们即将或已经采取的处置措施和补救措施，以及我们对您的应对建议。如果难以实现逐一告知，我们将通过公告等方式发布警示。</p><p>我们如何使用信息</p><p>我们严格遵守法律法规的规定以及与用户的约定，按照本隐私保护指引所述使用收集的信息，以向您提供更为优质的服务。</p><p>信息使用规则</p><p>我们会按照如下规则使用收集的信息：</p><p>1） 为了保证帐号安全，我们会使用您的地理位置信息进行安全检测，以便为您提供更安全的帐号注册服务；</p><p>2） 我们会根据您使用商城的频率、故障信息、性能信息等分析我们产品的运行情况，以确保服务的安全性并优化我们的产品，提高我们的服务质量。我们不会将我们存储在分析软件中的信息与您提供的个人身份信息相结合。</p><p>5）告知变动目的后征得同意的方式</p><p>我们将会在本隐私保护指引所涵盖的用途内使用收集的信息。如我们使用您的个人信息，超出了与收集时所声称的目的及具有直接或合理关联的范围，我们将在使用您的个人信息前，再次向您告知并征得您的明示同意。</p><p>7.本隐私政策的更改</p><p>我们可能会适时对本指引进行修订。当指引的条款发生变更时，我们会在您登录及版本更新时以弹窗的形式向您展示变更后的指引。请您注意，只有在您点击弹窗中的同意按钮后，我们才会按照更新后的指引收集、使用、存储您的个人信息。(b)本公司保留随时修改本政策的权利，因此请经常查看。如对本政策作出重大更改，本公司会通过网站通知的形式告知。</p><p>请您妥善保护自己的个人信息，仅在必要的情形下向他人提供。如您发现自己的个人信息泄密，尤其是本应用用户名及密码发生泄露，请您立即联络本应用客服，以便本应用采取相应措施。</p><p>如您对本政策或其他相关事宜有疑问，请通过 官方客服联系我们！</p>', '0', '0', '0', '1642671743', '1', '', '1968', '1', '');
INSERT INTO `lizhili_shuoming` VALUES ('4', '服务协议', '', '在此特别提醒您在注册成为用户之前，请认真阅读本《服务协议》（以下简称“协议”），确保您充分理解本协议中的各项条款。请您谨慎阅读并选择接受或不接受本协议。除非您接', '', null, '<p>&nbsp;在此特别提醒您在注册成为用户之前，请认真阅读本《服务协议》（以下简称“协议”），确保您充分理解本协议中的各项条款。请您谨慎阅读并选择接受或不接受本协议。除非您接受本协议所有条款a否则您无权注册、登录或使用本协议所涉服务。您的注册、登录、使用等行为将视为对本协议的接受，并同意接受本协议各项条款的约束。本协议约定我方与用户之间关于App软件服务（以下简称“服务”）的权利义务。用户，是指注册、登录、使用本服务的个人或组织。本协议可由我方随时更新，更新后的协议条款一旦公布即代替原来的协议条款，恕不再另行通知，用户可在本App中查阅最新版协议条款。在修改协议条款后，如果用户不接受修改后的条款，请立即停止使用我方提供的服务，用户继续使用我方提供的服务将被视为接受修改后的协议。</p><p>一、账号注册</p><p>1、用户在使用本服务前需要注册一个App账号。本App账号应当使用手机号码、邮箱绑定注册，请用户使用尚未与本App账号绑定的手机号码，以及未被我方协议封禁的手机号码注册本App账号。我方可以根据用户需求或产品需要对账号注册和绑定的方式进行变更，而无须事先通知用户。</p><p>2、鉴于本App账号的绑定注册方式，您同意在注册本App时将允许您的手机号码及手机设备识别码等信息用于注册。</p><p>3、在用户注册及使用本服务时，我方需要搜集能识别用户身份的个人信息以便我方可以在必要时联系用户，或为用户提供更好的使用体验。我方搜集的信息包括但不限于用户的姓名、地址；我方同意对这些信息的使用将受限于第三条用户个人隐私信息保护的约束。</p><p>二、用户个人隐私信息保护</p><p>1、如果我方发现或收到他人举报或投诉用户违反本协议约定的，我方有权不经通知随时对相关内容，包括但不限于用户资料、发布记录进行审查、删除，并视情节轻重对违规账号处以包括但不限于警告、账号封禁、设备封禁、功能封禁的处罚，且通知用户处理结果。</p><p>2、因违反用户协议被封禁的用户，可以自行与我方联系。其中，被实施功能封禁的用户会在封禁期届满后自动恢复被封禁功能。被封禁用户可提交申诉，我方将对申诉进行审查，并自行合理判断决定是否变更处罚措施。</p><p>3、用户理解并同意，我方有权依合理判断对违反有关法律法规或本协议规定的行为进行处罚，对违法违规的任何用户采取适当的法律行动，并依据法律法规保存有关信息向有关部门报告等，用户应承担由此而产生的一切法律责任。</p><p>4、用户理解并同意，因用户违反本协议约定，导致或产生的任何第三方主张的任何索赔、要求或损失，包括合理的律师费，用户应当赔偿我方与合作公司、关联公司，并使之免受损害。</p><p>三、用户发布内容规范</p><p>1、本条所述内容是指用户使用本App的过程中所制作、上载、复制、发布、传播的任何内容，包括但不限于账号头像、名称、用户说明等注册信息及认证资料，或文字、语音、图片、视频、图文等发送、回复或自动回复消息和相关链接页面，以及其他使用账号或本服务所产生的内容。</p><p>2、用户不得利用本App账号或本服务制作、上传、复制、发布、传播如下法律、法规和政策禁止的内容：</p><p>(1) 反对宪法所确定的基本原则的；</p><p>(2) 危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；</p><p>(3) 损害国家荣誉和利益的；</p><p>(4) 煽动民族仇恨、民族歧视，破坏民族团结的；</p><p>(5) 破坏国家宗教政策，宣扬邪教和封建迷信的；</p><p>(6) 散布谣言，扰乱社会秩序，破坏社会稳定的；</p><p>(7) 散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；</p><p>(8) 侮辱或者诽谤他人，侵害他人合法权益的；</p><p>(9) 含有法律、行政法规禁止的其他内容的信息。</p><p>3、用户不得利用本App账号或本服务制作、上载、复制、发布、传播如下干扰本App正常运营，以及侵犯其他用户或第三方合法权益的内容：</p><p>(1) 含有任何性或性暗示的；</p><p>(2) 含有辱骂、恐吓、威胁内容的；</p><p>(3) 含有骚扰、垃圾广告、恶意信息、诱骗信息的；</p><p>(4) 涉及他人隐私、个人信息或资料的；</p><p>(5) 侵害他人名誉权、肖像权、知识产权、商业秘密等合法权利的；</p><p>(6) 含有其他干扰本服务正常运营和侵犯其他用户或第三方合法权益内容的信息。</p><p>四、使用规则</p><p>1、用户在本服务中或通过本服务所传送、发布的任何内容并不反映或代表，也不得被视为反映或代表我方的观点、立场或政策，我方对此不承担任何责任。</p><p>2、用户不得利用本App账号或本服务进行如下行为：</p><p>(1) 提交、发布虚假信息，或盗用他人头像或资料，冒充、利用他人名义的；</p><p>(2) 强制、诱导其他用户关注、点击链接页面或分享信息的；</p><p>(3) 虚构事实、隐瞒真相以误导、欺骗他人的；</p><p>(4) 利用技术手段批量建立虚假账号的；</p><p>(5) 利用本App账号或本服务从事任何违法犯罪活动的；</p><p>(6) 制作、发布与以上行为相关的方法、工具，或对此类方法、工具进行运营或传播，无论这些行为是否为商业目的；</p><p>(7) 其他违反法律法规规定、侵犯其他用户合法权益、干扰本App正常运营或我方未明示授权的行为。</p><p>3、用户须对利用本App账号或本服务传送信息的真实性、合法性、无害性、准确性、有效性等全权负责，与用户所传播的信息相关的任何法律责任由用户自行承担，与我方无关。如因此给我方或第三方造成损害的，用户应当依法予以赔偿。</p><p>4、我方提供的服务中可能包括广告，用户同意在使用过程中显示我方和第三方供应商、合作伙伴提供的广告。除法律法规明确规定外，用户应自行对依该广告信息进行的交易负责，对用户因依该广告信息进行的交易或前述广告商提供的内容而遭受的损失或损害，我方不承担任何责任。</p><p>五、其他</p><p>1、我方郑重提醒用户注意本协议中免除我方责任和限制用户权利的条款，请用户仔细阅读，自主考虑风险。未成年人应在法定监护人的陪同下阅读本协议。</p><p>2、本协议的效力、解释及纠纷的解决，适用于中华人民共和国法律。若用户和我方之间发生任何纠纷或争议，首先应友好协商解决，协商不成的，用户同意将纠纷或争议提交我方住所地有管辖权的人民法院管辖。</p><p>3、本协议的任何条款无论因何种原因无效或不具可执行性，其余条款仍有效，对双方具有约束力。</p><p>4、本协议最终解释权归我方所有，据苹果公司免责条款特此声明：该App注册及用户协议与苹果公司无关。</p>', '0', '0', '0', '1642671764', '1', '', '1306', '1', '');
INSERT INTO `lizhili_shuoming` VALUES ('5', '联系我们', '', '有任何问题咨询微信客服', '', null, '<p><strong>我的邮箱：</strong></p><p><strong><br/></strong></p><p style=\"text-align: center;\">lizhilimaster@163.com</p><p><br/></p><p><strong>我的微信：（请注明原因）</strong><br/></p><p style=\"text-align: center;\"><img src=\"/ueditor/20230204/1675504568912871.jpg\" title=\"1675504568912871.jpg\" alt=\"微信图片_20230204175552.jpg\"/></p>', '0', '0', '0', '1642671772', '1', '', '1845', '1', '');

-- ----------------------------
-- Table structure for lizhili_slide
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_slide`;
CREATE TABLE `lizhili_slide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `isopen` tinyint(1) DEFAULT '0' COMMENT '1代表启用，0代表不启用',
  `desc` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_slide
-- ----------------------------
INSERT INTO `lizhili_slide` VALUES ('6', '/slide/20220924/67972e8792102dc2e8619d9d939906a3.jpg', '', '4', '0', '1', '', '1658456153', '1663992360');

-- ----------------------------
-- Table structure for lizhili_statistics
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_statistics`;
CREATE TABLE `lizhili_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `ip` int(11) DEFAULT '0',
  `pv` int(11) DEFAULT '0',
  `uv` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lizhili_statistics
-- ----------------------------
INSERT INTO `lizhili_statistics` VALUES ('1', '1684921873', '1', '1', '1');
INSERT INTO `lizhili_statistics` VALUES ('2', '1684921873', '1', '1', '1');
INSERT INTO `lizhili_statistics` VALUES ('3', '1684978967', '5', '14', '1');

-- ----------------------------
-- Table structure for lizhili_statistics_show
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_statistics_show`;
CREATE TABLE `lizhili_statistics_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '方法',
  `controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` int(11) DEFAULT '0' COMMENT '几个文件',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lizhili_statistics_show
-- ----------------------------
INSERT INTO `lizhili_statistics_show` VALUES ('1', '1684921873', '106.117.91.121', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('2', '1684921873', '106.117.91.121', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('3', '1684978967', '106.117.91.121', 'https://lizhili.biaotian.ltd/', '[]', 'GET', 'Index', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('4', '1684994654', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('5', '1684994654', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('6', '1684994654', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('7', '1684994654', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('8', '1684994655', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('9', '1684994655', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('10', '1684994655', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('11', '1684994655', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('12', '1684994663', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('13', '1684994663', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('14', '1684994664', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('15', '1684996187', '171.214.239.196', 'https://lizhili.biaotian.ltd/api/index', '[]', 'OPTIONS', 'Api', 'index', 'index', '0');
INSERT INTO `lizhili_statistics_show` VALUES ('16', '1685001732', '167.248.133.33', 'https://lizhili.biaotian.ltd/', '[]', 'GET', 'Index', 'index', 'index', '0');

-- ----------------------------
-- Table structure for lizhili_system
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_system`;
CREATE TABLE `lizhili_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cnname` varchar(100) DEFAULT NULL,
  `enname` varchar(100) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1',
  `value` varchar(1000) DEFAULT NULL,
  `kxvalue` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `desc` varchar(255) DEFAULT NULL,
  `st` tinyint(3) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `shuoming` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_system
-- ----------------------------
INSERT INTO `lizhili_system` VALUES ('1', '网站名称', 'webname', '1', 'unishop', '', '0', '网站名称', '1', null, '1567676416', null);
INSERT INTO `lizhili_system` VALUES ('2', '关键词', 'keyword', '1', 'unicms内容管理系统,cms,lizhili,unishop,李志立', '', '0', '网站关键字', '1', null, '1567676416', null);
INSERT INTO `lizhili_system` VALUES ('3', '描述', 'miaoshu', '1', '一款优秀的内容管理系统', '', '0', '网站描述', '1', null, '1567676416', null);
INSERT INTO `lizhili_system` VALUES ('4', '底部版权信息', 'copyright', '1', '', '', '0', '网站版权信息', '1', null, '1567676416', null);
INSERT INTO `lizhili_system` VALUES ('5', '备案号', 'No', '1', '', '', '0', '网站备案号', '1', null, '1567676416', null);
INSERT INTO `lizhili_system` VALUES ('6', '统计代码', 'statistics', '2', '', '', '0', '网站统计代码', '1', null, '1567676416', '使用51la，修改代码配置，可以生成报告');
INSERT INTO `lizhili_system` VALUES ('7', '网站状态', 'value', '3', '开启', '开启,关闭', '0', '网站的状态', '1', null, '1567676416', null);
INSERT INTO `lizhili_system` VALUES ('8', '闭站重定向', 'redirect', '1', 'http://linglukeji.com/', '', '0', '', '1', '1585987185', '1585987185', null);

-- ----------------------------
-- Table structure for lizhili_ti
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_ti`;
CREATE TABLE `lizhili_ti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1是提交，2是审核通过，3是驳回',
  `time` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `true_money` decimal(10,2) DEFAULT NULL,
  `out_biz_no` varchar(255) DEFAULT NULL,
  `order_id_zfb` varchar(255) DEFAULT NULL,
  `pay_fund_order_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lizhili_ti
-- ----------------------------
INSERT INTO `lizhili_ti` VALUES ('2', '2', '1000.00', '1', '1672130868', '李志立', '18611111111', '167213086825047', null, null, null, null);

-- ----------------------------
-- Table structure for lizhili_tuan
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_tuan`;
CREATE TABLE `lizhili_tuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lon` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `is_dong` tinyint(1) DEFAULT '1' COMMENT '是否支持冷藏',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `idNo` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `address_name` varchar(255) DEFAULT NULL COMMENT '地址名称',
  `idCard1` varchar(255) DEFAULT NULL COMMENT '身份证图片',
  `idCard2` varchar(255) DEFAULT NULL COMMENT '身份证图片',
  `lei` tinyint(1) DEFAULT '0' COMMENT '0代表为审核，1是通过，2是驳回',
  `uid` int(11) DEFAULT '0' COMMENT '申请的用户id',
  `sheng` int(11) DEFAULT NULL,
  `shi` int(11) DEFAULT NULL,
  `xian` int(11) DEFAULT NULL,
  `sheng_html` varchar(255) DEFAULT NULL,
  `fid` int(11) DEFAULT '0' COMMENT '上一级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lizhili_tuan
-- ----------------------------
INSERT INTO `lizhili_tuan` VALUES ('2', '火车站附近小区', '/tuan/20230131/59e8a694c1468eaf94c884d8b80f057b.png', '河北省石家庄市新华区泰华街133号', '38.053497802114094', '114.46313287464795', '', '李志立1', '18611111111', '1', '1675135944', '1676347633', null, '', '', null, null, '1', '2', '0', '0', '0', '', '0');
INSERT INTO `lizhili_tuan` VALUES ('3', '汽车站附近小区', '/tuan/20230131/fc349132e85e02a5d00ac1a195a1992b.png', '河北省石家庄市新华区滨华路18号', '38.06715967737798', '114.46655247028662', '', '李志立2', '18622222222', '1', '1675137659', '1675760361', null, '', '', null, null, '1', '3', '0', '0', '0', '', '1');
INSERT INTO `lizhili_tuan` VALUES ('4', '塔谈国际', '/tuan/20230202/43fb4f0d200d90a3b7772ee9cd220756.png', '河北省石家庄市桥西区塔谈大街16号', '38.00073302213455', '114.48643058577363', '', '李志立', '18633333333', '1', '1675320765', '1675760433', null, '', '', null, null, '1', '0', '0', '0', '0', '', '1');

-- ----------------------------
-- Table structure for lizhili_tuan_goods
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_tuan_goods`;
CREATE TABLE `lizhili_tuan_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `tuan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lizhili_tuan_goods
-- ----------------------------
INSERT INTO `lizhili_tuan_goods` VALUES ('7', '1685005257', '22', '2');
INSERT INTO `lizhili_tuan_goods` VALUES ('8', '1685005257', '4', '2');
INSERT INTO `lizhili_tuan_goods` VALUES ('9', '1685005257', '24', '2');

-- ----------------------------
-- Table structure for lizhili_user
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_user`;
CREATE TABLE `lizhili_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `openid_app` varchar(255) DEFAULT NULL,
  `openid_wx` varchar(255) DEFAULT NULL,
  `unionid` varchar(255) DEFAULT NULL COMMENT '联合id',
  `token` varchar(255) DEFAULT NULL,
  `fid` int(11) DEFAULT '0',
  `phone` varchar(11) DEFAULT NULL,
  `avatarUrl` varchar(255) DEFAULT NULL COMMENT '头像',
  `province` varchar(255) DEFAULT NULL COMMENT '省',
  `city` varchar(255) DEFAULT NULL COMMENT '市',
  `country` varchar(255) DEFAULT NULL COMMENT '国家',
  `nickName` varchar(255) DEFAULT NULL COMMENT '姓名',
  `gender` varchar(255) DEFAULT NULL COMMENT '性别',
  `isopen` tinyint(1) DEFAULT '1',
  `haibao` varchar(255) DEFAULT NULL,
  `fen_num` int(11) NOT NULL DEFAULT '0' COMMENT '分享的次数',
  `sheng` int(11) DEFAULT '0',
  `shi` int(11) DEFAULT '0',
  `xian` int(11) DEFAULT '0',
  `sheng_html` varchar(255) DEFAULT NULL,
  `user_no` varchar(30) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `password` varchar(32) DEFAULT NULL COMMENT '登陆密码',
  `address` varchar(255) DEFAULT NULL,
  `yzm` int(6) DEFAULT NULL COMMENT '验证码',
  `level` tinyint(1) DEFAULT '0' COMMENT '级别',
  `beizhu` varchar(255) DEFAULT NULL COMMENT '自己使用',
  `erweima` varchar(255) DEFAULT NULL COMMENT '生成海报使用',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '当前余额',
  `money_zong` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '挣得钱，只增不减',
  `shenfen` tinyint(1) DEFAULT '0' COMMENT '0代表没有身份就是普通用户，1代表是渠道，2代表是代理',
  `tuan_address` int(11) DEFAULT '0',
  `is_gu` tinyint(1) DEFAULT '0' COMMENT '是否股份',
  `gu_ben` decimal(10,2) DEFAULT '0.00' COMMENT '股份本金',
  `gu_money` decimal(10,2) DEFAULT '0.00' COMMENT '股份利润',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lizhili_user
-- ----------------------------
INSERT INTO `lizhili_user` VALUES ('1', '1664002688', '1664002688', null, null, null, null, null, '0', '顶级用户', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, '123456', null, null, null, null, '0', null, null, '0.00', '0.00', '1', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('2', '1664002855', '1664002855', null, null, 'oCyJm5Jdd60zTxzxmH7rbcfjPGNs', null, '4202c4286b17b50843a227d43ce4fb67', '1', '18611111111', '/uploads_tou/20220924/34480d4c5a8792e53858703df50776fd.png', null, null, null, null, '0', '1', null, '0', '0', '0', '0', '河北省,石家庄市,长安区', 'F1XRP2', 'haha', 'd93a5def7511da3d0f2d171d9c344e91', '红旗大街333号', '938587', '0', null, 'haibao/erweima/2.png', '0.00', '0.00', '2', '3', '1', '1300.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('3', '1675176418', '1675176418', null, null, null, null, null, '1', '13611111111', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, null, null, null, null, '409579', '0', null, null, '0.00', '0.00', '2', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('4', '1675409160', '1675409160', null, null, null, null, 'bf73db05f45ed034767b420f4233a9d94', '1', '13880223068', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, '4II3G4', null, 'b9f04a4633f60b7f70daf2dd3b696d64', null, '333673', '0', null, null, '0.00', '0.00', '0', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('5', '1677115486', '1677115486', null, null, 'oCyJm5Bz-SfEJ2wNpGgWLWjxZjkA', '', 'ec7840bfe5adb275a6d05921a8a57d3e', '0', '17610395777', 'https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132', '', '', '', '微信用户', '1', '1', null, '0', '130000', '130100', '130104', '河北省,石家庄市,桥西区', 'F6RD25', '浩菲', null, '石家庄市桥西区海悦天地B座913室', null, '0', null, null, '0.00', '0.00', '0', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('6', '1677381697', '1677381697', null, null, null, null, null, '0', '17315264521', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, null, null, null, null, '556530', '0', null, null, '0.00', '0.00', '0', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('7', '1677381765', '1677381765', null, null, null, null, '8d4b5baff23382516e4cc8030db8020a7', '1', '17715527055', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, '36UFC7', null, 'd93a5def7511da3d0f2d171d9c344e91', null, '606743', '0', null, null, '0.00', '0.00', '2', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('8', '1678766418', '1678766418', null, null, 'oCyJm5G44OG1vZfw0iO98ez7hB1c', '', '87dc8f8f3bf29ac7dc545184f68302b5', '0', '15830880039', '/uploads_tou/20230410/1970d4f406ac70e16285e09a83797de7.png', '', '', '', '微信用户', '1', '1', null, '0', '130000', '130100', '130104', '河北省,石家庄市,桥西区', 'UEGUK8', '龙龙', null, '红旗大街', null, '0', null, null, '0.00', '0.00', '0', '2', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('9', '1679450688', '1679450688', null, null, null, null, null, '0', '12345678910', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, null, null, null, null, '512362', '0', null, null, '0.00', '0.00', '0', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('10', '1679570167', '1679570167', null, null, null, null, null, '0', '13810187795', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, null, null, null, null, '194479', '0', null, null, '0.00', '0.00', '0', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('11', '1679988294', '1679988294', null, null, null, null, 'eed79a3e30e3a4b62a71873609afd8ed11', '0', '15617664392', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, 'H6NZ711', null, '7e36f72ca83e9384c0a934c0bdf44275', null, '636890', '0', null, null, '0.00', '0.00', '0', '0', '0', '0.00', '0.00');
INSERT INTO `lizhili_user` VALUES ('12', '1680518587', '1680518587', null, null, null, null, null, '0', '13333333333', null, null, null, null, null, null, '1', null, '0', '0', '0', '0', null, null, null, null, null, '589689', '0', null, null, '0.00', '0.00', '0', '0', '0', '0.00', '0.00');
