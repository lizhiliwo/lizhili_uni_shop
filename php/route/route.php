<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::redirect('manage','/manage.php/login/index.html',302);
Route::get('show/:id','index/show');
Route::get('channel/:id','index/cate');
Route::get('search','index/search');
Route::get('/','index');

Route::post('api/index','api/index/ajax');
// ->header('Access-Control-Allow-Headers:Accept,Referer,Host,Keep-Alive,User-Agent,X-Requested-With,Cache-Control,Content-Type,Cookie,Token')
// ->allowCrossDomain();
Route::post('api/my','api/my/ajax');
// ->header('Access-Control-Allow-Headers:Accept,Referer,Host,Keep-Alive,User-Agent,X-Requested-With,Cache-Control,Content-Type,Cookie,Token')
// ->allowCrossDomain();
Route::post('api/update','api/my/update');
// ->header('Access-Control-Allow-Headers:Accept,Referer,Host,Keep-Alive,User-Agent,X-Requested-With,Cache-Control,Content-Type,Cookie,Token')
// ->allowCrossDomain();
return [

];
