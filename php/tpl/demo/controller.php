<?php
namespace app\manage\controller;
use app\manage\controller\Conn;
use app\manage\model\{da} as {da}model;
use think\Db;
class {da} extends Conn
{
	//这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
	protected $beforeActionList = [
        
    ];
    public function index()
    {
    	${xiao}=new {da}model();
    	$key=input('key') ? input('key') : '';
    	$this->assign('key',$key);
		
		${xiao}=${xiao}{con_cha}->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
			//$item->nickname = 'think';
		});
		$this->assign('{xiao}',${xiao});
    	
		$count1=Db::name('{xiao}')->count();
		$this->assign('count1', $count1);
       	return $this->fetch();
    }
	public function ajax()
    {
    	$data=input('param.');
		${xiao}=new {da}model();
		if($data['type']=='{xiao}_del'){
			$info=${xiao}->destroy($data['id']);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='{xiao}_sort'){
			$arrlength=count($data['id']);
			$ar=[];
			for($x=0;$x<$arrlength;$x++)
			{
			    $ar[$x]=['id'=>$data['id'][$x], 'sort'=>$data['sort'][$x]];
			}
			$info=${xiao}->saveAll($ar);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='{xiao}_start'){
		    if(Db::name('{xiao}')->where('id',$data['id'])->setField('isopen',1)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		if($data['type']=='{xiao}_stop'){
		    if(Db::name('{xiao}')->where('id',$data['id'])->setField('isopen',0)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		return 0;
    }
	public function add()
    {
    	if(request()->isPost()){
			$data=input('post.');
			$validate = new \app\manage\validate\{da};
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
			{file}
			${xiao}=new {da}model();
			${xiao}->data($data);
			$res=${xiao}->save();
			if($res){
				return $this->success('添加成功',url('{xiao}/index',['st'=>1]));
			}else{
				$this->error('添加失败了');
			}
    	}
       return $this->fetch();
    }
	public function edit()
    {
    	${xiao} = new {da}model();
    	if(request()->isPost()){
    		$data=input('post.');
			$validate = new \app\manage\validate\{da};
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
			{edit_file}
			$res=${xiao}->save($data,['id' => input('id')]);
			if($res){
				return $this->success('修改成功',url('{xiao}/index',['st'=>1]));
			}else{
				$this->error('修改失败了');
			}
    	}
		
		$id=input('id');
		$data= ${xiao}->get($id);
		$this->assign('data',$data);
       return $this->fetch();
    }
}
