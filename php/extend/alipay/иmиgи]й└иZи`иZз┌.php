<?php
//这个需要实际使用后在进行

public function tixian($order_info,$qian){ //提现 转账给个人用户
		$dir='/www/wwwroot/xxx/application/api/controller';
		include_once $dir.'/alipay/aop/AopCertClient.php';
		include_once $dir.'/alipay/aop/request/AlipayFundTransUniTransferRequest.php';

		$aop = new \AopCertClient ();
		$config = config('aop_config');
		$appCertPath = "/www/wwwroot/xxx/cert/appCertPublicKey_2021001193684464.crt";
		$alipayCertPath = "/www/wwwroot/xxx/cert/alipayCertPublicKey_RSA2.crt";
		$rootCertPath = "/www/wwwroot/xxx/cert/alipayRootCert.crt";
		$aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
		$aop->appId = $config['appid'];
		$aop->rsaPrivateKey = $config['rsaPrivateKey'];
		$aop->alipayrsaPublicKey = $aop->getPublicKey($alipayCertPath);//调用getPublicKey从支付宝公钥证书中提取公钥
		$aop->apiVersion = '1.0';
		$aop->signType = 'RSA2';
		$aop->postCharset='utf-8';
		$aop->format='json';
		$aop->isCheckAlipayPublicCert = true;//是否校验自动下载的支付宝公钥证书，如果开启校验要保证支付宝根证书在有效期内
		$aop->appCertSN = $aop->getCertSN($appCertPath);//调用getCertSN获取证书序列号
		$aop->alipayRootCertSN = $aop->getRootCertSN($rootCertPath);//调用getRootCertSN获取支付宝根证书序列号
		
		
		$request = new \AlipayFundTransUniTransferRequest();
		$body = array(
				'out_biz_no'=>$order_info['order_no'],
				'trans_amount'=>(float)$qian,
				'product_code'=>'TRANS_ACCOUNT_NO_PWD',
				'order_title'=>'爱乐旭官方转账提现！',
				'biz_scene'=>'DIRECT_TRANSFER',
				'payee_info'=>[
					'identity'=>$order_info['pay_on'],
					'identity_type'=>'ALIPAY_LOGON_ID',
					'name'=>$order_info['pay_name'],
				]
			);
		 $request->setBizContent(json_encode($body, JSON_UNESCAPED_UNICODE));	
		//$request->setNotifyUrl($config['ali_dongjie_notify_url']); //请求成功后的回调地址
		//$result = $aop->sdkexecute ($request);
		$result = $aop->execute ( $request); 
		return $result;
		
		die;
		//return \json(['code'=>0,'message'=>'登陆失败']);
	}
	
	public function yajin($order_info){ //小程序 支付押金
	
		$dir='/www/wwwroot/xxx/application/api/controller';
		include_once $dir.'/alipay/aop/AopCertClient.php';
		include_once $dir.'/alipay/aop/request/AlipayTradeCreateRequest.php';
	
		$aop = new \AopCertClient ();
		$config = config('aop_config');
		$appCertPath = "/www/wwwroot/xxx/cert/appCertPublicKey_2021001193684464.crt";
		$alipayCertPath = "/www/wwwroot/xxx/cert/alipayCertPublicKey_RSA2.crt";
		$rootCertPath = "/www/wwwroot/xxx/cert/alipayRootCert.crt";
		$aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
		$aop->appId = $config['appid'];
		$aop->rsaPrivateKey = $config['rsaPrivateKey'];
		$aop->alipayrsaPublicKey = $aop->getPublicKey($alipayCertPath);//调用getPublicKey从支付宝公钥证书中提取公钥
		$aop->apiVersion = '1.0';
		$aop->signType = 'RSA2';
		$aop->postCharset='utf-8';
		$aop->format='json';
		$aop->isCheckAlipayPublicCert = true;//是否校验自动下载的支付宝公钥证书，如果开启校验要保证支付宝根证书在有效期内
		$aop->appCertSN = $aop->getCertSN($appCertPath);//调用getCertSN获取证书序列号
		$aop->alipayRootCertSN = $aop->getRootCertSN($rootCertPath);//调用getRootCertSN获取支付宝根证书序列号
		
		$request = new \AlipayTradeCreateRequest();
		$body = [
			    'out_trade_no' => $order_info['order_no'],
			    'total_amount' => 99,//押金99
			    'subject' => '爱乐旭共享充电',
			    'buyer_id' => Db::name('member')->where('id',$order_info['member_id'])->value('user_id'),//'2088612652830542',//$user['ap_userid'],
			    'body' => '爱乐旭共享充电宝租金支付',
		];
			   
		 $request->setBizContent(json_encode($body, JSON_UNESCAPED_UNICODE));	
		$request->setNotifyUrl($config['ali_xiadan_99_notify_url']); //请求成功后的回调地址
		//$result = $aop->sdkexecute ( $request);
		$result = $aop->execute ( $request); 
		return $result;
		
		die;
		//return \json(['code'=>0,'message'=>'登陆失败']);
	}