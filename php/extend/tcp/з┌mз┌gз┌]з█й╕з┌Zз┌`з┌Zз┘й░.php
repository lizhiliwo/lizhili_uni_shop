<?php

//根据实际情况直接使用就行，不一定用到返回值

$fp = stream_socket_client("tcp://127.0.0.1:2347");
$msg = \json_encode([
	'type'=>'send_data'
]);
//向句柄中写入数据
fwrite($fp,$msg);
$ret = "";
//循环遍历获取句柄中的数据，其中 feof() 判断文件指针是否指到文件末尾
while (!feof($fp)){
	stream_set_timeout($fp, 2);
	$ret .= fgets($fp, 128);
	if($ret=='99'){ //这里根据返回不同进行跳出循环
		break;
	}
}
//关闭句柄
fclose($fp);
echo $ret;