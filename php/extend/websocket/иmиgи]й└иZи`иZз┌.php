<?php
//先开启workerman 注意数据加密，
$client = new \websocket\WebSocketClient();
$client->connect('127.0.0.1', 2345, '/');
$payload = json_encode(array(
	'type' => 'send_data',
));
$rs = $client->sendData($payload);
$client->disconnect(); //停止

if( $rs !== true ){
	echo "sendData error...\n";
}else{
	echo "ok\n";
}


//关于workerman使用，这个是用来直接推送数据的，原理是，他本身就是一个 workerman 集群里面的一个服务
use GatewayClient\Gateway;

Gateway::$registerAddress = '127.0.0.1:1236';
// GatewayClient支持GatewayWorker中的所有接口(Gateway::closeCurrentClient Gateway::sendToCurrentClient除外)
Gateway::sendToAll('7777');
//手册地址  https://github.com/walkor/GatewayClient