<?php
//生成xls 并下载，注意生成文件权限，数量不能过大，一遍需要条件判断
set_time_limit(0);
// 避免内存不足
ini_set('memory_limit', '-1');

//导入 数据
$inputFileName = dirname(__FILE__) . '/2.xlsx'; //找到文件
//读取文件方法一，建议使用
$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
//读取文件方法二
$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xml();
//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Ods();
//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Slk();
//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Gnumeric();
//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
$spreadsheet = $reader->load($inputFileName);


//获取数据 方法一
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
//获取数据 方法二 , 建议使用
$sheetData = $spreadsheet->getSheet(0)->toArray();
//获取数据 方法三
$cells=[];
foreach ( $spreadsheet->getWorksheetIterator() as $data ) {
 	dump($data->toArray());
	$cells = $data->toArray();
}
	
//导出数据
$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
//方法一，使用a1循环操作，建议使用这个
$sheet->setCellValue('A1', 'Hello World !');
//方法二，注意，下面使用的是，1（列）,1（行）方法，从1开始，不是0开始
$sheet->setCellValueByColumnAndRow(1, 1, '我是数据');
$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
//保存到 public 里面，注意，如果有目录，请先创建目录，并且设置权限
$writer->save('helloworld.xlsx');
	
