<?php

use think\facade\Env;

//生成xls 并下载，注意生成文件权限，数量不能过大，一遍需要条件判断
set_time_limit(0);
// 避免内存不足
ini_set('memory_limit', '-1');
$order=[
	[
		'name'=>'lizhili',
		'age'=>45,
		'sex'=>'男'
	],
	[
		'name'=>'lizhili2',
		'age'=>84,
		'sex'=>'女'
	],
	[
		'name'=>'lizhili3',
		'age'=>35,
		'sex'=>'男'
	],
	[
		'name'=>'lizhili4',
		'age'=>15,
		'sex'=>'女'
	],
];
//dump($order);
//引入数据
include(Env::get('extend_path').'/PHPExcel/PHPExcelReader.php');
$excel =new \PHPExcel();
//基础信息
$excel->getProperties()->setCreator("lizhili")->setLastModifiedBy("lizhili")->setTitle("lizhili demo")->setSubject("lizhliPHPExcel");
//设置 sheet 名称
$excel->getActiveSheet(0)->setTitle('订单表');
//标题
$excel->setActiveSheetIndex(0)
->setCellValue('A1', '姓名')
->setCellValue('B1', '年龄')
->setCellValue('C1', '性别');
//数据填充【此数据可以来自数据库】
$i = 2;
foreach ($order as $rows) {
	$excel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $rows['name'])
	->setCellValue('B'.$i, $rows['age'])
	->setCellValue('C'.$i, $rows['sex']);
	$i++;
}
//保存为 xls
$objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$na='xls/'.time().'.xls';
$objWriter->save($na);
return download($na, '订单列表(下载时间'.date('Y-m-d', time()).').xls');


//导入数据 读取xls 数据
$file = request()->file('');
if (isset($file['xls'])) {
	$info = $file['xls']->move('uploads_xls');
	$li=strtr($info->getSaveName(), " \ ", " / ");
	$li=env('root_path').'public/uploads_xls/'.$li;
	include(Env::get('extend_path').'/PHPExcel/PHPExcelReader.php');
	$PHPExcelReader =new \PHPExcelReader();
	$sheets = $PHPExcelReader->read($li);
	if(isset($sheets[0]['data'])){
		$data=$sheets[0]['data'];
		dump($data);
		$info = [];
		foreach($data as $k=>$v){
			if($k>0){
				// $info[]=[];
			}
		}
	}else{
		 $this->error('导入文件失败了');
	}
}