<?php
//目前学习的是，消息队列不能 进程守护
//如果需要进程守护，建议使用 workerman 

//注意修改 

// 1, 修改app/http/jobs
// 2, 把下面复制到 需要地方调用
// 3, php think queue:listen --queue lizhili   //开启最后是，队列名称

public function actionWithHelloJob($data){
	// 1.当前任务将由哪个类来负责处理。
	//   当轮到该任务时，系统将生成一个该类的实例，并调用其 fire 方法
	$jobHandlerClassName  = 'app\http\Jobs';
	// 2.当前任务归属的队列名称，如果为新队列，会自动创建
	$jobQueueName  	  = "lizhili";
	// 3.当前任务所需的业务数据 . 不能为 resource 类型，其他类型最终将转化为json形式的字符串
	//   ( jobData 为对象时，需要在先在此处手动序列化，否则只存储其public属性的键值对)
	$jobData       	  = $data; //['name'=>1]
	$isPushed = \think\Queue::push( $jobHandlerClassName , $jobData, $jobQueueName );
	// dump($isPushed);
	// database 驱动时，返回值为 1|false  ;   redis 驱动时，返回值为 随机字符串|false
	if( $isPushed !== false ){
		echo date('Y-m-d H:i:s') . " a new Hello Job is Pushed to the MQ"."<br>";
	}else{
		echo 'Oops, something went wrong.';
	}
}

//workerman 定时器
//查看 app/http/worker 里面是示例，示例是七牛云的上传

