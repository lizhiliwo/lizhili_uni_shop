<?php
//1，生成二维码方法(注意生成的权限问题) 注意组件问题

use think\facade\Env;

 $data['id']=Db::name('member')->where('tonken',$data['tonken'])->value('id');
		   
   $host=request()->header('host');
   $filename='haibao/erweima/'.$data['id'].'.png';
   \erweima\Qrcode::draw('http://'.$host.'?fid='.$data['id'], $filename);
   $config = array(
	 'image'=>array(
	   array(
		 'url'=>Env::get('root_path').'public/'.$filename,     //二维码资源
		 'stream'=>0,
		 'left'=>-10,
		 'top'=>-10,
		 'right'=>0,
		 'bottom'=>0,
		 'width'=>115,
		 'height'=>115,
		 'opacity'=>100
	   )
	 ),
	 'background'=>Env::get('root_path').'public/'.'haibao.jpg'          //背景图
   );
   $filename = 'haibao/haibao/'.$data['id'].'.jpg';
   $img='/'.\erweima\CreatePoster::draw($config, $filename);
   Db::name('member')->where('id',$data['id'])->update(['haibao'=>$img ]);
   return \json(['code'=>1,'message'=>'完成','data'=>$img]);
   
  