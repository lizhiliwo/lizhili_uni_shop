<?php
namespace app\manage\validate;

use think\Validate;

class Gu extends Validate
{
    protected $rule = [
         'uid' =>  'require','money' =>  'require','gufen' =>  'require','days' =>  'require'
    ];
    protected $message  =   [
         'uid.require' =>  '用户 必须填写','money.require' =>  '金额 必须填写','gufen.require' =>  '股份 必须填写','days.require' =>  '天数 必须填写'
    ];
    protected $scene = [
        'edit'  =>  [],
    ];
}
