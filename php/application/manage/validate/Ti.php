<?php
namespace app\manage\validate;
use think\Validate;
class Ti extends Validate
{
     protected $rule = [
		 'phone' =>  'mobile',
    ];
	 protected $message  =   [
		 'phone.mobile' =>  '手机格式不正确',
    ];
	protected $scene = [
        'edit'  =>  [],
    ];
}
