<?php
namespace app\manage\validate;
use think\Validate;
class Goods extends Validate
{
     protected $rule = [
        'goods_name' =>  'require|unique:goods|length:3,200',
        'markte_price'=>'float|egt:0',
        'money'=>'require|float|egt:0',
        'goods_desc'=>'require',
		'money'=>'egt:0',

    ];
	 protected $message  =   [
        'goods_name.require' => '商品名称必须填写', 
        'goods_name.unique'     => '当前商品名称已经存在，不能重复！',
        'goods_name.length'=>'商品名称在3-60个字符之间！',
        'markte_price.require' => '市场价格必须填写', 
        'markte_price.float'     => '市场价格必须为数字',
		'money.require' => '本店价格必须填写',
		'money.float'     => '本店价格必须为数字',
		
		
		'markte_price.egt' => '市场价格不能小于0',
		'money.egt' => '本店价格不能小于0', 
		'fen_money.egt'     => '分润不能小于0',
	  
        'goods_desc.require' => '产品描述必须填写', 

    ];
	protected $scene = [
        'edit'  =>  [
		        'markte_price'=>'float|egt:0',
		        'money'=>'require|float|egt:0',
		        'goods_desc'=>'require',
		        'money'=>'egt:0',

			],
    ];
}

