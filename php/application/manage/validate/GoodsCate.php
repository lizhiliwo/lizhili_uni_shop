<?php
namespace app\manage\validate;
use think\Validate;
class GoodsCate extends Validate
{
     protected $rule = [
        'name'  =>  'require|unique:goods_cate',
    ];
	 protected $message  =   [
        'name.require' => '中文名称必须填写',
        'name.unique'     => '中文名称已经存在，不能重复！',
    ];
	protected $scene = [
        'edit'  =>  [
			'name'  =>  'require',
        ],
        
    ];
}
