<?php
namespace app\manage\validate;
use think\Validate;
class MallUpdate extends Validate
{
     protected $rule = [
		 'desc' =>  'require',
    ];
	 protected $message  =   [
		 'desc.require' =>  '简介必须填写',
    ];
	protected $scene = [
        'edit'  =>  [],
    ];
}
