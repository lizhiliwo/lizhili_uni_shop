<?php
namespace app\manage\validate;
use think\Validate;
class Huodong extends Validate
{
     protected $rule = [
		 'title' =>  'require','desc' =>  'require',
    ];
	 protected $message  =   [
		 'title.require' =>  'title 必须填写','desc.require' =>  'desc 必须填写',
    ];
	protected $scene = [
        'edit'  =>  [],
    ];
}
