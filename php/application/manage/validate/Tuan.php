<?php

namespace app\manage\validate;

use think\Validate;

class Tuan extends Validate
{
    protected $rule = [
        'title' => 'require',
		 'address' => 'require', 
		 'lat' => 'require',
		  'lon' => 'require', 
		  'name' => 'require', 
		  'fid' => 'require', 
		  'phone' => 'require|mobile',
    ];
    protected $message = [
        'title.require' => '名称 必须填写', 
		'address.require' => '地址 必须填写', 
		'lat.require' => '经纬度 必须填写', 
		'lon.require' => '经纬度 必须填写', 
		'name.require' => '联系人 必须填写', 
		'fid.require' => '上级渠道商 必须填写', 
		'phone.require' => '手机 必须填写', 'phone.mobile' => '手机格式不正确',
    ];
    protected $scene = [
        'edit' => [],
    ];
}
