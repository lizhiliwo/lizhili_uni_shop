<?php
namespace app\manage\controller;
use app\manage\controller\Conn;
use think\Db;
use think\helper\Time;

class MallConfig extends Conn
{
    public function index()
    {
       
        if (request()->isPost()) {
            $data=input('post.');
            $file = request()->file();
            foreach ($file as $k1=>$v1){
				$info = $v1->move('mall_config');
				if ($info) {
					$li=strtr($info->getSaveName(), " \ ", " / ");
					$pic='/mall_config/'.$li;
					Db::name('mall_config')->where('key', $k1)->update(['value' => $pic]);
				}
            }
			foreach ($data as $k=>$v){
			    if($v){
                    Db::name('mall_config')->where('key',$k)->update([
                        'value'=>$v,
                        'time'=>time()
                    ]);
                }
            }
            $this->success('修改成功');
        }
		
		$fenlei=Db::name('mall_config')->where('fenlei','<>','')->distinct(true)->order('sort asc,id desc')->column('fenlei');
		$data=[];
		foreach($fenlei as $vv){
			$data1=Db::name('mall_config')->where('fenlei',$vv)->order('sort asc,id desc')->select();
			foreach ($data1 as &$v){
			    if($v['value'] and !$v['is_bian']){
			        $v['value']=substr($v['value'], 0, 3) . "***" . substr($v['value'], -1);
			    }
			}
			$data[$vv]=$data1;
		}
		$data1=Db::name('mall_config')->where('fenlei','')->order('sort asc,id desc')->select();
		foreach ($data1 as &$v){
		    if($v['value'] and !$v['is_bian']){
		        $v['value']=substr($v['value'], 0, 3) . "***" . substr($v['value'], -1);
		    }
		}
		$data['其他']=$data1;
		

        $this->assign('data', $data);
        return $this->fetch();
    }
	public function ajax()
	{
		$data=input('param.');
		if($data['type']=='mall_config_qing'){
			if(Db::name('mall_config')->where('id',$data['id'])->setField('value','')){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		return 0;
	}
    public function order()
    {
       if (request()->isPost()) {
           $data=input('post.');
           foreach ($data as $k=>$v){
               if($v and $v >=0){
                   Db::name('mall_config_order')->where('key',$k)->update([
                       'value'=>$v,
                       'time'=>time()
                   ]);
               }else{
       			 $this->error('提交数据有误');
       		}
           }
           $this->success('修改成功');
       }
        $data=Db::name('mall_config_order')->order('sort asc')->select();
        $this->assign('data', $data);
        return $this->fetch();
    }
	
	public function erweima(){
		
		dump(444);
	}
	
}
