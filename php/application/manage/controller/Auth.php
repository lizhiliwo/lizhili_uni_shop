<?php

namespace app\manage\controller;
use think\Db;
class Auth{
    
    public function check($name, $uid) {
		$name= strtolower($name);
		$role=Db::name('admin')->where('isopen',1)->where('id',$uid)->value('role');
		$ids=Db::name('auth_group')->where('status',1)->where('id',$role)->value('rules');
		$authList=Db::name('auth_rule')->where('status',1)->where('id','in',$ids)->column('name');
		
        if (is_string($name)) {
            $name = strtolower($name);
            if (strpos($name, ',') !== false) {
                $name = explode(',', $name);
            } else {
                $name = array($name);
            }
        }
		$name = explode("/",$name[0]);
        foreach ( $authList as $auth ) {
            $query = preg_replace('/^.+\?/U','',$auth);
			$query=explode("/",$query);
			if(strpos($query[0],"_")){
				$query[0]=str_replace("_","",$query[0]);
			}
			if($query[1]=='all'){
				if($query[0]==$name[0]){
					return true;
				}
			}else{
				if($query[0]==$name[0] and $query[1]==$name[1]){
					return true;
				}
			}
		}
        return false;
    }
	
}