<?php
namespace app\manage\controller;
use app\manage\controller\Conn;
use app\manage\model\Huodong as Huodongmodel;
use think\Db;
class Huodong extends Conn
{
	//这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
	protected $beforeActionList = [
        
    ];
    public function index()
    {
    	$huodong=new Huodongmodel();
    	$key=input('key') ? input('key') : '';
    	$this->assign('key',$key);
		
		$huodong=$huodong->where('title','like','%'.$key.'%')->order('id desc')->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
			$item->goods_name = Db::name('goods')->where('id',$item->goods_id)->value('goods_name');
		});
		$this->assign('huodong',$huodong);
    	
		$count1=Db::name('huodong')->count();
		$this->assign('count1', $count1);
       	return $this->fetch();
    }
	public function ajax()
    {
    	$data=input('param.');
		$huodong=new Huodongmodel();
		if($data['type']=='huodong_del'){
			$info=$huodong->destroy($data['id']);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='huodong_sort'){
			$arrlength=count($data['id']);
			$ar=[];
			for($x=0;$x<$arrlength;$x++)
			{
			    $ar[$x]=['id'=>$data['id'][$x], 'sort'=>$data['sort'][$x]];
			}
			$info=$huodong->saveAll($ar);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='huodong_start'){
		    if(Db::name('huodong')->where('id',$data['id'])->setField('isopen',1)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		if($data['type']=='huodong_stop'){
		    if(Db::name('huodong')->where('id',$data['id'])->setField('isopen',0)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		return 0;
    }
	public function add()
    {
    	if(request()->isPost()){
			$data=input('post.');
			$validate = new \app\manage\validate\Huodong;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
							
			$file = request()->file();
			if (isset($file['img'])) {
				$info = $file['img']->move("huodong");
				$li=strtr($info->getSaveName(), " \ ", " / ");
				$data['img']='/huodong/'.$li;
			}			else{
				$this->error('附件必须上传');
			}
			$huodong=new Huodongmodel();
			$huodong->data($data);
			$res=$huodong->save();
			if($res){
				return $this->success('添加成功',url('huodong/index',['st'=>1]));
			}else{
				$this->error('添加失败了');
			}
    	}
       return $this->fetch('',[
           'goods'=>Db::name('goods')->where('isopen',1)->select()
       ]);
    }
	public function edit()
    {
    	$huodong = new Huodongmodel();
    	if(request()->isPost()){
    		$data=input('post.');
			$validate = new \app\manage\validate\Huodong;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
							
			$file = request()->file();
			if (isset($file['img'])) {
				$info = $file['img']->move("huodong");
				$li=strtr($info->getSaveName(), " \ ", " / ");
				$data['img']='/huodong/'.$li;
			}
			$res=$huodong->save($data,['id' => input('id')]);
			if($res){
				return $this->success('修改成功',url('huodong/index',['st'=>1]));
			}else{
				$this->error('修改失败了');
			}
    	}
		
		$id=input('id');
		$data= $huodong->get($id);
		$this->assign('data',$data);
        return $this->fetch('',[
            'goods'=>Db::name('goods')->where('isopen',1)->select()
        ]);
    }
}
