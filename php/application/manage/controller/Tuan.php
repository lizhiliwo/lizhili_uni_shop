<?php
namespace app\manage\controller;
use app\manage\controller\Conn;
use app\manage\model\Tuan as Tuanmodel;
use think\Db;
use app\manage\model\User as Usermodel;
class Tuan extends Conn
{
	//这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
	protected $beforeActionList = [
        
    ];
	public function xuan()
	{
	    $user=new Usermodel();
	    
	    $key=input('key') ? input('key') : '';
	    $this->assign('key', $key);
	    $order='id desc';
	    $user=$user
	        ->where('name|phone', 'like', '%'.$key.'%')
			->where('shenfen','in',[1,2])
	        ->order($order)->paginate(10, false, ['query'=>request()->param()])->each(function ($item, $key) {
	            $u= Db::name('tuan')->where('uid', $item->id)->find();
				if($u){
					$item->xuan = $u['name'] ?: $u['phone'];
				}else{
					$item->xuan = false;
				}
	            
	        });
			
	    $this->assign('user', $user);
	    $count1=Db::name('user')->where('shenfen','in',[1,2])->count();
	    $this->assign('count1', $count1);
	    return $this->fetch('',[
	
		]);
	}
    public function index()
    {
    	$tuan=new Tuanmodel();
    	$key=input('key') ? input('key') : '';
    	$this->assign('key',$key);
		
		$tuan=$tuan->where('title|name|phone','like','%'.$key.'%')->order('id desc')->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
			$u=Db::name('user')->where('id',$item->uid)->find();
			if($u){
				$item->nn = $u['name'] ?: $u['phone'];
			}else{
				$item->nn = '无';
			}
			$us=Db::name('user')->where('id',$item->fid)->find();
			if($us){
				$item->ns = $us['name'] ?: $us['phone'];
			}
		});
		$this->assign('tuan',$tuan);
    	
		$count1=Db::name('tuan')->whereNull('delete_time')->count();
		$this->assign('count1', $count1);
       	return $this->fetch();
    }
	public function ajax()
    {
    	$data=input('param.');
		$tuan=new Tuanmodel();
		if($data['type']=='tuan_del'){
			$info=$tuan->destroy($data['id']);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='tuan_xuan'){
			if(Db::name('tuan')->where('id',$data['tuan_id'])->setField('uid',$data['id'])){
			    return 1;//修改成功返回1
			}else{
			    return 0;
			}
		}
		if($data['type']=='tuan_sort'){
			$arrlength=count($data['id']);
			$ar=[];
			for($x=0;$x<$arrlength;$x++)
			{
			    $ar[$x]=['id'=>$data['id'][$x], 'sort'=>$data['sort'][$x]];
			}
			$info=$tuan->saveAll($ar);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='tuan_start'){
		    if(Db::name('tuan')->where('id',$data['id'])->setField('isopen',1)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		if($data['type']=='tuan_stop'){
		    if(Db::name('tuan')->where('id',$data['id'])->setField('isopen',0)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		return 0;
    }
	public function add()
    {
    	if(request()->isPost()){
			$data=input('post.');
			$validate = new \app\manage\validate\Tuan;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
							
			$file = request()->file();
			if (isset($file['img'])) {
				$info = $file['img']->move("tuan");
				$li=strtr($info->getSaveName(), " \ ", " / ");
				$data['img']='/tuan/'.$li;
			}			else{
				$this->error('附件必须上传');
			}
			//修改省市县
			if($data['sheng']){
				$data['sheng']=Db::name('area')->where('label',$data['sheng'])->where('level_id',1)->value('value');
			}
			if($data['shi']){
				$data['shi']=Db::name('area')->where('label',$data['shi'])->where('level_id',2)->value('value');
			}
			if($data['xian']){
				$data['xian']=Db::name('area')->where('label',$data['xian'])->where('level_id',3)->value('value');
			}
			$tuan=new Tuanmodel();
			$tuan->data($data);
			$res=$tuan->save();
			if($res){
				return $this->success('添加成功',url('tuan/index',['st'=>1]));
			}else{
				$this->error('添加失败了');
			}
    	}
    	$mall_config=Db::name('mall_config')->column('value','key');


       return $this->fetch('',[
           'key'=>$mall_config['txmap_key'],
		   'qudao'=>Db::name('user')->where('shenfen',1)->select()
       ]);
    }
	public function edit()
    {
    	$tuan = new Tuanmodel();
    	if(request()->isPost()){
    		$data=input('post.');
			$validate = new \app\manage\validate\Tuan;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
							
			$file = request()->file();
			if (isset($file['img'])) {
				$info = $file['img']->move("tuan");
				$li=strtr($info->getSaveName(), " \ ", " / ");
				$data['img']='/tuan/'.$li;
			}
			//修改省市县
			if($data['sheng']){
				$data['sheng']=Db::name('area')->where('label',$data['sheng'])->where('level_id',1)->value('value');
			}
			if($data['shi']){
				$data['shi']=Db::name('area')->where('label',$data['shi'])->where('level_id',2)->value('value');
			}
			if($data['xian']){
				$data['xian']=Db::name('area')->where('label',$data['xian'])->where('level_id',3)->value('value');
			}
			
			$res=$tuan->save($data,['id' => input('id')]);
			if($res){
				return $this->success('修改成功',url('tuan/index',['st'=>1]));
			}else{
				$this->error('修改失败了');
			}
    	}
		
		$id=input('id');
		$data= $tuan->get($id);
		$this->assign('data',$data);
        $mall_config=Db::name('mall_config')->column('value','key');
        return $this->fetch('',[
            'key'=>$mall_config['txmap_key'],
			'qudao'=>Db::name('user')->where('shenfen',1)->select()
        ]);
    }
}
