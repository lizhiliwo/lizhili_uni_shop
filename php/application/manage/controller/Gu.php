<?php
namespace app\manage\controller;
use app\manage\controller\Conn;
use app\manage\model\Gu as Gumodel;
use think\Db;
class Gu extends Conn
{
	//这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
	protected $beforeActionList = [
        
    ];
    public function index()
    {
    	$gu=new Gumodel();
    	$key=input('key') ? input('key') : '';
    	$this->assign('key',$key);
		
		$gu=$gu->where('name|phone','like','%'.$key.'%')->order('id desc')->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
			//$item->nickname = 'think';
		});
		$this->assign('gu',$gu);
    	
		$count1=Db::name('gu')->count();
		$this->assign('count1', $count1);
       	return $this->fetch();
    }
	public function ajax()
    {
    	$data=input('param.');
		$gu=new Gumodel();
		if($data['type']=='gu_del'){
			$gu1=Db::name('gu')->where('id',$data['id'])->find();
			$info=$gu->destroy($data['id']);
			if($info){
				Db::name('user')->where('id',$gu1['uid'])->setDec('gu_ben', $gu1['money']);
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='gu_sort'){
			$arrlength=count($data['id']);
			$ar=[];
			for($x=0;$x<$arrlength;$x++)
			{
			    $ar[$x]=['id'=>$data['id'][$x], 'sort'=>$data['sort'][$x]];
			}
			$info=$gu->saveAll($ar);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='gu_start'){
		    if(Db::name('gu')->where('id',$data['id'])->setField('isopen',1)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		if($data['type']=='gu_stop'){
		    if(Db::name('gu')->where('id',$data['id'])->setField('isopen',0)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		return 0;
    }
	public function add()
    {
    	if(request()->isPost()){
			$data=input('post.');
			$validate = new \app\manage\validate\Gu;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
			//股份不能太大
			$gufens=Db::name('gu')->sum('gufen');
			if($gufens +$data['gufen'] > 95){
				$this->error('总股份不能超过95%！');
			}
			$user=Db::name('user')->where('id',$data['uid'])->find();
			$info=[
				'name'=>$user['name'],
				'uid'=>$user['id'],
				'phone'=>$user['phone'],
				'days'=>$data['days'],
				'time'=>time(),
				'end_time'=>time()+$data['days']*24*60*60,
				'gufen'=>$data['gufen'],
				'money'=>$data['money']
			];
			$gu=new Gumodel();
			$gu->data($info);
			$res=$gu->save();
			if($res){
				Db::name('user')->where('id',$user['id'])->setInc('gu_ben', $data['money']);
				return $this->success('添加成功',url('gu/index',['st'=>1]));
			}else{
				$this->error('添加失败了');
			}
    	}
       return $this->fetch('',[
		   'user'=>Db::name('user')->where('isopen',1)->where('is_gu',1)->select()
	   ]);
    }
	public function edit()
    {
    	$gu = new Gumodel();
    	if(request()->isPost()){
    		$data=input('post.');
			$validate = new \app\manage\validate\Gu;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
			
			//股份不能太大
			$gufens=Db::name('gu')->sum('gufen');
			if($gufens +$data['gufen'] > 95){
				$this->error('总股份不能超过95%！');
			}
			$gu1=Db::name('gu')->where('id',input('id'))->find();
			$user=Db::name('user')->where('id',$data['uid'])->find();
			$info=[
				'name'=>$user['name'],
				'uid'=>$user['id'],
				'phone'=>$user['phone'],
				'days'=>$data['days'],
				'time'=>$gu1['time'],
				'end_time'=>$gu1['time']+$data['days']*24*60*60,
				'gufen'=>$data['gufen'],
				'money'=>$data['money']
			];
			$res=$gu->save($info,['id' => input('id')]);
			if($res){
				return $this->success('修改成功',url('gu/index',['st'=>1]));
			}else{
				$this->error('修改失败了');
			}
    	}
		
		$id=input('id');
		$data= $gu->get($id);
		$this->assign('data',$data);
       return $this->fetch('',[
       		   'user'=>Db::name('user')->where('isopen',1)->where('is_gu',1)->select()
       ]);
    }
}
