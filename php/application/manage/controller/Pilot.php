<?php
namespace app\manage\controller;
use \think\Loader;
use app\manage\controller\Conn;
use think\Db;
use think\facade\Env;
class Pilot extends Conn
{
	//这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
	protected $beforeActionList = [
        
    ];
    public function nav()
    {
    	$pilot_nav=model('pilot_nav');
        $pilot_nav=$pilot_nav->order('sort asc,id desc')->paginate(4,false,['query'=>request()->param()]);
        $this->assign('pilot_nav',$pilot_nav);
    	
		$count1=Db::name('pilot_nav')->count();
		$this->assign('count1', $count1);
       	return $this->fetch();
    }
	
	public function cms()
	{
		if(request()->isPost()){
			$data=\input('post.');
			if (!isset($data['iswo'])) {
			    $data['iswo']=0;
			} else {
			    $data['iswo']=1;
			}
			$cms=\model('cms');
			if($cms->save($data,['id' => 1])){
			    $this->success('添加成功');
			}else{
			    $this->error('添加失败了');
			}
		}
		$this->assign('data',Db::name('cms')->find(1));
	   	return $this->fetch();
	}
	
    public function lit()
    {
        $pilot_list=model('pilot_list');
        $pilot_list=$pilot_list->tree();
        $this->assign('pilot_list',$pilot_list);

        $count1=Db::name('pilot_list')->count();
        $this->assign('count1', $count1);
        return $this->fetch();
    }
	public function icon()
	{
	    return $this->fetch();
	}
	public function litadd()
	{
		if(request()->isPost()){
			$data=input('post.');
			if (!isset($data['isopen'])) {
			    $data['isopen']=0;
			} else {
			    $data['isopen']=1;
			}
			$data['url']=trim($data['url']);
			$data['name']=trim($data['name']);
	        $pilot_list=model('pilot_list');
			if($pilot_list->save($data)){
				return $this->success('添加成功',url('pilot/lit',['st'=>1]));
			}else{
				$this->error('添加失败了');
			}
		}
		$ding=Db::name('pilot_nav')->where('isopen',1)->select();
		 $this->assign('ding',$ding);
		$ce=Db::name('pilot_list')->where('fid',0)->where('isopen',1)->where('pn_id',1)->select();
		 $this->assign('ce',$ce);
	   return $this->fetch();
	}
	public function litedit()
	{
	    $pilot_list=model('pilot_list');
	    if(request()->isPost()){
	        $data=input('post.');
	        if (!isset($data['isopen'])) {
	            $data['isopen']=0;
	        } else {
	            $data['isopen']=1;
	        }
			if($data['fid']){
				$data['pn_id']=Db::name('pilot_list')->where('id',$data['fid'])->value('pn_id');
			}
			if($data['fid']==input('id')){
				$this->error('自己不能成为自己下一级');
			}
			$data['url']=trim($data['url']);
			$data['name']=trim($data['name']);
	        if($pilot_list->save($data,['id' => input('id')])){
	            return $this->success('添加成功',url('pilot/nav',['st'=>1]));
	        }else{
	            $this->error('添加失败了');
	        }
	    }
		
	    $id=input('id');
	    $data= $pilot_list->get($id);
	    $this->assign('data',$data);
		$ding=Db::name('pilot_nav')->where('isopen',1)->select();
		$this->assign('ding',$ding);
		 
		$ce=Db::name('pilot_list')->where('fid',0)->where('pn_id',Db::name('pilot_list')->where('id',$data['fid'])->value('pn_id'))->where('isopen',1)->select();
		$this->assign('ce',$ce);
		
	    return $this->fetch();
	}
	public function ajax()
    {
    	$data=input('param.');
        $pilot_nav=model('pilot_nav');
		$pilot_list=model('pilot_list');
		if($data['type']=='pilotl_del'){
			$id=$data['id'];
			$info=$pilot_list->destroy($id);
			if($info){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		if($data['type']=='pilotl_sort'){
			$arrlength=count($data['id']);
			$ar=[];
			for($x=0;$x<$arrlength;$x++)
			{
			    $ar[$x]=['id'=>$data['id'][$x], 'sort'=>$data['sort'][$x]];
			}
			$info=$pilot_list->saveAll($ar);
			if($info){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		if($data['type']=='pilotl_start'){
		    if(Db::name('pilot_list')->where('id',$data['id'])->setField('isopen',1)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		if($data['type']=='pilotl_stop'){
		    if(Db::name('pilot_list')->where('id',$data['id'])->setField('isopen',0)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		
		
		if($data['type']=='pilotn_del'){
			$id=$data['id'];
			$info=$pilot_nav->destroy($id);
			if($info){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		if($data['type']=='pilotn_sort'){
			$arrlength=count($data['id']);
			$ar=[];
			for($x=0;$x<$arrlength;$x++)
			{
			    $ar[$x]=['id'=>$data['id'][$x], 'sort'=>$data['sort'][$x]];
			}
			$info=$pilot_nav->saveAll($ar);
			if($info){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
        if($data['type']=='pilotn_start'){
            if(Db::name('pilot_nav')->where('id',$data['id'])->setField('isopen',1)){
                return 1;//修改成功返回1
            }else{
                return 0;
            }
        }
        if($data['type']=='pilotn_stop'){
            if(Db::name('pilot_nav')->where('id',$data['id'])->setField('isopen',0)){
                return 1;//修改成功返回1
            }else{
                return 0;
            }
        }
		//修改下拉选项
		if($data['type']=='pilotnav_change'){
			return Db::name('pilot_list')->where('fid',0)->where('pn_id',$data['id'])->select();
		}
		//查询数据库字段
		if($data['type']=='demo_cha'){
			try {
			    // 这里是主体代码
				$res=Db::query("show columns from lizhili_".trim($data['id']));
				if($res){
					return json(['code'=>1,'data'=>$res]);
				}
				return json(['code'=>0]);
			} catch (ValidateException $e) {
			    // 这是进行验证异常捕获
			    return json(['code'=>0]);
			} catch (\Exception $e) {
			    // 这是进行异常捕获
			   return json(['code'=>0]);
			}
		}
		
		
		return 0;
    }
	public function navadd()
    {
    	if(request()->isPost()){
			$data=input('post.');
			if(!isset($data['name'])){
                $this->error('顶部导航名称没有填写！');
			}
            $pilot_nav=model('pilot_nav');
			if($pilot_nav->save($data)){
				return $this->success('添加成功',url('pilot/nav',['st'=>1]));
			}else{
				$this->error('添加失败了');
			}
    	}
       return $this->fetch();
    }
    public function navedit()
    {
        $pilot_nav=model('pilot_nav');
        if(request()->isPost()){
            $data=input('post.');
            if(!isset($data['name'])){
                $this->error('顶部导航名称没有填写！');
            }
            $pilot_nav=model('pilot_nav');
            if($pilot_nav->save($data,['id' => input('id')])){
                return $this->success('添加成功',url('pilot/nav',['st'=>1]));
            }else{
                $this->error('添加失败了');
            }
        }

        $id=input('id');
        $data= $pilot_nav->get($id);
        $this->assign('data',$data);
        return $this->fetch();
    }
	public function demo()
	{
	    if(request()->isPost()){
	        $data=input('post.');
			//这里应该添加判断
			if(!$data['en_name'] or !$data['cn_name'] or empty($data['name'])){
				$this->error('上传数据不对，检查参数！');
			}
			
			if(!isset($data['is_fu'])){
				if(file_exists(Env::get('module_path').'controller/'.$this->zhuan($data['en_name']).'.php'))
				{
				    $this->error('文件已经存在！');
				}
			}
	        if(!isset($data['is_file'])){
                $data['is_file']=[];
            }
			//替换控制器
			$controller=file_get_contents(Env::get('root_path').'tpl/demo/controller.php');
			$file=Env::get('module_path').'controller/'.$this->zhuan($data['en_name']).'.php';
			$controller=preg_replace('/{xiao}/',strtolower($data['en_name']),$controller);
			$controller=preg_replace('/{da}/',$this->zhuan($data['en_name']),$controller);
			$controller=preg_replace('/{name}/',ucfirst(strtolower($data['cn_name'])),$controller);
			//替换 首页搜索  ->where('title','like','%'.$key.'%')->order('sort asc') => {con_cha}
			if(empty($data['is_so'])){
				$is_so='';
			}else{
				$is_so="->where('".implode("|", array_keys($data['is_so']))."','like','%'.\$key.'%')";
			}
			if(\in_array('sort',$data['name'])){
				$sort="->order('sort asc,id desc')";
			}else{
				$sort="->order('id desc')";
			}
			$controller=preg_replace('/{con_cha}/',$is_so.$sort,$controller);
			//替换 {file} ->
			// $file = request()->file();
			// if (isset($file['pic'])) {
			//     $info = $file['pic']->move('demo');
			//     $li=strtr($info->getSaveName(), " \ ", " / ");
			//     $data['pic']='/demo/'.$li;
			// }
			$is_file='';
			$is_file1='';
		if(isset($data['is_file'])){
			foreach($data['is_file'] as $k=>$v){
				$is_file.=<<<EOF
				
			\$file = request()->file();
			if (isset(\$file['{$k}'])) {
				\$info = \$file['{$k}']->move("{$data['en_name']}");
				\$li=strtr(\$info->getSaveName(), " \ ", " / ");
				\$data['{$k}']='/{$data['en_name']}/'.\$li;
			}
EOF;
if(isset($data['is_bi'][$k])){
	$is_file.=<<<EOF
			else{
				\$this->error('附件必须上传');
			}
EOF;
}
			}
			
			$is_file1='';
			foreach($data['is_file'] as $k=>$v){
				$is_file1.=<<<EOF
				
			\$file = request()->file();
			if (isset(\$file['{$k}'])) {
				\$info = \$file['{$k}']->move("{$data['en_name']}");
				\$li=strtr(\$info->getSaveName(), " \ ", " / ");
				\$data['{$k}']='/{$data['en_name']}/'.\$li;
			}
EOF;
			}
			
		}
			$controller=preg_replace('/{file}/',$is_file,$controller);
			$controller=preg_replace('/{edit_file}/',$is_file1,$controller);
			
			file_put_contents($file,$controller);
			
			
			//替换model，模型
			$model=file_get_contents(Env::get('root_path').'tpl/demo/model.php');
			$file=Env::get('module_path').'model/'.$this->zhuan($data['en_name']).'.php';
			$model=preg_replace('/{xiao}/',strtolower($data['en_name']),$model);
			$model=preg_replace('/{da}/',$this->zhuan($data['en_name']),$model);
			$model=preg_replace('/{name}/',ucfirst(strtolower($data['cn_name'])),$model);
			file_put_contents($file,$model);

			//替换 validate 验证文件
			$validate=file_get_contents(Env::get('root_path').'tpl/demo/validate.php');
			$file=Env::get('module_path').'validate/'.$this->zhuan($data['en_name']).'.php';
			$validate=preg_replace('/{xiao}/',strtolower($data['en_name']),$validate);
			$validate=preg_replace('/{da}/',$this->zhuan($data['en_name']),$validate);
			$validate=preg_replace('/{name}/',ucfirst(strtolower($data['cn_name'])),$validate);
			//{rule} 替换
			//'title' =>  'require|unique:{xiao}',
			$rule='';
			$message='';
			if(isset($data['is_bi'])){
				foreach($data['is_bi'] as $k=>$v){
					if($k!='pic' and $k!='img' and !in_array($k,array_keys($data['is_file']))){
						$rule.="'$k' =>  'require',";
						$message.="'$k.require' =>  '$k 必须填写',";
						if($k=='url'){
							$rule=substr($rule,0,strlen($rule)-2)."|url',";
							$message.="'$k.url' =>  '网址必须包含http://',";
						}
						if($k=='phone'){
							$rule=substr($rule,0,strlen($rule)-2)."|mobile',";
							$message.="'$k.mobile' =>  '手机格式不正确',";
						}
					}
					
				}
			}
			//添加特别 url 和phone
			if(!strstr($rule,"url") and \in_array('url',$data['name'])){
				$rule.="'url' =>  'url',";
				$message.="'url.url' =>  '网址必须包含http://',";
			}
			if(!strstr($rule,"phone") and \in_array('phone',$data['name'])){
				$rule.="'phone' =>  'mobile',";
				$message.="'phone.mobile' =>  '手机格式不正确',";
			}
			$validate=preg_replace('/{rule}/',$rule,$validate);
			$validate=preg_replace('/{message}/',$message,$validate);
			file_put_contents($file,$validate);
			
			
			//替换index.html 文件
			$index=file_get_contents(Env::get('root_path').'tpl/demo/view/index.html');
			$file=Env::get('module_path').'view/'.strtolower($data['en_name']).'/index.html';
			$dir=Env::get('module_path').'view/'.strtolower($data['en_name']);
			if(!(file_exists($dir) && is_dir($dir))){
			    mkdir ($dir,0777);
			}
			$index=preg_replace('/{xiao}/',strtolower($data['en_name']),$index);
			$index=preg_replace('/{da}/',$this->zhuan($data['en_name']),$index);
			$index=preg_replace('/{name}/',ucfirst(strtolower($data['cn_name'])),$index);
			//{index_sou}
			// <div class="text-c">
			// 	<form action="" method="get">
			// 		<input type="text" name="key" value="{$key}" placeholder="{name}名称" style="width:250px" class="input-text">
			// 		<button name="" id="" class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
			// 	</form>
			// </div>
			if(empty($data['is_so'])){
				$index_sou='';
			}else{
			$index_sou=<<<EOF
										
			<div class="text-c">
				<form action="" method="get">
					<input type="text" name="key" value="{\$key}" placeholder="请输入搜索关键字" style="width:250px" class="input-text">
					<button name="" id="" class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
				</form>
			</div>
EOF;	
			}
			$index=preg_replace('/{index_sou}/',$index_sou,$index);
			//是否排序
			if(\in_array('sort',$data['name'])){
				$index=preg_replace('/{index_sort1}/','<a href="javascript:;" onclick="catesort()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 更新排序</a>',$index);
				$index=preg_replace('/{index_sort2}/','<th width="20">排序</th>',$index);
				$index=preg_replace('/{index_sort3}/','<td style="width: 15px;"><input type="text" name="{\$vo.id}" class="input-text lizhi" value="{\$vo.sort}"></td>',$index);
			}else{
				$index=preg_replace('/{index_sort1}/','',$index);
				$index=preg_replace('/{index_sort2}/','',$index);
				$index=preg_replace('/{index_sort3}/','',$index);
			}
			//是否 isopen
			if(\in_array('isopen',$data['name'])){
				$index_isopen1='<th width="10">状态</th>';
				$index_isopen2=<<<LIZHI2
				
				<td>
					{if condition="\$vo.isopen == 1 "}
					<span class="label label-success radius">已启用</span>
					{else /}
					<span class="label label-default radius">已禁用</span>
					{/if}
				</td>
LIZHI2;
				$index_isopen3=<<<LIZHI
				{if condition="\$vo.isopen == 1 "}
				<a style="text-decoration:none" onclick="admin_stop(this,{\$vo.id})" href="javascript:;" title="停用"><i class="Hui-iconfont">&#xe631;</i></a>
				{else /}
				<a onclick="admin_start(this,{\$vo.id})" href="javascript:;" title="启用" style="text-decoration:none"><i class="Hui-iconfont">&#xe615;</i></a>
				{/if}
LIZHI;				
				$index=preg_replace('/{index_isopen1}/',$index_isopen1,$index);
				$index=preg_replace('/{index_isopen2}/',$index_isopen2,$index);
				$index=preg_replace('/{index_isopen3}/',$index_isopen3,$index);
			}else{
				$index=preg_replace('/{index_isopen1}/','',$index);
				$index=preg_replace('/{index_isopen2}/','',$index);
				$index=preg_replace('/{index_isopen3}/','',$index);
			}
			//替换上方 index_mian1
			//<th width="10">操作</th>
			$index_min1='';
			$index_min2='';
			foreach($data['name'] as $k=>$v){
				$wo=empty($data['is_bu']) ? [] : array_keys($data['is_bu']);
				if($v!='sort' and $v!='isopen' and $v!='text' and !\in_array($v,$wo)){
					//dump($v);
					if($v=='url'){
						$index_min1.='<th width="30">网址链接</th>';
						$index_min2.='<td><a href="{\$vo.url}" target="_blank">{\$vo.url|default="无连接"}</a></td>';
					}elseif($v=='create_time'){
						$index_min1.='<th width="30">加入时间</th>';
						$index_min2.='<td>{\$vo.create_time|date="Y-m-d H:i:s"}</td>';
					}elseif($v=='pic'){
						$index_min1.='<th width="30">图片</th>';
						$img=<<<LIZH
						
						<td>
							{if condition="\$vo.pic == '' "}
								暂无缩率图
							{else /}
								<img src="{\$vo.pic}" height="50" />
							{/if}
						</td>
LIZH;
						$index_min2.=$img;
					}elseif($v=='img'){
						$index_min1.='<th width="30">图片</th>';
						$img=<<<LIZH
						
						<td>
							{if condition="\$vo.img == '' "}
								暂无缩率图
							{else /}
								<img src="{\$vo.img}" height="50" />
							{/if}
						</td>
LIZH;
						$index_min2.=$img;
					}elseif($v=='desc'){
						$index_min1.='<th width="80">简介</th>';
						$index_min2.='<td>{\$vo.desc|default="尚未填写"}</td>';
					}else{
						$v_name=$v;
						if($v=='phone'){
							$v_name='电话号码';
						}
						if($v=='name'){
							$v_name='姓名';
						}
						if($v=='title'){
							$v_name='标题';
						}
						$index_min1.='<th width="30">'.$v_name.'</th>';
						$index_min2.='<td>{\$vo.'.$v.'|default="尚未填写"}</td>';
					}
	
				}
			}
			$index=preg_replace('/{index_min1}/',$index_min1,$index);
			$index=preg_replace('/{index_min2}/',$index_min2,$index);
			
			file_put_contents($file,$index);
			
			
			
			
			//替换add.html 文件
			$add=file_get_contents(Env::get('root_path').'tpl/demo/view/add.html');
			$file=Env::get('module_path').'view/'.strtolower($data['en_name']).'/add.html';
			$dir=Env::get('module_path').'view/'.strtolower($data['en_name']);
			if(!(file_exists($dir) && is_dir($dir))){
			    mkdir ($dir,0777);
			}
			$add=preg_replace('/{xiao}/',strtolower($data['en_name']),$add);
			$add=preg_replace('/{da}/',$this->zhuan($data['en_name']),$add);
			$add=preg_replace('/{name}/',ucfirst(strtolower($data['cn_name'])),$add);
			//替换主体
			//{add_min}
			$is_bi=[];
			if(isset($data['is_bi'])){
				$is_bi=array_keys($data['is_bi']);
			}
			
			$add_min='';
			$add_min2='';
			foreach($data['name'] as $k=>$v){
				if($v!='sort' and $v!='isopen' and $v!='create_time'){
					//dump($v);
					if($v=='url'){
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$add_min2.='url: { required : true , url : true },';
							}else{
								$add_min2.='url: { url : true },';
							}
								
						$img.=<<<LIZH
链接地址：</label>
							<div class="formControls col-xs-8 col-sm-9">
								<input type="text" class="input-text" value="" placeholder="请填写正确链接地址：http://" id="url" name="url">
							</div>
							<div class="col-3">
							</div>
						</div>
LIZH;						
						$add_min.=$img;
					}elseif($v=='text'){
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$add_min2.='text:  {	required	: true	},';
							}else{
								$add_min2.='';
							}
								
						$img.=<<<LIZH
内容详情：</label>
						<div class="formControls col-xs-9 col-sm-9">
							<script id="editor" name='text' type="text/plain" style="width:100%;height:400px;"></script>
						</div>
					</div>
LIZH;						
						$add_min.=$img;
					}elseif($v=='pic'){
						$img=<<<LIZH
						<div class="row cl">
<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$add_min2.='pic:  {	required	: true	},';
							}else{
								$add_min2.='';
							}
								
						$img.=<<<LIZH
图片：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<div class="uploader-thum-container">
									<a href="javascript:void();" class="btn btn-primary radius"><i class="icon Hui-iconfont">&#xe642;</i> 浏览文件</a>
									<input type="file" class="input-file" onchange='onpic(this)' name="pic"  value="" accept='image/*' style="font-size: 20px;left:0;" /><span calss="sp"></span>
								</div>
							</div>
						</div>
LIZH;						
						$add_min.=$img;
					}elseif($v=='img'){
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$add_min2.='img:  {	required	: true	},';
							}else{
								$add_min2.='';
							}
								
						$img.=<<<LIZH
图片：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<div class="uploader-thum-container">
									<a href="javascript:void();" class="btn btn-primary radius"><i class="icon Hui-iconfont">&#xe642;</i> 浏览文件</a>
									<input type="file" class="input-file" onchange='onpic(this)' name="img"  value="" accept='image/*' style="font-size: 20px;left:0;" /><span calss="sp"></span>
								</div>
							</div>
						</div>
LIZH;						
						$add_min.=$img;
					}elseif(isset($data['is_file']) and in_array($v,array_keys($data['is_file']))){
                        $img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;
                        if(\in_array($v,$is_bi)){
                            $img.='<span class="c-red">*</span>';
                            $add_min2.=$v.':  {	required	: true	},';
                        }else{
                            $add_min2.='';
                        }

                        $img.=<<<LIZH
文件：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<div class="uploader-thum-container">
									<a href="javascript:void();" class="btn btn-primary radius"><i class="icon Hui-iconfont">&#xe642;</i> 浏览文件</a>
									<input type="file" class="input-file" onchange='onpic(this)' name="$v"  value="" accept='*/*' style="font-size: 20px;left:0;" /><span calss="sp"></span>
								</div>
							</div>
						</div>
LIZH;
                        $add_min.=$img;
                    }elseif($v=='desc'){
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$add_min2.='desc: {	required	: true	},';
							}else{
								$add_min2.='';
							}
								
						$img.=<<<LIZH
内容简介：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<textarea name="desc" cols="" rows="" id="desc" class="textarea" placeholder="说点什么...最少输入10个字符" datatype="*10-200"
								 dragonfly="true" nullmsg="备注不能为空！" onKeyUp="textlength()"></textarea>
								<p class="textarea-numberbar"><em class="textarea-length">0</em>/200</p>
							</div>
						</div>
LIZH;						
						$add_min.=$img;
					}else{
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$add_min2.=$v.': {	required	: true	},';
							}else{
								$add_min2.='';
							}
							$v_name=$v;
							if($v=='phone'){
								$v_name='电话号码';
							}
							if($v=='name'){
								$v_name='姓名';
							}
							if($v=='title'){
								$v_name='标题';
							}	
						$img.=<<<LIZH
$v_name ：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<input type="text" class="input-text" value="" placeholder="" id="$v" name="$v">
							</div>
						</div>
LIZH;						
						$add_min.=$img;
					}
	
				}
			}
			$add=preg_replace('/{add_min}/',$add_min,$add);
			$add=preg_replace('/{add_min2}/',$add_min2,$add);
			file_put_contents($file,$add);
			
			
			//替换edit.html 文件
			$edit=file_get_contents(Env::get('root_path').'tpl/demo/view/edit.html');
			$file=Env::get('module_path').'view/'.strtolower($data['en_name']).'/edit.html';
			$dir=Env::get('module_path').'view/'.strtolower($data['en_name']);
			if(!(file_exists($dir) && is_dir($dir))){
			    mkdir ($dir,0777);
			}
			$edit=preg_replace('/{xiao}/',strtolower($data['en_name']),$edit);
			$edit=preg_replace('/{da}/',$this->zhuan($data['en_name']),$edit);
			$edit=preg_replace('/{name}/',ucfirst(strtolower($data['cn_name'])),$edit);
			
			$is_bi=[];
			if(isset($data['is_bi'])){
				$is_bi=array_keys($data['is_bi']);
			}
			
			$edit_min='';
			$edit_min2='';
			foreach($data['name'] as $k=>$v){
				if($v!='sort' and $v!='isopen' and $v!='create_time'){
					//dump($v);
					if($v=='url'){
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$edit_min2.='url: { required : true , url : true },';
							}else{
								$edit_min2.='url: { url : true },';
							}
								
						$img.=<<<LIZH
链接地址：</label>
							<div class="formControls col-xs-8 col-sm-9">
								<input type="text" class="input-text" value="{\$data.url}" placeholder="请填写正确链接地址：http://" id="url" name="url">
							</div>
							<div class="col-3">
							</div>
						</div>
LIZH;						
						$edit_min.=$img;
					}elseif($v=='text'){
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$edit_min2.='text:  {	required	: true	},';
							}else{
								$edit_min2.='';
							}
								
						$img.=<<<LIZH
内容详情：</label>
						<div class="formControls col-xs-9 col-sm-9">
							<script id="editor" name='text' type="text/plain" style="width:100%;height:400px;">{\$data.text|raw}</script>
						</div>
					</div>
LIZH;						
						$edit_min.=$img;
					}elseif($v=='pic'){
						$img=<<<LIZH
						<div class="row cl">
<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$edit_min2.='pic:  {	required	: true	},';
							}else{
								$edit_min2.='';
							}
								
						$img.=<<<LIZH
图片：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<div class="uploader-thum-container">
									<a href="javascript:void();" class="btn btn-primary radius"><i class="icon Hui-iconfont">&#xe642;</i> 浏览文件</a>
									<input type="file" class="input-file" onchange='onpic(this)' name="pic"  value="" accept='image/*' style="font-size: 20px;left:0;" /><span calss="sp"></span>
								</div>
								<div style="margin-top: 15px;">
									{if condition="\$data.pic != ''"}
									<img src="{\$data.pic}" height="60" style="margin: 20px;" />
									{else /}
									<div>暂无缩率图</div>
									{/if}
								</div>
							</div>
						</div>
LIZH;						
						$edit_min.=$img;
					}elseif($v=='img'){
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$edit_min2.='img:  {	required	: true	},';
							}else{
								$edit_min2.='';
							}
								
						$img.=<<<LIZH
图片：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<div class="uploader-thum-container">
									<a href="javascript:void();" class="btn btn-primary radius"><i class="icon Hui-iconfont">&#xe642;</i> 浏览文件</a>
									<input type="file" class="input-file" onchange='onpic(this)' name="img"  value="" accept='image/*' style="font-size: 20px;left:0;" /><span calss="sp"></span>
								</div>
								<div style="margin-top: 15px;">
									{if condition="\$data.img != ''"}
									<img src="{\$data.img}" height="60" style="margin: 20px;" />
									{else /}
									<div>暂无缩率图</div>
									{/if}
								</div>
							</div>
						</div>
LIZH;						
						$edit_min.=$img;
					}elseif(isset($data['is_file']) and in_array($v,array_keys($data['is_file']))){
                    $img=<<<LIZH
<div class="row cl">
					<label class="form-label col-xs-3 col-sm-2">
LIZH;
                    if(\in_array($v,$is_bi)){
                        $img.='<span class="c-red">*</span>';
                        $edit_min2.=$v.':  {	required	: true	},';
                    }else{
                        $edit_min2.='';
                    }

                    $img.=<<<LIZH
文件：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<div class="uploader-thum-container">
									<a href="javascript:void();" class="btn btn-primary radius"><i class="icon Hui-iconfont">&#xe642;</i> 浏览文件</a>
									<input type="file" class="input-file" onchange='onpic(this)' name="$v"  value="" accept='*/*' style="font-size: 20px;left:0;" /><span calss="sp"></span>
								</div>
							</div>
							<div style="margin-top: 15px;">
									{if condition="\$data.$v != ''"}
									{$v}
									{else /}
									<div>暂无缩率图</div>
									{/if}
								</div>
						</div>
LIZH;

                    $edit_min.=$img;
                }elseif($v=='desc'){
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								$edit_min2.='desc: {	required	: true	},';
							}else{
								$edit_min2.='';
							}
								
						$img.=<<<LIZH
内容简介：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<textarea name="desc" cols="" rows="" id="desc" class="textarea" placeholder="说点什么...最少输入10个字符" datatype="*10-200"
								 dragonfly="true" nullmsg="备注不能为空！" onKeyUp="textlength()">{\$data.desc}</textarea>
								<p class="textarea-numberbar"><em class="textarea-length">{\$data.desc|mb_strlen}</em>/200</p>
							</div>
						</div>
LIZH;						
						$edit_min.=$img;
					}else{
						$img=<<<LIZH
<div class="row cl">
							<label class="form-label col-xs-3 col-sm-2">
LIZH;	
							if(\in_array($v,$is_bi)){
								$img.='<span class="c-red">*</span>';
								if(!in_array($v,array_keys($data['is_file']))){
                                    $edit_min2.=$v.': {	required	: true	},';
                                }
							}else{
								$edit_min2.='';
							}
							$v_name=$v;
							if($v=='phone'){
								$v_name='电话号码';
							}
							if($v=='name'){
								$v_name='姓名';
							}
							if($v=='title'){
								$v_name='标题';
							}
						$img.=<<<LIZH
$v_name ：</label>
							<div class="formControls col-xs-9 col-sm-9">
								<input type="text" class="input-text" value="{\$data.$v}" placeholder="" id="$v" name="$v">
							</div>
						</div>
LIZH;						
						$edit_min.=$img;
					}
	
				}
			}
			$edit=preg_replace('/{edit_min}/',$edit_min,$edit);
			$edit=preg_replace('/{edit_min2}/',$edit_min2,$edit);
			file_put_contents($file,$edit);
			
			$this->success('添加成功');
	    }
	    return $this->fetch();
	}
    private function zhuan($str){
        $str = ucwords(str_replace('_', ' ', $str));
        $str = str_replace(' ','',$str);
        return ucfirst($str);
    }
}
