<?php
namespace app\manage\controller;
use app\manage\controller\Conn;
use think\Db;
use think\Cache;
use app\manage\model\Admin as Adminmodel;
class Admin extends Conn
{
    public function index()
    {
    		$key=input('key') ? input('key') : '';	
			$this->assign('key',$key);
			
			$data=Db::name('admin')
			->alias('a')
			->join('auth_group w','a.role = w.id', 'LEFT')
			->where('username','like','%'.$key.'%')
			->order('a.id','ASC')->field( 'a.id,username,w.title,create_time,update_time,mark,isopen' )->paginate(10,false,['query'=>request()->param()]);

			$this->assign('data',$data);
    	
		$count1=Db::name('admin')->count();
		$this->assign('count1', $count1);
    	return $this->fetch();
    }
	 public function add()
    {
    	$admin=new Adminmodel();

		if(request()->isPost()){
    		$data=input('post.');
			$validate = new \app\manage\validate\Admin;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}
			$shield=Db::name('shield')->where('id',1)->value('shield');
			   if(\in_array($data['username'],explode("|",$shield))){
			    $this->error('用户名非法');
			   }
			
			
			$info=$admin->add($data);
			if($info == 1){
				return $this->success('添加成功',url('admin/index',['st'=>1]));
			}else{
				$this->error('添加失败了');
			}
    	}
		$res=Db::name('auth_group')->select();
		$this->assign('res',$res);
    	return $this->fetch();
    }
	public function ajax()
    {
    	$data=input('param.');
		if($data['type']=='admin_start'){
			if(Db::name('admin')->where('id',$data['id'])->setField('isopen',1)){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
       	if($data['type']=='admin_stop' and $data['id'] != 1){
			if(Db::name('admin')->where('id',$data['id'])->setField('isopen',0)){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		if($data['type']=='admin_del' and $data['id'] != 1){
			if(Db::name('admin')->delete($data['id'])){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		if($data['type']=='admin_all'){
			if(in_array("1", $data['id'])){
				return 0;
			}
			if(Db::name('admin')->delete($data['id'])){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		return 0;
    }
	public function edit()
    {
    	if(request()->isPost()){
    		$data=input('post.');
			$validate = new \app\manage\validate\Admin;
			if(!$validate->scene('edit')->check($data)){
				$this->error($validate->getError());
			}
	
			if($data['password']!=''){
				$data['password']=md5(substr(md5($data['password']),0,25).'lizhili');
			}else{
				unset($data['password']);
			}

			if(Db::name('admin')->where('id', $data['id'])->update($data)){
				return $this->success('修改成功',url('admin/index',['st'=>1]));
			}else{
				$this->error('修改失败了');
			}
    	}
		$cid=input('id');
		$data=Db::name('admin')->where('id',$cid)->field('id,username,mark,role')->find();
		$this->assign('data',$data);
		
		$res=Db::name('auth_group')->select();
		$this->assign('res',$res);
       return $this->fetch('edit');
    }
	
	//清除缓存
	public function cahe(){
		if(\Cache::clear()){
			return $this->success('清除缓存成功',url('index/main'));
		}else{
			return  $this->error('清除缓存失败了');
		}
	}
	
}
