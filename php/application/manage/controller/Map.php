<?php
namespace app\manage\controller;

use app\manage\controller\Conn;
use think\Db;
class Map extends Conn
{
	//这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
	protected $beforeActionList = [
        
    ];
    public function index()
    {
		$domain=request()->domain();
		$cate=Db::name('cate')->column('id');
		$article=Db::name('article')->column('id');
		
		$html='<?xml version="1.0" encoding="utf-8"?>';
		$html.='<urlset>';
		//添加主域名
		$html.='<url>';
		$html.='<loc>'.$domain.'</loc>';
		$html.=' <changefreq>Always</changefreq>';
		$html.=' <priority>1</priority>';
		$html.='</url>';
		//添加栏目
		foreach($cate as $v){
			$html.='<url>';
			$html.='<loc>'.$domain.'/channel/'.$v.'.html</loc>';
			$html.=' <changefreq>Always</changefreq>';
			$html.=' <priority>0.9</priority>';
			$html.='</url>';
		}
		//详情页
		foreach($article as $v){
			$html.='<url>';
			$html.='<loc>'.$domain.'/show/'.$v.'.html</loc>';
			$html.=' <changefreq>Always</changefreq>';
			$html.=' <priority>0.8</priority>';
			$html.='</url>';
		}
		$html.='</urlset>';
		file_put_contents('sitemap.xml',$html);
		$seo=Db::name('cms')->find(1);
       	return $this->fetch('',[
			'sitemap'=>$domain.'/sitemap.xml',
            'baidu_token'=>$seo['baidu_token'],
		]);
    }
    public function ajax()
    {
        $data=input('param.');
        if($data['type']=='xiu_baidu'){
            if(Db::name('cms')->where('id',1)->update(['baidu_token'=>$data['token']])){
                return 1;
            }else{
                return 0;
            }
        }
        //提交到百度

        if($data['type']=='ti_baidu'){
            $domain=request()->domain();
            $seo=Db::name('cms')->find(1);

            $cate=Db::name('cate')->column('id');
            $article=Db::name('article')->column('id');
            $urls = [$domain];
            foreach ($cate as $k=>$v){
                $urls[] =$domain.'/channel/'.$v.'.html';
            }
            foreach ($article as $k=>$v){
                $urls[] =$domain.'/show/'.$v.'.html';
            }
            $api = 'http://data.zz.baidu.com/urls?site='.$domain.'&token='.$seo['baidu_token'];
            $ch = curl_init();
            $options =  array(
                CURLOPT_URL => $api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => implode("\n", $urls),
                CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
            );
            curl_setopt_array($ch, $options);
            $result = curl_exec($ch);
           $res=\json_decode($result,true);
            if(isset($res['error'])){
                return \json(['code'=>0,'message'=>$res['message']]);
            }else{
                return \json(['code'=>1]);
            }
        }
        if($data['type']=='xiu_num'){
            $domain=request()->domain();
            $seo=Db::name('cms')->find(1);

            $cate=Db::name('cate')->column('id');
            $article=Db::name('article')->limit($data['num'])->order('time desc')->column('id');
            $urls = [$domain];
            foreach ($cate as $k=>$v){
                $urls[] =$domain.'/channel/'.$v.'.html';
            }
            foreach ($article as $k=>$v){
                $urls[] =$domain.'/show/'.$v.'.html';
            }
            if(!$urls){
                return \json(['code'=>0]);
            }else{
                return \json(['code'=>1,'data'=>$urls]);
            }
        }


        return 0;
    }
	
}
