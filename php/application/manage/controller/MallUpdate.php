<?php
namespace app\manage\controller;
use app\manage\controller\Conn;
use app\manage\model\MallUpdate as MallUpdatemodel;
use think\Db;
class MallUpdate extends Conn
{
	//这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
	protected $beforeActionList = [
        
    ];
    public function index()
    {
    	$mall_update=new MallUpdatemodel();
    	$key=input('key') ? input('key') : '';
    	$this->assign('key',$key);
		
		$mall_update=$mall_update->order('id desc')->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
			//$item->nickname = 'think';
		});
		$this->assign('mall_update',$mall_update);
    	
		$count1=Db::name('mall_update')->count();
		$this->assign('count1', $count1);
       	return $this->fetch();
    }
	public function ajax()
    {
    	$data=input('param.');
		$mall_update=new MallUpdatemodel();
		if($data['type']=='mall_update_del'){
			$info=$mall_update->destroy($data['id']);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='mall_update_sort'){
			$arrlength=count($data['id']);
			$ar=[];
			for($x=0;$x<$arrlength;$x++)
			{
			    $ar[$x]=['id'=>$data['id'][$x], 'sort'=>$data['sort'][$x]];
			}
			$info=$mall_update->saveAll($ar);
			if($info){
				return 1;
			}else{
				return 0;
			}
		}
		if($data['type']=='mall_update_start'){
		    if(Db::name('mall_update')->where('id',$data['id'])->setField('isopen',1)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		if($data['type']=='mall_update_stop'){
		    if(Db::name('mall_update')->where('id',$data['id'])->setField('isopen',0)){
		        return 1;//修改成功返回1
		    }else{
		        return 0;
		    }
		}
		return 0;
    }
	public function add()
    {
    	if(request()->isPost()){
			$data=input('post.');
			$validate = new \app\manage\validate\MallUpdate;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}
			$file = request()->file();
			if (isset($file['data'])) {
			    $name=100;
                if($last=Db::name('mall_update')->order('time desc')->find()){
                    $name=$last['app_v']+1;
                }
				$info = $file['data']->validate(['ext'=>'apk'])->move("update",'update_'.$name);
                if($info){
                    $li=strtr($info->getSaveName(), " \ ", " / ");
                }else{
                    $this->error($file['data']->getError());
                }
                $data['app_v']=$name;
                $data['time']=time();
				$data['data']='/update/'.$li;
			}else{
				$this->error('附件必须上传');
			}
			$mall_update=new MallUpdatemodel();
			$mall_update->data($data);
			$res=$mall_update->save();
			if($res){
				return $this->success('添加成功',url('mall_update/index',['st'=>1]));
			}else{
				$this->error('添加失败了');
			}
    	}
       return $this->fetch();
    }
	public function edit()
    {
    	$mall_update = new MallUpdatemodel();
    	if(request()->isPost()){
    		$data=input('post.');
			$validate = new \app\manage\validate\MallUpdate;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
							
			$file = request()->file();
            if (isset($file['data'])) {
                $last=Db::name('mall_update')->where('id',input('id'))->find();
                $name=$last['app_v'];
                $info = $file['data']->validate(['ext'=>'apk'])->move("update",'update_'.$name);
                if($info){
                    $li=strtr($info->getSaveName(), " \ ", " / ");
                }else{
                    $this->error($file['data']->getError());
                }
                $data['app_v']=$name;
                $data['time']=time();
                $data['data']='/update/'.$li;
            }
			$res=$mall_update->save($data,['id' => input('id')]);
			if($res){
				return $this->success('修改成功',url('mall_update/index',['st'=>1]));
			}else{
				$this->error('修改失败了');
			}
    	}
		
		$id=input('id');
		$data= $mall_update->get($id);
		$this->assign('data',$data);
       return $this->fetch();
    }
}
