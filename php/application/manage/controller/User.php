<?php
namespace app\manage\controller;

use app\manage\controller\Conn;
use app\manage\model\User as Usermodel;
use think\Db;

class User extends Conn
{
    //这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
    protected $beforeActionList = [
        
    ];
    public function index()
    {
        $user=new Usermodel();
        
        $key=input('key') ? input('key') : '';
        $this->assign('key', $key);
        $order='id desc';
        $user=$user
			
           ->where('name|phone', 'like', '%'.$key.'%')
            ->order($order)->paginate(10, false, ['query'=>request()->param()])->each(function ($item, $key) {
				
                $shang= Db::name('user')->where('id', $item->fid)->find();
                $item->fname = isset($shang) ? ($shang['name'] ?: $shang['phone']) : '';
                $item->xia = Db::name('user')->where('fid', $item->id)->count('id');
            });
		
			
        $this->assign('user', $user);
        $count1=Db::name('user')->count();
        $this->assign('count1', $count1);
        return $this->fetch('',[

		]);
    }
	
    public function add()
    {
        if (request()->isPost()) {
            $data=input('post.');
            if (!($data['f_phone'] and preg_match("/^1[3456789]{1}\d{9}$/", $data['f_phone']))) {
                $this->error('上一级手机号不正确');
            }
            if (!($data['phone'] and preg_match("/^1[3456789]{1}\d{9}$/", $data['phone']))) {
                $this->error('填写手机号不正确');
            }
            if (!$f_user=Db::name('user')->where('phone', $data['f_phone'])->find()) {
                $this->error('找不到上一级信息');
            }
            if (Db::name('user')->where('phone', $data['phone'])->find()) {
                $this->error('手机号已经存在不能重复添加');
            }
            $msg=123456;
            $info=[
                'phone'=>$data['phone'],
                'yzm'=>$msg,
                'fid'=>$f_user['id'],
                'password'=>md5(sha1($msg))
            ];
            $id=Db::name('user')->insertGetId($info);
            Db::name('user')->where('id', $id)->update(['user_no'=>$this->sui($id)]);
            $this->success('添加成功');
        }
        return $this->fetch();
    }
    public function ajax()
    {
        $data=input('param.');
        $user=new Usermodel();
        
        if ($data['type']=='user_start') {
            if (Db::name('user')->where('id', $data['id'])->setField('isopen', 1)) {
                return 1;//修改成功返回1
            } else {
                return 0;
            }
        }
        if ($data['type']=='user_stop') {
            if (Db::name('user')->where('id', $data['id'])->setField('isopen', 0)) {
                return 1;//修改成功返回1
            } else {
                return 0;
            }
        }
		if ($data['type']=='gu_start') {
		    if (Db::name('user')->where('id', $data['id'])->setField('is_gu', 1)) {
		        return 1;//修改成功返回1
		    } else {
		        return 0;
		    }
		}
		if ($data['type']=='gu_stop') {
		    if (Db::name('user')->where('id', $data['id'])->setField('is_gu', 0)) {
		        return 1;//修改成功返回1
		    } else {
		        return 0;
		    }
		}
        if ($data['type']=='user_fid') {
            if ($wo=Db::name('user')->where('fid', $data['id'])->select()) {
                return $wo;//修改成功返回1
            } else {
                return [];
            }
        }
        if ($data['type']=='user_xuan') {
            if ($data['zi']==$data['id']) {
                return ['code'=>0,'msg'=>'自己不能为自己下一级'];
            }
                        
            if ($wo=Db::name('user')->where('id', $data['zi'])->update(['fid'=>$data['id']])) {
                return ['code'=>1,'msg'=>'成功'];//修改成功返回1
            } else {
                return ['code'=>0,'msg'=>'失败'];
            }
        }
        return 0;
    }
    public function lit()
    {
        $user=new Usermodel();
        
        $key=input('key') ? input('key') : '';
        $this->assign('key', $key);
            
        $user=$user
            ->where('fid', input('id'))
            ->where('nickName', 'like', '%'.$key.'%')
            ->order('id desc')->paginate(5, false, ['query'=>request()->param()]);
            
        $this->assign('user', $user);
        $count1=Db::name('user')->where('fid', input('id'))->count();
        $this->assign('count1', $count1);
        return $this->fetch();
    }
    
    public function xiu()
    {
        if (request()->isPost()) {
            $data=input('post.');
            if ($wo=Db::name('user')->where('id', $data['id'])->find()) {
                if (!$wo['user_no']) {
                    Db::name('user')->where('id', $wo['id'])->update(['user_no'=>$this->sui($wo['id'])]);
                }
                Db::name('user')->where('id', $wo['id'])->update(['password'=>md5(sha1($data['password']))]);
                return $this->success('修改成功', url('user/index', ['st'=>1]));
            } else {
                $this->error('修改失败了');
            }
        }
        return $this->fetch('', [
                'data'=>Db::name('user')->find(input('id'))
            ]);
    }
	
	public function zhiding()
	{
	    if (request()->isPost()) {
			$data=input('post.');
			if(!isset($data['shenfen'])){
				$this->error('修改失败了');
			}
			
			if ($wo=Db::name('user')->where('id', $data['id'])->find()) {
				
				if($wo['shenfen']==1){
					$this->error('渠道身份不能修改');
				}
				if($wo['shenfen']==2){
					$this->error('代理身份不能修改');
				}
				if($data['shenfen']==2){
					if(!$data['fid']){
						$this->error('请选择上一级');
					}
					 Db::name('user')->where('id', $wo['id'])->update([
						 'shenfen'=>$data['shenfen'],
						 'fid'=>$data['fid']
						 ]);
				}
				if($data['shenfen']==1){
					Db::name('user')->where('id', $wo['id'])->update([
											 'shenfen'=>$data['shenfen']
											 ]);
				}
				return $this->success('修改成功', url('user/index', ['st'=>1]));
			} else {
				$this->error('修改失败了');
			}
		}
		return $this->fetch('', [
				'data'=>Db::name('user')->find(input('id')),
				'shang'=>Db::name('user')->where('shenfen',1)->select()
			]);
	}
	
	
    private function sui($id=0)
    {
        $str=[
                0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
            ];
        $sui='';
        for ($i=0;$i<5;$i++) {
            $sui.=$str[mt_rand(0, 35)];
        }
        
        return $sui.$id;
    }
    public function lit2()
    {
        $user=Db::name('user')->where('fid', input('id'))->order('id desc')->select();
        $this->assign('user', $user);
        return $this->fetch();
    }






}
