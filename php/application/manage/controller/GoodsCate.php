<?php
namespace app\manage\controller;
use app\manage\controller\Conn;
use think\Db;
use app\manage\model\GoodsCate as GoodsCatemodel;
class GoodsCate extends Conn
{
   //这里用前置操作，表示提前运行，本来要用于栏目删除子栏目呢，现在不用了
	protected $beforeActionList = [
        
    ];
    public function index()
    {
		$key=input('key') ? input('key') : '';
		$this->assign('key',$key);
    	
		$data=input('post.');
		$datasort=Db::name('goods_cate')->where('name','like','%'.$key.'%')->order('id','ASC')->select();
		$this->assign('datasort',$datasort);

		$count1=Db::name('goods_cate')->count();
		$this->assign('count1', $count1);
       	return $this->fetch();
    }
	public function ajax()
    {
    	$data=input('param.');
		if($data['type']=='goods_cate_sort'){
			$arrlength=count($data['id']);
			$ar=[];
			for($x=0;$x<$arrlength;$x++)
			{
			    $ar[$x]=['id'=>$data['id'][$x], 'sort'=>$data['sort'][$x]];
			}
			$goods_cate=new GoodsCatemodel();
			$info=$goods_cate->saveAll($ar);
			if($info){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		if($data['type']=='goods_cate_del'){
            if(Db::name('goods_cate')->delete($data['id'])){
                return 1;//修改成功返回1
            }else{
                return 0;
            }
		}
		if($data['type']=='goods_cate_start'){
			if(Db::name('goods_cate')->where('id',$data['id'])->setField('isopen',1)){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		if($data['type']=='goods_cate_stop'){
			if(Db::name('goods_cate')->where('id',$data['id'])->setField('isopen',0)){
				return 1;//修改成功返回1
			}else{
				return 0;
			}
		}
		return 0;
    }
	public function add()
    {
    	if(request()->isPost()){
			$data=input('post.');
			
			$validate = new \app\manage\validate\GoodsCate;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
			$file = request()->file('');
            if (isset($file['pic'])) {
                $info = $file['pic']->move('uploads');
                $li=strtr($info->getSaveName(), " \ ", " / ");
                $data['pic']='/uploads/'.$li;
            }
			$user = new GoodsCatemodel($data);
			$res=$user->save();
			if($res){
				$this->success('添加成功',url('goods_cate/index',['st'=>1]));
			}else{
				$this->error('栏目添加失败了');
			}
    	}
		
		
       return $this->fetch();
    }
	public function edit()
    {
    	if(request()->isPost()){
			$data=input('post.');
			$validate = new \app\manage\validate\GoodsCate;
			if(!$validate->check($data)){
				$this->error($validate->getError());
			}	
			$file = request()->file('');
			if (isset($file['pic'])) {
			    $info = $file['pic']->move('uploads');
			    $li=strtr($info->getSaveName(), " \ ", " / ");
			    $data['pic']='/uploads/'.$li;
			}
    		$user = new GoodsCatemodel();
			$res=$user->allowField(true)->save($data,['id' => input('id')]);
			if($res){
				return $this->success('修改成功',url('goods_cate/index',['st'=>1]));
			}else{
				$this->error('栏目修改失败了');
			}
    	}
		
		$cid=input('id');
		$data=Db::name('goods_cate')->where('id',$cid)->find();
		$this->assign('data',$data);
       return $this->fetch();
    }
}
