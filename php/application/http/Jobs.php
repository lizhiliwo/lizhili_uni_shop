<?php
namespace app\http;
use think\queue\Job;

use think\Db;
//thinkphp 自带的消息队列
class Jobs
{
	/**
	* fire方法是消息队列默认调用的方法
	* @param Job            $job      当前的任务对象
	* @param array|mixed    $data     发布任务时自定义的数据
	*/
	public function fire(Job $job,$data)
	{
	  // 这里应该检查一遍是否应该去执行
	  $isJobStillNeedToBeDone = $this->checkDatabaseToSeeIfJobNeedToBeDone($data);
	  if($isJobStillNeedToBeDone){
		  $job->delete();
		  return;
	  }
	  $isJobDone = $this->doHelloJob($data);
	  if ($isJobDone) {
		  $job->delete();// 如果任务执行成功， 记得删除任务
	  }else{
		  if ($job->attempts() > 3) { 
			$job->delete();//正在执行，执行三遍，不行就删除
			$job->release(10); //$delay为延迟时间，表示该任务延迟10秒后再执行
		  }
	  }
	}

	//这里检查一遍
	private function checkDatabaseToSeeIfJobNeedToBeDone($data){
		//返回 true 就成功了，不需要执行，返回false 就是接着执行
		if(Db::name('book_show')->where('mp3',$data)->value('is_qiniu')){ 
			return true;
		}else{
			return false;
		}
	}

	//具体工作
	private function doHelloJob($data) 
	{
		//上传到七牛云
		$uploadMgr = new \Qiniu\Storage\UploadManager();
		$auth = new \Qiniu\Auth(config('qiniu')['accessKey'],config('qiniu')['secretKey']);
		$token = $auth->uploadToken(config('qiniu')['bucket']);
		list($ret, $error) = $uploadMgr->putFile($token,substr($data,1) , env('root_path').'public/'.substr($data,1));
		if($error){
			 return false;
		   // $this->error('附件上传静态云失败');
		}
	  Db::name('book_show')->where('mp3',$data)->update(['is_qiniu'=>1]);
	  return true;
	}
}

