<?php
namespace app\api\controller;
use app\api\model\Base as Basemodel;
use think\Controller;
use think\Db;
class Base extends Controller
{
    //date_default_timezone_set("PRC");
	public function _initialize()
    {
		
    }
	protected static function lizhili($data,$lei=false){
	    if($data['type']=="shua_token"){
            if($data["lizhili"]!= "0d89b868429be6158ba1ebc0f7c073de" or time() > $data['time']+60 or $data['token']!=md5($data['time'])){
				//	dump(1);
                return [
                    'code'=>0,
                    'data'=>''
                ];
            }else{
                //刷新token
                $shua_token=md5(uniqid()).mt_rand(1000,99999);
                cache('shua_token', $shua_token, 3600);
                return [
                    'code'=>4,
                    'data'=>$shua_token
                ];
            }

        }
	    //查看token
		if(request()->header()["token"]!='461f6924bed805723485e08b985049ebbb3a9774'){
		//	dump(2);
            return [
                'code'=>0,
                'data'=>''
            ];
		}
        //这个判断刷新token
        if(!isset($data["lizhili"]) or !isset($data["type"]) or cache('shua_token')!=$data['lizhili']){
				//dump(3);
            // return [ //token过期，刷新token
            //     'code'=>0,
            //     'data'=>''
            // ];
        }
		$app_key=$data['app_key'];
         //查看提交时间
		if($data['time']+60<time()){
            return [
                'code'=>1,
                'data'=>''
            ]; //时间超时
		}
		//查看是否重复提交
		if(cache($app_key)){
            // return [
            //     'code'=>2,
            //     'data'=>''
            // ]; //重复提交
		}else{
			cache($app_key, $app_key, 1);
		}
		unset($data['app_key']);
		ksort($data);
		//$str = http_build_query($data);
		$str =http_build_query($data,'','&',PHP_QUERY_RFC3986);
		if(md5($str.'197686741c09329446b110070f88f54d')!=$app_key){
            dump(4);
			return [
			    'code'=>0,
			    'data'=>''
			]; 
		}
		if($lei and !Db::name('user')->where('token',$data['token'])->where('isopen',1)->find()){
            return [
                'code'=>3,
                'data'=>''
            ]; //token已经修改
		}
        return [ //通过
            'code'=>10000,
            'data'=>''
        ];
	}
	//远程下载工具
	protected function curl($url='', $data=[])
	{
		if(!$url){
			return '地址必须填写';
		}
		$ch = curl_init();
		$params[CURLOPT_URL] = $url;    //请求url地址
		$params[CURLOPT_HEADER] = FALSE; //是否返回响应头信息
		$params[CURLOPT_SSL_VERIFYPEER] = false;
		$params[CURLOPT_SSL_VERIFYHOST] = false;
		$params[CURLOPT_RETURNTRANSFER] = true; //是否将结果返回
		 if (!empty($data)) {
			$params[CURLOPT_POST] = true;
			$params[CURLOPT_POSTFIELDS] = \json_encode($data);
		}
		curl_setopt_array($ch, $params); //传入curl参数
		$content = curl_exec($ch); //执行
		curl_close($ch); //关闭连接
		if(json_decode($content, true)){
			return json_decode($content, true);
		}
		return $content;
	}
	
}