<?php
namespace app\index\controller;

use app\index\controller\Base;
use think\Db;
use think\facade\Env;
use think\api\Client;

class Xia extends Base
{
    public function _empty()
    {
        //重定向浏览器
        header("Location:/404.html");
        //确保重定向后，后续代码不会被执行
        exit;
    }
    public function index()
    {
        $mall_config=Db::name('mall_config')->column('value','key');
        $last=Db::name('mall_update')->where('isopen',1)->order('app_v desc')->find();
		return $this->fetch('',[
			'app_v'=>$last['app_v'],
			'app_desc'=>$mall_config['app_desc'],
			'app_name'=>$mall_config['app_name'],
            'app_logo'=> $mall_config['app_logo']
		]);
    }
    public function ajax(){
        $data=input('param.');
        if($data['type']=='get_yzm'){
            $msg=mt_rand(111111, 999999);
            $user=\model('user');
            if ($this->sendsmg($data['phone'], $msg)) {
                if ($u=Db::name('user')->where('phone', $data['phone'])->find()) {
                    $user->where('id', $u['id'])->update([
                        'yzm'=>$msg
                    ]);
                } else {
                    $user->save([
                        'phone'=>$data['phone'],
                        'yzm'=>$msg,
                    ]);
                }
                return ['code'=>1,'message'=>'成功'];
            } else {
                return ['code'=>0,'message'=>'发送验证码失败'];
            }
        }
    }
    public function main()
    {
        if(request()->isPost()){
            $data=input('post.');
            if(!isset($data['code'])){
                $this->error('邀请码不正确');
            }
            if(!isset($data['password']) or !$data['password']){
                $this->error('请填写密码');
            }
            if($data['password']!=$data['password2']){
                $this->error('两次密码不一致');
            }
            $f_user=Db::name('user')->where('user_no',$data['code'])->find();
            if(!$f_user){
                $this->error('邀请码不正确');
            }
            if ($wo=Db::name('user')->where('phone', $data['phone'])->where('yzm',$data['yzm'])->where('isopen',1)->find()) {
                if (!$wo['user_no']) {
                    Db::name('user')->where('id', $wo['id'])->update(['user_no'=>$this->sui($wo['id'])]);
                }
                if (!$wo['fid']) {
                    Db::name('user')->where('id', $wo['id'])->update(['fid'=>$f_user['id']]);
                }
                Db::name('user')->where('id', $wo['id'])->update(['password'=>md5(sha1($data['password']))]);
                $this->success('注册成功，请下载app',url('xia/index',['st'=>1]));
            } else {
                $this->error('注册失败了');
            }
        }
        $mall_config=Db::name('mall_config')->column('value','key');
        $last=Db::name('mall_update')->where('isopen',1)->order('app_v desc')->find();
        return $this->fetch('',[
            'app_v'=>$last['app_v'],
            'app_desc'=>$mall_config['app_desc'],
            'app_name'=>$mall_config['app_name'],
            'app_logo'=> $mall_config['app_logo']
        ]);
    }
    public function shuoming(){
        return $this->fetch('',[
            'data'=>Db::name('shuoming')->where('id',input('id'))->find()
        ]);
    }

    private function sui($id=0)
    {
        $str=[
            0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        ];
        $sui='';
        for ($i=0;$i<5;$i++) {
            $sui.=$str[mt_rand(0, 35)];
        }

        return $sui.$id;
    }
    private function sendsmg($phone, $msg)
    {
        $mall_config=Db::name('mall_config')->column('value','key');
        $client = new Client($mall_config['think_secret']);
        $result = $client->smsSend()
            ->withSignId($mall_config['think_qian'])
            ->withTemplateId($mall_config['think_moban'])
            ->withPhone($phone)
            ->withParams('{"code": "'.$msg.'"}')
            ->request();
        if($result){
            return true;
        }
        return false;
    }
}
