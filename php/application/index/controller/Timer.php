<?php
namespace app\index\controller;
use app\index\controller\Base;
use think\Db;
use think\facade\Log;
use think\helper\Time;

class Timer extends Base
{
    public function _empty()
    {
        //重定向浏览器
        header("Location:/404.html");
        //确保重定向后，后续代码不会被执行
        exit;
    }
    public function fen(){
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '0');
		$info=input('param.');
		if(!isset($info['lizhili']) or $info['lizhili']!='0d89b868429be6158ba1ebc0f7c073de'){
			//重定向浏览器
			header("Location:/404.html");
			//确保重定向后，后续代码不会被执行
			exit;
		}
        list($start, $end) = Time::today();
       

		return date('Y-m-d H:i:s');
	}

    private function cha_arr($fids=[],$wo=false){ //查看下一级有多少人，如果1就是算自己，2不算自己
        $m=Db::name('user')->where('fid','in',$fids)->column('id');
        static $arr_li=[];
        if($wo){
            $arr_li=[];
        }
        if($m){
            $arr_li=array_merge($arr_li,$m);
            $this->cha_arr($m);
        }
		$arr_li=array_merge($arr_li,$fids);
		$arr_li=array_unique($arr_li);
        return $arr_li;
    }
 public function cutstr_html($content)
   {
   	    $content=preg_replace("/<a[^>]*>/i","",$content);
   	    $content=preg_replace("/<\/a>/i","",$content);
   	    $content=preg_replace("/<div[^>]*>/i","",$content);
   	    $content=preg_replace("/<\/div>/i","",$content);
   	    $content=preg_replace("/<!--[^>]*-->/i","",$content);//注释内容    
   	    $content=preg_replace("/style=.+?['|\"]/i",'',$content);//去除样式    
   	    $content=preg_replace("/class=.+?['|\"]/i",'',$content);//去除样式    
   	    $content=preg_replace("/id=.+?['|\"]/i",'',$content);//去除样式       
   	    $content=preg_replace("/lang=.+?['|\"]/i",'',$content);//去除样式        
   	    $content=preg_replace("/width=.+?['|\"]/i",'',$content);//去除样式     
   	    $content=preg_replace("/height=.+?['|\"]/i",'',$content);//去除样式     
   	    $content=preg_replace("/border=.+?['|\"]/i",'',$content);//去除样式     
   	    $content=preg_replace("/face=.+?['|\"]/i",'',$content);//去除样式     
   	    $content=preg_replace("/face=.+?['|\"]/",'',$content);//去除样式 只允许小写 正则匹配没有带 i 参数  
   	    return $content;
   }
}
