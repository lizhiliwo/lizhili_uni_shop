<?php
namespace app\index\controller;

use app\index\controller\Base;
use think\Db;
use think\facade\Env;


class Safe extends Base
{
    public function _empty()
    {
		if(!session('?uid')){
			header("Location:/404.html");
			//确保重定向后，后续代码不会被执行
			exit;
		}
		if(\is_file(env('root_path').request()->path())){
			$data=file_get_contents(env('root_path').request()->path());
			return response($data)->header(['Content-Type' => 'image/jpeg']);
		}else{
			header("Location:/404.html");
			//确保重定向后，后续代码不会被执行
			exit;
		}
       
    }
	
  
}
