<?php
namespace app\index\controller;

use app\index\controller\Base;
use think\Db;
use think\facade\Env;


class Lizhili extends Base
{
    public function _empty()
    {
        //重定向浏览器
        header("Location:/404.html");
        //确保重定向后，后续代码不会被执行
        exit;
    }
	
    public function index()
    {
		//用于测试或者，临时管理服务器，网站使用
		//重定向浏览器
		header("Location:/404.html");
		//确保重定向后，后续代码不会被执行
		exit;
    }
	
}
