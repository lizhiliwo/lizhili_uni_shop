<?php
namespace app\index\controller;
use think\Db;
use think\Controller;
use think\Validate;
class Install extends Controller
{
    public function _empty()
    {
        //重定向浏览器
        header("Location:/404.html");
        //确保重定向后，后续代码不会被执行
        exit;
    }
	public function ajax()
	{
		$data=input('post.');
		if(!isset($data['token']) or $data['token']!=$this->request->token){
			return json(['code'=>0,'msg'=>'验证错误，请刷新页面，重新提交']);
		}
		error_reporting(1);
		$conn = new \mysqli('127.0.0.1', $data['user'], $data['pass']);

        if ($conn->connect_error) {
            return json(['code'=>0,'msg'=>'服务器账号秘密错误']);
        }


        if(!$conn->client_info){
			return json(['code'=>0,'msg'=>'服务器账号秘密错误']);
		}else{
			$data1=[];
			$result = \mysqli_query($conn,'show databases;');
			While($row = \mysqli_fetch_assoc($result)){
				$data1[] = $row['Database'];
			}
			unset($result, $row);
			\mysqli_close($conn);
			if(\in_array($data['data'],$data1)){
				return json(['code'=>1,'msg'=>'已经存在']);
			}else{
				return json(['code'=>2,'msg'=>'是否创建']);
			}
		}
	}
	public function chuang()
	{
		$data=input('post.');
		if(!isset($data['token']) or $data['token']!=$this->request->token){
			return json(['code'=>0,'msg'=>'验证错误，请刷新页面，重新提交']);
		}
		// 创建连接
		$conn = new \mysqli('127.0.0.1', $data['user'], $data['pass']);
		// 检测连接
		if ($conn->connect_error) {
		 die("连接失败: " . $conn->connect_error);
		} 
		// 创建数据库
		$sql = "CREATE DATABASE ".$data['data'];
		if ($conn->query($sql) === TRUE) {
			$conn->close();
			return json(['code'=>1,'msg'=>'创建成功，请点击"导入数据"']);
		} else {
			$conn->close();
			return json(['code'=>0,'msg'=>'创建失败，请重试或者手动创建！']);
		}
	}
	public function index()
	{
		$file=env('root_path').'tpl/'.'install.txt';
		if(file_get_contents($file)!='install'){
			header("Location:/404.html");
			//确保重定向后，后续代码不会被执行
			exit;
		}
		if(request()->isPost()){
			$data=input('post.');
			if(!$data["database"] or !$data["username"] or !$data["password"]){
				$this->error('数据信息必须填写');
			}	
			try{
				$mysql_server_name = '127.0.0.1'; //改成自己的mysql数据库服务器
				$mysql_username = $data["username"]; //改成自己的mysql数据库用户名
				$mysql_password = $data["password"]; //改成自己的mysql数据库密码
				$mysql_database = $data["database"]; //改成自己的mysql数据库名
				$conn=mysqli_connect($mysql_server_name,$mysql_username,$mysql_password,$mysql_database); //连接数据库
				mysqli_close($conn);
			}catch(\Exception $e){
			    $this->error('数据信息不正确,请先验证数据库后提交！');
			}
			//还原数据库
			$dir=env('root_path').'sql';
			$wo=scandir($dir);
			$sqlarr=[];
			foreach($wo as $k=>$v){
				if(\is_file($dir.'/'.$v)){
					$sqlarr[filemtime($dir.'/'.$v)]=[
						'dir'=>strtr($dir.'/'.$v, " \ ", " / "),
						'name'=>$v,
						'time'=>filemtime($dir.'/'.$v)
					];
				}
			}
			rsort($sqlarr);
			if(count($sqlarr)<=0){
				 $this->error('找不到sql文件');
			}
			$sql = new \sql\DatabaseTool([
			    'host' => '127.0.0.1',
			    'port' => 3306,
			    'user' => $data["username"],
			    'password' => $data["password"],
			    'database' => $data["database"],
			    'charset' => 'utf8',
			]);
			$wo=$sql->restore($sqlarr[0]['dir']);
			
			if($wo['code']==1){
				//修改数据配置
				
				$app=env('config_path').'database.php';
				$apphtml=file_get_contents($app);
				$apphtml=str_replace('rootroot',$data["password"],$apphtml);
				$apphtml=str_replace('root',$data["username"],$apphtml);
				$apphtml=str_replace('cms_com',$data["database"],$apphtml);
				$apphtml=str_replace('_lizhili_database',$data["database"],$apphtml);
				$apphtml=str_replace('_lizhili_username',$data["username"],$apphtml);
				$apphtml=str_replace('_lizhili_password',$data["password"],$apphtml);
                
				file_put_contents($app,$apphtml); 
				
				$file=env('root_path').'tpl/'.'install.txt';
				file_put_contents($file,'lock'); 
				 $this->success('还原成功','index/index');
			}
			 $this->error('还原数据库失败');
		}
	
		
		//这里请使用缓存
		return $this->fetch('install');
	}
}
