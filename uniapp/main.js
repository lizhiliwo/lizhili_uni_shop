import Vue from 'vue'
import App from './App'
import store from './store'
Vue.prototype.$store = store
Vue.config.productionTip = false
App.mpType = 'app'
import common from './common.js'
uni.common = common;
const app = new Vue({
    ...App,
	store
})
app.$mount()